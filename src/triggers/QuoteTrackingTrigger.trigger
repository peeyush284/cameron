/**
* @author       Peeyush Awadhiya
* @date         09/24/2014
* @description  This trigger does the following:
*               1. When a new quote tracking record is created, it adds the Inside Sales User to the Opportunity Sales Team as Secondary Sales.
*               2. When Inside sales changes on quote tracking record, the new Inside sales user is added to the Opportunity Sales Team as Secondary Sales.
*/


trigger QuoteTrackingTrigger on QuoteTracking__c (after insert, after update, before insert, before update) {
    if(system.isFuture()) return;
    
    if (Trigger.isBefore)
    {
        QuoteTrackingHelper.HandleBeforeActions(Trigger.new);
    }
    if (Trigger.isAfter)
    {
        if (Trigger.isInsert)
        {
            QuoteTrackingHelper.HandleAfterInsert(Trigger.new);

        }else if (Trigger.isUpdate)
        {
            QuoteTrackingHelper.HandleAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
    
    
}