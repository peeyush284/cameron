/**
    * @author       Chris Pitman
    * @date         09/04/2014
    * @description  Account Trigger added to support DDC Account Hierarchy Features
    * 
    */
trigger AccountTrigger on Account (after insert, after update, after delete) {
    if(system.isFuture()) return;
    if (Trigger.isAfter)
    {
        if (Trigger.isInsert)
        {
            AccountHelper.HandleAfterInsert(Trigger.new);

        }else if (Trigger.isUpdate)
        {
            AccountHelper.HandleAfterUpdate(Trigger.new, Trigger.oldMap);
            
        }else if (Trigger.isDelete)
        {
            AccountHelper.HandleAfterDelete(Trigger.old);
        }
    }
}