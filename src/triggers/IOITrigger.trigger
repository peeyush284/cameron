/**
* @author       Peeyush Awadhiya
* @date         07/15/2014
* @description  This trigger on IOI object does the following:
*               1. When a IOI record is updated it captures the old and new values as related custom object record.
*               2. It tracks following fields: Item of Interest, Description, Owner, Status.
*               3. Populate and sync the period based on IOI_Date__c field.
*               
*               
* 
*/

trigger IOITrigger on IOI__c (before insert, before update, after update) {
    // Call the helper class method.
    if(Trigger.isAfter && Trigger.isUpdate){
        IOIHelper.trackIOIUpdates(trigger.new, trigger.oldMap);
    }
    else if (Trigger.isBefore){
        IOIHelper.populatePeriod(trigger.new);
    }
}