/**
* @author       Peeyush Awadhiya
* @date         10/10/2014
* @description  Trigger on opportunity product object.
* 
*/

trigger OpportunityLineItemTrigger on OpportunityLineItem (after insert, after update, before delete) {
    if (Trigger.isAfter)
    {
        if (Trigger.isInsert)
        {
            OpportunityLineItemHelper.HandleAfterInsert(Trigger.new);

        }else if (Trigger.isUpdate)
        {
            OpportunityLineItemHelper.HandleAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
    else if (Trigger.isBefore){
        if(Trigger.isDelete){
            OpportunityLineItemHelper.HandleBeforeDelete(Trigger.oldMap);
        }    
    }
}