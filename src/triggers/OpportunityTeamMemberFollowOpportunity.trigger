/*
* @author Peeyush Awadhiya
* @date 08/20/2014
* @description : The trigger should do following:
*                1. When a team member is added to the Sales team, they should automatically follow the record.
*                2. When a team member is removed, the member should "unfollow" the opportunity record.
*/

trigger OpportunityTeamMemberFollowOpportunity on OpportunityTeamMember (after insert, after delete, after update, before update) {
     if(trigger.isInsert){
         OpportunityTeamMemberFollowOpptyHelper.handleInsert(trigger.new);   
     }
     
     else if(trigger.isUpdate && trigger.isBefore){
         OpportunityTeamMemberFollowOpptyHelper.handleBeforeUpdate(trigger.new, trigger.oldMap);    
     } 
     
     else if (trigger.isDelete){
         OpportunityTeamMemberFollowOpptyHelper.handleDelete(trigger.old);
     }   
}