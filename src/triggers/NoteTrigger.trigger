/**
* @author       Peeyush Awadhiya
* @date         09/25/2014
* @description  This trigger on Note object does the following:
*               1. If a note is created related to the Quote Tracking object, then copy the note text to Quote tracking custom field.
*               
*/             



trigger NoteTrigger on Note (after insert, after update) {
    
    if (Trigger.isAfter)
    {
        if (Trigger.isInsert)
        {
            NoteHelper.handleAfterInsert(trigger.new);  
        }
        if (Trigger.isUpdate)
        {     
            NoteHelper.handleAfterUpdate (Trigger.new, Trigger.oldMap);
        }
    }
}

/*
trigger NoteTrigger on Note (after insert) {
    
    NoteHelper.handleAfterInsert(trigger.new);   
}
*/