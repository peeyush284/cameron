trigger ProductTrigger on Product2 (after insert, after update) {
    if(trigger.isInsert){
        ProductHelper.HandleAfterInsert(Trigger.new);  
    }else if (Trigger.isUpdate)
    {
        ProductHelper.HandleAfterUpdate(Trigger.new, Trigger.oldMap);
    }  
}