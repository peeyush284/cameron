trigger OpportunityTrigger on Opportunity (after insert, after update, before insert, before update) {
    if (Trigger.isAfter)
    {
        if (Trigger.isInsert)
        {
            OpportunityHelper.HandleAfterInsert(Trigger.new);

        }else if (Trigger.isUpdate)
        {
            OpportunityHelper.HandleAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
    /**
    * @author       Peeyush Awadhiya
    * @date         08/06/2014
    * @description  This block updates the Account lookup with End User lookup field value
    * 
    */
    
    else if (Trigger.isBefore){
        if (Trigger.isInsert)
        {
            OpportunityHelper.HandleBeforeInsert(Trigger.new);

        }else if (Trigger.isUpdate)
        {
            OpportunityHelper.HandleBeforeUpdate(Trigger.new, Trigger.oldMap);
        }    
    }
}