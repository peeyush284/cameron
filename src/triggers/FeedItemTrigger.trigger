/**
* @author       Peeyush Awadhiya
* @date         07/09/2014
* @description  This trigger on FeedItem object does the following:
* 				1. If the FeedItem is deleted and there is a corresponding Chatter Post custom object record,
*				then it deletes the record as well.
* 
*/


trigger FeedItemTrigger on FeedItem (after delete) {
	
	// Call the helper class method.
	FeedItemHelper.deleteChatterPostWhenFeedItemIsDeleted(trigger.old);
}