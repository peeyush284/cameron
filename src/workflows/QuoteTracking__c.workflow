<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CAM_Snd_Email_Outside_Sales</fullName>
        <description>Send Email to Outside Sales Person</description>
        <protected>false</protected>
        <recipients>
            <field>Outside_Sales_Person__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Cameron_Sales_Email_Templates/CAM_QT_Outside_Sales_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>SetValidationMessage</fullName>
        <field>Validation_Message__c</field>
        <formula>if(
Count_of_Quote_Tracking_Items__c = 0,
&apos;At least 1 Quote Tracking Item must be added to each Quote Tracking record&apos;,&apos;&apos;)</formula>
        <name>SetValidationMessage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Owner_Div_and_Bus_Unit</fullName>
        <description>Populate Opportunity Owner&apos;s Division and Business Unit on a Quote Tracker record upon lookup to Opportunity record</description>
        <field>Opportunity_Owner_Div_BU__c</field>
        <formula>text(Opportunity__r.Owner.camDivision__c)&amp;&quot;-&quot;&amp;text(Opportunity__r.Owner.camBusinessUnit__c)</formula>
        <name>Set Oppty Owner Div and Bus Unit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_PS_Quote_Tracking_Closed_Date_Null</fullName>
        <field>Closed_Date__c</field>
        <name>Set PS Quote Tracking Closed Date Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_PS_Quote_Tracking_RT_to_Closed</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Quote_Tracking_PS_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set PS Quote Tracking RT to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_PS_Quote_Tracking_RT_to_Open</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Quote_Tracking_PS</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set PS Quote Tracking RT to Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Quote_Tracking_Amount_from_Line_Item</fullName>
        <field>Amount__c</field>
        <formula>Quote_Amount__c</formula>
        <name>Set Quote Tracking Amount from Line Item</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdatePSQuoteName</fullName>
        <field>Quote_Name__c</field>
        <formula>End_User__r.Name + &apos; &apos; + Name</formula>
        <name>UpdatePSQuoteName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Quote_Item_Amount</fullName>
        <field>Amount__c</field>
        <formula>Quote_Line_Item_TotalPrice__c</formula>
        <name>Update Quote Item Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CAM_Send_Email_To_Outside_Sales</fullName>
        <actions>
            <name>CAM_Snd_Email_Outside_Sales</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send Email to Outside Sales when Opty Type &lt;&gt; Bucket</description>
        <formula>(Opportunity__r.RecordType.Name &lt;&gt;&apos;Bucket&apos; &amp;&amp; not(isblank( Opportunity__c ))) || value($Label.CAM_QT_Bucket_Email_Opty_Threshold )  &lt;=   Quote_Amount__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Opportunity Lookup on Quote Tracker</fullName>
        <actions>
            <name>Set_Oppty_Owner_Div_and_Bus_Unit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populate Opportunity Owner&apos;s Division and Business Unit on a Quote Tracker record upon lookup to Opportunity record</description>
        <formula>and(
not(isblank(text( Opportunity__r.Owner.camDivision__c ))),
or(
isnew(),
ISCHANGED( Opportunity__c )
)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change PS Record Type to Closed</fullName>
        <actions>
            <name>Set_PS_Quote_Tracking_RT_to_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Changes record type from Quote Tracking - PS to Quote Tracking - PS Closed when one of the Closed Status values is selected (via Close Quote FLOW)</description>
        <formula>AND( RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, ISCHANGED(Quote_Status__c), LEFT(text(Quote_Status__c), 6)= &quot;Closed&quot; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change PS Record Type to Open</fullName>
        <actions>
            <name>Set_PS_Quote_Tracking_Closed_Date_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_PS_Quote_Tracking_RT_to_Open</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Changes record type from Quote Tracking - PS Closed to Quote Tracking - PS when one of the Open Status values is selected (from Closed Page Layout)</description>
        <formula>AND( RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot;, ISCHANGED(Quote_Status__c), LEFT(text(Quote_Status__c), 6)&lt;&gt; &quot;Closed&quot; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SetValidationMessage</fullName>
        <actions>
            <name>SetValidationMessage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 or 2</booleanFilter>
        <criteriaItems>
            <field>QuoteTracking__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Quote Tracking - PS</value>
        </criteriaItems>
        <criteriaItems>
            <field>QuoteTracking__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Quote Tracking - CPS</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Quote Item Total Amount</fullName>
        <actions>
            <name>Update_Quote_Item_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 
not(RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;), 
ISCHANGED( Quote_Line_Item_TotalPrice__c ) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Quote Tracking Amount from Line Items</fullName>
        <actions>
            <name>Set_Quote_Tracking_Amount_from_Line_Item</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>and(
not(RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;),
ischanged( Quote_Amount__c )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UpdatePSQuoteName</fullName>
        <actions>
            <name>UpdatePSQuoteName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>QuoteTracking__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Quote Tracking - PS,Quote Tracking - CPS</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
