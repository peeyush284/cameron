<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Product_External_ID</fullName>
        <field>Product_External_ID__c</field>
        <formula>Division__c&amp;&quot;|&quot;&amp;Business_Unit__c&amp;&quot;|&quot;&amp;Name</formula>
        <name>Set Product External ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Product External ID</fullName>
        <actions>
            <name>Set_Product_External_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(
ISNEW(),
ISCHANGED(Division__c),
ISCHANGED(Business_Unit__c),
ISCHANGED(Name)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
