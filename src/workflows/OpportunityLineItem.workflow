<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Historical_Price</fullName>
        <field>Historical_Price__c</field>
        <formula>UnitPrice</formula>
        <name>Set Historical Price = Sales Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Prod_BU_OSS_Proc_Sys_Pumps</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>OSS Proc Sys Pumps</literalValue>
        <name>Set Oppty Prod BU - OSS Proc Sys Pumps</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Prod_BU_PS_CPS_Aftermarket</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>CPS Aftermarket</literalValue>
        <name>Set Oppty Prod BU PS - CPS Aftermarket</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Prod_BU_V_M_Valve_Automation</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>Valve Automation</literalValue>
        <name>Set Oppty Prod BU V&amp;M - Valve Automation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Product_BU_Drilling_Drilling</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>Drilling</literalValue>
        <name>Set Oppty Product BU Drilling - Drilling</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Product_BU_Drilling_LeTourne</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>LeTourneau</literalValue>
        <name>Set Oppty Product BU Drilling - LeTourne</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Product_BU_Drilling_Pressure</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>Pressure Control</literalValue>
        <name>Set Oppty Product BU Drilling - Pressure</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Product_BU_Drilling_Sense</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>Sense</literalValue>
        <name>Set Oppty Product BU Drilling - Sense</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Product_BU_PS_CPS</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>CPS</literalValue>
        <name>Set Oppty Product BU PS - CPS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Product_BU_PS_Midstream</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>Midstream</literalValue>
        <name>Set Oppty Product BU PS - Midstream</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Product_BU_PS_Wellhead</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>Wellhead</literalValue>
        <name>Set Oppty Product BU PS - Wellhead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Product_BU_Processing_Systems</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>Processing Systems</literalValue>
        <name>Set Oppty Product BU Processing Systems</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Product_BU_Production_Systems</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>Production Systems</literalValue>
        <name>Set Oppty Product BU Production Systems</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Product_BU_Subsea_Services</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>Subsea Services</literalValue>
        <name>Set Oppty Product BU Subsea Services</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Product_BU_Surface_ALS</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>ALS</literalValue>
        <name>Set Oppty Product BU Surface - ALS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Product_BU_Surface_CAMSHALE</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>CAMSHALE</literalValue>
        <name>Set Oppty Product BU Surface - CAMSHALE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Product_BU_Surface_FLC</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>FLC</literalValue>
        <name>Set Oppty Product BU Surface - FLC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Product_BU_Surface_SUR</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>SUR</literalValue>
        <name>Set Oppty Product BU Surface - SUR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Product_BU_Swivel_Marine</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>Swivel &amp; Marine Systems</literalValue>
        <name>Set Oppty Product BU Swivel &amp; Marine</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Product_BU_V_M_DSV</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>DSV</literalValue>
        <name>Set Oppty Product BU V&amp;M - DSV</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Product_BU_V_M_EPV</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>EPV</literalValue>
        <name>Set Oppty Product BU V&amp;M - EPV</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Product_BU_V_M_Measurement</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>Measurement</literalValue>
        <name>Set Oppty Product BU V&amp;M - Measurement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_Product_BU_V_M_Valves</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>Valves</literalValue>
        <name>Set Oppty Product BU V&amp;M - Valves</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opty_Prod_BU_OSS_Proc_Sys_Meters</fullName>
        <field>Opportunity_Business_Unit_Picklist__c</field>
        <literalValue>OSS Proc Sys Meters</literalValue>
        <name>Set Opty Prod BU - OSS Proc Sys Meters</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Product_Get_from_Oppty</fullName>
        <field>Get__c</field>
        <formula>value(text(Opportunity.CAM_GET_PCT__c))/100</formula>
        <name>Set Product Get from Oppty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Product_Get_to_Zero</fullName>
        <field>Get__c</field>
        <formula>0</formula>
        <name>Set Product Get to Zero</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Product_Go_from_Oppty</fullName>
        <field>Go__c</field>
        <formula>value(text(Opportunity.CAM_Go__c))/100</formula>
        <name>Set Product Go from Oppty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Product_Go_to_Zero</fullName>
        <field>Go__c</field>
        <formula>0</formula>
        <name>Set Product Go to Zero</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Sales_Price_0</fullName>
        <field>UnitPrice</field>
        <formula>0</formula>
        <name>Set Sales Price = 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Sales_Price_Historical_Price</fullName>
        <field>UnitPrice</field>
        <formula>Historical_Price__c</formula>
        <name>Set Sales Price = Historical Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Standard_Quantity</fullName>
        <description>Copies value from Quantity (Standard) field into Quantity field on Opportunity Product</description>
        <field>Quantity</field>
        <formula>Quantity_Standard__c</formula>
        <name>Set Standard Quantity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Tree_Quantity</fullName>
        <field>Tree_Quantity__c</field>
        <formula>QuantityCalc__c</formula>
        <name>Set Tree Quantity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Brand</fullName>
        <field>Brand__c</field>
        <formula>Product2.Brand__c</formula>
        <name>Update Brand</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Model</fullName>
        <field>Model__c</field>
        <formula>Product2.Model__c</formula>
        <name>Update Model</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Levels</fullName>
        <actions>
            <name>Update_Brand</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Model</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Product2.Brand__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Model__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Historical Price</fullName>
        <actions>
            <name>Set_Historical_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Sales_Price_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Forecasted__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>OpportunityLineItem.UnitPrice</field>
            <operation>greaterThan</operation>
            <value>USD 0</value>
        </criteriaItems>
        <description>Forecasted = False
Set Historical Price =  Sales Price
Set Sales Price = 0</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU Drilling - Drilling</fullName>
        <actions>
            <name>Set_Oppty_Product_BU_Drilling_Drilling</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( Product_Division__c )) &amp;&amp;  Opportunity.CAM_Business_Unit__c  = &apos;Drilling&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU Drilling - LeTourneau</fullName>
        <actions>
            <name>Set_Oppty_Product_BU_Drilling_LeTourne</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( Product_Division__c )) &amp;&amp;  Opportunity.CAM_Business_Unit__c  = &apos;LeTourneau&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU Drilling - Pressure Control</fullName>
        <actions>
            <name>Set_Oppty_Product_BU_Drilling_Pressure</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( Product_Division__c )) &amp;&amp;  Opportunity.CAM_Business_Unit__c  = &apos;Pressure Control&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU Drilling - Sense</fullName>
        <actions>
            <name>Set_Oppty_Product_BU_Drilling_Sense</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( Product_Division__c )) &amp;&amp;  Opportunity.CAM_Business_Unit__c  = &apos;Sense&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU OSS - OSS Proc Sys Meters</fullName>
        <actions>
            <name>Set_Opty_Prod_BU_OSS_Proc_Sys_Meters</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( Product_Division__c )) &amp;&amp;  Opportunity.CAM_Business_Unit__c  = &apos;OSS Proc Sys Meters&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU OSS - OSS Proc Sys Pumps</fullName>
        <actions>
            <name>Set_Oppty_Prod_BU_OSS_Proc_Sys_Pumps</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( Product_Division__c )) &amp;&amp;  Opportunity.CAM_Business_Unit__c  = &apos;OSS Proc Sys Pumps&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU OSS - Processing Systems</fullName>
        <actions>
            <name>Set_Oppty_Product_BU_Processing_Systems</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( Product_Division__c )) &amp;&amp;  Opportunity.CAM_Business_Unit__c  = &apos;Processing Systems&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU OSS - Subsea Services</fullName>
        <actions>
            <name>Set_Oppty_Product_BU_Subsea_Services</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( Product_Division__c )) &amp;&amp;  Opportunity.CAM_Business_Unit__c  = &apos;Subsea Services&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU OSS - Swivel %26 Marine Systems</fullName>
        <actions>
            <name>Set_Oppty_Product_BU_Swivel_Marine</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( Product_Division__c )) &amp;&amp;  Opportunity.CAM_Business_Unit__c  = &apos;Swivel &amp; Marine Systems&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU OSS -Production Systems</fullName>
        <actions>
            <name>Set_Oppty_Product_BU_Production_Systems</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( Product_Division__c )) &amp;&amp;  Opportunity.CAM_Business_Unit__c  = &apos;Production Systems&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU PS - CPS</fullName>
        <actions>
            <name>Set_Oppty_Product_BU_PS_CPS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( Product_Division__c )) &amp;&amp;  Opportunity.CAM_Business_Unit__c  = &apos;CPS&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU PS - CPS Aftermarket</fullName>
        <actions>
            <name>Set_Oppty_Prod_BU_PS_CPS_Aftermarket</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( Product_Division__c )) &amp;&amp;  Opportunity.CAM_Business_Unit__c  = &apos;CPS Aftermarket&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU PS - Midstream</fullName>
        <actions>
            <name>Set_Oppty_Product_BU_PS_Midstream</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( Product_Division__c )) &amp;&amp;  Opportunity.CAM_Business_Unit__c  = &apos;Midstream&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU PS - Wellhead</fullName>
        <actions>
            <name>Set_Oppty_Product_BU_PS_Wellhead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( Product_Division__c )) &amp;&amp;  Opportunity.CAM_Business_Unit__c  = &apos;Wellhead&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU Surface - ALS</fullName>
        <actions>
            <name>Set_Oppty_Product_BU_Surface_ALS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( Product_Division__c )) &amp;&amp;  Opportunity.CAM_Business_Unit__c  = &apos;ALS&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU Surface - CAMSHALE</fullName>
        <actions>
            <name>Set_Oppty_Product_BU_Surface_CAMSHALE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( Product_Division__c )) &amp;&amp;  Opportunity.CAM_Business_Unit__c  = &apos;CAMSHALE&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU Surface - FLC</fullName>
        <actions>
            <name>Set_Oppty_Product_BU_Surface_FLC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( Product_Division__c )) &amp;&amp;  Opportunity.CAM_Business_Unit__c  = &apos;FLC&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU Surface - SUR</fullName>
        <actions>
            <name>Set_Oppty_Product_BU_Surface_SUR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( Product_Division__c )) &amp;&amp;  Opportunity.CAM_Business_Unit__c  = &apos;SUR&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU V%26M - DSV</fullName>
        <actions>
            <name>Set_Oppty_Product_BU_V_M_DSV</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( Product_Division__c )) &amp;&amp;  Opportunity.CAM_Business_Unit__c  = &apos;DSV&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU V%26M - EPV</fullName>
        <actions>
            <name>Set_Oppty_Product_BU_V_M_EPV</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( Product_Division__c )) &amp;&amp;  Opportunity.CAM_Business_Unit__c  = &apos;EPV&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU V%26M - Measurement</fullName>
        <actions>
            <name>Set_Oppty_Product_BU_V_M_Measurement</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( Product_Division__c )) &amp;&amp;  Opportunity.CAM_Business_Unit__c  = &apos;Measurement&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU V%26M - Valve Automation</fullName>
        <actions>
            <name>Set_Oppty_Prod_BU_V_M_Valve_Automation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( Product_Division__c )) &amp;&amp;  Opportunity.CAM_Business_Unit__c  = &apos;Valve Automation&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty Product BU V%26M - Valves</fullName>
        <actions>
            <name>Set_Oppty_Product_BU_V_M_Valves</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Oppty Business Unit value</description>
        <formula>AND(
OR(
ISNEW(),
ISCHANGED( Product_Division__c)
),
OR(
Opportunity.CAM_Business_Unit__c  = &apos;Valves&apos;,
text( Opportunity.Business_Unit_Picklist__c ) = &apos;Valves&apos;
)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Product Go and Get from Oppty</fullName>
        <actions>
            <name>Set_Product_Get_from_Oppty</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Product_Go_from_Oppty</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Forecasted__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Product Go and Get to Zero</fullName>
        <actions>
            <name>Set_Product_Get_to_Zero</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Product_Go_to_Zero</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>not(Forecasted__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Sales Price</fullName>
        <actions>
            <name>Set_Sales_Price_Historical_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Forecasted = True
Set Sales Price =  Historical Price</description>
        <formula>Forecasted__c = True &amp;&amp;   not(isnull(Historical_Price__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Standard Quantity</fullName>
        <actions>
            <name>Set_Standard_Quantity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Quantity_Standard__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <description>Changes Standard Quantity (not on the page layout) for users that change Quantity (Standard) field on Oppty Product</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Tree Quantity</fullName>
        <actions>
            <name>Set_Tree_Quantity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Product2.Name = &quot;Tree&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
