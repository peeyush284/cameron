<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Competitor_Update_Background_Division</fullName>
        <field>Background_Division__c</field>
        <formula>if(INCLUDES(Division__c, &quot;OneSubSea&quot;), &quot;OneSubSea; &quot;,&quot;&quot;) +
if(INCLUDES(Division__c, &quot;Drilling&quot;), &quot;Drilling; &quot;,&quot;&quot;) +
if(INCLUDES(Division__c, &quot;Process Systems&quot;), &quot;Process Systems; &quot;,&quot;&quot;) +
if(INCLUDES(Division__c, &quot;Valves &amp; Measurement&quot;), &quot;Valves &amp; Measurement; &quot;,&quot;&quot;) +
if(INCLUDES(Division__c, &quot;Surface&quot;), &quot;Surface; &quot;,&quot;&quot;)</formula>
        <name>Competitor Update Background Division</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Competitor Update Background Division</fullName>
        <actions>
            <name>Competitor_Update_Background_Division</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the Multi-Select division is updated, update the background Division Field</description>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
