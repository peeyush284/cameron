<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Rejected_Account</fullName>
        <description>Rejected Account</description>
        <protected>false</protected>
        <recipients>
            <field>COE_Regional_Approver__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Cameron_Sales_Email_Templates/Account_Approval_Request_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Remove_Approver</fullName>
        <field>COE_Approver__c</field>
        <name>Remove Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Acct_Record_Type_to_D_B_Acct</fullName>
        <field>RecordTypeId</field>
        <lookupValue>D_B_Account</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Acct Record Type to D&amp;B Acct</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approver</fullName>
        <field>COE_Approver__c</field>
        <formula>$User.Username</formula>
        <name>Set Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Owner_to_Cameron_Admin</fullName>
        <field>OwnerId</field>
        <lookupValue>cameronadmin@cam_sfdc.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set Owner to Cameron Admin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Pending_COE_Approval</fullName>
        <field>Pending_Divisional_COE_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Pending COE Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateAccountOwner</fullName>
        <description>Update Owner without updating child object ownership</description>
        <field>OwnerId</field>
        <lookupValue>cameronadmin@cam_sfdc.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>UpdateAccountOwner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_COE_Regional_Email</fullName>
        <field>COE_Regional_Approver__c</field>
        <formula>$User.Email</formula>
        <name>Update COE Regional Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>chrysti.orr@c-a-m.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Pending_COE_Approval</fullName>
        <field>Pending_Divisional_COE_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Update Pending COE Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>D_B_Account</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Prospect Add or Change</fullName>
        <actions>
            <name>UpdateAccountOwner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR (2 AND 3)</booleanFilter>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Prospect Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>D&amp;B Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.COE_Approval__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Remove Approver</fullName>
        <actions>
            <name>Remove_Approver</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>Pending_Divisional_COE_Approval__c == False &amp;&amp; len( COE_Approver__c ) &gt; 0</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver</fullName>
        <actions>
            <name>Set_Approver</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Pending_Divisional_COE_Approval__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Record Type</fullName>
        <actions>
            <name>Update_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Prospect Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.COE_Approval__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
