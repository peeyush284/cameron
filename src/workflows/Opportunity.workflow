<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Project_Lead_Reminder_Email_for_Follow_Up</fullName>
        <description>Project Lead Reminder Email for Follow-Up</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Cameron_Sales_Email_Templates/Project_Project_Lead_Follow_Up</template>
    </alerts>
    <alerts>
        <fullName>Project_Project_Lead_Initial_Assessment</fullName>
        <description>Project: Project Lead Initial Assessment</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Cameron_Sales_Email_Templates/Project_Project_Lead_Initial_Assessment</template>
    </alerts>
    <alerts>
        <fullName>Project_Project_Lead_Initial_Assessment_Overdue</fullName>
        <description>Project: Project Lead Initial Assessment Overdue</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Cameron_Sales_Email_Templates/Project_Project_Lead_Initial_Assessment_Overdue</template>
    </alerts>
    <alerts>
        <fullName>Send_Project_Lead_Follow_Up_Reminder</fullName>
        <description>Send Project Lead Follow-Up Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Cameron_Sales_Email_Templates/Project_Project_Lead_Follow_Up</template>
    </alerts>
    <alerts>
        <fullName>test</fullName>
        <description>test</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Cameron_Sales_Email_Templates/Project_Project_Lead_Follow_Up</template>
    </alerts>
    <fieldUpdates>
        <fullName>CAM_Set_Oppty_RT_Standard</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Standard</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Opportunity Type to Standard</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Closed_Date_Default_to_Today</fullName>
        <field>Closed_Date__c</field>
        <formula>Today()</formula>
        <name>Closed Date Default to Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Stage_to_Project_Lead</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Project_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Opportunity Stage to Project Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Water_Depth_from_Infield_Data</fullName>
        <description>Populates Water Depth (Meters) from Infield Data&apos;s Field Water Depth value (when available).  Water Depth (Meters) is an editable field, allowing user to provide a value in this field</description>
        <field>Water_Depth__c</field>
        <formula>if(not(isblank( Field_Water_Depth__c )),value(Field_Water_Depth__c),null)</formula>
        <name>Populate Water Depth from Infield Data</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetValidationMessage</fullName>
        <field>ValidationMessage__c</field>
        <formula>if( 
    or( 
        and( 
            Division_Abbrev_calc__c == &apos;PS&apos;, 
            contains(&quot;Universe|&quot;, Text( StageName )), 
            Number_of_Products__c == 0 
            ), 
        and( 
            contains(&quot;Above the Funnel|In the Funnel|Best Few|&quot;, Text( StageName )), 
            Number_of_Products__c == 0 
            )
      ) 
    , &apos;At least one product must be selected at this Stage&apos;, 
    &apos;&apos; 
)</formula>
        <name>SetValidationMessage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Close_Date_from_Follow_Up_Date</fullName>
        <description>Populate Close Date from Project Lead Follow-Up Date upon Stage change</description>
        <field>CloseDate</field>
        <formula>IF(MONTH(  REMOVE_Project_Lead_Follow_Up_Date__c  ) = 12,
DATE( YEAR(  REMOVE_Project_Lead_Follow_Up_Date__c  ), 12, 31 ),
DATE( YEAR(  REMOVE_Project_Lead_Follow_Up_Date__c  ), MONTH (   REMOVE_Project_Lead_Follow_Up_Date__c   ) + 1, 1 ) - 1 
)</formula>
        <name>Set Close Date from Follow-Up Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Created_Stage</fullName>
        <description>Populate &quot;Created State&quot; with the initial value stored in (Opportunity) &quot;StageName&quot; field</description>
        <field>CAM_Created_Stage__c</field>
        <formula>text(StageName)</formula>
        <name>Set Created Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Get_to_0</fullName>
        <field>CAM_GET_PCT__c</field>
        <literalValue>0</literalValue>
        <name>Set Get to 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Get_to_null</fullName>
        <field>CAM_GET_PCT__c</field>
        <name>Set Get to null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Go_to_0</fullName>
        <field>CAM_Go__c</field>
        <literalValue>0</literalValue>
        <name>Set Go to 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Go_to_null</fullName>
        <field>CAM_Go__c</field>
        <name>Set Go to null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opportunity_Bus_Unit_to_Owner_Bus_Un</fullName>
        <description>Set Opportunity Bus Unit to Owner Bus Unit when the owner changes</description>
        <field>CAM_Business_Unit__c</field>
        <formula>if(isblank(text( Owner.camBusinessUnit__c )), text(Business_Unit_Picklist__c ),
text( Owner.camBusinessUnit__c ))</formula>
        <name>Set Opportunity Bus Unit to Owner Bus Un</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opportunity_Division_to_Owner_Div</fullName>
        <description>Set Opportunity Division to Owner Division</description>
        <field>CAM_Division__c</field>
        <formula>text(Owner.camDivision__c )</formula>
        <name>Set Opportunity Division to Owner Div</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_BU_Picklist_Drilling_Drillin</fullName>
        <field>Business_Unit_Picklist__c</field>
        <literalValue>Drilling</literalValue>
        <name>Set Oppty BU Picklist Drilling - Drillin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_BU_Picklist_Drilling_LeTourn</fullName>
        <field>Business_Unit_Picklist__c</field>
        <literalValue>LeTourneau</literalValue>
        <name>Set Oppty BU Picklist Drilling - LeTourn</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_BU_Picklist_Drilling_Pressur</fullName>
        <field>Business_Unit_Picklist__c</field>
        <literalValue>Pressure Control</literalValue>
        <name>Set Oppty BU Picklist Drilling - Pressur</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_BU_Picklist_Drilling_Sense</fullName>
        <field>Business_Unit_Picklist__c</field>
        <literalValue>Sense</literalValue>
        <name>Set Oppty BU Picklist Drilling - Sense</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_BU_Picklist_OSS_Proc_Sys_Meter</fullName>
        <field>Business_Unit_Picklist__c</field>
        <literalValue>OSS Proc Sys Meters</literalValue>
        <name>Set Oppty BU Picklist OSS Proc Sys Meter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_BU_Picklist_OSS_Proc_Sys_Pumps</fullName>
        <field>Business_Unit_Picklist__c</field>
        <literalValue>OSS Proc Sys Pumps</literalValue>
        <name>Set Oppty BU Picklist OSS Proc Sys Pumps</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_BU_Picklist_PS_CPS</fullName>
        <field>Business_Unit_Picklist__c</field>
        <literalValue>CPS</literalValue>
        <name>Set Oppty BU Picklist PS - CPS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_BU_Picklist_PS_CPS_Aftermark</fullName>
        <field>Business_Unit_Picklist__c</field>
        <literalValue>CPS Aftermarket</literalValue>
        <name>Set Oppty BU Picklist PS - CPS Aftermark</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_BU_Picklist_PS_Wellhead</fullName>
        <field>Business_Unit_Picklist__c</field>
        <literalValue>Wellhead</literalValue>
        <name>Set Oppty BU Picklist PS - Wellhead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_BU_Picklist_Processing_Systems</fullName>
        <field>Business_Unit_Picklist__c</field>
        <literalValue>Processing Systems</literalValue>
        <name>Set Oppty BU Picklist Processing Systems</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_BU_Picklist_Production_Systems</fullName>
        <field>Business_Unit_Picklist__c</field>
        <literalValue>Production Systems</literalValue>
        <name>Set Oppty BU Picklist Production Systems</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_BU_Picklist_Subsea_Services</fullName>
        <field>Business_Unit_Picklist__c</field>
        <literalValue>Subsea Services</literalValue>
        <name>Set Oppty BU Picklist Subsea Services</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_BU_Picklist_Surface_ALS</fullName>
        <field>Business_Unit_Picklist__c</field>
        <literalValue>ALS</literalValue>
        <name>Set Oppty BU Picklist Surface - ALS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_BU_Picklist_Surface_CAMSHALE</fullName>
        <field>Business_Unit_Picklist__c</field>
        <literalValue>CAMSHALE</literalValue>
        <name>Set Oppty BU Picklist Surface - CAMSHALE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_BU_Picklist_Surface_FLC</fullName>
        <field>Business_Unit_Picklist__c</field>
        <literalValue>FLC</literalValue>
        <name>Set Oppty BU Picklist Surface - FLC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_BU_Picklist_Surface_SUR</fullName>
        <field>Business_Unit_Picklist__c</field>
        <literalValue>SUR</literalValue>
        <name>Set Oppty BU Picklist Surface - SUR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_BU_Picklist_Swivel_Marine</fullName>
        <field>Business_Unit_Picklist__c</field>
        <literalValue>Swivel &amp; Marine Systems</literalValue>
        <name>Set Oppty BU Picklist Swivel &amp; Marine</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_BU_Picklist_V_M_DSV</fullName>
        <field>Business_Unit_Picklist__c</field>
        <literalValue>DSV</literalValue>
        <name>Set Oppty BU Picklist V&amp;M - DSV</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_BU_Picklist_V_M_EPV</fullName>
        <field>Business_Unit_Picklist__c</field>
        <literalValue>EPV</literalValue>
        <name>Set Oppty BU Picklist V&amp;M - EPV</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_BU_Picklist_V_M_Measurement</fullName>
        <field>Business_Unit_Picklist__c</field>
        <literalValue>Measurement</literalValue>
        <name>Set Oppty BU Picklist V&amp;M - Measurement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_BU_Picklist_V_M_Valves</fullName>
        <field>Business_Unit_Picklist__c</field>
        <literalValue>Valves</literalValue>
        <name>Set Oppty BU Picklist V&amp;M - Valves</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oppty_BU_Pklst_V_M_Valve_Automation</fullName>
        <field>Business_Unit_Picklist__c</field>
        <literalValue>Valve Automation</literalValue>
        <name>Set Oppty BU Pklst V&amp;M Valve Automation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opty_GO_to_100</fullName>
        <field>CAM_Go__c</field>
        <literalValue>100</literalValue>
        <name>Set Opty GO to 100</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opty_Get_to_0</fullName>
        <field>CAM_GET_PCT__c</field>
        <literalValue>0</literalValue>
        <name>Set Opty Get to 0%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opty_Get_to_100</fullName>
        <field>CAM_GET_PCT__c</field>
        <literalValue>100</literalValue>
        <name>Set Opty Get to 100%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opty_Go_to_0</fullName>
        <field>CAM_Go__c</field>
        <literalValue>0</literalValue>
        <name>Set Opty Go to 0%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opty_Quote_Status_to_Closed_No_Bid</fullName>
        <description>Set Quote Status to Not Quoted</description>
        <field>Quote_Status_OSS__c</field>
        <literalValue>Not Quoted</literalValue>
        <name>Set Opty Quote Status to Closed - Not Qu</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opty_Type_to_Closed_No_Bid</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Closed_No_Bid</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Opty Type to Closed No Bid</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opty_type_to_Above_Funnel</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Pre_RFQ</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Opty type to Above Funnel</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opty_type_to_Bucket</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Bucket</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Opty type to Bucket</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opty_type_to_Closed_Cancelled</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Closed_Cancelled</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Opty type to Closed Cancelled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opty_type_to_Closed_Lost</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Closed_Lost</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Opty type to Closed Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opty_type_to_Closed_Won</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Closed_Won</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Opty type to Closed Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opty_type_to_In_Funnel_Best_Few</fullName>
        <field>RecordTypeId</field>
        <lookupValue>RFQ_Clarification</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Opty type to In Funnel/Best Few</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opty_type_to_Project_Lead</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Project_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Opty type to Project Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opty_type_to_Universe</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Project_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Opty type to Universe</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opty_Amount</fullName>
        <field>Amount</field>
        <formula>Forecasted_Total_Price__c</formula>
        <name>Update Opty Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>yTest_Forecasting</fullName>
        <field>Estimated_Booking_Amount__c</field>
        <formula>CAM_Forecast_Amount__c</formula>
        <name>yTest Forecasting</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Change Opty Project Lead Record Type to Standard for Pre-RFQ%2FRFQ Stage</fullName>
        <actions>
            <name>CAM_Set_Oppty_RT_Standard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Recordtype = Universe
Stage Is Changed</description>
        <formula>RecordType.Name  =  &apos;Universe&apos; &amp;&amp; ISCHANGED( StageName ) &amp;&amp; not(ISPICKVAL( StageName, &apos;Closed Cancelled&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change Opty Standard Record Type to Project Lead for Project Lead Stage</fullName>
        <actions>
            <name>Opportunity_Stage_to_Project_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Standard</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Universe</value>
        </criteriaItems>
        <description>Record Type = Standard
Stage = Universe</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Closed Date Default to Today</fullName>
        <actions>
            <name>Closed_Date_Default_to_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Closed Date default to Today</description>
        <formula>IsClosed &amp;&amp;  isnull(Closed_Date__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Water Depth from Infield Data</fullName>
        <actions>
            <name>Populate_Water_Depth_from_Infield_Data</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populates Water Depth from Infield Data into an editable field, allowing for user over-writes.</description>
        <formula>ISCHANGED(Infield_Data__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Project Lead Needs Assessment</fullName>
        <actions>
            <name>Project_Project_Lead_Initial_Assessment</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Project Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Project Lead</value>
        </criteriaItems>
        <description>Sends email to Project Lead owner 2 weeks from creation when Project Lead Amount remains 0 and Stage = Project Lead</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Project_Project_Lead_Initial_Assessment_Overdue</name>
                <type>Alert</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Project Lead Send Follow-Up Email</fullName>
        <active>true</active>
        <description>Send Product Lead Follow up eMail when the follow up date is reached</description>
        <formula>not(isnull(Project_Lead_Follow_Up_Date__c )) &amp;&amp;  ISPICKVAL( StageName , &quot;Project Lead&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Project_Lead_Reminder_Email_for_Follow_Up</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Project_Lead_Follow_Up_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Created Stage</fullName>
        <actions>
            <name>Set_Created_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populates the custom field &quot;Created Stage&quot; on opportunity when record is created or when Created Stage was previously not populated</description>
        <formula>AND( NOT(ISBLANK(TEXT(StageName))), OR(RecordType.DeveloperName=&quot;Standard&quot;, RecordType.DeveloperName=&quot;Bucket&quot;, RecordType.DeveloperName=&quot;Universe&quot;) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty BU Picklist Drilling - Drilling</fullName>
        <actions>
            <name>Set_Oppty_BU_Picklist_Drilling_Drillin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Owner Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( OwnerId )) &amp;&amp;   	ISPICKVAL( Owner.camBusinessUnit__c,&apos;Drilling&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty BU Picklist Drilling - LeTourneau</fullName>
        <actions>
            <name>Set_Oppty_BU_Picklist_Drilling_LeTourn</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Owner Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( OwnerId )) &amp;&amp;   	ISPICKVAL( Owner.camBusinessUnit__c,&apos;LeTourneau&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty BU Picklist Drilling - Pressure Control</fullName>
        <actions>
            <name>Set_Oppty_BU_Picklist_Drilling_Pressur</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Owner Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( OwnerId )) &amp;&amp;   	ISPICKVAL( Owner.camBusinessUnit__c,&apos;Pressure Control&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty BU Picklist Drilling - Sense</fullName>
        <actions>
            <name>Set_Oppty_BU_Picklist_Drilling_Sense</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Owner Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( OwnerId )) &amp;&amp;   	ISPICKVAL( Owner.camBusinessUnit__c,&apos;Sense&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty BU Picklist OSS Proc Sys Meters</fullName>
        <actions>
            <name>Set_Oppty_BU_Picklist_OSS_Proc_Sys_Meter</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Owner Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( OwnerId )) &amp;&amp;   	ISPICKVAL( Owner.camBusinessUnit__c,&apos;OSS Proc Sys Meters&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty BU Picklist OSS Proc Sys Pumps</fullName>
        <actions>
            <name>Set_Oppty_BU_Picklist_OSS_Proc_Sys_Pumps</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Owner Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( OwnerId )) &amp;&amp;   	ISPICKVAL( Owner.camBusinessUnit__c,&apos;OSS Proc Sys Pumps&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty BU Picklist PS - CPS</fullName>
        <actions>
            <name>Set_Oppty_BU_Picklist_PS_CPS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Owner Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( OwnerId )) &amp;&amp;   	ISPICKVAL( Owner.camBusinessUnit__c,&apos;CPS&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty BU Picklist PS - CPS Aftermarket</fullName>
        <actions>
            <name>Set_Oppty_BU_Picklist_PS_CPS_Aftermark</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Owner Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( OwnerId )) &amp;&amp;   	ISPICKVAL( Owner.camBusinessUnit__c,&apos;CPS Aftermarket&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty BU Picklist PS - Wellhead</fullName>
        <actions>
            <name>Set_Oppty_BU_Picklist_PS_Wellhead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Owner Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( OwnerId )) &amp;&amp;   	ISPICKVAL( Owner.camBusinessUnit__c,&apos;Wellhead&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty BU Picklist Processing Systems</fullName>
        <actions>
            <name>Set_Oppty_BU_Picklist_Processing_Systems</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Owner Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( OwnerId )) &amp;&amp;   	ISPICKVAL( Owner.camBusinessUnit__c,&apos;Processing Systems&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty BU Picklist Production Systems</fullName>
        <actions>
            <name>Set_Oppty_BU_Picklist_Production_Systems</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Owner Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( OwnerId )) &amp;&amp;   	ISPICKVAL( Owner.camBusinessUnit__c,&apos;Production Systems&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty BU Picklist Subsea Services</fullName>
        <actions>
            <name>Set_Oppty_BU_Picklist_Subsea_Services</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Owner Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( OwnerId )) &amp;&amp;   	ISPICKVAL( Owner.camBusinessUnit__c,&apos;Subsea Services&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty BU Picklist Surface - ALS</fullName>
        <actions>
            <name>Set_Oppty_BU_Picklist_Surface_ALS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Owner Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( OwnerId )) &amp;&amp;   	ISPICKVAL( Owner.camBusinessUnit__c,&apos;ALS&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty BU Picklist Surface - CAMSHALE</fullName>
        <actions>
            <name>Set_Oppty_BU_Picklist_Surface_CAMSHALE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Owner Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( OwnerId )) &amp;&amp;   	ISPICKVAL( Owner.camBusinessUnit__c,&apos;CAMSHALE&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty BU Picklist Surface - FLC</fullName>
        <actions>
            <name>Set_Oppty_BU_Picklist_Surface_FLC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Owner Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( OwnerId )) &amp;&amp;   	ISPICKVAL( Owner.camBusinessUnit__c,&apos;FLC&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty BU Picklist Surface - SUR</fullName>
        <actions>
            <name>Set_Oppty_BU_Picklist_Surface_SUR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Owner Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( OwnerId )) &amp;&amp;   	ISPICKVAL( Owner.camBusinessUnit__c,&apos;SUR&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty BU Picklist Swivel %26 Marine Systems</fullName>
        <actions>
            <name>Set_Oppty_BU_Picklist_Swivel_Marine</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Owner Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( OwnerId )) &amp;&amp;   	ISPICKVAL( Owner.camBusinessUnit__c,&apos;Swivel &amp; Marine Systems&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty BU Picklist V%26M - DSV</fullName>
        <actions>
            <name>Set_Oppty_BU_Picklist_V_M_DSV</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Owner Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( OwnerId )) &amp;&amp;   	ISPICKVAL( Owner.camBusinessUnit__c,&apos;DSV&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty BU Picklist V%26M - EPV</fullName>
        <actions>
            <name>Set_Oppty_BU_Picklist_V_M_EPV</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Owner Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( OwnerId )) &amp;&amp;   	ISPICKVAL( Owner.camBusinessUnit__c,&apos;EPV&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty BU Picklist V%26M - Measurement</fullName>
        <actions>
            <name>Set_Oppty_BU_Picklist_V_M_Measurement</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Owner Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( OwnerId )) &amp;&amp;   	ISPICKVAL( Owner.camBusinessUnit__c,&apos;Measurement&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty BU Picklist V%26M - Valve Automation</fullName>
        <actions>
            <name>Set_Oppty_BU_Pklst_V_M_Valve_Automation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Business Unit Picklist to the Owner Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( OwnerId )) &amp;&amp;   	ISPICKVAL( Owner.camBusinessUnit__c,&apos;Valve Automation&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Oppty BU Picklist V%26M - Valves</fullName>
        <actions>
            <name>Set_Oppty_BU_Picklist_V_M_Valves</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Product Business Unit Picklist to the Owner Business Unit value</description>
        <formula>(ISNEW() || ISCHANGED( OwnerId )) &amp;&amp;   	ISPICKVAL( Owner.camBusinessUnit__c,&apos;Valves&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Opty Div%2FBusUnit when Owner changes</fullName>
        <actions>
            <name>Set_Opportunity_Bus_Unit_to_Owner_Bus_Un</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Opportunity_Division_to_Owner_Div</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Opty Div/BusUnit when Owner changes</description>
        <formula>ISNEW() || ISCHANGED( OwnerId )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Opty type to Above the Funnel</fullName>
        <actions>
            <name>Set_Get_to_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Go_to_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Opty_type_to_Above_Funnel</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Record Type = Above the Funnel
Stage = Above the Funnel</description>
        <formula>ISPICKVAL( StageName, &apos;Above the Funnel&apos;)&amp;&amp; RecordType.Name &lt;&gt; &apos;Above the Funnel&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Opty type to Best Few</fullName>
        <actions>
            <name>Set_Opty_type_to_In_Funnel_Best_Few</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Record Type = In the Funnel/Best Few
Stage = Best Few</description>
        <formula>ISPICKVAL( StageName, &apos;Best Few&apos;) &amp;&amp; RecordType.Name &lt;&gt; &apos;In the Funnel/Best Few&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Opty type to Bucket</fullName>
        <actions>
            <name>Set_Opty_type_to_Bucket</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Bucket</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Opty type to Closed Cancelled by Customer</fullName>
        <actions>
            <name>Set_Opty_Go_to_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Opty_type_to_Closed_Cancelled</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Record Type = Closed Cancelled
Stage =  Closed Cancelled by Customer</description>
        <formula>ISCHANGED( StageName ) &amp;&amp; ISPICKVAL( StageName, &apos;Closed Cancelled by Customer&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Opty type to Closed Lost</fullName>
        <actions>
            <name>Set_Opty_GO_to_100</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Opty_Get_to_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Opty_type_to_Closed_Lost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Record Type = Closed Lost
Stage = Closed Lost</description>
        <formula>ISCHANGED( StageName ) &amp;&amp; ISPICKVAL( StageName, &apos;Closed Lost&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Opty type to Closed No Bid</fullName>
        <actions>
            <name>Set_Opty_GO_to_100</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Opty_Get_to_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Opty_Quote_Status_to_Closed_No_Bid</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Opty_Type_to_Closed_No_Bid</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Record Type = Closed No-Bid
Stage = Closed No-Bid</description>
        <formula>ISCHANGED( StageName ) &amp;&amp; ISPICKVAL( StageName, &apos;Closed No-Bid&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Opty type to Closed Rejected</fullName>
        <actions>
            <name>Set_Opty_type_to_Project_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Stage = Closed Rejected
Record Type = Universe</description>
        <formula>ISCHANGED( StageName ) &amp;&amp; ISPICKVAL( StageName, &apos;Closed Rejected&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Opty type to Closed Won</fullName>
        <actions>
            <name>Set_Opty_GO_to_100</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Opty_Get_to_100</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Opty_type_to_Closed_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Record Type = Closed Won
Stage = Closed Won</description>
        <formula>ISCHANGED( StageName ) &amp;&amp; ISPICKVAL( StageName, &apos;Closed Won&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Opty type to In The Funnel</fullName>
        <actions>
            <name>Set_Opty_type_to_In_Funnel_Best_Few</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Record Type = In the Funnel
Stage = In the Funnel</description>
        <formula>ISPICKVAL( StageName, &apos;In the Funnel&apos;) &amp;&amp; RecordType.Name &lt;&gt; &apos;In the Funnel/Best Few&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Opty type to Universe</fullName>
        <actions>
            <name>Set_Get_to_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Go_to_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Opty_type_to_Universe</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Record Type = Universe
Stage = Universe</description>
        <formula>AND(or(isnew(),ischanged( StageName )),ISPICKVAL( StageName, &apos;Universe&apos;),RecordType.Name &lt;&gt; &apos;Universe&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Project Lead Close Date</fullName>
        <actions>
            <name>Set_Close_Date_from_Follow_Up_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the &apos;Close Date&apos; to the Last day of the month of the Project Lead Follow Up Date value.</description>
        <formula>and(  RecordType.Name = &quot;Universe&quot;,  not(ischanged(CloseDate)),   ischanged(REMOVE_Project_Lead_Follow_Up_Date__c),  not(isblank(REMOVE_Project_Lead_Follow_Up_Date__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Project Lead Follow-Up Date</fullName>
        <active>true</active>
        <formula>AND( RecordType.Name = &quot;Universe&quot;, REMOVE_Project_Lead_Follow_Up_Date__c&lt;&gt;Datevalue(&quot;1900-01-01&quot;), REMOVE_Project_Lead_Follow_Up_Date__c&gt;Today(), NOT(ISBLANK(TEXT(Project_Lead_Follow_Up_Month__c))), NOT(ISBLANK(TEXT(Project_Lead_Follow_Up_Year__c))) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Project_Lead_Follow_Up_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.REMOVE_Project_Lead_Follow_Up_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SetValidationMessage</fullName>
        <actions>
            <name>SetValidationMessage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This will set the validation Message if the stage has not changed</description>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>yTest Forecasting</fullName>
        <actions>
            <name>yTest_Forecasting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ischanged( CAM_Forecast_Amount__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
