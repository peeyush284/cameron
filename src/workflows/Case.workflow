<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Case_Comment_Added_Account</fullName>
        <description>New Case Comment Added (Account)</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Cameron_Sales_Email_Templates/New_Case_Comment_Added_Account</template>
    </alerts>
    <alerts>
        <fullName>New_Case_Comment_Added_CRM_Ticket</fullName>
        <description>New Case Comment Added (CRM Ticket)</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Cameron_Sales_Email_Templates/New_Case_Comment_Added_CRM_Ticket</template>
    </alerts>
    <alerts>
        <fullName>New_Case_Comment_Added_Oppty</fullName>
        <description>New Case Comment Added (Oppty)</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Cameron_Sales_Email_Templates/New_Case_Comment_Added_Oppty</template>
    </alerts>
    <fieldUpdates>
        <fullName>Escalate_to_Admin</fullName>
        <field>OwnerId</field>
        <lookupValue>CRM_Admins</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Escalate to Admin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Escalate to Admin</fullName>
        <actions>
            <name>Escalate_to_Admin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
 ISCHANGED( Status ),
  text(Status) = &quot;Escalated&quot;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New Comment Added %28Account%29</fullName>
        <actions>
            <name>New_Case_Comment_Added_Account</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>and( RecordType.DeveloperName = &quot;Account_Change_Request&quot;, ISCHANGED( Last_Comment__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New Comment Added %28CRM Ticket%29</fullName>
        <actions>
            <name>New_Case_Comment_Added_CRM_Ticket</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>and( RecordType.DeveloperName = &quot;CRM_Change_Request&quot;, ISCHANGED( Last_Comment__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New Comment Added %28Oppty%29</fullName>
        <actions>
            <name>New_Case_Comment_Added_Oppty</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>and( RecordType.DeveloperName = &quot;Opportunity_Change_Request&quot;, ISCHANGED( Last_Comment__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
