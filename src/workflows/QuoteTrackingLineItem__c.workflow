<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Primary_Product_Helper_Null</fullName>
        <field>Unique_Primary_Product_Helper__c</field>
        <name>Primary Product Helper Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Parent_Quote_Tracking_ID</fullName>
        <field>Unique_Primary_Product_Helper__c</field>
        <formula>Quote_Tracking__r.Id</formula>
        <name>Set Parent Quote Tracking ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Brand_CDX</fullName>
        <field>Product_Brand__c</field>
        <literalValue>CDX</literalValue>
        <name>Set QT Line Brand CDX</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Brand_CYNARA</fullName>
        <field>Product_Brand__c</field>
        <literalValue>CYNARA</literalValue>
        <name>Set QT Line Brand CYNARA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Brand_Cameron</fullName>
        <field>Product_Brand__c</field>
        <literalValue>Cameron</literalValue>
        <name>Set QT Line Brand Cameron</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Brand_ConSept</fullName>
        <field>Product_Brand__c</field>
        <literalValue>ConSept</literalValue>
        <name>Set QT Line Brand ConSept</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Brand_CryoCAM</fullName>
        <field>Product_Brand__c</field>
        <literalValue>CryoCAM</literalValue>
        <name>Set QT Line Brand CryoCAM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Brand_Generic</fullName>
        <field>Product_Brand__c</field>
        <literalValue>Generic</literalValue>
        <name>Set QT Line Brand Generic</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Brand_KCC</fullName>
        <field>Product_Brand__c</field>
        <literalValue>KCC</literalValue>
        <name>Set QT Line Brand KCC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Brand_METROL</fullName>
        <field>Product_Brand__c</field>
        <literalValue>METROL</literalValue>
        <name>Set QT Line Brand METROL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Brand_MOZLEY</fullName>
        <field>Product_Brand__c</field>
        <literalValue>MOZLEY</literalValue>
        <name>Set QT Line Brand MOZLEY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Brand_NATCO</fullName>
        <field>Product_Brand__c</field>
        <literalValue>NATCO</literalValue>
        <name>Set QT Line Brand NATCO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Brand_PETRECO</fullName>
        <field>Product_Brand__c</field>
        <literalValue>PETRECO</literalValue>
        <name>Set QT Line Brand PETRECO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Brand_POLYMEM</fullName>
        <field>Product_Brand__c</field>
        <literalValue>POLYMEM</literalValue>
        <name>Set QT Line Brand POLYMEM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Brand_PORTA_TEST</fullName>
        <field>Product_Brand__c</field>
        <literalValue>PORTA-TEST</literalValue>
        <name>Set QT Line Brand PORTA-TEST</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Brand_Paques</fullName>
        <field>Product_Brand__c</field>
        <literalValue>Paques</literalValue>
        <name>Set QT Line Brand Paques</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Brand_PuraSpec</fullName>
        <field>Product_Brand__c</field>
        <literalValue>PuraSpec</literalValue>
        <name>Set QT Line Brand PuraSpec</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Brand_TST</fullName>
        <field>Product_Brand__c</field>
        <literalValue>TST</literalValue>
        <name>Set QT Line Brand TST</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Brand_UNICEL</fullName>
        <field>Product_Brand__c</field>
        <literalValue>UNICEL</literalValue>
        <name>Set QT Line Brand UNICEL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Brand_VORTOIL</fullName>
        <field>Product_Brand__c</field>
        <literalValue>VORTOIL</literalValue>
        <name>Set QT Line Brand VORTOIL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Brand_VRU</fullName>
        <field>Product_Brand__c</field>
        <literalValue>VRU</literalValue>
        <name>Set QT Line Brand VRU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Brand_WEMCO</fullName>
        <field>Product_Brand__c</field>
        <literalValue>WEMCO</literalValue>
        <name>Set QT Line Brand WEMCO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Class_Gas_CO2</fullName>
        <field>Product_Class__c</field>
        <literalValue>Gas CO2</literalValue>
        <name>Set QT Line Class Gas CO2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Class_Gas_MEG_Reclamation</fullName>
        <field>Product_Class__c</field>
        <literalValue>Gas MEG Reclamation</literalValue>
        <name>Set QT Line Class Gas MEG Reclamation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Class_Gas_Scrub_Separation</fullName>
        <field>Product_Class__c</field>
        <literalValue>Gas Scrubbing/Separation</literalValue>
        <name>Set QT Line Class Gas Scrub/Separation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Class_Gas_Treating</fullName>
        <field>Product_Class__c</field>
        <literalValue>Gas Treating</literalValue>
        <name>Set QT Line Class Gas Treating</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Class_Oil_2_3_Phs_Separation</fullName>
        <field>Product_Class__c</field>
        <literalValue>Oil 2 and 3 Phase Separation</literalValue>
        <name>Set QT Line Class Oil 2/3 Phs Separation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Class_Oil_Dehyd_Desalt</fullName>
        <field>Product_Class__c</field>
        <literalValue>Oil Dehydration &amp; Desalting</literalValue>
        <name>Set QT Line Class Oil Dehyd/Desalt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Class_Oil_Distill_Treatment</fullName>
        <field>Product_Class__c</field>
        <literalValue>Oil Distillate Treatment</literalValue>
        <name>Set QT Line Class Oil Distill Treatment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Class_Solids_Sep_Treatment</fullName>
        <field>Product_Class__c</field>
        <literalValue>Solids Separation Treatment</literalValue>
        <name>Set QT Line Class Solids Sep Treatment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Class_Studies</fullName>
        <field>Product_Class__c</field>
        <literalValue>Studies</literalValue>
        <name>Set QT Line Class Studies</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Class_Water_Produced</fullName>
        <field>Product_Class__c</field>
        <literalValue>Water - Produced</literalValue>
        <name>Set QT Line Class Water - Produced</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QT_Line_Class_Water_Seawater_Raw</fullName>
        <field>Product_Class__c</field>
        <literalValue>Water - Seawater/Raw Water</literalValue>
        <name>Set QT Line Class Water - Seawater/ Raw</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Primary Product Helper Null</fullName>
        <actions>
            <name>Primary_Product_Helper_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>QuoteTrackingLineItem__c.Primary_Product__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Primary Product Populate</fullName>
        <actions>
            <name>Set_Parent_Quote_Tracking_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>QuoteTrackingLineItem__c.Primary_Product__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Brand CDX</fullName>
        <actions>
            <name>Set_QT_Line_Brand_CDX</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Brand__c = &quot;CDX&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Brand CYNARA</fullName>
        <actions>
            <name>Set_QT_Line_Brand_CYNARA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Brand__c = &quot;CYNARA&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Brand Cameron</fullName>
        <actions>
            <name>Set_QT_Line_Brand_Cameron</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Brand__c = &quot;Cameron&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Brand ConSept</fullName>
        <actions>
            <name>Set_QT_Line_Brand_ConSept</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Brand__c = &quot;ConSept&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Brand CryoCAM</fullName>
        <actions>
            <name>Set_QT_Line_Brand_CryoCAM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Brand__c = &quot;CryoCAM&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Brand Generic</fullName>
        <actions>
            <name>Set_QT_Line_Brand_Generic</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Brand__c = &quot;Generic&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Brand KCC</fullName>
        <actions>
            <name>Set_QT_Line_Brand_KCC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Brand__c = &quot;KCC&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Brand METROL</fullName>
        <actions>
            <name>Set_QT_Line_Brand_METROL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Brand__c = &quot;METROL&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Brand MOZLEY</fullName>
        <actions>
            <name>Set_QT_Line_Brand_MOZLEY</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Brand__c = &quot;MOZLEY&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Brand NATCO</fullName>
        <actions>
            <name>Set_QT_Line_Brand_NATCO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Brand__c = &quot;NATCO&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Brand PETRECO</fullName>
        <actions>
            <name>Set_QT_Line_Brand_PETRECO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Brand__c = &quot;PETRECO&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Brand POLYMEM</fullName>
        <actions>
            <name>Set_QT_Line_Brand_POLYMEM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Brand__c = &quot;POLYMEM&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Brand PORTA-TEST</fullName>
        <actions>
            <name>Set_QT_Line_Brand_PORTA_TEST</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Brand__c = &quot;PORTA-TEST&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Brand Paques</fullName>
        <actions>
            <name>Set_QT_Line_Brand_Paques</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Brand__c = &quot;Paques&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Brand PuraSpec</fullName>
        <actions>
            <name>Set_QT_Line_Brand_PuraSpec</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Brand__c = &quot;PuraSpec&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Brand TST</fullName>
        <actions>
            <name>Set_QT_Line_Brand_TST</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Brand__c = &quot;TST&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Brand UNICEL</fullName>
        <actions>
            <name>Set_QT_Line_Brand_UNICEL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Brand__c = &quot;UNICEL&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Brand VORTOIL</fullName>
        <actions>
            <name>Set_QT_Line_Brand_VORTOIL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Brand__c = &quot;VORTOIL&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Brand VRU</fullName>
        <actions>
            <name>Set_QT_Line_Brand_VRU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Brand__c = &quot;VRU&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Brand WEMCO</fullName>
        <actions>
            <name>Set_QT_Line_Brand_WEMCO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Brand__c = &quot;WEMCO&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Class Gas CO2</fullName>
        <actions>
            <name>Set_QT_Line_Class_Gas_CO2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Class__c = &quot;Gas CO2&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Class Gas MEG Reclamation</fullName>
        <actions>
            <name>Set_QT_Line_Class_Gas_MEG_Reclamation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Class__c = &quot;Gas MEG Reclamation&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Class Gas Scrubbing%2F Separation</fullName>
        <actions>
            <name>Set_QT_Line_Class_Gas_Scrub_Separation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Class__c = &quot;Gas Scrubbing/Separation&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Class Gas Treating</fullName>
        <actions>
            <name>Set_QT_Line_Class_Gas_Treating</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Class__c = &quot;Gas Treating&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Class Oil 2 and 3 Phase Separation</fullName>
        <actions>
            <name>Set_QT_Line_Class_Oil_2_3_Phs_Separation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Class__c = &quot;Oil 2 and 3 Phase Separation&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Class Oil Dehydration %26 Desalting</fullName>
        <actions>
            <name>Set_QT_Line_Class_Oil_Dehyd_Desalt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Class__c = &quot;Oil Dehydration &amp; Desalting&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Class Oil Distillate Treatment</fullName>
        <actions>
            <name>Set_QT_Line_Class_Oil_Distill_Treatment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Class__c = &quot;Oil Distillate Treatment&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Class Solids Separation Treatment</fullName>
        <actions>
            <name>Set_QT_Line_Class_Solids_Sep_Treatment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Class__c = &quot;Solids Separation Treatment&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Class Studies</fullName>
        <actions>
            <name>Set_QT_Line_Class_Studies</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Class__c = &quot;Studies&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Class Water - Produced</fullName>
        <actions>
            <name>Set_QT_Line_Class_Water_Produced</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Class__c = &quot;Water - Produced&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set QT Line Class Water - Seawater%2F Raw Water</fullName>
        <actions>
            <name>Set_QT_Line_Class_Water_Seawater_Raw</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Products__r.Class__c = &quot;Water - Seawater/Raw Water&quot;, ISNEW(), OR( Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS&quot;, Quote_Tracking__r.RecordType.DeveloperName = &quot;Quote_Tracking_PS_Closed&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
