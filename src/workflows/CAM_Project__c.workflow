<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_eMail_if_Project_External_Status_changes_to_Permanent</fullName>
        <description>Send eMail if Project(External) Status changes to Permanent</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Cameron_Sales_Email_Templates/Project_External_Stage_Change_to_Permanent</template>
    </alerts>
    <alerts>
        <fullName>Send_eMail_if_Project_Internal_Status_changes_to_Permanent</fullName>
        <description>Send eMail if Project(Internal) Status changes to Permanent</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Cameron_Sales_Email_Templates/Project_Internal_Stage_Change_to_Permanent</template>
    </alerts>
    <alerts>
        <fullName>Send_eMail_to_Project_Owner</fullName>
        <description>Send eMail to Project Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Cameron_Sales_Email_Templates/Project_New_Project_Created</template>
    </alerts>
    <fieldUpdates>
        <fullName>CAM_Set_Status_Date</fullName>
        <field>Status_Changed_Date__c</field>
        <formula>Today()</formula>
        <name>Set Status Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ChangeOwnerToCameron</fullName>
        <field>OwnerId</field>
        <lookupValue>cameronadmin@cam_sfdc.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>ChangeOwnerToCameron</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ChangeOwnerToCameron</fullName>
        <actions>
            <name>ChangeOwnerToCameron</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When project is made permanent, change the owner to Cameron Admin</description>
        <formula>NOT(ISNEW())&amp;&amp;  (RecordType.Name = &apos;External&apos;&amp;&amp;ISCHANGED(Status__c) &amp;&amp; ISPICKVAL(Status__c, &apos;Permanent&apos;))||(ISCHANGED( RecordTypeId ) &amp;&amp; RecordType.Name = &apos;External&apos; &amp;&amp; ISPICKVAL(Status__c, &apos;Permanent&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Project Set Status Date</fullName>
        <actions>
            <name>CAM_Set_Status_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( ISNEW(), ISCHANGED(Status__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Email - New Project</fullName>
        <actions>
            <name>Send_eMail_to_Project_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send Email to Project Owner
Project Record Type = External
Status changes to &quot;Permanant&quot;</description>
        <formula>AND(RecordType.Name=&apos;External&apos;,ISPICKVAL(Status__c, &apos;Permanent&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send Email - Project %28External%29 Status Change to Permanent</fullName>
        <actions>
            <name>Send_eMail_if_Project_External_Status_changes_to_Permanent</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send Email to Project Owner
Status changes to &quot;Permanent&quot;
Record Type = External</description>
        <formula>NOT(ISNEW())&amp;&amp;  (RecordType.Name = &apos;External&apos;&amp;&amp;ISCHANGED(Status__c) &amp;&amp; ISPICKVAL(Status__c, &apos;Permanent&apos;))||(ISCHANGED( RecordTypeId ) &amp;&amp; RecordType.Name = &apos;External&apos; &amp;&amp; ISPICKVAL(Status__c, &apos;Permanent&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Email - Project %28Internal%29 Status Change to Permanent</fullName>
        <actions>
            <name>Send_eMail_if_Project_Internal_Status_changes_to_Permanent</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send Email to Project Owner
Status changes to &quot;Permanent&quot;
Record Type = Internal</description>
        <formula>NOT(ISNEW())&amp;&amp;  RecordType.Name = &apos;Internal&apos;&amp;&amp;ISCHANGED(Status__c)  &amp;&amp; ISPICKVAL(Status__c, &apos;Permanent&apos;)||(ISCHANGED( RecordTypeId ) &amp;&amp; RecordType.Name = &apos;Internal&apos; &amp;&amp; ISPICKVAL(Status__c, &apos;Permanent&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
