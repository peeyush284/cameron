<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PicklistValue_Update_External_Id</fullName>
        <field>External_Id__c</field>
        <formula>Parent_External_Id__c &amp; &quot;|&quot; &amp;  Name</formula>
        <name>PicklistValue Update External Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Picklist_Value_External_Id</fullName>
        <field>External_Id__c</field>
        <formula>if( Type__c = &quot;DIVISION&quot;,Name,Parent_External_Id__c &amp; &quot;|&quot; &amp; Name)</formula>
        <name>Set Picklist Value External Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Ch_Characterstics_Hierarchy_Level</fullName>
        <field>Hierarchy_Level__c</field>
        <formula>Parent_Pick_List_Value__r.Hierarchy_Level__c + 1</formula>
        <name>Update Ch Characterstics Hierarchy Level</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Characterstics_Hierarchy</fullName>
        <field>Characterstics_Hierarchy__c</field>
        <formula>Parent_Pick_List_Value__r.Characterstics_Hierarchy__c+&apos;,&apos;+  CASESAFEID(Parent_Pick_List_Value__r.Id)</formula>
        <name>Update Characterstics Hierarchy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Characterstics_Hierarchy_ParentId</fullName>
        <field>Characterstics_Hierarchy__c</field>
        <formula>CASESAFEID(Parent_Pick_List_Value__r.Id)</formula>
        <name>Update Characterstics Hierarchy ParentId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Division_Level_Field_Update</fullName>
        <field>Hierarchy_Level__c</field>
        <formula>0</formula>
        <name>Update Division Level Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Hierarchial_API</fullName>
        <field>Hierarchy_Control_API__c</field>
        <formula>Parent_Pick_List_Value__r.Hierarchy_Control_API__c</formula>
        <name>Update Hierarchial API</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Hierarchial_API_with_Parent_Field</fullName>
        <field>Hierarchy_Control_API__c</field>
        <formula>Parent_Pick_List_Value__r.Field_API__c</formula>
        <name>Update Hierarchial API with Parent Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Hierarchial API</fullName>
        <actions>
            <name>Update_Hierarchial_API</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISBLANK(Parent_Pick_List_Value__r.Hierarchy_Control_API__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Picklist Value External Id</fullName>
        <actions>
            <name>Set_Picklist_Value_External_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Concatenate Parent External Id with Name</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Characterstics Hierarchy</fullName>
        <actions>
            <name>Update_Characterstics_Hierarchy</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISBLANK( Parent_Pick_List_Value__r.Characterstics_Hierarchy__c )),NOT(ISNULL(Parent_Pick_List_Value__r.Characterstics_Hierarchy__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Child Characteristics Hierarchy Level</fullName>
        <actions>
            <name>Update_Ch_Characterstics_Hierarchy_Level</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISNULL(Parent_Pick_List_Value__c)), NOT(ISBLANK(Parent_Pick_List_Value__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Division Level</fullName>
        <actions>
            <name>Update_Division_Level_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PicklistValues__c.Type__c</field>
            <operation>equals</operation>
            <value>DIVISION</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Hierarchial Parent API</fullName>
        <actions>
            <name>Update_Hierarchial_API_with_Parent_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(ISBLANK( Parent_Pick_List_Value__r.Hierarchy_Control_API__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Parent Id Characterstic</fullName>
        <actions>
            <name>Update_Characterstics_Hierarchy_ParentId</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISNULL(Parent_Pick_List_Value__r.Characterstics_Hierarchy__c),ISBLANK(Parent_Pick_List_Value__r.Characterstics_Hierarchy__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
