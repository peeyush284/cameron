/**
* @author       Peeyush Awadhiya
* @date         07/09/2014
* @description  This class serves unit tests to the webservice class to be invoked from a custom buttom on IOI object.
*               It does following:
*               1. On click of the custom button "Submit Chatter Posts":
*                   A. If Status = Submitted and not already posted, it creates a FeedItem on related account, opportunity, project records.
*                   B. Creates custom object Chatter Post records with Ids of FeedItems posted.
*                   C. Set Visibility picklist value to 1 Default Public Read only.
*
*               2. On click of the custom button "Delete Chatter Posts":
*                   A. Delete FeedItems on related account, opportunity, project records.
*                   B. Delete all custom object Chatter Post records with Ids of FeedItems posted.
*                   C. Set Visibility picklist value to 0 Private Owner Manager only.
*/
@isTest


public with sharing class IOISetVisibility_TEST {
    
    /**
    * @author       Peeyush Awadhiya
    * @date         07/09/2014
    * @description  This method test the method which asynchronously post the chatter post of IOI information to Account, Opportunity and Project record 
    *               and saves the chatter post Id as related custom object record.
    * @param        void
    * @return       void 
    */ 
    public static testMethod void testIOISubmitChatterPost(){
        // Prepare the test data
        Account acc;
        CAM_Project__c project;
        Opportunity oppty;
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('Bucket', 'RFQ', project);
        oppty.end_user__c = acc.id;
        oppty.CAM_Division__c = 'Surface';
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        insert oppty;
        
        Set<Id> parentIdSet = new Set<Id>();
        
        parentIdSet.add(acc.Id);
        parentIdSet.add(oppty.Id);
        parentIdSet.add(project.Id);
        
        IOI__c ioiRecord = TestUtilities.generateIOI('Test IOI', acc.Id, oppty.Id, project.Id, 'Submitted');
        Period__c period =  new Period__c(Name = 'Test', StartDate__c = Date.TODAY(), EndDate__c = Date.TODAY());
        insert period;
        ioiRecord.Period__c = period.Id;
        insert ioiRecord;
        
        // Post the chatter records
        Test.startTest();
            IOISetVisibility.submitChatterPost((String)ioiRecord.Id);
        Test.stopTest();
        
        // Assert 3 Feed items posted
        System.assertEquals([SELECT Id FROM FeedItem WHERE ParentId IN :parentIdSet].size(), 3);
        
        // Assert 3 Chatter Post Records
        System.assertEquals([SELECT Id FROM IOI_Chatter_Feed__c WHERE IOI__c =:ioiRecord.Id].size(), 3);
        
        // Assert the visibility
        System.assertEquals([SELECT Id, Visibility_Level__c FROM IOI__c WHERE Id =:ioiRecord.Id LIMIT 1].Visibility_Level__c,System.Label.IOI_Set_Visibility_to_1_Default_Public_Read_only);
        
        
    }
    
    
    /**
    * @author       Peeyush Awadhiya
    * @date         07/09/2014
    * @description  This method test the method which asynchronously delete the chatter post of IOI information to Account, Opportunity and Project record 
    *               and deletes the chatter post related custom object records.
    * @param        void
    * @return       void 
    */ 
    public static testMethod void testIOIDeleteChatterPost(){
        // Prepare the test data
        Account acc;
        CAM_Project__c project;
        Opportunity oppty;
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('Bucket', 'RFQ', project);
        oppty.end_user__c = acc.id;
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Surface';
        insert oppty;
        
       
        IOI__c ioiRecord = TestUtilities.generateIOI('Test IOI', acc.Id, oppty.Id, project.Id, 'Submitted');
        Period__c period =  new Period__c(Name = 'Test', StartDate__c = Date.TODAY(), EndDate__c = Date.TODAY());
        insert period;
        ioiRecord.Period__c = period.Id;
        insert ioiRecord;
        
        // Post the chatter records
        List<FeedItem> feedList = new List<FeedItem>();
        
        FeedItem feed1 = TestUtilities.generateFeedItem('This is feed 1',acc.Id);
        feedList.add(feed1);
        
        FeedItem feed2 = TestUtilities.generateFeedItem('This is feed 2',oppty.Id);
        feedList.add(feed2);
        
        FeedItem feed3 = TestUtilities.generateFeedItem('This is feed 3',project.Id);
        feedList.add(feed3);
        
        insert feedList;
        
        Set<Id> parentIdSet = new Set<Id>();
        
        parentIdSet.add(acc.Id);
        parentIdSet.add(oppty.Id);
        parentIdSet.add(project.Id);
        
        // Create IOI Chatter Post 
        
        List<IOI_Chatter_Feed__c> chatterFeedList = new List<IOI_Chatter_Feed__c>();
        
        chatterFeedList.add(TestUtilities.generateChatterPostRecord('Account', feed1.Id));
        chatterFeedList.add(TestUtilities.generateChatterPostRecord('Opportunity', feed2.Id));
        chatterFeedList.add(TestUtilities.generateChatterPostRecord('Project', feed3.Id));
        
        for (IOI_Chatter_Feed__c feedRec:chatterFeedList){
            feedRec.IOI__c = ioiRecord.Id;
        }
        
        insert chatterFeedList;
        
        // Now delete the chatter posts
        
        Test.startTest();
            IOISetVisibility.deleteChatterPost((String)ioiRecord.Id);
        Test.stopTest();
        
        // Assert 0 Feed items posted
        System.assertEquals([SELECT Id FROM FeedItem WHERE ParentId IN :parentIdSet].size(), 0);
        
        // Assert 0 Chatter Post Records
        System.assertEquals([SELECT Id FROM IOI_Chatter_Feed__c WHERE IOI__c =:ioiRecord.Id].size(), 0);
        
        // Assert the visibility
        System.assertEquals([SELECT Id, Visibility_Level__c FROM IOI__c WHERE Id =:ioiRecord.Id LIMIT 1].Visibility_Level__c,System.Label.IOI_Set_Visibility_to_0_Private_Owner_Manager_only);
    
    }
    
   public static testMethod void testIOISubmitChatterPost_Exception_Status(){
        // Prepare the test data
        Account acc;
        CAM_Project__c project;
        Opportunity oppty;
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('Bucket', 'RFQ', project);
        oppty.end_user__c = acc.id;
        oppty.CAM_Division__c = 'Surface';
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        insert oppty;
        
        Set<Id> parentIdSet = new Set<Id>();
        
        parentIdSet.add(acc.Id);
        parentIdSet.add(oppty.Id);
        parentIdSet.add(project.Id);
        
        IOI__c ioiRecord = TestUtilities.generateIOI('Test IOI', acc.Id, oppty.Id, project.Id, 'In Progress');
        Period__c period =  new Period__c(Name = 'Test', StartDate__c = Date.TODAY(), EndDate__c = Date.TODAY());
        insert period;
        ioiRecord.Period__c = period.Id;
        insert ioiRecord;
        
        // Post the chatter records
        Test.startTest();
            IOISetVisibility.submitChatterPost((String)ioiRecord.Id);
        Test.stopTest();
        
    }
    public static testMethod void testIOISubmitChatterPost_Exception_Chatter(){
        // Prepare the test data
        Account acc;
        CAM_Project__c project;
        Opportunity oppty;
        User u = [Select id from user where isActive = True and ID!=:UserInfo.getUserId()  limit 1];
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('Bucket', 'RFQ', project);
        oppty.end_user__c = acc.id;
        oppty.CAM_Division__c = 'Surface';
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        insert oppty;
        
        Set<Id> parentIdSet = new Set<Id>();
        
        parentIdSet.add(acc.Id);
        parentIdSet.add(oppty.Id);
        parentIdSet.add(project.Id);
        
        IOI__c ioiRecord = TestUtilities.generateIOI('Test IOI', acc.Id, oppty.Id, project.Id, 'In Progress');
        Period__c period =  new Period__c(Name = 'Test', StartDate__c = Date.TODAY(), EndDate__c = Date.TODAY());
        insert period;
        ioiRecord.Period__c = period.Id;
        ioiRecord.Published_to_Chatter__c = True;
        insert ioiRecord;
        
        // Post the chatter records
        Test.startTest();
            IOISetVisibility.submitChatterPost((String)ioiRecord.Id);
        Test.stopTest();
        
    }
     
    public static testMethod void testIOIAssignReviewstoChatterPost(){
        // Prepare the test data
        Account acc;
        CAM_Project__c project;
        Opportunity oppty;
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('Bucket', 'RFQ', project);
        oppty.end_user__c = acc.id;
        oppty.CAM_Division__c = 'Surface';
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        insert oppty;
        
        Set<Id> parentIdSet = new Set<Id>();
        
        parentIdSet.add(acc.Id);
        parentIdSet.add(oppty.Id);
        parentIdSet.add(project.Id);
        
        IOI__c ioiRecord = TestUtilities.generateIOI('Test IOI', acc.Id, oppty.Id, project.Id, 'Submitted');
        Period__c period =  new Period__c(Name = 'Test', StartDate__c = Date.TODAY(), EndDate__c = Date.TODAY());
        insert period;
        ioiRecord.Period__c = period.Id;
        insert ioiRecord;
        
        // Post the chatter records
        Test.startTest();
            IOISetVisibility.submitChatterPost((String)ioiRecord.Id);
            ioiSetvisibility.assignReviewers((String)ioiRecord.Id);
            ioiSetvisibility.validateCurrentUser((String)ioiRecord.Id);
        Test.stopTest();
        
        // Assert 3 Feed items posted
        //System.assertEquals([SELECT Id FROM FeedItem WHERE ParentId IN :parentIdSet].size(), 3);
        
        // Assert 3 Chatter Post Records
        //System.assertEquals([SELECT Id FROM IOI_Chatter_Feed__c WHERE IOI__c =:ioiRecord.Id].size(), 3);
        
        // Assert the visibility
        //System.assertEquals([SELECT Id, Visibility_Level__c FROM IOI__c WHERE Id =:ioiRecord.Id LIMIT 1].Visibility_Level__c,System.Label.IOI_Set_Visibility_to_1_Default_Public_Read_only);
        
        
    }  
    public static testMethod void testIOIAssignReviewstoChatterPost_Ex1(){
        // Prepare the test data
        Account acc;
        CAM_Project__c project;
        Opportunity oppty;
        User u = [Select id from user where isActive = True and ID!=:UserInfo.getUserId()  limit 1];
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('Bucket', 'RFQ', project);
        oppty.end_user__c = acc.id;
        oppty.CAM_Division__c = 'Surface';
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        insert oppty;
        
        Set<Id> parentIdSet = new Set<Id>();
        
        parentIdSet.add(acc.Id);
        parentIdSet.add(oppty.Id);
        parentIdSet.add(project.Id);
        
        IOI__c ioiRecord = TestUtilities.generateIOI('Test IOI', acc.Id, oppty.Id, project.Id, 'Submitted');
        Period__c period =  new Period__c(Name = 'Test', StartDate__c = Date.TODAY(), EndDate__c = Date.TODAY());
        insert period;
        ioiRecord.Period__c = period.Id;
        ioiRecord.OwnerId = u.id;
        insert ioiRecord;
        
        // Post the chatter records
        Test.startTest();
            IOISetVisibility.submitChatterPost((String)ioiRecord.Id);
            ioiSetvisibility.assignReviewers((String)ioiRecord.Id);
            ioiSetvisibility.validateCurrentUser((String)ioiRecord.Id);
        Test.stopTest();
        
        // Assert 3 Feed items posted
        //System.assertEquals([SELECT Id FROM FeedItem WHERE ParentId IN :parentIdSet].size(), 3);
        
        // Assert 3 Chatter Post Records
        //System.assertEquals([SELECT Id FROM IOI_Chatter_Feed__c WHERE IOI__c =:ioiRecord.Id].size(), 3);
        
        // Assert the visibility
        //System.assertEquals([SELECT Id, Visibility_Level__c FROM IOI__c WHERE Id =:ioiRecord.Id LIMIT 1].Visibility_Level__c,System.Label.IOI_Set_Visibility_to_1_Default_Public_Read_only);
        
        
    }     
    public static testMethod void testIOIAssignReviewstoChatterPost_Ex2(){
        // Prepare the test data
        Account acc;
        CAM_Project__c project;
        Opportunity oppty;
        User u = [Select id from user where isActive = True and ID!=:UserInfo.getUserId()  limit 1];
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('Bucket', 'RFQ', project);
        oppty.end_user__c = acc.id;
        oppty.CAM_Division__c = 'Surface';
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        insert oppty;
        
        Set<Id> parentIdSet = new Set<Id>();
        
        parentIdSet.add(acc.Id);
        parentIdSet.add(oppty.Id);
        parentIdSet.add(project.Id);
        
        IOI__c ioiRecord = TestUtilities.generateIOI('Test IOI', acc.Id, oppty.Id, project.Id, 'Submitted');
        Period__c period =  new Period__c(Name = 'Test', StartDate__c = Date.TODAY(), EndDate__c = Date.TODAY());
        insert period;
        ioiRecord.Period__c = period.Id;
        ioiRecord.Next_Reviewer__c = u.id;
        insert ioiRecord;
        
        // Post the chatter records
        Test.startTest();
            IOISetVisibility.submitChatterPost((String)ioiRecord.Id);
            ioiSetvisibility.assignReviewers((String)ioiRecord.Id);
            ioiSetvisibility.validateCurrentUser((String)ioiRecord.Id);
        Test.stopTest();
        
        // Assert 3 Feed items posted
        //System.assertEquals([SELECT Id FROM FeedItem WHERE ParentId IN :parentIdSet].size(), 3);
        
        // Assert 3 Chatter Post Records
        //System.assertEquals([SELECT Id FROM IOI_Chatter_Feed__c WHERE IOI__c =:ioiRecord.Id].size(), 3);
        
        // Assert the visibility
        //System.assertEquals([SELECT Id, Visibility_Level__c FROM IOI__c WHERE Id =:ioiRecord.Id LIMIT 1].Visibility_Level__c,System.Label.IOI_Set_Visibility_to_1_Default_Public_Read_only);
        
        
    }  
     
}