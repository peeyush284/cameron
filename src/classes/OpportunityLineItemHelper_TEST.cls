@isTest (SeeAllData=true)
public class OpportunityLineItemHelper_TEST{

    static testMethod void testInsert() {
       
        Test.startTest(); 
        Account acc;
        CAM_Project__c project = new CAM_Project__c(); 
        Opportunity oppty = new Opportunity(); 
        //id opptyRecordTypeId = Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get("Standard").getRecordTypeId();
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc);              
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        PriceBook2 pb = [Select id from PriceBook2 where IsStandard = true limit 1];
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('In the Funnel/Best Few', 'Best Few', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'OneSubsea';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = project.Id;
        oppty.Pricebook2Id = pb.id;
        
        oppty.AccountId = acc.Id;
       
        insert oppty;
        /*
        Product2 prod = [Select id from Product2 where business_unit__c='OneSubsea' limit 1];
        //Product2 prod = TestUtilities.generateProduct('Prod1', 'Drilling', 'Drilling', '', '');  
        //insert prod;
        
        PriceBookEntry pbe = [Select id from PriceBookEntry where Pricebook2Id = :pb.id and Product2Id = :prod.id  limit 1];
        //PriceBookEntry pbe = new PriceBookEntry(Product2Id = prod.id,
        //                                        Pricebook2Id = pb.id, 
        //                                        UnitPrice = 1.0);
        //insert pbe;
          
        //OpportunityLineItem oLi = new OpportunityLineItem(OpportunityId=oppty.id, 
        //                                                  Quantity=1, 
        //                                                  UnitPrice=1.0);
                                                          //, 
                                                          //PricebookEntryId = pbe.id);
        
        //insert oli;
        
        */
        // Product
        Product2 prod = new Product2(Name = 'Surface Stack', Division__c = 'Surface', Group__c = 'ARTIFICIAL LIFT', isActive = true);
        insert prod;
        
        Pricebook2 priceBook= new Pricebook2(Name='Test Pricebook');
        insert priceBook;
        
        PricebookEntry pbEntry = new PricebookEntry(isActive = true,pricebook2Id = Test.getStandardPricebookId(), product2Id = prod.Id, UnitPrice=100, UseStandardPrice = false);
        insert pbEntry;
        
        OpportunityLineItem optyLine = new OpportunityLineItem(OpportunityId = oppty.Id, PricebookEntryId = pbEntry.Id);
        insert optyLine;
        
        optyLine = [SELECT Product_Division__c, Opportunity.AccountId, Opportunity.isClosed, Opportunity.Primary_Account_Manager_calc__c, Group__c, Opportunity.CloseDate, Aftermarket__c, Invoice_Location__c, Requested_Shipment_Date__c,Opportunity_Business_Unit_Picklist__c,Market_Requested_Delivery_Date__c,Product_Characterstics__c,Market_Requested_Delivery_Months__c,Market_Requested_Delivery_Weeks__c,Rig_Type__c,Forecasted__c,Order_Type__c,Brand__c,Model__c,Quantity__c,UnitPrice,Quantity, PricebookEntry.Product2.Name,PricebookEntry.Product2.Division__c,OpportunityId,Product_Hierarchy_calc__c,Opportunity.RecordType.Name FROM OpportunityLineItem WHERE Id =:optyLine.Id LIMIT 1]; 
       
        Test.stopTest();
                


    } 
     
     static testMethod void testUpdate() {

        
         Account acc;
        CAM_Project__c project = new CAM_Project__c(); 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc);              
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        PriceBook2 pb = [Select id from PriceBook2 where IsStandard = true limit 1];        
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('In the Funnel/Best Few', 'Best Few', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Drilling';
        oppty.stagename = 'Closed Won';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = project.Id;
        oppty.Pricebook2Id=pb.id;
        
        oppty.AccountId = acc.Id;

        insert oppty;     
        /*
        Product2 prod = TestUtilities.generateProduct('Prod1', 'Drilling', 'Drilling', '', '');  
        insert prod;        
        
        PriceBookEntry pbe = new PriceBookEntry(Product2Id = prod.id,
                                                Pricebook2Id = pb.id, 
                                                UnitPrice = 1.0);
        insert pbe;        
        
        //OpportunityLineItem oLi = new OpportunityLineItem(OpportunityId=oppty.id, 
        //                                                  Quantity=1, 
        //                                                  UnitPrice=1.0,
        //                                                  description='Line 1');
        
        //insert oli;           
        
        //oli.description = 'line 1 update';
        //update oli;
        
        //delete oli;
        */
        
        Product2 prod = new Product2(Name = 'Surface Stack', Division__c = 'Surface', Group__c = 'ARTIFICIAL LIFT', isActive = true);
        insert prod;
        
        Pricebook2 priceBook= new Pricebook2(Name='Test Pricebook');
        insert priceBook;
        
        PricebookEntry pbEntry = new PricebookEntry(isActive = true,pricebook2Id = Test.getStandardPricebookId(), product2Id = prod.Id, UnitPrice=100, UseStandardPrice = false);
        insert pbEntry;
        
        OpportunityLineItem optyLine = new OpportunityLineItem(OpportunityId = oppty.Id, PricebookEntryId = pbEntry.Id);
        insert optyLine;
        
        optyLine = [SELECT Product_Division__c, Opportunity.AccountId, Opportunity.isClosed, Opportunity.Primary_Account_Manager_calc__c, Group__c, Opportunity.CloseDate, Aftermarket__c, Invoice_Location__c, Requested_Shipment_Date__c,Opportunity_Business_Unit_Picklist__c,Market_Requested_Delivery_Date__c,Product_Characterstics__c,Market_Requested_Delivery_Months__c,Market_Requested_Delivery_Weeks__c,Rig_Type__c,Forecasted__c,Order_Type__c,Brand__c,Model__c,Quantity__c,UnitPrice,Quantity, PricebookEntry.Product2.Name,PricebookEntry.Product2.Division__c,OpportunityId,Product_Hierarchy_calc__c,Opportunity.RecordType.Name FROM OpportunityLineItem WHERE Id =:optyLine.Id LIMIT 1]; 
        Test.startTest();
            optyLine.Invoice_Location__c = 'USA';
            update optyLine; 
        Test.stopTest();
     }  
      
}