@isTest(SeeAllData=True)
                                
private class DDCOrgDnBAccountHierarchy_TEST {

    static testmethod void test0() {
        // the batch job query is contant and set in the batch job
       
       Test.startTest();
        List < Account > myList = [SELECT Id, Name, ParentId, 
                                                    DunsNumber, 
                                                    DandBCompany.GlobalUltimateDunsNumber, 
                                                    DandBCompany.DomesticUltimateDunsNumber, 
                                                    DandBCompany.ParentOrHqDunsNumber 
                                               FROM Account 
                                              WHERE DunsNumber != null 
                                                AND DandBCompany.GlobalUltimateDunsNumber != null
                                                AND DandBCompany.GlobalUltimateDunsNumber != '000000000'
                                                AND DandBCompany.ParentOrHqDunsNumber != null 
                                               AND DandBCompany.ParentOrHqDunsNumber != '000000000'   LIMIT 10];
 
        Database.BatchableContext bc = null;    
        DDCOrgDnBAccountHierarchy myClass = new DDCOrgDnBAccountHierarchy();
        myClass.execute(bc, (List <sObject>) myList);
        myClass.finish(bc);       
 
       //DDCOrgDnbAccountHierarchy c = new DDCOrgDnbAccountHierarchy();
       //c.query += ' LIMIT 10';

       //Database.executeBatch(c);
   
       Test.stopTest();
                              
    }
    

}