/**
* @author       Peeyush Awadhiya
* @date         07/14/2014
* @description  This class serves as extension to the IOIPrePopulatePage
* 
*/



public with sharing class IOIPrepopulateExtension {
    public IOI__c ioiRecord {get;set;}
    public String retURL {get;set;}
    public String baseURL {get;set;}
    public id opportunityId {get;set;}
    public Id projectId {get;set;}
    public Id accountId {get;set;}
    /**
    * @author       Peeyush Awadhiya
    * @date         07/014/2014
    * @description  This method is constructor for the extension which pre-populates the following fields:
    *               Next Approver: This field is populated from the owner
    *               Visibility: Private
    *               Period: Current period
    * @param        ApexPages.StandardController controller : This is an instance of standard controller for IOI record that is being inserted.
    * @return       void 
    */

    public IOIPrepopulateExtension(ApexPages.StandardController controller) {
        
        retURL = ApexPages.CurrentPage().getParameters().get('retURL');
        //opportunityId = controller.getRecord().Id;
    }
    
    /**
    * @author       Peeyush Awadhiya
    * @date         07/014/2014
    * @description  This method pre-populates the following fields:
    *               Next Approver: This field is populated from the owner
    *               Visibility: Private
    *               Period: Current period
    * @param        String ioiRecordid : This is id of IOI record that is being inserted.
    * @return       void 
    */
    public PageReference returnEditPage(){
        Period__c period = new Period__c();
        
        User currentUser = [SELECT Id, IOI_Reviewer__c, IOI_Reviewer__r.firstName, IOI_Reviewer__r.LastName FROM USER WHERE ID =:UserInfo.getUserId() LIMIT 1];
            
        List<Period__c> periodList = [SELECT Id,Name, IsCurrentPeriod__c, Type__c FROM Period__c WHERE IsCurrentPeriod__c = true AND Type__c = 'Week' LIMIT 1];  
        String nextReviewer = currentUser.IOI_Reviewer__r.firstName+' '+currentUser.IOI_Reviewer__r.LastName;
        String nextReviewerId = currentUser.IOI_Reviewer__c;
        if(periodList!=null && !periodList.isEmpty())
        {
            period = periodList[0];
            
        }
        String objectPrefix = IOI__c.SObjectType.getDescribe().getKeyPrefix();
        PageReference returnPage = new PageReference('/' + objectPrefix + '/e');
        
        returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().Visibility_Level__c,'Public');
        returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().Period__c,period.Name);
        returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().Next_Reviewer__c,nextReviewer);
        returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().Next_Reviewer__c+'_lkid',nextReviewerId);
        returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().Period__c+'_lkid',period.Id);
        
        // If opportunityId is not null then populate the OpportunityId and recordType
        /*
        if(opportunityId!=null && opportunityId!=''){
            returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().Opportunity__c+'_lkid',opportunityId);
            List<Opportunity> opList = [SELECT Id,Name, End_User__c, End_User__r.Name,CAM_Project__c, CAM_Project__r.Name  FROM Opportunity WHERE Id =:opportunityId LIMIT 1];
            
            if(opList!=null && !opList.isEmpty()){
                returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().Opportunity__c,opList[0].Name);
                returnPage.getParameters().put('RecordType',Schema.Sobjecttype.IOI__c.getRecordTypeInfosByName().get('Opportunity IOI').getRecordTypeId());
            }   
            returnPage.getParameters().put('retURL', opportunityId);
        }
        */
        if(retURL!=null && retURL!='' && retURL!=String.valueOf('/'+objectPrefix+'/o') && !retURL.contains(String.valueOf(objectPrefix+'?fcf=')) && !retURL.contains(objectPrefix)){
            returnPage.getParameters().put('retURL', retURL);
            // CGP 30-Jan-2015 removed following line
            //Id parentId = (Id)retURL.substring(1,retURL.length());
            // CGP 30-Jan-2015 inserted following line
            Id parentId = (Id)retURL.substring(1,16);            
            
            
            
            if(parentId.getSObjectType() == Account.getSObjectType()) {
                returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().Account__c+'_lkid',parentId);
                returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().Account__c,[SELECT Id,Name FROM Account WHERE Id =:parentId LIMIT 1].Name);    
            }
            else if(parentId.getSObjectType() == Opportunity.getSObjectType()) {
                
                returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().Opportunity__c+'_lkid',parentId);
                List<Opportunity> opList = [SELECT Id,Name, End_User__c, End_User__r.Name,CAM_Project__c, CAM_Project__r.Name  FROM Opportunity WHERE Id =:parentId LIMIT 1];
                
                if(opList!=null && !opList.isEmpty()){
                    returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().Opportunity__c,opList[0].Name);
                    //returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().Account__c+'_lkid',opList[0].End_User__c);
                    //returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().Account__c,opList[0].End_User__r.Name);
                    
                    //returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().Project__c+'_lkid',opList[0].CAM_Project__c);
                    //returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().Project__c,opList[0].CAM_Project__r.Name);    
                    returnPage.getParameters().put('RecordType',Schema.Sobjecttype.IOI__c.getRecordTypeInfosByName().get('Opportunity IOI').getRecordTypeId());
                }    
            }
            else if(parentId.getSObjectType() == CAM_Project__c.getSObjectType()) {
                returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().Project__c+'_lkid',parentId);
                List<CAM_Project__c> projectList = [SELECT Id,Name,CAM_Owner_End_User__c,CAM_Owner_End_User__r.Name FROM CAM_Project__c WHERE Id =:parentId LIMIT 1];
                if(projectList!=null && !projectList.isEmpty()){
                    returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().Project__c,projectList[0].Name);    
                    returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().Account__c+'_lkid',projectList[0].CAM_Owner_End_User__c);
                    returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().Account__c,projectList[0].CAM_Owner_End_User__r.Name);
                    
                }   
            }
            else if(parentId.getSObjectType() == Contact.getSObjectType()) {
                returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().CAM_Contact__c+'_lkid', parentId);
                List<Contact> contactList = [SELECT Id, Name, AccountId, Account.Name FROM Contact WHERE Id=:parentId LIMIT 1];
                
                if(contactList!=null && !contactList.isEmpty()){
                    returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().CAM_Contact__c,contactList[0].Name);    
                    returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().Account__c+'_lkid',contactList[0].AccountId);
                    returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().Account__c,contactList[0].Account.Name);    
                }    
            }
        }
        
        returnPage.getParameters().put('nooverride','1');
        
        
        return returnPage ;
        
    } 

}