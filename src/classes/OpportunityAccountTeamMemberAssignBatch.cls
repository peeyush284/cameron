/**
* @author       Peeyush Awadhiya
* @date         10/08/2014
* @description  This batch class does the following:
*               1. Queries for all opportunities and related account team members.
*               2. Updates the division specific account team owner to Account_Team_Owner__c field on opportunity. 
*/

global class OpportunityAccountTeamMemberAssignBatch implements Database.batchable<sObject>{
    string query;
    
    global OpportunityAccountTeamMemberAssignBatch(String queryStr){
        this.query = queryStr;
    }  
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        Set<Id> accountIdSet = new Set<Id>();
        List<Opportunity> opportunityList = new List<Opportunity>();
        Opportunity oppty;
        for(sObject obj: scope){
            oppty = (Opportunity)obj;
            accountIdSet.add(oppty.AccountId);
            opportunityList.add(oppty);    
        }
        
        // Now query all account team members related to opportunity and assign them to map.
        Map<Id, Map<String,AccountTeamMember>>accountMemberMap = new Map<Id, Map<String,AccountTeamMember>>();
        Map <String,AccountTeamMember> teamMemberTempMap;
        
        for(AccountTeamMember teamMember:[SELECT Id, AccountId, UserId, TeamMemberRole FROM AccountTeamMember WHERE AccountId IN:accountIdSet]){
            if(accountMemberMap.containsKey(teamMember.AccountId)){
                accountMemberMap.get(teamMember.AccountId).put(teamMember.TeamMemberRole, teamMember);
            }
            else{
                teamMemberTempMap = new Map<String,AccountTeamMember>();
                teamMemberTempMap.put(teamMember.TeamMemberRole, teamMember);
                accountMemberMap.put(teamMember.AccountId, teamMemberTempMap);   
            }          
        } 
        
        // Once all data structures have been populated, then again iterate through all opportunities and assign the account team member.
        
        for(Opportunity opptyRec:opportunityList){
            if(accountMemberMap.containsKey(opptyRec.AccountId) && accountMemberMap.get(opptyRec.AccountId).containsKey(opptyRec.Primary_Account_Manager_calc__c)){
                opptyRec.Account_Team_Owner__c = accountMemberMap.get(opptyRec.AccountId).get(opptyRec.Primary_Account_Manager_calc__c).UserId;
                opptyRec.ownerId = accountMemberMap.get(opptyRec.AccountId).get(opptyRec.Primary_Account_Manager_calc__c).UserId;
            }
        } // End of for loop
        
        if(opportunityList!= null && !opportunityList.isEmpty()){
            Database.SaveResult [] saveResults = Database.update(opportunityList, false);
            for(Database.SaveResult result:saveResults){
                if(!result.isSuccess()){
                    for(Database.Error err : result.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Opportunity fields that affected this error: ' + err.getFields());
                    }
                }
            } // End of for loop
            
        } // End of if
    }
    
    global void finish(Database.BatchableContext BC){
    
    }
}