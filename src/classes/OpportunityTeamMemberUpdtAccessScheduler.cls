/**
* @author       Peeyush Awadhiya
* @date         11/06/2014
* @description  This batch class does the following:
*               1. Queries for all sales team member with role 'Opportunity Creator'.
*               2. Update the sharing access to Edit. 
*/

global class OpportunityTeamMemberUpdtAccessScheduler implements Schedulable {
    
    global void execute(SchedulableContext sch) {
    
        String queryStr = 'SELECT Id, OpportunityId, UserId, TeamMemberRole FROM OpportunityTeamMember WHERE TeamMemberRole =\'' + System.Label.Sales_Team_Opportunity_Creator+'\'';
        
        OpportunityTeamMemberUpdateAccessBatch batchObj = new OpportunityTeamMemberUpdateAccessBatch(queryStr);
        ID batchProcessId = Database.executeBatch(batchObj, 100);  
        
        System.debug('Executing the batch with process Id:'+batchProcessId);
    }
}