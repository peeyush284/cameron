/**
* @author       Peeyush Awadhiya
* @date         09/25/2014
* @description  This batch class does the following:
*               1. Copies the AS Bucket Oppty Object Records into AS Forecast Oppty Object.
*               2. Upon completion, it invokes the batch which copies opportunity records into AS Forecast Oppty Object
*/

global class ASInsrtASFcstBcktOpptyToASFcstOpptyBatch implements Database.batchable<sObject>{
    string query;
    global ASInsrtASFcstBcktOpptyToASFcstOpptyBatch(String queryStr){
        this.query = queryStr;
    }  
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){       
        
        List<Analytic_Snapshot_Forecast_Oppty__c> asForecastOpportunityToCopyList = new List<Analytic_Snapshot_Forecast_Oppty__c>();
        for(Analytic_Snapshot_Bucket_Oppty__c bucketOppty:(List<Analytic_Snapshot_Bucket_Oppty__c>) scope){
            
            Analytic_Snapshot_Forecast_Oppty__c temp = new Analytic_Snapshot_Forecast_Oppty__c(
                Booking_Date__c = bucketOppty.Schedule_Date__c,
                Business_Unit__c = bucketOppty.Business_Unit__c,
                Division__c = bucketOppty.Division__c,
                Forecast_Category__c = bucketOppty.Forecast_Category__c,
                Opportunity_ID__c = bucketOppty.Opportunity_ID__c,
                Opportunity_Name__c = bucketOppty.Opportunity_Name__c,
                Opportunity_Owner__c = bucketOppty.Opportunity_Owner__c,
                Stage__c = bucketOppty.Stage__c,
                Opportunity_Type__c = bucketOppty.Opportunity_Type__c
            );

            if(bucketOppty.Forecast__c =='Y'){
                temp.Amount__c = 0;
                temp.Forecast_Amount__c = bucketOppty.Schedule_Amount__c;
                temp.Closed_Amount__c = 0;
            }
            else{
                temp.Amount__c = bucketOppty.Schedule_Amount__c;
                temp.Forecast_Amount__c = 0;
                temp.Closed_Amount__c = bucketOppty.Schedule_Amount__c;
            }
            asForecastOpportunityToCopyList.add(temp);
        }
        
        // Now delete records from Analytics Snapshot
        if(asForecastOpportunityToCopyList!=null && !asForecastOpportunityToCopyList.isEmpty()){
            Database.SaveResult [] results = Database.insert(asForecastOpportunityToCopyList, false);
            
            // Iterate through each returned result
            for (Database.SaveResult sr : results) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Analytic Snapshot: Forecast Oppty fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }  
    }    
    global void finish(Database.BatchableContext BC){
        // Upon completion, invoke the batch which copies the opportunity into AS Forecast Oppty Object.
        String querySelectStr = 'SELECT Id, Name, CloseDate, StageName, Amount, RecordType.Name, CAM_Forecast_Amount__c, CAM_Business_Unit__c, CAM_Division__c, ForecastCategoryName, Owner_Name_calc__c';
        String queryWhereStr = 'WHERE CloseDate = THIS_YEAR AND Amount > 0 AND ';
        String querySecondWhereStr = '(StageName = \'Closed Won\' OR StageName = \'Universe\' OR StageName = \'Above the Funnel\' OR StageName = \'In the Funnel\' OR StageName = \'Best Few\')';
        
        String queryStr = querySelectStr+ ' FROM Opportunity '+queryWhereStr+querySecondWhereStr;
        
        ASInsrtStandardOpptyIntoASFcstOpptyBatch batchObj = new ASInsrtStandardOpptyIntoASFcstOpptyBatch(queryStr);
        ID batchProcessId = Database.executeBatch(batchObj, 100);  
        
        System.debug('Executing the ASInsrtASFcstBcktOpptyToASFcstOpptyBatch with process Id:'+batchProcessId);

    }
}