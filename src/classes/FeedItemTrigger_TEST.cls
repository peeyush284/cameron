/**
* @author       Peeyush Awadhiya
* @date         07/09/2014
* @description  This class have test methods that test the following:
*               1. If the FeedItem is deleted and there is a corresponding Chatter Post custom object record,
*               then it deletes the record as well.
* 
*/


@isTest

public with sharing class FeedItemTrigger_TEST {
    
    /**
    * @author       Peeyush Awadhiya
    * @date         07/09/2014
    * @description  This method performs the delete on FeedItem to test the trigger
    * @param        void
    * @return       void 
    */
    
    static testMethod void testFeedItemTrigger(){
        // Prepare the test data
        Account acc;
        CAM_Project__c project;
        Opportunity oppty;
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('Bucket', 'RFQ', project);
        oppty.end_user__c = acc.id;
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Surface';
        insert oppty;
        
        IOI__c ioiRecord = TestUtilities.generateIOI('Test IOI', acc.Id, oppty.Id, project.Id, 'Submitted');
        Period__c period =  new Period__c(Name = 'Test', StartDate__c = Date.TODAY(), EndDate__c = Date.TODAY());
        insert period;
        ioiRecord.Period__c = period.Id;
        insert ioiRecord;
        
        List<FeedItem> feedList = new List<FeedItem>();
        
        FeedItem feed1 = TestUtilities.generateFeedItem('This is feed 1',acc.Id);
        feedList.add(feed1);
        
        FeedItem feed2 = TestUtilities.generateFeedItem('This is feed 2',oppty.Id);
        feedList.add(feed2);
        
        FeedItem feed3 = TestUtilities.generateFeedItem('This is feed 3',project.Id);
        feedList.add(feed3);
        
        insert feedList;
        
        // Create IOI Chatter Post 
        
        List<IOI_Chatter_Feed__c> chatterFeedList = new List<IOI_Chatter_Feed__c>();
        
        chatterFeedList.add(TestUtilities.generateChatterPostRecord('Account', feed1.Id));
        chatterFeedList.add(TestUtilities.generateChatterPostRecord('Opportunity', feed2.Id));
        chatterFeedList.add(TestUtilities.generateChatterPostRecord('Project', feed3.Id));
        
        insert chatterFeedList;
        
        // Now delete feed1
        Test.startTest();
            delete feed1;
        Test.stopTest();
        
        System.assertEquals([SELECT Id FROM IOI_Chatter_Feed__c].size(),2);
        
    }
}