@isTest
private class AccountHelper_TEST {

    // Test Inserting null Accounts
    static TestMethod void Test0_HandleAfterInsertWithNull()
    {        
        List<Account> newRecords = new List<Account>();
        
        AccountHelper.HandleAfterInsert(newRecords);
        
        //system.AssertEqual(1,1);
    }  
    
    // Test Inserting Real Accounts
    static TestMethod void Test1_HandleAfterInsertWithAccts()
    {
        List<Account> newAccounts = new List<Account>();
        Account myNew = new Account(name = 'Test Account ', billingCountry = 'United States');
        
        newAccounts.add(myNew);

        insert newAccounts;
        
        AccountHelper.HandleAfterInsert(newAccounts);
        
        //system.AssertEqual(1,1);
    }  
    
    // Test updateing null Accounts
    static TestMethod void Test0_HandleAfterUpdateWithNullAccounts()
    {        
        List<Account> newRecords = new List<Account>();
        Map<Id, Account> oldRecords = new Map<Id, Account>();
        
        AccountHelper.HandleAfterUpdate(newRecords, oldRecords);
        
        //system.AssertEqual(1,1);
    }  
    
        // Test updateing null Accounts
    static TestMethod void Test0_HandleAfterUpdateWithAccounts()
    {        
        List<Account> newParents = new List<Account>();
        List<Account> newChildren = new List<Account>();
        Map<Id, Account> oldRecords = new Map<Id, Account>();
        
        Profile luProfile = [Select id from Profile where name = 'CAM Sales - All' Limit 1];
        UserRole luUserRole = [Select id from UserRole where name = 'Drilling' Limit 1];
        
        /*
        User newUser = new User(LastName='TesterName',
                                Username='abcTest@test.com',
                                isActive=True,
                                ProfileId=luProfile.id,
                                Role__c='Drilling|Drilling|Acct Mgr|Primary',
                                UserRoleId=luUserRole.id,
                                Email='Test@test.com', 
                                Alias='tester', 
                                TimeZoneSidKey='America/Chicago', 
                                LocaleSidKey='en_US', 
                                EmailEncodingKey='ISO-8859-1', 
                                LanguageLocaleKey='en_US');
        insert newUser;
        */
        
        User newUser = [Select id from User where isActive = True and Role__c='Drilling|Drilling|Acct Mgr|Primary' limit 1];
                                
        District__c newDistrict = new District__c(AssignmentAttribute__c='Venezuela, Bolivarian Republic of|',
                                            CountryCode__c='VE',
                                            DistrictAssignment__c='xyz',
                                            User__c=newUser.id);    
        insert newDistrict;                            
                               
        
        for (integer idx = 0; idx < 10; idx++)
        {
            Account pacct = new Account(COE_Approval__c = TRUE, name = 'Parent Account idx = ' + idx, billingCountry = 'Venezuela, Bolivarian Republic of', Force_Account_Team_Assignments__c = True, AssignableRoles__c = 'Drilling|Drilling|Acct Mgr|Primary');
            newParents.add(pacct);
            Account cacct = new Account(COE_Approval__c = TRUE, name = 'Child Account idx = ' + idx, billingCountry = 'United States', Force_Account_Team_Assignments__c = True, AssignableRoles__c = 'Drilling|Drilling|Acct Mgr|Primary');
            newChildren.add(cacct);
        }
        Account dacct = new Account(name = 'Child Account idx = 100', billingCountry = 'United States', Force_Account_Team_Assignments__c = True);
        newChildren.add(dacct);
        
        insert newParents;
        insert newChildren;
        
        oldRecords.putall(newChildren);
                
        for (integer idx = 0; idx < 10; idx++)
        {
            newChildren[idx].billingCountry = 'Venezuela, Bolivarian Republic of';
            if (idx == 3) {
                newChildren[idx].parentId = newParents[idx+1].id;
            }
            else
            {
                newChildren[idx].parentId = newParents[idx].id;
            }
        }
        
        update newChildren;
        
        
        
        //AccountHelper.HandleAfterUpdate(newChildren, oldRecords);
        
        //system.AssertEqual(1,1);
    } 
    
    static TestMethod void Test0_HandleAfterUpdateWithDirectAccTs()
    {        
        List<Account> newParents = new List<Account>();
        List<Account> newChildren = new List<Account>();
        Map<Id, Account> oldRecords = new Map<Id, Account>();
        
        Profile luProfile = [Select id from Profile where name = 'CAM Sales - All' Limit 1];
        UserRole luUserRole = [Select id from UserRole where name = 'Drilling' Limit 1];
        
        /*
        User newUser = new User(LastName='TesterName',
                                Username='abcTest@test.com',
                                isActive=True,
                                ProfileId=luProfile.id,
                                Role__c='Drilling|Drilling|Acct Mgr|Primary',
                                UserRoleId=luUserRole.id,
                                Email='Test@test.com', 
                                Alias='tester', 
                                TimeZoneSidKey='America/Chicago', 
                                LocaleSidKey='en_US', 
                                EmailEncodingKey='ISO-8859-1', 
                                LanguageLocaleKey='en_US');
        insert newUser;
        */
        
        User newUser = [Select id from User where isActive = True and Role__c='Drilling|Drilling|Acct Mgr|Primary' limit 1];
                                
        District__c newDistrict = new District__c(AssignmentAttribute__c='Venezuela, Bolivarian Republic of|',
                                            CountryCode__c='VE',
                                            Direct_Assignment_Criteria__c = 'ASSIGNME',
                                            DistrictAssignment__c='xyz',
                                            User__c=newUser.id);    
        insert newDistrict;                            
                               
        
        for (integer idx = 0; idx < 10; idx++)
        {
            Account pacct = new Account(COE_Approval__c = TRUE, Direct_Assignment_Criteria__c  = 'ASSIGNME', name = 'Parent Account idx = ' + idx, billingCountry = 'Venezuela, Bolivarian Republic of', Force_Account_Team_Assignments__c = True, AssignableRoles__c = 'Drilling|Drilling|Acct Mgr|Primary');
            newParents.add(pacct);
            Account cacct = new Account(COE_Approval__c = TRUE, Direct_Assignment_Criteria__c  = 'ASSIGNME', name = 'Child Account idx = ' + idx, billingCountry = 'United States', Force_Account_Team_Assignments__c = True, AssignableRoles__c = 'Drilling|Drilling|Acct Mgr|Primary');
            newChildren.add(cacct);
        }
        Account dacct = new Account(name = 'Child Account idx = 100', billingCountry = 'United States', Force_Account_Team_Assignments__c = True);
        newChildren.add(dacct);
        
        insert newParents;
        insert newChildren;
        
        oldRecords.putall(newChildren);
                
        for (integer idx = 0; idx < 10; idx++)
        {
            newChildren[idx].billingCountry = 'Venezuela, Bolivarian Republic of';
            if (idx == 3) {
                newChildren[idx].parentId = newParents[idx+1].id;
            }
            else
            {
                newChildren[idx].parentId = newParents[idx].id;
            }
        }
        
        update newChildren;
        
        
        
        //AccountHelper.HandleAfterUpdate(newChildren, oldRecords);
        
        //system.AssertEqual(1,1);
    }     
    
    static TestMethod void Test0_HandleAfterUpdateWithAccountsChanged()
    {        
        List<Account> newParents = new List<Account>();
        List<Account> newChildren = new List<Account>();
        Map<Id, Account> oldRecords = new Map<Id, Account>();
        
        for (integer idx = 0; idx < 10; idx++)
        {
            Account pacct = new Account(COE_Approval__c = TRUE, name = 'Parent Account idx = ' + idx, billingCountry = 'United States');
            newParents.add(pacct);
            Account cacct = new Account(COE_Approval__c = TRUE, name = 'Child Account idx = ' + idx, billingCountry = 'United States', parentid = pacct.id);
            newChildren.add(cacct);
        }
        insert newParents;
        insert newChildren;
        for (integer idx = 0; idx < 10; idx++)
        {
            if (idx == 0 || idx == 5 || idx == 9) {
                newChildren[idx].parentid = null ;
            }
            else if (idx == 1) {
                newChildren[idx].parentid = newParents[9].id ;
            }
        }
        oldRecords.putall(newChildren);
        
        AccountHelper.HandleAfterUpdate(newChildren, oldRecords);
        
        //system.AssertEqual(1,1);
    }  
    
    // Test deleting Real Accounts
    static TestMethod void Test1_HandleAfterDeleteWithAccts()
    {
        List<Account> newAccounts = new List<Account>();
        integer idx = 0;
        
        Account pacct = new Account(name = 'Parent Account idx = ' + idx, billingCountry = 'United States');
        newAccounts.add(pacct);
        Account cacct = new Account(name = 'Child Account idx = ' + idx, dunsnumber = '045449624', billingCountry = 'United States', parentid = pacct.id);
        newAccounts.add(cacct);
        
        insert newAccounts;
        
        delete newAccounts;
        
        AccountHelper.HandleAfterDelete(newAccounts);
        
        //system.AssertEqual(1,1);
    }  
    

}