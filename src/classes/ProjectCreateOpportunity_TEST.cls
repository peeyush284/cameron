@isTest (SeeAllData=true)
public class ProjectCreateOpportunity_TEST {
    
    static testMethod void testProjectOneTeamMember() {
                     
        String atmRole = 'Drilling|Drilling|Acct Mgr|Primary';        
        String projRecordType = 'External';
        String projStatus = 'Permanent';                      
        Account a = TestUtilities.generateAccount();

        //Find User with Create Opportunty flag = True
        User u = [SELECT UserName, Email, role__c FROM User WHERE isActive = true AND role__c = :atmRole AND Create_Opportunity__c = true LIMIT 1];  
        atmrole = u.role__c;
        AccountTeamMember atm = new AccountTeamMember();
        atm.AccountID = a.Id;
        atm.TeamMemberRole = atmRole;       
        atm.UserId = u.Id;
        insert atm;         
            
        CAM_Project__c p = TestUtilities.generateProject(projRecordType, projStatus, a);        
        p.CAM_Startup_Date__c = date.today();
        p.CAM_Final_Destination__c = 'Algeria';
            update p;
        
        system.debug('p.Id '+ p.Id);
        system.debug('a.Id '+ a.Id);     
        system.debug('p.CAM_Owner_End_User__c '+ String.valueOf(p.CAM_Owner_End_User__c));
        system.debug('p.Name '+ p.Name);
        system.debug('p.CAM_Startup_Date__c.format '+ p.CAM_Startup_Date__c.format());
        system.debug('p.CAM_Final_Destination__c '+ p.CAM_Final_Destination__c );
        
        string result = ProjectCreateOpportunity.createOpportunities(p.Id, 
                                                            a.Id,
                                                            String.valueOf(p.CAM_Owner_End_User__c),
                                                            p.Name,
                                                            p.CAM_Startup_Date__c.format(),
                                                            p.CAM_Final_Destination__c );
        system.debug('result '+ result );                                                    
        system.AssertEquals('1',result);
    }

    static testMethod void testCreateOpptyFlagFalse() {
                     
        String atmRole = 'Drilling|Drilling|Acct Mgr|Primary';        
        String projRecordType = 'External';
        String projStatus = 'In Progress';                      
        Account a = TestUtilities.generateAccount();

        //Find User with Create Opportunty flag = false
        User u = [SELECT UserName, role__c, Email FROM User WHERE isActive = true AND role__c = :atmRole and Create_Opportunity__c = false LIMIT 1]; 
        atmrole = u.role__c;
        AccountTeamMember atm = new AccountTeamMember();
        atm.AccountID = a.Id;
        atm.TeamMemberRole = atmRole;       
        atm.UserId = u.Id;
        insert atm;         
            
        CAM_Project__c p = TestUtilities.generateProject(projRecordType, projStatus, a);        
        p.CAM_Startup_Date__c = date.today();
        p.CAM_Final_Destination__c = 'Algeria';
            update p;
        
        system.debug('p.Id '+ p.Id);
        system.debug('a.Id '+ a.Id);     
        system.debug('p.CAM_Owner_End_User__c '+ String.valueOf(p.CAM_Owner_End_User__c));
        system.debug('p.Name '+ p.Name);
        system.debug('p.CAM_Startup_Date__c.format '+ p.CAM_Startup_Date__c.format());
        system.debug('p.CAM_Final_Destination__c '+ p.CAM_Final_Destination__c );
        
        string result = ProjectCreateOpportunity.createOpportunities(p.Id, 
                                                            a.Id,
                                                            String.valueOf(p.CAM_Owner_End_User__c),
                                                            p.Name,
                                                            p.CAM_Startup_Date__c.format(),
                                                            p.CAM_Final_Destination__c );
        system.debug('result '+ result );                                                    
        system.AssertEquals('0',result);
    }
}