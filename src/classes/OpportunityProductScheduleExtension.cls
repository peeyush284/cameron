/**
* @author       Peeyush Awadhiya
* @date         08/06/2014
* @description  This class is extension for the OpportunityProductSchedulePage. It does the following:
*               1. Enables end user to establish schedules for opportunity line items.
*/


public with sharing class OpportunityProductScheduleExtension {
    public String userLocale {get;set;}
    public Id opportunityId {get;set;}
    public Id opportunityRecordTypeId {get{
        if(opportunityRecordTypeId==null){
            opportunityRecordTypeId = Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get(Label.Project_Create_Opportunity_Button_RecordType).getRecordTypeId();
        }
        return opportunityRecordTypeId;
    } set;}
    public Integer yearInt {get{    
                                    if(opportunityId!=null){
                                        yearInt = [SELECT Id, CloseDate FROM Opportunity WHERE Id=:opportunityId LIMIT 1].CloseDate.Year();
                                        return yearInt;
                                    }
                                    else return null;
                                }set;}
    
    public List<OpportunityLineItemScheduleWrapper> scheduleWrapperList {
        get{
            
            return scheduleWrapperList;
        }
        set;
    }
    public OpportunityProductScheduleExtension(ApexPages.StandardController controller) {
    	userLocale = UserInfo.getLocale();
        String retURL = ApexPages.currentPage().getParameters().get('retURL');
        if(retURL!=null && retURL!=''){
            opportunityId = retURL.substring(1,16); 
        }
        else {
            opportunityId = ApexPages.currentPage().getParameters().get('id');
        }
        loadWrapperListData();
    }
    
    public void loadWrapperListData(){
        scheduleWrapperList = new List<OpportunityLineItemScheduleWrapper>();
        for(OpportunityLineItem optyLine:[
                                            SELECT Id,UnitPrice, PricebookEntry.Product2.Name, CurrencyISOCode, Opportunity.CloseDate,
                                            (SELECT Id,OpportunityLineItemId, Type, Quantity, Revenue, ScheduleDate,CurrencyISOCode FROM OpportunityLineItemSchedules ORDER BY ScheduleDate ASC LIMIT 12)
                                            FROM OpportunityLineItem
                                            WHERE OpportunityId =:opportunityId 
                                            LIMIT 80
                                         ])
        {    
    
            if(optyLine.OpportunityLineItemSchedules!=null && !optyLine.OpportunityLineItemSchedules.isEmpty()){
                scheduleWrapperList.add(new OpportunityLineItemScheduleWrapper(optyLine.Id, optyLine.PricebookEntry.Product2.Name, optyLine.OpportunityLineItemSchedules, optyLine.CurrencyISOCode, optyLine.Opportunity.CloseDate.Year()));
            }
            else {
                scheduleWrapperList.add(new OpportunityLineItemScheduleWrapper(optyLine.Id, optyLine.PricebookEntry.Product2.Name, optyLine.CurrencyISOCode, optyLine.Opportunity.CloseDate.Year()));    
            }                
        }
    
    }
    public class OpportunityLineItemScheduleWrapper{
    
        public Id opportunityProductId{get;set;}
        public String productName {get;set;}
        public List<OpportunityLineItemSchedule> lineItemScheduleList {get;set;}
        public Integer rowTotal {get;set;}
        public String currencyISOCode {get;set;}
        public Integer year{get;set;}
        
        public OpportunityLineItemScheduleWrapper(Id opportunityProductId, String productName, String currencyISOCode, Integer year){
            this.productName = productName;
            this.currencyISOCode = currencyISOCode;
            this.opportunityProductId = opportunityProductId;
            this.lineItemScheduleList = new List<OpportunityLineItemSchedule>();
            this.year = year;
            rowTotal = 0;
            for(integer i=0;i<12;i++){
                this.lineItemScheduleList.add(new OpportunityLineItemSchedule(Quantity = null, OpportunityLineItemId = opportunityProductId, Type ='Revenue', Revenue = 0, ScheduleDate = Date.newInstance(year,i+1,Date.daysInMonth(year,i+1))));  
            }
        }
        
        public OpportunityLineItemScheduleWrapper(Id opportunityProductId, String productName, List<OpportunityLineItemSchedule> scheduleList, String currencyISOCode, Integer year){
            this.opportunityProductId = opportunityProductId;
            this.productName = productName;
            this.year = year;
            rowTotal = 0;
            this.currencyISOCode = currencyISOCode;
            if(scheduleList!=null){
                lineItemScheduleList = new List<OpportunityLineItemSchedule>();
                lineItemScheduleList.addAll(scheduleList);
                for(OpportunityLineItemSchedule lineItemSchedule:scheduleList){
                    rowTotal += (Integer)lineItemSchedule.Revenue;  
                    if(lineItemSchedule.ScheduleDate!=null){
                        lineItemSchedule.ScheduleDate = Date.newInstance(year,lineItemSchedule.ScheduleDate.Month(),lineItemSchedule.ScheduleDate.Day());
                    }
                }
            }
        }
    }
    
    public PageReference saveLineItems(){
        List<OpportunityLineItemSchedule> schedulesToUpsert = new List<OpportunityLineItemSchedule>();
        
        for(OpportunityLineItemScheduleWrapper wrap:scheduleWrapperList){
            for(OpportunityLineItemSchedule schedule:wrap.lineItemScheduleList){
                if( schedule!=null){
                    schedulesToUpsert.add(schedule);   
                }
            }
        }
        Set<Id> scheduleIdSet = new Set<Id>();
        if(schedulesToUpsert!=null && !schedulesToUpsert.isEmpty()){

            List<Database.UpsertResult> upresult = Database.upsert(schedulesToUpsert,false);
            
            for(Database.UpsertResult result:upresult){
                if(!result.isSuccess()){
                    // Operation failed, so get all errors                
                    for(Database.Error err : result.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Opportunity Line Item Schedule fields that affected this error: ' + err.getFields());
                    }
                }
               
            }
            for(OpportunityLineItemSchedule scheduleRec:schedulesToUpsert){
                scheduleIdSet.add(scheduleRec.Id);
            }
            Map<Id,OpportunityLineItemSchedule> scheduleMap = new Map<Id,OpportunityLineItemSchedule>(
                [
                    SELECT Id,OpportunityLineItem.Opportunity.OwnerId, ScheduleDate,
                    OpportunityLineItem.Opportunity.ForecastCategoryName, Revenue, OpportunityLineItem.PricebookEntry.Product2.Name,
                    OpportunityLineItem.Opportunity.CloseDate,OpportunityLineItem.OpportunityId,
                    OpportunityLineItem.Name,OpportunityLineItem.Opportunity.RecordType.Name, CurrencyISOCode,
                    OpportunityLineItem.Order_Type__c, OpportunityLineItem.Product_Class__c,
                    OpportunityLineItem.QuantityCalc__c, OpportunityLineItem.Manufacturing_Plant__c, OpportunityLineItem.Market_Requested_Delivery_Weeks__c
                    FROM OpportunityLineItemSchedule 
                    WHERE Id IN:scheduleIdSet
                ]
            );
           
            List<Opportunity_Reporting__c> reportingList = new List<Opportunity_Reporting__c> ();
            
            // Now populate the schedule data into Opportunity Reporting custom object records
            Map<Id,Opportunity_Reporting__c> reportingMap = new Map<Id,Opportunity_Reporting__c>(
                
            );
            for (Opportunity_Reporting__c reporting:
                [
                    SELECT Forecast_Amount__c, Forecast_Category__c, Opportunity_Amount__c,   
                    Opportunity_Closed_Amount__c, Opportunity_Name__c, Opportunity_Booking_Date__c,  
                    Opportunity_Product_Name__c, Opportunity_Type__c, Source_Id__c, CurrencyISOCode,
                    Order_Type__c, Product_Class__c, Quantity__c, Manufacturing_Plant__c, Market_Requested_Delivery_Weeks__c
                    FROM Opportunity_Reporting__c
                    WHERE Source_Id__c IN :scheduleIdSet
                ]
            ){
                reportingMap.put(reporting.Source_Id__c, reporting);    
            }     
              
            if(reportingMap == null || reportingMap.isEmpty()){
                for (OpportunityLineItemSchedule schedule:scheduleMap.values()){
                    
                    Opportunity_Reporting__c reporting = new Opportunity_Reporting__c(
                        OwnerId = schedule.OpportunityLineItem.Opportunity.OwnerId,
                        Forecast_Category__c = schedule.OpportunityLineItem.Opportunity.ForecastCategoryName,
                        Opportunity_Booking_Date__c = schedule.ScheduleDate,
                        Opportunity_Name__c = schedule.OpportunityLineItem.OpportunityId,
                        Opportunity_Product_Name__c = schedule.OpportunityLineItem.PricebookEntry.Product2.Name,
                        Opportunity_Type__c = schedule.OpportunityLineItem.Opportunity.RecordType.Name,
                        Opportunity_Amount__c = schedule.Revenue,
                        Forecast_Amount__c = schedule.Revenue,
                        CurrencyISOCode = schedule.CurrencyISOCode,
                        Source_Id__c = schedule.Id,
                        Order_Type__c = schedule.OpportunityLineItem.Order_Type__c,
                        Product_Class__c = schedule.OpportunityLineItem.Product_Class__c,
                        Quantity__c = schedule.OpportunityLineItem.QuantityCalc__c,
                        Manufacturing_Plant__c = schedule.OpportunityLineItem.Manufacturing_Plant__c,
                        Market_Requested_Delivery_Weeks__c = schedule.OpportunityLineItem.Market_Requested_Delivery_Weeks__c    
                    );
                    
                    if(schedule.ScheduleDate>DATE.TODAY()||(schedule.ScheduleDate.Month() == Date.TODAY().Month() && schedule.ScheduleDate.Year() == Date.TODAY().Year())){
                        
                        /*
                        reporting.Opportunity_Amount__c = 0;
                        reporting.Forecast_Amount__c = schedule.Revenue;
                        reporting.Opportunity_Closed_Amount__c = 0;
                        
                        */
                        
                        reporting.Forecast_Category__c = 'Committed';
                    }
                    else{
                        /*
                        reporting.Opportunity_Amount__c = schedule.Revenue;
                        reporting.Forecast_Amount__c = 0;
                        reporting.Opportunity_Closed_Amount__c = schedule.Revenue;
                        */
                        reporting.Forecast_Category__c = 'Closed';
                    }
                    reportingList.add(reporting);
                }
            } // End of if (reportingMap == null || reportingMap.isEmpty())
            
            else{
                Opportunity_Reporting__c reporting;
                for (OpportunityLineItemSchedule schedule:scheduleMap.values()){
                
                    if(reportingMap.containsKey(schedule.Id)){
                        reporting = reportingMap.get(schedule.Id);  
                    }
                    else{
                        reporting = new Opportunity_Reporting__c();
                    }
                    
                    reporting.OwnerId = schedule.OpportunityLineItem.Opportunity.OwnerId;
                    reporting.Forecast_Category__c = schedule.OpportunityLineItem.Opportunity.ForecastCategoryName;
                    reporting.Opportunity_Booking_Date__c = schedule.ScheduleDate;
                    reporting.Opportunity_Name__c = schedule.OpportunityLineItem.OpportunityId;
                    reporting.Opportunity_Product_Name__c = schedule.OpportunityLineItem.PricebookEntry.Product2.Name;
                    reporting.Opportunity_Type__c = schedule.OpportunityLineItem.Opportunity.RecordType.Name;
                    reporting.Opportunity_Amount__c = schedule.Revenue;
                    reporting.Forecast_Amount__c = schedule.Revenue;
                    reporting.Source_Id__c = schedule.Id;
                    reporting.CurrencyISOCode = schedule.CurrencyISOCode;
                    reporting.Order_Type__c = schedule.OpportunityLineItem.Order_Type__c;
                    reporting.Product_Class__c = schedule.OpportunityLineItem.Product_Class__c;
                    reporting.Quantity__c = schedule.OpportunityLineItem.QuantityCalc__c;
                    reporting.Manufacturing_Plant__c = schedule.OpportunityLineItem.Manufacturing_Plant__c;
                    reporting.Market_Requested_Delivery_Weeks__c = schedule.OpportunityLineItem.Market_Requested_Delivery_Weeks__c;
                    
                    if(schedule.ScheduleDate>DATE.TODAY()||(schedule.ScheduleDate.Month() == Date.TODAY().Month() && schedule.ScheduleDate.Year() == Date.TODAY().Year())){
                        /*
                        reporting.Opportunity_Amount__c = 0;
                        reporting.Forecast_Amount__c = schedule.Revenue;
                        reporting.Opportunity_Closed_Amount__c = 0;
                        */
                        reporting.Forecast_Category__c = 'Committed';
                    }
                    else{
                        /*
                        reporting.Opportunity_Amount__c = schedule.Revenue;
                        reporting.Forecast_Amount__c = 0;
                        reporting.Opportunity_Closed_Amount__c = schedule.Revenue;
                        */
                        reporting.Forecast_Category__c = 'Closed';
                    }
                    
                    reportingList.add(reporting);   
                   
                } // End of for loop
            
            } // End of else
            
            if(reportingList!=null && !reportingList.isEmpty()){
                
                List<Database.UpsertResult> reportingUpsertResult = Database.upsert(reportingList,false);
                
                for(Database.UpsertResult result:reportingUpsertResult){
                    if(!result.isSuccess()){
                        // Operation failed, so get all errors                
                        for(Database.Error err : result.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Opportunity Reporting fields that affected this error: ' + err.getFields());
                        }
                    }
                    
                } // End of for loop
            
            } // End of if(reportingList!=null && !reportingList.isEmpty())
        }
        
        return new PageReference('/'+opportunityId);
    }
    
    public PageReference cancelSchedule(){
        return new PageReference('/'+opportunityId);
    }
}