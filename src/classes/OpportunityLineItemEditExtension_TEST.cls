/**
 * @author      Peeyush Awadhiya
 * @date        09/10/2014
 * @description Test methods for OpportunityLineItemEditExtension Visualforce extension. Enables editing opportunity line item characteristics.
 *
 */

@isTest

public class OpportunityLineItemEditExtension_TEST{
    
    public static testMethod void testEditCharacteristics(){
        
        // Prepare the data stub
        Account acc;
        CAM_Project__c project = new CAM_Project__c(); 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc);              
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunity
        oppty = TestUtilities.generateOpportunityWithoutInsert('In the Funnel/Best Few', 'Best Few', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Surface';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = project.Id;
        
        oppty.AccountId = acc.Id;
        insert oppty;
        
        // Product
        Product2 prod = new Product2(Name = 'Surface Stack', Division__c = 'Surface', Group__c = 'ARTIFICIAL LIFT', isActive = true);
        insert prod;
        
        Pricebook2 priceBook= new Pricebook2(Name='Test Pricebook');
        insert priceBook;
        
        PricebookEntry pbEntry = new PricebookEntry(isActive = true,pricebook2Id = Test.getStandardPricebookId(), product2Id = prod.Id, UnitPrice=100, UseStandardPrice = false);
        insert pbEntry;
        
        // Picklist Value records
        
        PicklistValues__c divisionPicklist = new PicklistValues__c(Name = 'Surface', Type__c = 'DIVISION', External_Id__c = 'Surface');
        insert divisionPicklist;
        
        PicklistValues__c level1record = new PicklistValues__c(Parent_Pick_List_Value__c = divisionPicklist.Id, Name = 'ARTIFICIAL LIFT', Type__c = 'LEVEL1', External_Id__c = 'Surface|ARTIFICIAL LIFT');
        insert level1record;
        
        PicklistValues__c level1picklist = new PicklistValues__c(Parent_Pick_List_Value__c = level1record.Id,Name = 'Product', Type__c ='CHARTYPE', Field_API__c = 'Product__c', Field_Type__c = 'Picklist', External_Id__c = 'Surface|ARTIFICIAL LIFT|Product');
        insert level1picklist;
        
        PicklistValues__c level1picklistVal = new PicklistValues__c(Parent_Pick_List_Value__c = level1picklist.Id, Name ='PCP Pumps', Type__c = 'CHARVAL', External_Id__c = 'Surface|ARTIFICIAL LIFT|Product|PCP Pumps');
        insert level1picklistVal;
        
        PicklistValues__c level1picklistChild = new PicklistValues__c(Parent_Pick_List_Value__c = level1picklistVal.Id, Name ='Type', Type__c = 'CHARTYPE',Field_API__c = 'Type__c', Field_Type__c = 'Picklist', External_Id__c = 'Surface|ARTIFICIAL LIFT|Product|PCP Pumps|Type');
        insert level1picklistChild;
        
        PicklistValues__c level1picklistChildVal = new PicklistValues__c(Parent_Pick_List_Value__c = level1picklistChild.Id, Name ='CAM-CT', Type__c = 'CHARVAL', External_Id__c = 'Surface|ARTIFICIAL LIFT|Product|PCP Pumps|Type|CAM-CT');
        insert level1picklistChildVal;
        
        PicklistValues__c level1picklistChildVal1 = new PicklistValues__c(Parent_Pick_List_Value__c = level1picklistChild.Id, Name ='Conventional', Type__c = 'CHARVAL', External_Id__c = 'Surface|ARTIFICIAL LIFT|Product|PCP Pumps|Type|Conventional');
        insert level1picklistChildVal1;
        
        PicklistValues__c level1picklistChild3 = new PicklistValues__c(Parent_Pick_List_Value__c = level1picklistChildVal.Id, Name ='STATOR/ROTOR', Type__c = 'CHARTYPE',Field_API__c = 'Stator_Rotor__c', Field_Type__c = 'Picklist', External_Id__c = 'Surface|ARTIFICIAL LIFT|Product|PCP Pumps|Type|Conventional|STATOR/ROTOR');
        insert level1picklistChild3;
        
        PicklistValues__c level1picklistChildVal3 = new PicklistValues__c(Parent_Pick_List_Value__c = level1picklistChild3.Id, Name ='ROTOR', Type__c = 'CHARVAL', External_Id__c = 'Surface|ARTIFICIAL LIFT|Product|PCP Pumps|Type|Conventional|STATOR/ROTOR|ROTOR');
        insert level1picklistChildVal3;
        
        PicklistValues__c level1picklistChild2 = new PicklistValues__c(Parent_Pick_List_Value__c = level1picklistVal.Id, Name ='STATOR/ROTOR - 1', Type__c = 'CHARTYPE',Field_API__c = 'Stator_Rotor__c', Field_Type__c = 'Picklist', External_Id__c = 'Surface|ARTIFICIAL LIFT|Product|PCP Pumps|Type|CAM-CT|STATOR/ROTOR - 1');
        insert level1picklistChild2;
        
        PicklistValues__c level1picklistChildVal2 = new PicklistValues__c(Parent_Pick_List_Value__c = level1picklistChild2.Id, Name ='ROTOR - 1', Type__c = 'CHARVAL', External_Id__c = 'Surface|ARTIFICIAL LIFT|Product|PCP Pumps|Type|CAM-CT|STATOR/ROTOR|ROTOR - 1');
        insert level1picklistChildVal2;
        
        PicklistValues__c level2record = new PicklistValues__c(Parent_Pick_List_Value__c = level1record.Id, Name ='Test Brand',Type__c='LEVEL2', External_Id__c = 'Surface|ARTIFICIAL LIFT|Test Brand');
        insert level2record;
        
        PicklistValues__c level3record = new PicklistValues__c(Parent_Pick_List_Value__c = level2record.Id, Name ='Test Model',Type__c='LEVEL3', External_Id__c = 'Surface|ARTIFICIAL LIFT|Test Brand|Test Model');
        insert level3record;
        
        Division_Settings__c setting1 = new Division_Settings__c(Name = 'Surface', Use_Standard_Quantity__c = true, Show_Rig_Type__c = true);
        Division_Settings__c setting2 = new Division_Settings__c(Name = 'CPS', Use_Standard_Quantity__c = false, Show_Rig_Type__c = true);
        List<Division_Settings__c> settingList = new List<Division_Settings__c>{setting1, setting2};
        
        insert settingList;
        
       
        OpportunityLineItem optyLine = new OpportunityLineItem(OpportunityId = oppty.Id, PricebookEntryId = pbEntry.Id);
        insert optyLine;
        
        optyLine = [SELECT Product_Division__c, Opportunity.AccountId, Opportunity.isClosed, Opportunity.Primary_Account_Manager_calc__c, Group__c, Opportunity.CloseDate, Aftermarket__c, Invoice_Location__c, Requested_Shipment_Date__c,Opportunity_Business_Unit_Picklist__c,Market_Requested_Delivery_Date__c,Product_Characterstics__c,Market_Requested_Delivery_Months__c,Market_Requested_Delivery_Weeks__c,Rig_Type__c,Forecasted__c,Order_Type__c,Brand__c,Model__c,Quantity__c,UnitPrice,Quantity, PricebookEntry.Product2.Name,PricebookEntry.Product2.Division__c,OpportunityId,Product_Hierarchy_calc__c,Opportunity.RecordType.Name FROM OpportunityLineItem WHERE Id =:optyLine.Id LIMIT 1]; 
       
        
        // Instantiate the controller.
        PageReference pageRef = Page.OpportunityLineItemEditPage;
        Test.setCurrentPage(pageRef);
        
        OpportunityLineItemEditExtension extension = new OpportunityLineItemEditExtension(new ApexPages.StandardController(optyLine));
        Test.startTest();
            List<OpportunityLineItemEditExtension.SelectOptionWrapper> brandOptions = OpportunityLineItemEditExtension.populateBrand(optyLine.Id);
            List<OpportunityLineItemEditExtension.SelectOptionWrapper> modelOptions = OpportunityLineItemEditExtension.populateModel(level2record.Id);
        Test.stopTest();
        
        // Assert the result
        //System.assertEquals(brandOptions.size(),1);
        //System.assertEquals(brandOptions[0].Id,level2record.Id);
        
        System.assertEquals(modelOptions.size(),1);
        System.assertEquals(modelOptions[0].Id,level3record.Id);
        
        String dateFormatResult = extension.formatDate('2014-12-24');
        System.assertEquals(dateFormatResult,'12/24/2014');
        
        extension.populateCharactersticBasedOnFormula();
        
        extension.marketRequestedDeliveryMonth = 5;
        
        extension.calculateRequestedDeliveryDate();
        
        extension.marketRequestedDeliveryWeek = 5;
        extension.calculateRequestedDeliveryDateInWeeks();
        
        extension.populateLayoutData(level1picklistVal.Id);
        
        
        extension.selectedPicklistAPI = 'Type__c';
        extension.selectedPicklistId = level1picklistChildVal.Name;
        
        extension.reCalculateCharacterstics();
        extension.selectedPicklistId = level1picklistVal.Id;
        extension.reRenderCharacterstics();
        
        
        // Test Picklistwrapper
        OpportunityLineItemEditExtension.PicklistWrapper parentRecord = new OpportunityLineItemEditExtension.PicklistWrapper(level1picklistChildVal);
        OpportunityLineItemEditExtension.PicklistWrapper parentRecord2 = new OpportunityLineItemEditExtension.PicklistWrapper(level1picklistChildVal2);
       
        System.assertEquals(parentRecord.compareTo(parentRecord2)==0, true);        
        extension.saveLineItems();
        
        extension.cancelEdit();
        
    }

}