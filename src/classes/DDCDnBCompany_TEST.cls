/**
* @author       Chris Pitman
* @date         09/04/2014
* @description  This class have test methods that asserts the following:
*               1. 
*/

@isTest

public class DDCDnBCompany_TEST{

  public static String jsonStr = 
          '{"DnBCompany":[' +
          '{"GUDN":"000000000","DUDN":"000000000","HPDN":"000000000",' +
           '"SiteDUNS":"000000000","CreditRisk":"Low"}' +
          ']}';
          
  public static String testJsonStr = 
          '{"GUDN":"000000000","DUDN":"000000000","HPDN":"000000000","TradeStyle":"Fred",' +
           '"SiteDUNS":"000000000","CreditRisk":"Low","SICDesc":"Manufacturing","SICCode":"0000"}';

  static testMethod void testParse() {
    DDCDnBCompany.DnBCompany obj = DDCDnBCompany.parseJSONString(jsonStr);
    System.assert(obj != null);
    
    DDCDnBCompany.DnBCompany nobj = DDCDnBCompany.parseJSONString('');
    System.assert(nobj == null);
  }
  
  static testMethod void testGetToken() {
    String GUDNobj = DDCDnBCompany.getTokenValue('GUDN', testJsonStr);
    System.assert(GUDNobj == '000000000');
    
    String DUDNobj = DDCDnBCompany.getTokenValue('DUDN', testJsonStr);
    System.assert(DUDNobj == '000000000');
    
    String HPDNobj = DDCDnBCompany.getTokenValue('HPDN', testJsonStr);
    System.assert(HPDNobj == '000000000');        
 
    String SiteDUNSobj = DDCDnBCompany.getTokenValue('SiteDUNS', testJsonStr);
    System.assert(SiteDUNSobj == '000000000');

    String CreditRiskobj = DDCDnBCompany.getTokenValue('CreditRisk', testJsonStr);
    System.assert(CreditRiskobj == 'Low');
    
    String SICCodeobj = DDCDnBCompany.getTokenValue('SICCode', testJsonStr);
    System.assert(SICCodeobj == '0000');
    
    String SICDescobj = DDCDnBCompany.getTokenValue('SICDesc', testJsonStr);
    System.assert(SICDescobj == 'Manufacturing');

    String TradeStyleobj = DDCDnBCompany.getTokenValue('TradeStyle', testJsonStr);
    System.assert(TradeStyleobj == 'Fred');

    String nGUDNobj = DDCDnBCompany.getTokenValue('GUDN', '');
    System.assert(nGUDNobj == null);
  }
  
  static testMethod void testGetSiteDUNS()
  {
    System.assert(DDCDnBCompany.getSiteDUNS(testJsonStr) != null);
  }
  
  static testMethod void testGetGlobalUltimateDUNS()
  {
    System.assert(DDCDnBCompany.getGlobalUltimateDUNS(testJsonStr) != null);
  }
  
  static testMethod void testGetDomesticUltimateDUNS()
  {
    System.assert(DDCDnBCompany.getDomesticUltimateDUNS(testJsonStr) != null);
  }
  
  static testMethod void testGetHQParentDUNS()
  {
    System.assert(DDCDnBCompany.getHQParentDUNS(testJsonStr) != null);
  }
  
  static testMethod void testGetSICCode()
  {
    System.assert(DDCDnBCompany.getSICCode(testJsonStr) != null);
  }
  
  static testMethod void testGetSICDesc()
  {
    System.assert(DDCDnBCompany.getSICDesc(testJsonStr) != null);
  }
  
  static testMethod void testGetTradeStyle()
  {
    System.assert(DDCDnBCompany.getTradeStyle(testJsonStr) != null);
  }


}