public with sharing class IOICreateSummaryExtension {
    
    public Set<Id> selectedIOIIdSet{get;set;}
    public IOI__c ioiRecord {get;set;}
    public String ownerName {get;set;}
    public PageReference cancelAndGoBack(){
        return new PageReference('/' + IOI__c.SObjectType.getDescribe().getKeyPrefix() + '/o');
    }
    public IOICreateSummaryExtension(ApexPages.StandardController controller) {
        selectedIOIIdSet = new Set<Id>();
        
        for(String selectedId:ApexPages.CurrentPage().getParameters().get('ids').split(',')){
            selectedIOIIdSet.add((Id)selectedId);    
        } 
        ioiRecord = (IOI__c)controller.getRecord();
        ioiRecord.Description__c = generateIOISummary(selectedIOIIdSet);
        ownerName = UserInfo.getName();
        
        Period__c period = new Period__c();
        
        User currentUser = [SELECT Id, IOI_Reviewer__c, IOI_Reviewer__r.firstName, IOI_Reviewer__r.LastName FROM USER WHERE ID =:UserInfo.getUserId() LIMIT 1];
            
        List<Period__c> periodList = [SELECT Id,Name, IsCurrentPeriod__c, Type__c FROM Period__c WHERE IsCurrentPeriod__c = true AND Type__c = 'Week' LIMIT 1];  
        
        if(periodList!=null && !periodList.isEmpty())
        {
            ioiRecord.Period__c = periodList[0].Id;  
            ioiRecord.Name = periodList[0].Name +': Summary IOI';
        }
        ioiRecord.Next_Reviewer__c = currentUser.IOI_Reviewer__c;
        ioiRecord.Visibility_Level__c = 'Public';
        ioiRecord.IOI_Summary__c = true;
    }
    
    public PageReference redirectToEditPage(){
        PageReference returnPage = new PageReference('/' + IOI__c.SObjectType.getDescribe().getKeyPrefix() + '/e');
        System.debug('Before the exception is caught:');
        try{
        returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().Description__c,generateIOISummary(selectedIOIIdSet)); 
        }catch(Exception ex){
            System.debug('Exception caught:'+ex.getMessage()+':'+IOI_Prepopulate_Fields__c.getOrgDefaults().Description__c);
        }
        returnPage.getParameters().put(IOI_Prepopulate_Fields__c.getOrgDefaults().IOI_Summary__c,'1');
        returnPage.getParameters().put('nooverride','1'); 
        System.debug('After the exception is caught:'); 
        return returnPage;  
    }
    public String generateIOISummary(Set<Id>selectedIOIIdSet){
        String ioiSummary = '';
        
        Map<Id, List<IOI__c>> currentUserIOIMap = new Map<Id, List<IOI__c>>();
        List<IOI__c> currentUserIOIList = new List<IOI__c>();
        List<IOI__c> otherUserIOIList = new List<IOI__c>();
        List<IOI__c> ioiSummaryList = new List<IOI__c>();
        Id currentUserid = UserInfo.getUserId();
        
        
        for(IOI__c ioiRecord : [
                                    SELECT Id, Period__c,Period__r.Name, Opportunity__r.Name,Opportunity__c,Project__c, Description__c,  
                                    Project__r.Name, Account__c, Account__r.Name, IOI_Summary__c, CreatedById, CreatedBy.Name, Name, Owner.Name
                                    FROM IOI__c 
                                    WHERE Id IN: selectedIOIIdSet 
                                    ORDER By IOI_Summary__c DESC, Period__r.Name ASC
                                    LIMIT 100
                               ]){
        
            if(ioiRecord.CreatedById == currentUserid){
                currentUserIOIList.add(ioiRecord);    
            } 
            else{
                otherUserIOIList.add(ioiRecord);    
            }      
        } // End of for loop
        
        // After two lists has been created process the IOI record for Summary
        for(IOI__c currentUserIOI:currentUserIOIList){
            ioiSummary += processIOIRecordSummary(currentUserIOI)+'<br/>';   
        }
        
        for(IOI__c currentUserIOI:otherUserIOIList){
            ioiSummary += processIOIRecordSummary(currentUserIOI)+'<br/>'; 
        }
        
        return ioiSummary.substring(0,ioiSummary.length()-1);
    }
    public String processIOIRecordSummary(IOI__c ioiRecord){
        if(ioiRecord.IOI_Summary__c == false){
            String summary = ioiRecord.Period__r.Name+' - Created By: '+ioiRecord.CreatedBy.Name+'<br/><a href=\"/'+ioiRecord.Id+'\"'+'>'+ioiRecord.Name+'</a>: '+ioiRecord.Description__c+'<br/>';
            
            if(ioiRecord.Project__c!=null){
                summary+='Project: '+'<a href=\"/'+ioiRecord.Project__c+'\"'+'>'+ioiRecord.Project__r.Name+'</a>';
            }

            if(ioiRecord.Opportunity__c!=null){
                if(ioiRecord.Project__c!=null){
                    summary+=' | Opportunity: '+'<a href=\"/'+ioiRecord.Opportunity__c+'\"'+'>'+ioiRecord.Opportunity__r.Name+'</a>';
                }
                else {
                    summary+='Opportunity: '+'<a href=\"/'+ioiRecord.Opportunity__c+'\"'+'>'+ioiRecord.Opportunity__r.Name+'</a>';
                }
            }
            if(ioiRecord.Account__c!=null){
                if(ioiRecord.Opportunity__c!=null){
                    summary+=' | Account: '+'<a href=\"/'+ioiRecord.Account__c+'\"'+'>'+ioiRecord.Account__r.Name+'</a><br/>';
                }
                else {
                    summary+='Account: '+'<a href=\"/'+ioiRecord.Account__c+'\"'+'>'+ioiRecord.Account__r.Name+'</a><br/>';
                }
            }
            
            return summary;
        }
        else{ 
            String summary = ioiRecord.Period__r.Name+' - Created By: '+ioiRecord.CreatedBy.Name+'<br/><a href=\"/'+ioiRecord.Id+'\"'+'>'+ioiRecord.Name+'</a>: '+ioiRecord.Description__c+'<br/>';
            
            if(ioiRecord.Project__c!=null){
                summary+='Project: '+'<a href=\"/'+ioiRecord.Project__c+'\"'+'>'+ioiRecord.Project__r.Name+'</a>';
            }

            if(ioiRecord.Opportunity__c!=null){
                if(ioiRecord.Project__c!=null){
                    summary+=' | Opportunity: '+'<a href=\"/'+ioiRecord.Opportunity__c+'\"'+'>'+ioiRecord.Opportunity__r.Name+'</a>';
                }
                else {
                    summary+='Opportunity: '+'<a href=\"/'+ioiRecord.Opportunity__c+'\"'+'>'+ioiRecord.Opportunity__r.Name+'</a>';
                }
            }
            if(ioiRecord.Account__c!=null){
                if(ioiRecord.Opportunity__c!=null){
                    summary+=' | Account: '+'<a href=\"/'+ioiRecord.Account__c+'\"'+'>'+ioiRecord.Account__r.Name+'</a><br/>';
                }
                else {
                    summary+='Account: '+'<a href=\"/'+ioiRecord.Account__c+'\"'+'>'+ioiRecord.Account__r.Name+'</a><br/>';
                }
            }
            
            return summary;
        }
    }
}