/**
 * @author      Peeyush Awadhiya
 * @date        09/18/2014
 * @description Test Methods for OpportunityRejectLead class. It covers the following on button click:
 *              1. Set close date to "Today"
 *              2. Set stage to "Closed - Cancelled"
 *              3. Set Description to "Lead Rejected"
 *              
 */
 
@isTest
public class OpportunityRejectLead_TEST{
    
    static testMethod void testLeadRejection(){
         // Prepare the data stub
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc);              
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        project.CAM_Final_Destination__c = 'Spain';
        project.CAM_Owner_End_User__c = acc.Id;
        update project;
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunity
        oppty = TestUtilities.generateOpportunityWithoutInsert('Project Lead', 'Universe', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Surface';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = null;
        oppty.CAM_Final_Destination__c = 'Spain';
        oppty.AccountId = acc.Id;
        insert oppty;
        
        Test.startTest();
            // Now invoke the webservice
            String response = OpportunityRejectLead.rejectLead(oppty.Id);
        Test.stopTest();
        
        oppty = [SELECT Id, CloseDate, StageName, Description FROM Opportunity WHERE Id=:oppty.Id LIMIT 1];
        
        // Now assert the lead reject
        System.assertEquals(response,'1 Opportunity saved');
        System.assertEquals(oppty.CloseDate, Date.Today());
        System.assertEquals(oppty.StageName,'Closed Rejected');
        System.assertEquals(oppty.Description, 'Lead Rejected');
        
    
    }     

}