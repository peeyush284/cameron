/**
* @author       Peeyush Awadhiya
* @date         09/23/2014
* @description  This batch class does the following:
*               1. Queries for all account team member created on the same day and inserts EntitySubscription record for each of the team member.
*               2. Since the platform doesn't allow triggers on AccountTeamMember object, the batch apex approach was adopted. 
*/

global class AccountTeamMemberFollowAccountBatch implements Database.batchable<sObject>{
    string query;
    global AccountTeamMemberFollowAccountBatch(String queryStr){
        this.query = queryStr;
    }  
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<EntitySubscription> newFollowers = new List<EntitySubscription>();
        
        List<AccountTeamMember> accountTeamList = (List<AccountTeamMember>) scope;
        for (AccountTeamMember teamMember:accountTeamList){
            newFollowers.add(new EntitySubscription(ParentId = teamMember.AccountId, SubscriberId = teamMember.UserId));    
        }
        if(newFollowers!= null && !newFollowers.isEmpty()){
            Database.SaveResult [] saveResults = Database.insert(newFollowers, false);
            for(Database.SaveResult result:saveResults){
                if(!result.isSuccess()){
                    for(Database.Error err : result.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('EntitySubscription fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
    }
    global void finish(Database.BatchableContext BC){
    
    }
}