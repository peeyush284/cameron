/**
* @author       Peeyush Awadhiya
* @date         11/06/2014
* @description  This batch class does the following:
*               1. Queries for all sales team member with role 'Opportunity Creator'.
*               2. Update the sharing access to Edit. 
*/

global class OpportunityTeamMemberUpdateAccessBatch implements Database.batchable<sObject>{
    string query;
    
    global OpportunityTeamMemberUpdateAccessBatch(String queryStr){
        this.query = queryStr;
    }  
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        List<OpportunityTeamMember> opportunityCreatorList = new List<OpportunityTeamMember>();
        Set<Id> opportunityIdSet = new Set<Id>();
        Set<Id> userIdSet = new Set<Id>();
        
        opportunityCreatorList = (List<OpportunityTeamMember>) scope;
        
        for (OpportunityTeamMember teamMember:opportunityCreatorList){
            opportunityIdSet.add(teamMember.OpportunityId);
            userIdSet.add(teamMember.UserId);           
        }
        
        List<OpportunityShare> opportunityShareToUpdate = new List<OpportunityShare>();
        
        for(OpportunityShare opShare:[
            SELECT Id, UserOrGroupId,RowCause, OpportunityId, OpportunityAccessLevel
            FROM OpportunityShare   
            WHERE OpportunityId IN :opportunityIdSet
            AND UserOrGroupId IN :userIdSet
            AND OpportunityAccessLevel = 'Read'
            AND RowCause = 'Team'
        ]){
            opShare.OpportunityAccessLevel = 'Edit';
            opportunityShareToUpdate.add(opShare);    
        }
        
        // Now share the opportunity and grant read/write access to secondary sales.
        if(opportunityShareToUpdate!=null && !opportunityShareToUpdate.isEmpty()){
        
            Database.UpsertResult [] results = Database.upsert(opportunityShareToUpdate, false);
            
            // Iterate through each returned result
            for (Database.UpsertResult sr : results) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed.
                    System.debug('Successfully shared opportunity record. OpportunityShare ID: ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('OpportunityShare fields that affected this error: ' + err.getFields());
                    }
                }
            } // End of for loop.  
              
        } // End of  if(opportunityShareToUpdate!=null && !opportunityShareToUpdate.isEmpty())
        
       
    }
    global void finish(Database.BatchableContext BC){
    
    }
}