@isTest(seeAllData=true)
private class DDCDnBAccountHierarchy_TEST {
    
    static TestMethod void Test0_TestInsertAccount()
    {
        Account newAcct = new Account();
        newAcct.Name = 'BP BERAU LTD';
        newAcct.dunsnumber = '728860276';
        newAcct.billingCountry = 'Indonesia';
        insert newAcct;
        
        Account acct = [SELECT Id, Name
                          FROM Account
                         WHERE ID = :newAcct.ID limit 1];
                         
        System.assertEquals(acct.Name, 'BP BERAU LTD');
        
        acct.Sic = '8999';
        update acct;                         
        
        System.assertEquals(acct.Sic, '8999');  
        
        delete acct;            
    }
    
    static TestMethod void Test0_TestbuildAccountHierarchyWithNoParms()
    {
        boolean dataExists = false;
        Account newAcct = new Account();
        newAcct.Name = 'BP BERAU LTD';
        newAcct.dunsnumber = '728860276';
        newAcct.billingCountry = 'Indonesia';
        insert newAcct;
        
        Account acct;
        
        dataExists = True;
        try {
         acct = [SELECT Id, Name, ParentId, DunsNumber, DandBCompany.GlobalUltimateDunsNumber
                          FROM Account
                         WHERE DunsNumber != null
                           AND DandBCompany.GlobalUltimateDunsNumber != null
                           AND ParentId != null
                       and (not name like 'Exxon%') 
                       and (not name like 'BP%')
                       and (not name like 'Shell%')
                         LIMIT 1];
                         
                  List<Id> acctIds;

        String result = DDCDnbAccountHierarchy.buildAccountHierarchy();
        //System.assertNotEquals(result, null);
        //System.assertNotEquals(result, 'INFO: No accounts passed.');
        //System.assertNotEquals(result, 'INFO: No accounts belong to a hierarchy');
        //System.assertEquals(result, 'SUCCESS: Done');

        acctIds = new List<Id>();
        acctIds.add('00QE000000IkCSA');
        result = DDCDnbAccountHierarchy.buildAccountHierarchy(acctIds);
        //System.assertNotEquals(result, null);
        //System.assertEquals(result, 'INFO: No accounts passed.');
        //System.assertNotEquals(result, 'INFO: No accounts belong to a hierarchy');
        //System.assertNotEquals(result, 'SUCCESS: Done');

        acctIds = new List<Id>();
        acctIds.add(acct.id);
        result = DDCDnbAccountHierarchy.buildAccountHierarchy(acctIds);
        //System.assertNotEquals(result, null);
        //System.assertNotEquals(result, 'INFO: No accounts passed.');
        //System.assertNotEquals(result, 'INFO: No accounts belong to a hierarchy');
        //System.assertEquals(result, 'SUCCESS: Done');
        } catch (Exception e) {
            dataExists = false;
        }
        
        if (dataExists) {

        }
    }

   static TestMethod void Test0_TestbuildAccountHierarchyFromTrigger()
    {
    
       Account newAcct = new Account();
        newAcct.Name = 'BP BERAU LTD';
        newAcct.dunsnumber = '728860276';
        newAcct.billingCountry = 'Indonesia';
        insert newAcct;
            
        List<Account> accts = new List<Account>();
        String result = null;

        result = DDCDnbAccountHierarchy.buildAccountHierarchy(accts, TRUE);
        system.debug(logginglevel.DEBUG, 'result = ' + result);
        System.assertEquals(result, 'INFO: No accounts passed.');

        for (Account acct:[SELECT Id, Name, ParentId,
                           DunsNumber, 
                           DandBCompany.GlobalUltimateDunsNumber,
                           DandBCompany.DomesticUltimateDunsNumber,                               
                           DandBCompany.ParentOrHqDunsNumber
                      FROM Account
                     WHERE DunsNumber != null
                       AND DandBCompany.GlobalUltimateDunsNumber != null
                       AND ParentId != null
                    
                   ]){ 
            accts.add(acct);
        }     
        
        result = DDCDnbAccountHierarchy.buildAccountHierarchy(accts, TRUE);
        //System.assertNotEquals(result, null);
        //System.assertNotEquals(result, 'INFO: No accounts belong to a hierarchy');
        
        result = DDCDnbAccountHierarchy.buildAccountHierarchy(accts, FALSE);
        //System.assertNotEquals(result, null);
        //System.assertNotEquals(result, 'INFO: No accounts belong to a hierarchy');        
    }
    
    static TestMethod void Test0_TestbuildAccountHierarchyWithIds()
    {
        boolean dataExists = false;
        Account newAcct = new Account();
        newAcct.Name = 'BP BERAU LTD';
        newAcct.dunsnumber = '728860276';
        newAcct.billingCountry = 'Indonesia';
        insert newAcct;
        Account Acct;
        dataExists = true;
        try {
        acct = [SELECT Id, Name, ParentId, DunsNumber, DandBCompany.GlobalUltimateDunsNumber
                          FROM Account
                         WHERE DunsNumber != null
                           AND DandBCompany.GlobalUltimateDunsNumber != null
                           AND ParentId != null
                         LIMIT 1];
        } catch (Exception e) {
            dataExists = false;
        }
        
        if (dataExists) {
        List<Id> acctIds;

        String result = DDCDnbAccountHierarchy.buildAccountHierarchy(acct.id);
        //System.assertNotEquals(result, null);
        //System.assertNotEquals(result, 'INFO: No accounts passed.');
        //System.assertNotEquals(result, 'INFO: No accounts belong to a hierarchy');
        //System.assertEquals(result, 'SUCCESS: Done');

        acctIds = new List<Id>();
        acctIds.add('00QE000000IkCSA');
        result = DDCDnbAccountHierarchy.buildAccountHierarchy(acctIds);
        //System.assertNotEquals(result, null);
        //System.assertEquals(result, 'INFO: No accounts passed.');
        //System.assertNotEquals(result, 'INFO: No accounts belong to a hierarchy');
        //System.assertNotEquals(result, 'SUCCESS: Done');

        acctIds = new List<Id>();
        acctIds.add(acct.id);
        result = DDCDnbAccountHierarchy.buildAccountHierarchy(acctIds);
        //System.assertNotEquals(result, null);
        //System.assertNotEquals(result, 'INFO: No accounts passed.');
        //System.assertNotEquals(result, 'INFO: No accounts belong to a hierarchy');
        //System.assertEquals(result, 'SUCCESS: Done');
        }
    }

    static TestMethod void Test0_TestbuildAccountHierarchyWithStrings()
    {
        boolean dataExists = true;
        Account newAcct = new Account();
        newAcct.Name = 'BP BERAU LTD';
        newAcct.dunsnumber = '728860276';
        newAcct.billingCountry = 'Indonesia';
        insert newAcct;
            
        dataExists = true;
        
        Account acct;
        
        try {
        acct = [SELECT Id, Name, ParentId, DunsNumber, DandBCompany.GlobalUltimateDunsNumber
                          FROM Account
                         WHERE DunsNumber != null
                           AND DandBCompany.GlobalUltimateDunsNumber != null
                           AND ParentId != null
                         LIMIT 1];
        } catch(Exception e) {
            dataExists = false;
        }
        
        if (dataExists) {
        List<String> acctStrings = new List<String>();
        acctStrings.add(acct.id);

        String result = DDCDnbAccountHierarchy.buildAccountHierarchy(acctStrings);
        //System.assertNotEquals(result, null);
        //System.assertNotEquals(result, 'INFO: No accounts passed.');
        //System.assertNotEquals(result, 'INFO: No accounts belong to a hierarchy');
        //System.assertEquals(result, 'SUCCESS: Done');
        }
    }

    static TestMethod void Test0_TestbuildAccountHierarchyWithAccounts()
    {
    
        Account newAcct = new Account();
        newAcct.Name = 'BP BERAU LTD';
        newAcct.dunsnumber = '728860276';
        newAcct.billingCountry = 'Indonesia';
        insert newAcct;
        
        List<Account> accts = new List<Account>();
        String result = null;
        boolean dataExists = false;

        result = DDCDnbAccountHierarchy.buildAccountHierarchy(accts);
        System.assertEquals(result, 'INFO: No accounts passed.');

        Account acct;
        dataExists = true;
        
        try
        {
            acct = [SELECT Id, Name, ParentId,
                           DunsNumber, 
                           DandBCompany.GlobalUltimateDunsNumber,
                           DandBCompany.DomesticUltimateDunsNumber,                               
                           DandBCompany.ParentOrHqDunsNumber
                      FROM Account
                     WHERE DunsNumber != null
                       AND DandBCompany.GlobalUltimateDunsNumber != null
                       AND ParentId != null
                     LIMIT 1
                   ];
                                         
        } catch (Queryexception qe) {
            System.assert(True);
            dataExists = false;
        }                   
        
        if (dataExists) { 
        accts.add(acct);

        result = DDCDnbAccountHierarchy.buildAccountHierarchy(accts);
        //System.assertNotEquals(result, null);
        //System.assertNotEquals(result, 'INFO: No accounts passed.');
        //System.assertNotEquals(result, 'INFO: No accounts belong to a hierarchy');
        //System.assertEquals(result, 'SUCCESS: Done');
        }

        String GUDN ;
        
        try {
        GUDN = acct.DandBCompany.GlobalUltimateDunsNumber;
        } catch (Exception e) {
            System.assert(True);
            dataExists = false;
        }
        accts = new List<Account>();
        dataExists = true;
        
        try
        {
            acct = [SELECT Id, Name, ParentId,
                           DunsNumber, 
                           DandBCompany.GlobalUltimateDunsNumber,
                           DandBCompany.DomesticUltimateDunsNumber,                               
                           DandBCompany.ParentOrHqDunsNumber
                      FROM Account
                     WHERE DunsNumber != null
                       AND DunsNumber != :GUDN
                       AND DandBCompany.ParentOrHqDunsNumber = :GUDN
                     LIMIT 1
                   ];
        } catch (Queryexception qe) {
            System.assert(True);
            dataExists = false;
        }                   
        
        if (dataExists) {         
        accts.add(acct);

        result = DDCDnbAccountHierarchy.buildAccountHierarchy(accts);
        }
        //System.assertEquals(result, 'SUCCESS: Done');

        accts = new List<Account>();
        dataExists = true;        
        
        try
        {
            acct = [SELECT Id, Name, ParentId,
                           DunsNumber, 
                           DandBCompany.GlobalUltimateDunsNumber,
                           DandBCompany.DomesticUltimateDunsNumber,                               
                           DandBCompany.ParentOrHqDunsNumber
                      FROM Account
                     WHERE DunsNumber != null
                       AND (DandBCompany.ParentOrHqDunsNumber = null
                        OR  DandBCompany.ParentOrHqDunsNumber = '000000000') 
                       and (not name like 'Exxon%') 
                       and (not name like 'BP%')
                       and (not name like 'Shell%')
                     LIMIT 1
                   ];
        } catch (Queryexception qe) {
            System.assert(true);
            dataExists = false;
        }                   
        
        if (dataExists) {        
        accts.add(acct);

        result = DDCDnbAccountHierarchy.buildAccountHierarchy(accts);
        //System.assertEquals(result, 'SUCCESS: Done');
        }

        // Test using a site that is a couple of levels down.
        accts = new List<Account>();
        dataExists = true;        
        
        try
        {
            acct = [SELECT Id, Name, ParentId,
                           DunsNumber, 
                           DandBCompany.GlobalUltimateDunsNumber,
                           DandBCompany.DomesticUltimateDunsNumber,                               
                           DandBCompany.ParentOrHqDunsNumber
                      FROM Account
                     WHERE DunsNumber = '850490360'
                     LIMIT 1
                   ];
        } catch (Queryexception qe) {
            System.assert(true);
            dataExists = false;
        }                   
        
        if (dataExists){
        acct.ParentId = null;       
        accts.add(acct);        

        result = DDCDnbAccountHierarchy.buildAccountHierarchy(accts);
        }
        //System.assertEquals(result, 'SUCCESS: Done');
    }
/*
    static TestMethod void Test0_TestRemoveUnwanted()
    {
        Account acct;
        
        try
        {
            acct = [SELECT Id, Name, ParentId, DnBCompanyJson__c,
                           DunsNumber, 
                           Global_Ultimate_DUNS_Nbr__c,
                           Domestic_Ultimate_DUNS_Nbr__c,                               
                           HQ_Parent_DUNS_Nbr__c
                      FROM Account
                     WHERE DunsNumber != null
                       AND Global_Ultimate_DUNS_Nbr__c != null
                       AND HQ_Parent_DUNS_Nbr__c != null
                       AND HQ_Parent_DUNS_Nbr__c != '000000000'
                     LIMIT 1
                   ];
                                         
        } catch (Queryexception qe) {
            System.assert(false);
        }                   
                
        List<Account> accts = new List<Account>();
        accts.add(acct);

        System.assertNotEquals((DDCDnbAccountHierarchy.removeUnwanted(accts)).size(), 0);
    }

    static TestMethod void Test1_TestRemoveUnwanted()
    {
        Account acct;
        
        try
        {
            acct = [SELECT Id, Name, ParentId, DnBCompanyJson__c,
                           DunsNumber, 
                           Global_Ultimate_DUNS_Nbr__c,
                           Domestic_Ultimate_DUNS_Nbr__c,                               
                           HQ_Parent_DUNS_Nbr__c
                      FROM Account
                     WHERE DunsNumber != null
                       AND (HQ_Parent_DUNS_Nbr__c = null
                        OR  HQ_Parent_DUNS_Nbr__c = '000000000')
                     LIMIT 1
                   ];
                                         
        } catch (Queryexception qe) {
            System.assert(false);
        }                   
                
        List<Account> accts = new List<Account>();
        accts.add(acct);

        System.assertEquals((DDCDnbAccountHierarchy.removeUnwanted(accts)).size(), 0);
    }

    static TestMethod void Test0_TestDnBCompany()
    {
        System.assertEquals(DDCDnbCompany.getTokenValue('GUDN', DDCDnBCompany.testJsonStr), '000000000');
        System.assertEquals(DDCDnbCompany.getSiteDUNS(DDCDnBCompany.testJsonStr), '000000000');
        System.assertEquals(DDCDnbCompany.getGlobalUltimateDUNS(DDCDnBCompany.testJsonStr), '000000000');
        System.assertEquals(DDCDnbCompany.getDomesticUltimateDUNS(DDCDnBCompany.testJsonStr), '000000000');
        System.assertEquals(DDCDnbCompany.getHQParentDUNS(DDCDnBCompany.testJsonStr), '000000000');
        System.assertEquals(DDCDnbCompany.getSICCode(DDCDnBCompany.testJsonStr), '0000');
        System.assertEquals(DDCDnbCompany.getSICDesc(DDCDnBCompany.testJsonStr), 'Manufacturing');
        
        DDCDnbCompany.DnBCompany cmpny = DDCDnbCompany.parseJSONString(DDCDnBCompany.jsonStr);
        System.assertEquals(cmpny.GUDN,'000000000');
    }
*/  
}