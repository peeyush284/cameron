/**
* @author       Gabor Bay
* @date         09/23/2014
* @description  This batch class does the following:
*               1. Deletes records from the "Analytic Snapshot: Bucket CY Open" Object
*               2. Upon completion, invokes the batch which deletes "Analytic Snapshot: Forecast Oppty" object records.
*
*/

global class ASDeleteASFcstBucketOpptyBatch implements Database.batchable<sObject>{
    string query;
    global ASDeleteASFcstBucketOpptyBatch(String queryStr){
        this.query = queryStr;
    }  
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){       
        List<Analytic_Snapshot_Bucket_Oppty__c> asOpportunityToDeleteList = (List<Analytic_Snapshot_Bucket_Oppty__c>) scope;

    // Now delete records from Analytics Snapshot
        if(asOpportunityToDeleteList!=null && !asOpportunityToDeleteList.isEmpty()){
            Database.DeleteResult [] results = Database.delete(asOpportunityToDeleteList, false);
            
            // Iterate through each returned result
            for (Database.DeleteResult sr : results) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('asOpportunity fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }  
    }    
    global void finish(Database.BatchableContext BC){
        // Upon completion, invokes the batch which deletes "Analytic Snapshot: Forecast Oppty" object records.
        String queryStr = 'SELECT Id FROM Analytic_Snapshot_Forecast_Oppty__c WHERE Name != null';
        ASDeleteASFcstOpptyBatch batchObj = new ASDeleteASFcstOpptyBatch(queryStr);
        ID batchProcessId = Database.executeBatch(batchObj, 100);  
        
        System.debug('Executing the ASDeleteASFcstOpptyBatch with process Id:'+batchProcessId);
    
    }
}