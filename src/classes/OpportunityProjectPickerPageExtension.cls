/**
* @author       Peeyush Awadhiya
* @date         07/28/2014
* @description  This class serves as an extension to the OpportunityProjectPickerPage. 
*/

global without sharing class OpportunityProjectPickerPageExtension {
    
    public List<CAM_Project__c> matchingProjects {get;set;}
    public boolean showErrorMsg {get;set;}
    public boolean matchingProjectsFound{get;set;}
    
    public String opportunityPageURL {get;set;}
    public Opportunity opportunityRecord {get;set;}
    public boolean showPopup {get;set;}
    public boolean showAltPopup {get;set;}
    public String projectPickerPageURL{get;set;}
    public Id projectId {get;set;}
    public String popupMessage {get;set;}
    public String sectionHeaderMessage {get;set;}
    public Id opportunityRecordTypeId {get;set;}
    public CAM_Project__c probationaryProject {get;set;}
    public String probationaryProjectName {get;set;}
    
    global OpportunityProjectPickerPageExtension(ApexPages.StandardController controller) {
        showErrorMsg = false;
        probationaryProjectName = '';
        matchingProjects = new List<CAM_Project__c>();
        if(!Test.isRunningTest()){
            controller.addFields(new List<String>{'CAM_Project__c','Parent_Project__c','RecordTypeId', 'OwnerId', 'AccountId','End_User__c','End_User__r.Name','Account.Name','CAM_Final_Destination__c','Popup_Viewed__c'});
        }
        opportunityRecord = (Opportunity)controller.getRecord();
        opportunityRecordTypeId = Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get(System.Label.Project_Create_Opportunity_Button_RecordType).getRecordTypeId();
        popupMessage = String.escapeSingleQuotes(String.valueOf(System.Label.Matching_Projects_Popup));
        sectionHeaderMessage = String.valueOf(System.Label.Matching_Projects_Section_Header);
       
        matchingProjects = [
            SELECT Id, Name, RecordType.Name, Block__c, CAM_Est_Value__c, Status__c, 
            CAM_Final_Destination__c, CAM_Owner_End_User__c, CAM_Project_Value_CUR__c, 
            CAM_Phase__c, Project_Type__c, CreatedDate 
            FROM CAM_Project__c
            WHERE CAM_Final_Destination__c =: opportunityRecord.CAM_Final_Destination__c
            AND CAM_Owner_End_User__c =: opportunityRecord.End_User__c
            LIMIT 1000
        ];
        
        if(!matchingProjects.isEmpty()){
            matchingProjectsFound = !matchingProjects.isEmpty();  
        }
        else{
              
            sectionHeaderMessage  = String.valueOf(System.Label.Matching_Projects_Popup);  
        }
        
        opportunityPageURL = String.valueOf(System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+OpportunityRecord.Id);
        
        String retURL = '/apex/OpportunityProjectPickerPage?id=';
        projectPickerPageURL = String.valueOf(System.URL.getSalesforceBaseUrl().toExternalForm()+retURL+OpportunityRecord.Id);
        
        if(UserInfo.getUserId() == OpportunityRecord.OwnerId && (OpportunityRecord.CAM_Project__c==null && OpportunityRecord.Parent_Project__c==null) && OpportunityRecord.Popup_Viewed__c == false){
            showPopup = true;      
        }
        
    }
    // Include
    public PageReference cancelProjectSelection(){
        opportunityRecord.Popup_Viewed__c = true;
        
        // Update the flag, if user wishes to opt-out project selection
        try{
            update opportunityRecord;    
        }catch(Exception ex){
            System.debug('Exception caught while updating opportunity:'+ex.getMessage());
        }
        return new Pagereference('/'+opportunityRecord.Id);
    }
    
    public PageReference goBackToTheProjectSelection(){
        PageReference projectSelectionPage =  new Pagereference('/apex/OpportunityProjectPickerPage?id='+opportunityRecord.Id);    
        projectSelectionPage.setRedirect(false);
        return projectSelectionPage;
    }
    
    public PageReference updateSelectedProjectOnOpportunity(){
        opportunityRecord.Popup_Viewed__c = true;
        if(opportunityRecord.RecordTypeId == opportunityRecordTypeId){
            opportunityRecord.Parent_Project__c = projectId;
        }else{
            opportunityRecord.CAM_Project__c = projectId;
        }
        // Update the flag, if user wishes to opt-out project selection
        try{
            update opportunityRecord;    
        }catch(Exception ex){
            System.debug('Exception caught while updating opportunity:'+ex.getMessage());
        }
        return (new ApexPages.StandardController(opportunityRecord)).view();
    }
    
    // Include
    public PageReference redirectToProbationaryProjectPage(){
        PageReference pageRef = new PageReference('/apex/OpportunityProjectProbationaryProject?id='+opportunityRecord.Id);
        pageRef.setRedirect(false);
        //probationaryProject = new CAM_Project__c(CAM_Owner_End_User__c = opportunityRecord.End_User__c, CAM_Final_Destination__c = opportunityRecord.CAM_Final_Destination__c, Name=String.valueOf('Project: '+opportunityRecord.End_User__r.Name+' '+opportunityRecord.CAM_Final_Destination__c), CAM_Phase__c = 'Phase 0'); 
        probationaryProject = new CAM_Project__c(CAM_Owner_End_User__c = opportunityRecord.End_User__c, CAM_Final_Destination__c = opportunityRecord.CAM_Final_Destination__c, CAM_Phase__c = 'Phase 0', Status__c = 'Probation'); 
        
        return pageRef;
    }
    public PageReference createProbationaryProjectAndRedirect(){
        List<CAM_Project__c> existingProject = [SELECT Id, Name FROM CAM_Project__c WHERE Name =:probationaryProjectName LIMIT 1];
        if(existingProject!=null && !existingProject.isEmpty()){
            ApexPages.getMessages().clear();
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Project with name:'+probationaryProjectName+' already exist. Please enter different project name.');
            ApexPages.addMessage(myMsg);
            showErrorMsg = true;
            return null;        
        }
        else{
            probationaryProject.Name = probationaryProjectName;
            insert probationaryProject; 
             
            opportunityRecord.Popup_Viewed__c = true;
            
            if(opportunityRecord.RecordTypeId == opportunityRecordTypeId){
                opportunityRecord.Parent_Project__c = probationaryProject.Id;
            }else{
                opportunityRecord.CAM_Project__c = probationaryProject.Id;
            }  
            // Update the flag, if user wishes to opt-out project selection
            try{
                update opportunityRecord;    
            }catch(Exception ex){
                System.debug('Exception caught while updating opportunity:'+ex.getMessage());
            }
            return (new ApexPages.StandardController(opportunityRecord)).view(); 
        }
    }
    
}