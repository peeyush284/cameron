@isTest(SeeAllData=True)
                                
private class DDCIsGlobalUltimateUpdate_TEST {

    static testmethod void test0() {
        // the batch job query is contant and set in the batch job
        
        Account newAcct = new Account();
        newAcct.Name = 'BP BERAU LTD';
        newAcct.dunsnumber = '728860276';
        newAcct.IsGlobalUltimate__c = TRUE;
        newAcct.billingCountry = 'Indonesia';
        insert newAcct; 
        
       // Create an account that fits the criteria of the batch job
       List<Account> accts = [SELECT Id, Name, IsGlobalUltimate__c, 
                                                DunsNumber, DandBCompany.GlobalUltimateDunsNumber,
                                                DandBCompany.DomesticUltimateDunsNumber, 
                                                DandBCompany.ParentOrHqDunsNumber
                                           FROM Account 
                                          WHERE DandBCompany.Id != null
                                          and IsGlobalUltimate__c = TRUE
                                          LIMIT 1]; 
                    
       List <Account> ultAccount = New List<Account>();                                   
       if (accts.size() == 0) {  
           Account y = new Account();
           y.id = null;
           y.name = 'Salesforce.com, Inc.'; 
           y.billingCountry = 'United States';  
           y.billingStreet = '1 Market Ste 300';
           y.billingState = 'California';
           y.billingCity = 'San Francisco';
           y.billingPostalCode = '94105';           
           y.IsGlobalUltimate__c = True;     
           ultAccount.add(y);   
           insert ultAccount;
       }                                          
                                          
       List <Account> newAccount = New List<Account>();
       for (Account x: accts) {
           x.id = null;
           x.name = 'Test DDCIsGlobalUltimateUpdate Account'; 
           x.billingCountry = 'United States';   
           x.IsGlobalUltimate__c = False;  
           newAccount.add(x);
       }
       
       if (newAccount.size() > 0) {           
           insert newAccount;
       }
       else
       {
           // assumption for this test is that there is at least one account that is a GlobalUltimate in the org to copy
           //System.assertEquals(newAccount.size(), 1);
       }
       
       Test.startTest();
 
       DDCIsGlobalUltimateUpdate c = new DDCIsGlobalUltimateUpdate();
       c.query += ' LIMIT 200';

       Database.executeBatch(c);
   
       Test.stopTest();
       
       List<Account> afteracct = [SELECT Id, Name, IsGlobalUltimate__c, 
                              DunsNumber, DandBCompany.GlobalUltimateDunsNumber,
                              DandBCompany.DomesticUltimateDunsNumber, 
                              DandBCompany.ParentOrHqDunsNumber
                              FROM Account 
                              WHERE name = 'Test DDCIsGlobalUltimateUpdate Account' LIMIT 1];
                              
       for (Account x: afteracct) {
          System.Debug(loggingLevel.INFO, 'After executeBatch name = ' + x.name + ' DunsNumber = ' + x.DunsNumber + ' IsGlobalUltimate__c = ' + x.IsGlobalUltimate__c);
          System.assertEquals(x.IsGlobalUltimate__c, true);
       }
                              
    }
    
    static testmethod void test1() {
        // the batch job query is contant and set in the batch job
        
         Account newAcct = new Account();
        newAcct.Name = 'BP BERAU LTD';
        newAcct.dunsnumber = '728860276';
        newAcct.IsGlobalUltimate__c = false;
        newAcct.billingCountry = 'Indonesia';
        insert newAcct; 
        
       // Create an account that fits the criteria of the batch job
       List<Account> accts = [SELECT Id, Name, IsGlobalUltimate__c, 
                                                DunsNumber, DandBCompany.GlobalUltimateDunsNumber,
                                                DandBCompany.DomesticUltimateDunsNumber, 
                                                DandBCompany.ParentOrHqDunsNumber
                                           FROM Account 
                                          WHERE DandBCompany.Id != null
                                          and IsGlobalUltimate__c = false
                                          LIMIT 1]; 
       
       List <Account> ultAccount = New List<Account>();                                   
       if (accts.size() == 0) {  
           Account y = new Account();
           y.id = null;
           y.name = 'Salesforce.com, Inc.'; 
           y.billingCountry = 'United States';  
           y.billingStreet = '1 Market Ste 300';
           y.billingState = 'California';
           y.billingCity = 'San Francisco';
           y.billingPostalCode = '94105';           
           y.IsGlobalUltimate__c = false;     
           ultAccount.add(y);   
           insert ultAccount;
       }  
                                         
       List <Account> newAccount = New List<Account>();
       for (Account x: accts) {
           x.id = null;
           x.name = 'Test DDCIsGlobalUltimateUpdate Account'; 
           x.billingCountry = 'United States';   
           x.IsGlobalUltimate__c = False;  
           newAccount.add(x);
       }
       
       if (newAccount.size() > 0) {           
           insert newAccount;
       }
       else
       {
           // assumption for this test is that there is at least one account that is a not a GlobalUltimate in the org to copy
           System.assertEquals(newAccount.size(), 1);
       }
       
       Test.startTest();
 
       DDCIsGlobalUltimateUpdate c = new DDCIsGlobalUltimateUpdate();
       c.query += ' LIMIT 200';

       Database.executeBatch(c);
   
       Test.stopTest();
       
       List<Account> afteracct = [SELECT Id, Name, IsGlobalUltimate__c, 
                              DunsNumber, DandBCompany.GlobalUltimateDunsNumber,
                              DandBCompany.DomesticUltimateDunsNumber, 
                              DandBCompany.ParentOrHqDunsNumber
                              FROM Account 
                              WHERE name = 'Test DDCIsGlobalUltimateUpdate Account' LIMIT 1];
                              
       for (Account x: afteracct) {
          System.Debug(loggingLevel.INFO, 'After executeBatch name = ' + x.name + ' DunsNumber = ' + x.DunsNumber + ' IsGlobalUltimate__c = ' + x.IsGlobalUltimate__c);
          //System.assertEquals(x.IsGlobalUltimate__c, false);
       }
                              
    }

}