/**
* @author       Peeyush Awadhiya
* @date         08/06/2014
* @description  This class contains test method for extension OpportunityProductScheduleExtension. It test the following:
*               1. Enables end user to establish schedules for opportunity line items.
*/
@isTest
public class OpportunityProductScheduleExt_TEST{
    
    public static testMethod void testProductSchedule(){
         // Prepare the data stub
        Account acc;
        CAM_Project__c project = new CAM_Project__c(); 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc);              
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunity
        oppty = TestUtilities.generateOpportunityWithoutInsert('In the Funnel/Best Few', 'Best Few', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Surface';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = project.Id;
        
        oppty.AccountId = acc.Id;
        insert oppty;
        
        // Product
        Product2 prod = new Product2(Name = 'Surface Stack', Division__c = 'Surface', Group__c = 'ARTIFICIAL LIFT', isActive = true, CanUseRevenueSchedule = true);
        insert prod;
        
        Pricebook2 priceBook= new Pricebook2(Name='Test Pricebook');
        insert priceBook;
        
        PricebookEntry pbEntry = new PricebookEntry(isActive = true,pricebook2Id = Test.getStandardPricebookId(), product2Id = prod.Id, UnitPrice=100, UseStandardPrice = false);
        insert pbEntry;
        
        // Set the current page and instantiate the extension
        PageReference pageRef = Page.OpportunityProductEstablishSchedulePage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('retURL', '/'+oppty.Id);
        
        OpportunityLineItem optyLine = new OpportunityLineItem(OpportunityId = oppty.Id, PricebookEntryId = pbEntry.Id);
        insert optyLine;
        
        OpportunityProductScheduleExtension extension = new OpportunityProductScheduleExtension(new ApexPages.StandardController(oppty));
        
//gb 100214        System.assertEquals(extension.monthMap.size(),12);
        System.assertEquals(extension.scheduleWrapperList.size(),1);
        
        System.assertEquals(extension.scheduleWrapperList[0].lineItemScheduleList.size(),12);
        // Set the revenue
        for(OpportunityLineItemSchedule schedule:extension.scheduleWrapperList[0].lineItemScheduleList){
            schedule.Revenue = 1000;
            System.assertEquals((schedule.Id!=null), false);
        }
        
        String detailPageURL = extension.saveLineItems().getURL();
        
        System.assertEquals(detailPageURL.contains('/'+oppty.Id),true);
        
        for(OpportunityLineItemSchedule schedule:extension.scheduleWrapperList[0].lineItemScheduleList){
            System.assertEquals(schedule.Revenue,1000);
            System.assertEquals((schedule.Id!=null), true);
        }
        
        for(OpportunityLineItem opLine:[    SELECT Id,UnitPrice, PricebookEntry.Product2.Name, CurrencyISOCode,
                                            (SELECT Id,OpportunityLineItemId, Type, Quantity, Revenue, ScheduleDate,CurrencyISOCode FROM OpportunityLineItemSchedules ORDER BY ScheduleDate ASC LIMIT 12)
                                            FROM OpportunityLineItem
                                            WHERE OpportunityId =:oppty.Id 
                                            LIMIT 80]){
            System.assertEquals(opLine.OpportunityLineItemSchedules.size(),12);                                    
        }
        
        // Cancel the schedule
        detailPageURL = extension.cancelSchedule().getURL();
        
        System.assertEquals(detailPageURL.contains('/'+oppty.Id),true);
        
        // Re-instantiate the extension 
        pageRef = Page.OpportunityProductEstablishSchedulePage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('retURL', '/'+oppty.Id);
        
        extension = new OpportunityProductScheduleExtension(new ApexPages.StandardController(oppty));
        
        // Re-assert the page load. On subsequent page load, rowTotal would be populated
        System.assertEquals(extension.scheduleWrapperList[0].rowTotal,12000);
        for(OpportunityLineItemSchedule schedule:extension.scheduleWrapperList[0].lineItemScheduleList){
            System.assertEquals(schedule.Revenue,1000);
            System.assertEquals((schedule.Id!=null), true);
        }
        
        for(OpportunityLineItem opLine:[    SELECT Id,UnitPrice, PricebookEntry.Product2.Name, CurrencyISOCode,
                                            (SELECT Id,OpportunityLineItemId, Type, Quantity, Revenue, ScheduleDate,CurrencyISOCode FROM OpportunityLineItemSchedules ORDER BY ScheduleDate ASC LIMIT 12)
                                            FROM OpportunityLineItem
                                            WHERE OpportunityId =:oppty.Id 
                                            LIMIT 80]){
            System.assertEquals(opLine.OpportunityLineItemSchedules.size(),12);                                    
        }
    }    

}