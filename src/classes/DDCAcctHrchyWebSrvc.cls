global class DDCAcctHrchyWebSrvc {
    
    WebService static void buildAccountHierarchy() 
    {
//      String result = DDCDnbAccountHierarchy.buildAccountHierarchy();
        Id batchId = Database.executeBatch(new DDCOrgDnBAccountHierarchy(), 10);
        System.debug(LoggingLevel.INFO, 'Result from buildAccountHierarchy(): '+ batchId);      
    }

    WebService static void buildAccountHierarchyById(Id pAccountId) 
    {
        String result = DDCDnbAccountHierarchy.buildAccountHierarchy(pAccountId);
        System.debug(LoggingLevel.INFO, 'Result from buildAccountHierarchy(): '+ result);       
    }

}