@isTest
private class DDCAcctHrchyWebSrvc_TEST {

    static testMethod void testAccountHierarchyWebSrvc() {
        DDCAcctHrchyWebSrvc.buildAccountHierarchy();
        System.assertEquals(1, 1);
    }
    
    static testMethod void testAccountHierarchyWebSrvcById() {
        
        DDCAccountHierachyData_TEST.createTestHierarchy(); 

        Account topAccount = [Select id, name from account where name = 'HierarchyTest0' LIMIT 1];
        Account middleAccount = [Select id, parentID, name from account where name = 'HierarchyTest4' LIMIT 1];
        Account bottomAccount = [Select id, parentID, name from account where name = 'HierarchyTest9' LIMIT 1];
        Account[] accountList = [Select id, parentID, name from account where name like 'HierarchyTest%'];

        test.startTest();

        DDCAcctHrchyWebSrvc.buildAccountHierarchyById(topAccount.ID);
        System.assertEquals(1, 1);
        
        test.stopTest();
    }
}