@isTest
private class OpportunityTeamMemberAssign_TEST {

   static testMethod void test0(){
        Account acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc);      
        
        SchedulableContext sc = null;
        OpportunityAccTeamMemberAssignScheduler myClass = new OpportunityAccTeamMemberAssignScheduler();
        myClass.execute(sc);
    }
    
    static testMethod void test1(){
    
        Account acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc);  
        
        // Project
        CAM_Project__c project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunit
        Opportunity oppty = TestUtilities.generateOpportunityWithoutInsert('In the Funnel/Best Few', 'Best Few', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Surface';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = project.Id;
        
        oppty.AccountId = acc.Id;
        

        insert oppty;
                 
        List < Opportunity > myList = [Select id, AccountId, Primary_Account_Manager_calc__c from Opportunity LIMIT 10];
        
        Database.BatchableContext bc = null;    
        OpportunityAccountTeamMemberAssignBatch myClass = new OpportunityAccountTeamMemberAssignBatch('Select id, AccountId, Primary_Account_Manager_calc__c from Opportunity LIMIT 10');
        myClass.execute(bc, (List <sObject>) myList);
        myClass.finish(bc);
    }
}