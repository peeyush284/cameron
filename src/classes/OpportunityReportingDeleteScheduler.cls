/**
* @author       Peeyush Awadhiya
* @date         10/27/2014
* @description  Scheduler class for the batch class that does the following:
*               1. Deletes records from the "Opportunity Reporting" custom object.
*               2. Upon completion, invokes the batch copies the OpportunityLineItemSchedules into Opportunity_Reporting__c object.
*               3. Copies the OpportunityLineItems into Opportunity_Reporting__c object.
*               
*/

global class OpportunityReportingDeleteScheduler implements Schedulable {
    
    global void execute(SchedulableContext sch) {
    
        String queryStr = 'SELECT Id FROM Opportunity_Reporting__c';
        OpportunityReportingDeleteBatch batchObj = new OpportunityReportingDeleteBatch(queryStr);
        ID batchProcessId = Database.executeBatch(batchObj, 20);  
        
        System.debug('Executing the batch with process Id:'+batchProcessId);
    }
}