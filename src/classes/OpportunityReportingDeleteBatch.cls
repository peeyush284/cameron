/**
* @author       Peeyush Awadhiya
* @date         10/27/2014
* @description  This batch class does the following:
*               1. Deletes records from the "Opportunity Reporting" custom object.
*               2. Upon completion, invokes the batch copies the OpportunityLineItemSchedules into Opportunity_Reporting__c object.
*
*/

global class OpportunityReportingDeleteBatch implements Database.batchable<sObject>{
    string query;
    global OpportunityReportingDeleteBatch(String queryStr){
        this.query = queryStr;
    }  
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){       
        List<Opportunity_Reporting__c> opportunityReportingToDeleteList = (List<Opportunity_Reporting__c>) scope;

        // Now delete all opportunity reporting records.
        if(opportunityReportingToDeleteList!=null && !opportunityReportingToDeleteList.isEmpty()){
            Database.DeleteResult [] results = Database.delete(opportunityReportingToDeleteList, false);
            
            // Iterate through each returned result
            for (Database.DeleteResult sr : results) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Opportunity Reporting fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }  
    }    
    global void finish(Database.BatchableContext BC){
        // Upon completion, invokes the batch copies the OpportunityLineItemSchedules into Opportunity_Reporting__c object.

        String queryStr = 'SELECT Id, OpportunityLineItem.Opportunity.OwnerId, CurrencyISOCode, '+
                            'OpportunityLineItem.Opportunity.ForecastCategoryName, ScheduleDate,OpportunityLineItem.OpportunityId, '+
                            'OpportunityLineItem.PricebookEntry.Product2.Name,OpportunityLineItem.Opportunity.RecordType.Name, '+
                            'Revenue, OpportunityLineItem.order_type__c, OpportunityLineItem.product_class__c,' +
                            'OpportunityLineItem.QuantityCalc__c, OpportunityLineItem.Manufacturing_Plant__c, '+
                            'OpportunityLineItem.Market_Requested_Delivery_Weeks__c ' +
                            ' FROM OpportunityLineItemSchedule';
                 
        OpportunityReportingCopyScheduleBatch batchObj = new OpportunityReportingCopyScheduleBatch(queryStr);
        ID batchProcessId = Database.executeBatch(batchObj, 100);  
        
        System.debug('Executing the OpportunityReportingCopyScheduleBatch with process Id:'+batchProcessId);
    }
}