/**
 * @author      Salesforce {GB}
 * @date        5/27/2014
 * @description From button click on Project, query for Account Team Members
 *              Where User.CreateOpportunity = true
 *              Create an opportunity for each team member
 */
 
global class ProjectCreateOpportunity{

    WebService static String createOpportunities(Id ProjectId, 
                                                Id AccountId,
                                                string projEndUserNm, 
                                                string projName,
                                                string projStartDate, 
                                                string projFinalDestNm) {
                        
        date dtProjStartDate = Date.parse(projStartDate);       
        String responseStr = '';
        String opptyStage = Label.Project_Create_Opportunity_Button_Stage;
        String opptyRecordType = Label.Project_Create_Opportunity_Button_RecordType;
        Set<String> validateProjectLeads = new Set<String>();
        
        /*
        * @author  Peeyush Awadhiya
        * @date 08/20/2014
        * @description When the COE member clicks on Add Project Leads, prior to creating the Project Leads, query for duplicate Account Team Roles
        *
        *              1. If any duplicates exist: Show a message that says "Duplicate Account Team Members exist.  Please resolve Account Team Membership duplicates and then Add Project Leads" 
        *              Do not create any Project Leads
        *              2. If no duplicates exists: Create Project Leads for all account team members with roles that are not in the Custom Object for Preventing Project Leadsnames
        */
        Map<String,Id> accountTeamMemberRoleDeDupeMap = new Map<String,Id>();
        for (AccountTeamMember AcctTeamMember: [SELECT UserId, TeamMemberRole  FROM AccountTeamMember WHERE AccountId = :AccountId]){
            if(!accountTeamMemberRoleDeDupeMap.containsKey(AcctTeamMember.TeamMemberRole )){
                accountTeamMemberRoleDeDupeMap.put(AcctTeamMember.TeamMemberRole, AcctTeamMember.Id);    
            }
            else{
                return 'Duplicate Account Team Members exist. Please resolve Account Team Membership duplicates and then Add Project Leads';
            }
        }
        for(Prevent_Project_Leads__c setting:Prevent_Project_Leads__c.getall().values()){
            validateProjectLeads.add(setting.Name);    
        }
        
        //get id for the Record Type        
        id opptyRecordTypeId = Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get(opptyRecordType).getRecordTypeId();
        list<Opportunity>OpptyList=New list <Opportunity>(); //**
        Set<Id> userIds = new Set<Id>();
        /*
        * @author  Peeyush Awadhiya
        * @date 08/20/2014
        * @description Do not create opportunities for the account team members who already own opportunities, related to the same project.
        * Also, exclude the team members whose roles are defined in Prevent Lead Assignment custom settings.
        *
        */
        Set<Id> accountTeamMembersToExclude = new Set<Id>();
        for (Opportunity relatedOpportunity: [SELECT Id, OwnerId, End_User__c FROM Opportunity WHERE ((Parent_Project__c =: projectId OR CAM_Project__c=:projectId) AND End_User__c =:AccountId) LIMIT 2000]){
            accountTeamMembersToExclude.add(relatedOpportunity.OwnerId);
        }
        for (AccountTeamMember AcctTeamMember: [SELECT UserId, TeamMemberRole  FROM AccountTeamMember WHERE AccountId = :AccountId AND UserId NOT IN:accountTeamMembersToExclude]) {       
            // Exclude the team members which have roles defined in custom setting.
            
            if(!validateProjectLeads.contains(string.valueOf(AcctTeamMember.TeamMemberRole))){
                userIds.add(AcctTeamMember.UserId);
            }
        }
        List<User> listUsers  =[SELECT Id, camDivision__c, camBusinessUnit__c  
                                FROM User 
                                WHERE Id IN :userIds AND User.Create_Opportunity__c = true];

        //Populate Opportunity Object fields

        for(User objUser: listUsers){

            Opportunity oppty = new Opportunity (
                                                OwnerId = objUser.Id,                                  
                                                Parent_Project__c = ProjectId,
                                                RecordTypeId = opptyRecordTypeId,
                                                StageName = opptyStage,
                                                CloseDate = dtProjStartDate,
                                                End_User__c = AccountId,
                                                AccountId=AccountId,
                                                CAM_Final_Destination__c = projFinalDestNm,
                                                CAM_Division__c = objUser.camDivision__c,
                                                CAM_Business_Unit__c = objUser.camBusinessUnit__c
                                                );
            //Name = projEndUserNm + ' - ' + projName + ' - ' + objUser.camDivision__c + '/' + objUser.camBusinessUnit__c
            if (objUser.camBusinessUnit__c == null && objUser.camDivision__c == null){
            oppty.Name = projEndUserNm + ' - ' + projName;}
            else if (objUser.camBusinessUnit__c == null) {
            oppty.Name = projEndUserNm + ' - ' + projName + ' - ' + objUser.camDivision__c;}
            else {
            oppty.Name = projEndUserNm + ' - ' + projName + ' - ' + objUser.camDivision__c + '/' + objUser.camBusinessUnit__c;}

            OpptyList.add(oppty);
        }
        //responseStr= '0 opportunities(s) inserted!';
        responseStr= string.valueof(OpptyList.size());
        if(OpptyList<>null && !OpptyList.isempty())
        {
            Database.SaveResult [] saveResult = Database.insert(OpptyList,false);
            for(Database.SaveResult result:saveResult){
                if(!result.isSuccess()){
                    for(Database.Error err : result.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Opportunity fields that affected this error: ' + err.getFields());
                    }
                    responseStr='Exception caught while adding Opportunities';                  
                }
                else{                   
                    //responseStr= OpptyList.size()+' opportunities(s) inserted!'; 
                    responseStr= string.valueof(OpptyList.size());                    
                }               
            }
        }           
        
        return responseStr;
    }
      
}