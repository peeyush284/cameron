@isTest(seeAllData=true)
private with sharing class DDCDnBCompanyController_TEST 
{
    // Test the batch class execution of the code.
    static TestMethod void Test0_DnBCompanyForAccount()
    {
        
        List<Account> acct = [SELECT Id, Name 
                                FROM Account 
                               WHERE DandBCompanyId != null
                                 AND isDeleted = false
                               LIMIT 1
                             ];
        
        System.assertNotEquals(acct.Size(),0);

        DDCDnBCompanyController controller = new DDCDnBCompanyController();
               
        controller.currentId = acct.get(0).Id;
        controller.objectType = 'Account';
        DDCDnBCompanyController.DnBCompanyStructure company = controller.getDnBCompany();
        
        System.Assert(company.isFromAccount);  
        System.Assert(! String.isBlank(company.company.Id));  
        System.Assert(! String.isBlank(company.getSmallBusiness()));  
        System.Assert(! String.isBlank(company.getMinorityOwned()));  
        System.Assert(! String.isBlank(company.getWomenOwned()));  
        System.Assert(! String.isBlank(company.getMarketingPreScreen()));  
        System.Assert(! String.isBlank(company.getSubsidiary()));  
        System.Assert(! String.isBlank(company.getLocationStatus()));  
        System.Assert(! String.isBlank(company.getOutOfBusiness()));  
        System.Assert(! String.isBlank(company.getLegalStatus()));  
        System.Assert(! String.isBlank(company.getMarketingSegmentationCluster()));  
    }
        
    static TestMethod void Test1_DnBCompanyForAccount()
    {
        List<Account> acct = [SELECT Id, Name 
                                FROM Account 
                               WHERE DandBCompanyId = null
                                 AND isDeleted = false
                               LIMIT 1
                             ];
        
        System.assertNotEquals(acct.Size(),0);

        DDCDnBCompanyController controller = new DDCDnBCompanyController();
               
        controller.currentId = acct.get(0).Id;
        controller.objectType = 'Account';
        DDCDnBCompanyController.DnBCompanyStructure company = controller.getDnBCompany();
        
        System.Assert(company.isFromAccount);  
        System.Assert(! String.isBlank(company.getSmallBusiness()));  
        System.Assert(! String.isBlank(company.getMinorityOwned()));  
        System.Assert(! String.isBlank(company.getWomenOwned()));  
        System.Assert(! String.isBlank(company.getMarketingPreScreen()));  
        System.Assert(! String.isBlank(company.getSubsidiary()));  
        System.Assert(! String.isBlank(company.getLocationStatus()));  
        System.Assert(! String.isBlank(company.getOutOfBusiness()));  
        System.Assert(! String.isBlank(company.getLegalStatus()));  
        System.Assert(! String.isBlank(company.getMarketingSegmentationCluster()));  
    }
        
    static TestMethod void Test0_DnBCompanyForLead()
    {
        List<Lead> lead = [SELECT Id, Name 
                             FROM Lead 
                            WHERE DandBCompanyId != null
                              AND isConverted = false
                              AND isDeleted = false
                            LIMIT 1];
        
        if (lead.size() > 0) {
        System.assertNotEquals(lead.Size(),0);

        DDCDnBCompanyController controller = new DDCDnBCompanyController();               
        
        //controller.currentId = '00QE000000E1LQXMA3';
        controller.currentId = lead.get(0).Id;
        controller.objectType = 'Lead';
        DDCDnBCompanyController.DnBCompanyStructure company = controller.getDnBCompany();
        
        System.Assert(company.isFromLead);  
        System.Assert(! String.isBlank(company.company.Id));  
        System.Assert(! String.isBlank(company.getSmallBusiness()));  
        System.Assert(! String.isBlank(company.getMinorityOwned()));  
        System.Assert(! String.isBlank(company.getWomenOwned()));  
        System.Assert(! String.isBlank(company.getMarketingPreScreen()));  
        System.Assert(! String.isBlank(company.getSubsidiary()));  
        System.Assert(! String.isBlank(company.getLocationStatus()));  
        System.Assert(! String.isBlank(company.getOutOfBusiness()));  
        System.Assert(! String.isBlank(company.getLegalStatus()));  
        System.Assert(! String.isBlank(company.getMarketingSegmentationCluster()));  
        }
    }

    static TestMethod void Test0_DnBCompanyInput()
    {
        DDCDnBCompanyController controller = new DDCDnBCompanyController();               
        
        controller.currentId = '00QE000000E1LQXMA3';
        DDCDnBCompanyController.DnBCompanyStructure company = controller.getDnBCompany();
        System.Assert(! String.isBlank(company.errMsg));
    }

    static TestMethod void Test1_DnBCompanyInput()
    {
        DDCDnBCompanyController controller = new DDCDnBCompanyController();               
        
        controller.objectType = 'Account';
        DDCDnBCompanyController.DnBCompanyStructure company = controller.getDnBCompany();
        System.Assert(! String.isBlank(company.errMsg));
    }

}