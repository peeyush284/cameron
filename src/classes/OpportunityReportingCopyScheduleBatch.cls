/**
* @author       Peeyush Awadhiya
* @date         10/27/2014
* @description  This batch class does the following:
*               1. Copies the OpportunityLineItemSchedules into Opportunity_Reporting__c object.
*               2. Upon completion, invokes the batch which copies all bucket OpportunityLineItem records to Opportunity Reporting custom object.
*
*/

global class OpportunityReportingCopyScheduleBatch implements Database.batchable<sObject>{
    string query;
    global OpportunityReportingCopyScheduleBatch(String queryStr){
        this.query = queryStr;
    }  
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){       
        List<OpportunityLineItemSchedule> scheduleList = (List<OpportunityLineItemSchedule>) scope;
        List<Opportunity_Reporting__c> reportingsToInsert = new List<Opportunity_Reporting__c>();
        
        for(OpportunityLineItemSchedule schedule:scheduleList){
            Opportunity_Reporting__c reporting = new Opportunity_Reporting__c(
                OwnerId = schedule.OpportunityLineItem.Opportunity.OwnerId,
                Forecast_Category__c = schedule.OpportunityLineItem.Opportunity.ForecastCategoryName,
                Opportunity_Booking_Date__c = schedule.ScheduleDate,
                Opportunity_Name__c = schedule.OpportunityLineItem.OpportunityId,
                Opportunity_Product_Name__c = schedule.OpportunityLineItem.PricebookEntry.Product2.Name,
                Opportunity_Type__c = schedule.OpportunityLineItem.Opportunity.RecordType.Name,
                Opportunity_Amount__c = schedule.Revenue,
                Forecast_Amount__c = schedule.Revenue,
                CurrencyISOCode = schedule.CurrencyISOCode,
                Source_Id__c = schedule.Id,
                Order_Type__c = schedule.OpportunityLineItem.Order_Type__c,
                Product_Class__c = schedule.OpportunityLineItem.Product_Class__c,
                Quantity__c = schedule.OpportunityLineItem.QuantityCalc__c,
                Manufacturing_Plant__c = schedule.OpportunityLineItem.Manufacturing_Plant__c,
                Market_Requested_Delivery_Weeks__c = schedule.OpportunityLineItem.Market_Requested_Delivery_Weeks__c    
            );
            
            if(schedule.ScheduleDate>DATE.TODAY()||(schedule.ScheduleDate.Month() == Date.TODAY().Month() && schedule.ScheduleDate.Year() == Date.TODAY().Year())){
            
                reporting.Forecast_Category__c = 'Committed';
            }
            else{
        
                reporting.Forecast_Category__c = 'Closed';
            }
            reportingsToInsert.add(reporting);

        }
        
        // Now insert the opportunity reporting records.
        if(reportingsToInsert!=null && !reportingsToInsert.isEmpty()){
            Database.SaveResult [] results = Database.Insert(reportingsToInsert, false);
            
            // Iterate through each returned result
            for (Database.SaveResult sr : results) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Opportunity Reporting fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }  
    }    
    global void finish(Database.BatchableContext BC){
        // Upon completion, invokes the batch which copies all bucket OpportunityLineItem records to Opportunity Reporting custom object.

        String queryStr = 'SELECT Id, Opportunity.OwnerId, Opportunity.ForecastCategoryName, Opportunity.CloseDate, OpportunityId, PricebookEntry.Product2.Name, Opportunity.RecordType.Name, TotalPrice, Forecast_Amount_Formula__c, Order_Type__c, Product_Class__c, QuantityCalc__c, Manufacturing_Plant__c, Market_Requested_Delivery_Weeks__c, CurrencyISOCode FROM OpportunityLineItem';
        queryStr += ' WHERE Opportunity.RecordType.Name!=\'Bucket\'';
        OpportunityReportingCopyLineItemBatch batchObj = new OpportunityReportingCopyLineItemBatch(queryStr);
        ID batchProcessId = Database.executeBatch(batchObj, 100);  
        
        System.debug('Executing the ASDeleteASFcstOpptyBatch with process Id:'+batchProcessId);
    }
}