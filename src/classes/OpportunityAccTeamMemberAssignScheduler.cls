/**
* @author       Peeyush Awadhiya
* @date         10/08/2014
* @description  Scheduler class for the batch class that does the following:
*               1. Queries for all opportunities and related account team members.
*               2. Updates the division specific account team owner to Account_Team_Owner__c field on opportunity. 
*/

global class OpportunityAccTeamMemberAssignScheduler implements Schedulable {
    
    global void execute(SchedulableContext sch) {
    
        String queryStr = 'SELECT Id, AccountId, Primary_Account_Manager_calc__c, Account_Team_Owner__c FROM Opportunity WHERE End_User__r.Name!=\'Multiple Accounts\'';
        OpportunityAccountTeamMemberAssignBatch batchObj = new OpportunityAccountTeamMemberAssignBatch(queryStr);
        ID batchProcessId = Database.executeBatch(batchObj, 20);  
        
        System.debug('Executing the batch with process Id:'+batchProcessId);
    }
}