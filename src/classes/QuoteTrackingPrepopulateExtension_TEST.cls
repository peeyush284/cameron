/**
* @author       Peeyush Awadhiya
* @date         10/27/2014
* @description  This class does the following:
*               1. When the Quote Tracking record is created from the opportunity related list, it populates the Quote tracking fields.
*/

@isTest
public class QuoteTrackingPrepopulateExtension_TEST {
    public static testMethod void testQuoteTrackingPrepopulateExtension(){
        // Prepare the data stub
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('Bucket', 'RFQ', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Process Systems';
        oppty.CAM_Business_Unit__c = 'CPS';        
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = project.Id;
        oppty.CAM_Final_Destination__c = 'USA';
        oppty.AccountId = acc.Id;
        insert oppty;
        
        oppty = [SELECT Id, CAM_Final_Destination__c, Market_Segment__c, End_User__c FROM Opportunity WHERE Id =: oppty.Id LIMIT 1 ];
        User salesUser = [SELECT Id FROM User WHERE camDivision__c = 'Process Systems' and camBusinessUnit__c = 'CPS' and isActive = true AND ID!=:UserInfo.getUserId() LIMIT 1];
        // Now insert the quote
        QuoteTracking__c quote = new QuoteTracking__c(Opportunity__c = oppty.Id, Outside_Sales_Person__c = salesUser.Id, Ultimate_Destination__c ='USA', End_User__c = acc.Id);
        insert quote;
        
        Quote_Tracking_Prepopulate_Field__c hierarchialCustomSetting;
        
        if(Quote_Tracking_Prepopulate_Field__c.getOrgDefaults() == null){
            // Need to change it to org specific values in the production
            hierarchialCustomSetting = new Quote_Tracking_Prepopulate_Field__c(); 
        }
        else{
            hierarchialCustomSetting = Quote_Tracking_Prepopulate_Field__c.getOrgDefaults();
            hierarchialCustomSetting.End_User__c = 'CF00Nc00000014EuK'; 
            hierarchialCustomSetting.Market_Segment__c = '00Nc00000014O1y';
            hierarchialCustomSetting.Ultimate_Destination__c = '00Nc00000014O1Z';
            hierarchialCustomSetting.Opportunity__c = 'CF00Nc00000014BBR';
        }
        
        upsert hierarchialCustomSetting;
        
        PageReference pageRef = Page.QuotesPrePopulateFromOpportunity;
        Test.setCurrentPage(pageRef);
        
        // Use case 1: When IOI is created from Account 
        pageRef.getParameters().put('retURL','/'+oppty.Id);
        
        QuoteTrackingPrepopulateExtension extension = new QuoteTrackingPrepopulateExtension(new ApexPages.StandardController(quote));
        String editPageURL = extension.returnEditPage().getURL();
        
          // Assert the PageReference
        System.assertEquals(editPageURL.contains(Quote_Tracking_Prepopulate_Field__c.getOrgDefaults().Opportunity__c+'_lkid='+oppty.Id), true);
        System.assertEquals(editPageURL.contains(Quote_Tracking_Prepopulate_Field__c.getOrgDefaults().End_User__c+'_lkid='+oppty.End_User__c), true);
        //System.assertEquals(editPageURL.contains(Quote_Tracking_Prepopulate_Field__c.getOrgDefaults().Market_Segment__c+'='+oppty.Market_Segment__c), true);
        System.assertEquals(editPageURL.contains(Quote_Tracking_Prepopulate_Field__c.getOrgDefaults().Ultimate_Destination__c+'='+oppty.CAM_Final_Destination__c), true);
        
    }       

}