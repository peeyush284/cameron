@isTest (SeeAllData=true)
public class OpportunityHelper_TEST{
    static testMethod void testInsert() {
        /* CGP commented out the following 8-Oct-2014
        String atmRole = 'PS|CPS|Acct Mgr|Primary';        
        String projRecordType = 'External';
        String projStatus = 'Permanent';        
        String opptyRecordType = 'Standard';
        String opptyStage = 'Clarification';               
        Account a = TestUtilities.generateAccount();
        
        User u = [SELECT UserName,Email FROM User WHERE isActive = true LIMIT 1]; 
        AccountTeamMember atm = new AccountTeamMember();
        atm.AccountID = a.Id;
        atm.TeamMemberRole = atmRole;       
        atm.UserId = u.Id;
        insert atm; 
          
        CAM_Project__c p = TestUtilities.generateProject(projRecordType, projStatus, a);
        Opportunity o = TestUtilities.generateOpportunity(opptyRecordType, opptyStage, p);
        */
        
        Account acc;
        CAM_Project__c project = new CAM_Project__c(); 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc);              
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('In the Funnel/Best Few', 'Best Few', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Surface';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = project.Id;
        
        oppty.AccountId = acc.Id;
        
        Test.startTest();
            insert oppty;
        Test.stopTest();
                


    } 
     
     static testMethod void testUpdate() {
        /* CGP commented out the following 8-Oct-2014
        String atmRole = 'PS|CPS|Acct Mgr|Primary';        
        String projRecordType = 'Internal';
        String projStatus = 'Permanent';        
        String opptyRecordType = 'Standard';
        String opptyStage = 'Clarification';               
        Account a = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember(atmRole, a);  
        CAM_Project__c p = TestUtilities.generateProject(projRecordType, projStatus, a);
        Opportunity o = TestUtilities.generateOpportunity(opptyRecordType, opptyStage, p);
        */
         Account acc;
        CAM_Project__c project = new CAM_Project__c(); 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc);              
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('In the Funnel/Best Few', 'Best Few', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Surface';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = project.Id;
        
        oppty.AccountId = acc.Id;
        
        Test.startTest();
            insert oppty;
        Test.stopTest();
                
        
        oppty.Description = 'TEST2';
        update oppty;
     }  
 
    static testMethod void testAddOpptyOwnerToProject(){
        // Test data 
        
        /* CGP commented out the following 8-Oct-2014
       
        String atmRole = 'PS|CPS|Acct Mgr|Primary';        
        String projRecordType = 'External';
        String projStatus = 'Permanent';        
        String opptyRecordType = 'Standard';
        String opptyStage = 'Clarification';               
        Account a = TestUtilities.generateAccount();
        
        User u = [SELECT UserName,Email FROM User WHERE isActive = true LIMIT 1]; 
        AccountTeamMember atm = new AccountTeamMember();
        atm.AccountID = a.Id;
        atm.TeamMemberRole = atmRole;       
        atm.UserId = u.Id;
        insert atm; 
          
        CAM_Project__c p = TestUtilities.generateProject(projRecordType, projStatus, a);
        Opportunity o = TestUtilities.generateOpportunity(opptyRecordType, opptyStage, p);
        */
        
        
   
    }
    
    static testMethod void testDelete() {
        /* CGP commented out the following 8-Oct-2014
        String atmRole = 'PS|CPS|Acct Mgr|Primary';        
        String projRecordType = 'External';
        String projStatus = 'Permanent';        
        String opptyRecordType = 'Standard';
        String opptyStage = 'Clarification';               
        Account a = TestUtilities.generateAccount();
        
        User u = [SELECT UserName,Email FROM User WHERE isActive = true LIMIT 1]; 
        AccountTeamMember atm = new AccountTeamMember();
        atm.AccountID = a.Id;
        atm.TeamMemberRole = atmRole;       
        atm.UserId = u.Id;
        insert atm; 
          
        CAM_Project__c p = TestUtilities.generateProject(projRecordType, projStatus, a);
        Opportunity o = TestUtilities.generateOpportunity(opptyRecordType, opptyStage, p);
        */
        
        Account acc;
        CAM_Project__c project = new CAM_Project__c(); 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc);              
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('In the Funnel/Best Few', 'Best Few', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Surface';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = project.Id;
        
        oppty.AccountId = acc.Id;
        
        Test.startTest();
            insert oppty;
            delete oppty;
        Test.stopTest();
                


    } 
}