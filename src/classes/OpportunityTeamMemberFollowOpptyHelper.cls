/*
* @author Peeyush Awadhiya
* @date 08/20/2014
* @description : The class methods should do following:
*                1. When a team member is added to the Sales team, they should automatically follow the record.
*                If Secondary Sales (Sold To/Contractor) is added then share the opportunity with Read/Write permission.
*                2. When a team member is removed, the member should "unfollow" the opportunity record.
*                3. Send email notification to newly added sales team member.
*                4. Prevent update of Opportunity Team Member with role Opportunity Creator.
*                5. Prevent Delete of Opportunity Team Member with role Opportunity Creator.
*/


public class OpportunityTeamMemberFollowOpptyHelper{
    
    /*                
    * 1. When a team member is added to the Sales team, they should automatically follow the record.
    *    If Secondary Sales (Sold To/Contractor) or Opportunity Creator is added then share the opportunity with Read/Write permission.
    *
    */   
    public static void handleInsert(List<OpportunityTeamMember> newRecords){
        Map<Id,Map<Id,boolean>>followerMap = new Map<Id,Map<Id,boolean>>();
        Map<Id,boolean> temp;
        //Map<Id,Map<Id,boolean>>assignEditShareMap = new Map<Id,Map<Id,boolean>>();
        boolean isSecondarySales = false;
        for(OpportunityTeamMember opTeam:newRecords){
            isSecondarySales = false;
            //Flag the record if team role == Secondary Sales (Sold To/Contractor) to share the opportunity with Read/Write permission.
            isSecondarySales = (opTeam.TeamMemberRole == System.Label.Sales_Team_Sold_To_Role || opTeam.TeamMemberRole == System.Label.Sales_Team_Contractor_Role) ? true:false;
            
            if(!followerMap.containsKey(opTeam.OpportunityId)){
                temp = new Map<Id,boolean>();
                temp.put(opTeam.UserId,isSecondarySales);
                followerMap.put(opTeam.OpportunityId, temp);    
            }else{
                followerMap.get(opTeam.OpportunityId).put(opTeam.UserId,isSecondarySales);
            }
            
        }
        if(followerMap!=null && !followerMap.isEmpty()){
            followOpportunityRecord(followerMap);
        } 
        
    }
    
    /*
    * @author Peeyush Awadhiya
    * @date 11/10/2014
    * 4. Prevent update of Opportunity Team Member with role Opportunity Creator.
    */
    
    public static void handleBeforeUpdate(List<OpportunityTeamMember> newRecords, Map<Id, OpportunityTeamMember> oldRecordMap){
        
        for(OpportunityTeamMember opTeam:newRecords){
           
            if(opTeam.TeamMemberRole!=oldRecordMap.get(opTeam.Id).TeamMemberRole && oldRecordMap.get(opTeam.Id).TeamMemberRole == System.Label.Sales_Team_Opportunity_Creator){
                opTeam.addError('Opportunity Team Member with role Opportunity Creator cannot be updated.');
            }
        }
    }
    
   
    public static void handleUpdate(List<OpportunityTeamMember> newRecords, Map<Id, OpportunityTeamMember> oldRecordMap){
        Map<Id,Map<Id,boolean>>followerMap = new Map<Id,Map<Id,boolean>>();
        Map<Id,boolean> temp;
        
       
        for(OpportunityTeamMember opTeam:newRecords){
            
           
            
            if(!followerMap.containsKey(opTeam.OpportunityId) && opTeam.TeamMemberRole!=oldRecordMap.get(opTeam.Id).TeamMemberRole && opTeam.TeamMemberRole == System.Label.Sales_Team_Opportunity_Creator){
                temp = new Map<Id,boolean>();
                temp.put(opTeam.UserId,true);
                followerMap.put(opTeam.OpportunityId, temp);    
            }else{
                followerMap.get(opTeam.OpportunityId).put(opTeam.UserId,true);
            }
            
        }
        if(followerMap!=null && !followerMap.isEmpty()){
            followOpportunityRecord(followerMap);
        } 
    }
    
    public static void followOpportunityRecord(Map<Id,Map<Id, boolean>> followerMap){
        Set<Id> entitySubscriptionIdSet = new Set<Id>();
        List<EntitySubscription> followerList = new List<EntitySubscription>();
        List<OpportunityShare> opportunitySharingList = new List<OpportunityShare>();
        Set<Id>opportunityIdSet = new Set<Id>();
        Set<Id> userIdSet = new Set<Id>();
        
        for(Id opportunityId:followerMap.keySet()){
            for(Id followerId:followerMap.get(opportunityId).keySet()){
                EntitySubscription follower = new EntitySubscription(PARENTID = opportunityId, SUBSCRIBERID = followerId);
                followerList.add(follower); 
                
                // If Secondary Sales (Sold To/Contractor) is added then share the opportunity with Read/Write permission.
                if(followerMap.get(opportunityId).get(followerId)){
                    opportunityIdSet.add(opportunityId);
                    userIdSet.add(followerId);    
                } 
            }
        } // End of for loop
        
        if(opportunityIdSet!=null && !opportunityIdSet.isEmpty() && userIdSet!=null && !userIdSet.isEmpty()){
            
            for(OpportunityShare share: [
                SELECT Id, UserOrGroupId,RowCause, OpportunityId, OpportunityAccessLevel
                FROM OpportunityShare   
                WHERE OpportunityId IN :opportunityIdSet
                AND UserOrGroupId IN :userIdSet
                AND OpportunityAccessLevel = 'Read'
                AND RowCause = 'Team'
            ]){
                share.OpportunityAccessLevel = 'Edit';
                opportunitySharingList.add(share);
                
            }
            
            // Now share the opportunity and grant read/write access to secondary sales.
            if(opportunitySharingList!=null && !opportunitySharingList.isEmpty()){
                Database.UpsertResult [] results = Database.upsert(opportunitySharingList, false);
                
                // Iterate through each returned result
                for (Database.UpsertResult sr : results) {
                    if (sr.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed.
                        System.debug('Successfully shared opportunity record. OpportunityShare ID: ' + sr.getId());
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('OpportunityShare fields that affected this error: ' + err.getFields());
                        }
                    }
                } // End of for loop.  
                  
            } // End of if(opportunitySharingList!=null && !opportunitySharingList.isEmpty())
        
        } // End of if(opportunityIdSet!=null && !opportunityIdSet.isEmpty() && userIdSet!=null && !userIdSet.isEmpty())
           
        // All sales team members follows the opportunity record.
        if(followerList!=null && !followerList.isEmpty()){
            Database.SaveResult [] results = Database.insert(followerList, false);
            
            // Iterate through each returned result
            for (Database.SaveResult sr : results) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    entitySubscriptionIdSet.add(sr.getId());
                    System.debug('Successfully inserted Opportunity follower. EntitySubscription ID: ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('EntitySubscription fields that affected this error: ' + err.getFields());
                    }
                }
            }
            // Use case 3. Send email notification to all the newly added sales team member.
            Map<Id, EntitySubscription> entitySubscriptionMap = new Map<Id, EntitySubscription>([SELECT ID, PARENTID, SUBSCRIBERID FROM EntitySubscription WHERE ID IN:entitySubscriptionIdSet LIMIT 1000]);
            
            List<EmailTemplate>templateList = [SELECT Id, Name FROM EmailTemplate WHERE Name ='AlertNewOpportunityTeamMemberVF' LIMIT 1];
            
            List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
            
            if(templateList!=null && !templateList.isEmpty()){
            
                for(Id entityId:entitySubscriptionMap.keySet()){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(entitySubscriptionMap.get(entityId).SUBSCRIBERID);
                    mail.setSaveAsActivity(false);
                    mail.setWhatId(entitySubscriptionMap.get(entityId).PARENTID);
                    mail.setTemplateId(templateList[0].Id);
                    if(followerMap.containsKey(entitySubscriptionMap.get(entityId).PARENTID) && followerMap.get(entitySubscriptionMap.get(entityId).PARENTID).containsKey(entitySubscriptionMap.get(entityId).SUBSCRIBERID) && followerMap.get(entitySubscriptionMap.get(entityId).PARENTID).get(entitySubscriptionMap.get(entityId).SUBSCRIBERID)){
                        emailList.add(mail);              
                }   }
                
                if(emailList!=null && !emailList.isEmpty()){
                    LIST<Messaging.SendEmailResult> emailResults = Messaging.sendEmail(emailList);
                    
                    if (emailResults!=null && !emailResults.isEmpty() && !emailResults.get(0).isSuccess()) {                      
                            System.debug('Error generated when sending mail - ' + emailResults.get(0).getErrors()[0].getMessage());
                    }                           
                  
                }        
                 
            }   
        }
        
        
    } // End of handleInsert
    
    /*                
    * 2. When a team member is removed, the member should "unfollow" the opportunity record.
    * 5. Prevent Delete of Opportunity Team Member with role Opportunity Creator.
    *
    */   
    public static void handleDelete(List<OpportunityTeamMember> oldRecords){
        Map<Id,Id> opportunitySalesTeamMemberMap = new Map<Id,Id>();
        for(OpportunityTeamMember opTeam:oldRecords){
        
            if(opTeam.TeamMemberRole == System.Label.Sales_Team_Opportunity_Creator){
                opTeam.addError('You cannot delete an Opportunity Team Member with role Opportunity Creator. If you are changing the opportunity owner then retain the sales team.');
            }
            else{   
                opportunitySalesTeamMemberMap.put(opTeam.OpportunityId,opTeam.UserId);
            }
        }
        
        if(opportunitySalesTeamMemberMap!=null && !opportunitySalesTeamMemberMap.isEmpty()){
            unfollowOpportunityRecord(opportunitySalesTeamMemberMap);   
        }   
    }
    
    
    public static void unfollowOpportunityRecord(Map<Id,Id> opportunitySalesTeamMemberMap){
        
        /*
        try{
            List<EntitySubscription>entityToDelete =  [SELECT Id,PARENTID, SUBSCRIBERID FROM EntitySubscription WHERE SUBSCRIBERID =:UserId AND PARENTID =:opportunityId LIMIT 1];
            if(entityToDelete!=null && !entityToDelete.isEmpty()){
                delete entityToDelete;
            }
        }catch(Exception ex){
            System.debug('Exception caught while unfollowing team member with Id:'+UserId+ ' from Opportunity with Id:'+OpportunityId+' for:'+ex.getMessage());
        }
        */
        List<EntitySubscription>entityToDelete = new List<EntitySubscription>();
        for(EntitySubscription unfollow:[SELECT Id,PARENTID, SUBSCRIBERID FROM EntitySubscription WHERE SUBSCRIBERID IN :opportunitySalesTeamMemberMap.values() AND PARENTID IN :opportunitySalesTeamMemberMap.keySet()]){
            if(opportunitySalesTeamMemberMap.containsKey(unfollow.PARENTID) && opportunitySalesTeamMemberMap.get(unfollow.PARENTID) == unfollow.SUBSCRIBERID){
                entityToDelete.add(unfollow);
            }
        }
        
        // Now delete the unsubscribers
        if(entityToDelete!=null && !entityToDelete.isEmpty()){
            Database.DeleteResult [] results = Database.delete(entityToDelete, false);
            
            // Iterate through each returned result
            for (Database.DeleteResult sr : results) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('EntitySubscription fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
        
        
    }
}