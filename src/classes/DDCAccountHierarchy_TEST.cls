@isTest
private class DDCAccountHierarchy_TEST {

    static testMethod void testAccountHierarchy() {
        // Instanciate Page
    
        //DDCAccountHierachyTestData.createTestHierarchy(); 
        DDCAccountHierachyData_TEST.createTestHierarchy(); 

        Account topAccount = [Select id, name from account where name = 'HierarchyTest0' LIMIT 1];
        Account middleAccount = [Select id, parentID, name from account where name = 'HierarchyTest4' LIMIT 1];
        Account bottomAccount = [Select id, parentID, name from account where name = 'HierarchyTest9' LIMIT 1];
        Account[] accountList = [Select id, parentID, name from account where name like 'HierarchyTest%'];

        test.startTest();
        
        System.debug('DEBUG: AccountList size: '+accountList.size());
    
        PageReference DDCAccountHierarchyPage = Page.DDCAccountHierarchyPage;
        Test.setCurrentPage(DDCAccountHierarchyPage);
        ApexPages.currentPage().getParameters().put('id', topAccount.id);
    
        // Instanciate Controller
        DDCAccountStructure controller = new DDCAccountStructure();
        
        // Call Methodes for top account
        controller.setcurrentId(null);
        DDCAccountStructure.ObjectStructureMap[] smt1 = new DDCAccountStructure.ObjectStructureMap[]{};
        smt1 = controller.getObjectStructure();
        System.Assert(smt1.size()>0, 'Test failed at Top account, no Id');

        controller.setcurrentId(String.valueOf(topAccount.id));
        DDCAccountStructure.ObjectStructureMap[] smt2 = new DDCAccountStructure.ObjectStructureMap[]{};
        smt2 = controller.getObjectStructure();
        System.Assert(smt2.size()>0, 'Test failed at Top account, with Id: '+smt2.size());

        //Call ObjectStructureMap methodes
        smt2[0].setnodeId('1234567890');
        smt2[0].setlevelFlag(true);
        smt2[0].setlcloseFlag(false);
        smt2[0].setnodeType('parent');
        smt2[0].setcurrentNode(false);
        smt2[0].setaccount(topAccount);
        String nodeId = smt2[0].getnodeId();
        Boolean[] levelFlag = smt2[0].getlevelFlag();
        Boolean[] closeFlag = smt2[0].getcloseFlag();
        String nodeType = smt2[0].getnodeType();
        Boolean currentName = smt2[0].getcurrentNode();
        Account smbAccount = smt2[0].getaccount();


        // Call Methodes for middle account
        controller.setcurrentId(String.valueOf(middleAccount.id));
        DDCAccountStructure.ObjectStructureMap[] smm = new DDCAccountStructure.ObjectStructureMap[]{};
        smm = controller.getObjectStructure();
        System.Assert(smm.size()>0, 'Test failed at middle account');

        // Call Methodes for bottom account
        controller.setcurrentId(String.valueOf(bottomAccount.id));
        DDCAccountStructure.ObjectStructureMap[] smb = new DDCAccountStructure.ObjectStructureMap[]{};
        smb = controller.getObjectStructure();
        System.Assert(smb.size()>0, 'Test failed at top account');
        
        test.stopTest();
    }

}