@isTest
private class AccountTeamMemberFollow_TEST {

   static testMethod void test0(){
        Account acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc);      
        
        SchedulableContext sc = null;
        AccountTeamMemberFollowAccountScheduler myClass = new AccountTeamMemberFollowAccountScheduler();
        myClass.execute(sc);
    }
    
    static testMethod void test1(){
    
        Account acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc);    
        
        List < AccountTeamMember > myList = [Select id, AccountId, UserId from accountTeamMember where CreatedDate = today LIMIT 10];
        
        Database.BatchableContext bc = null;    
        AccountTeamMemberFollowAccountBatch myClass = new AccountTeamMemberFollowAccountBatch('Select id, AccountId, UserId from accountTeamMember where CreatedDate = today LIMIT 10');
        myClass.execute(bc, (List <sObject>) myList);
        myClass.finish(bc);
    }
}