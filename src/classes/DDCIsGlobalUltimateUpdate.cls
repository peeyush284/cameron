global class DDCIsGlobalUltimateUpdate implements Database.Batchable<sObject>
{
    private static String OBJECT_QUERY = 'SELECT Id, Name, IsGlobalUltimate__c, ' +
                                                'DunsNumber, DandBCompany.GlobalUltimateDunsNumber, ' + 
                                                'DandBCompany.DomesticUltimateDunsNumber, ' +
                                                'DandBCompany.ParentOrHqDunsNumber ' +
                                           'FROM Account ' +
                                          'WHERE DandBCompany.Id != null';
    public String query;
   
    global DDCIsGlobalUltimateUpdate ()
    {
        system.debug(logginglevel.INFO, 'Invoked: DDCIsGlobalUltimateUpdate');
        query = OBJECT_QUERY;
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
       system.debug(logginglevel.INFO, 'Invoked: start Query = ' + query);
       return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Account> scope)
    {
        system.debug(logginglevel.INFO, 'Invoked: execute scope.size() = ' + scope.size());
        
        List<Account> updates = new List<Account>();
        //Account acct;
        
        for (Account acct : scope)
        {
            //acct = (Account) account;
            if (acct.name == 'Test DDCIsGlobalUltimateUpdate Account') {
                system.debug(logginglevel.INFO, 'Test Account Found');
                system.debug(logginglevel.INFO, 'Before if acct.DunsNumber =' + acct.DunsNumber + ' acct.DandBCompany.GlobalUltimateDunsNumber = ' + acct.DandBCompany.GlobalUltimateDunsNumber);
            }
            if ( acct.DunsNumber != null && acct.DunsNumber.equals(acct.DandBCompany.GlobalUltimateDunsNumber) )
            {
                if (acct.name == 'Test DDCIsGlobalUltimateUpdate Account') {
                    system.debug(logginglevel.INFO, 'setting IsGlobalUltimate__c = true');
                }
                acct.IsGlobalUltimate__c = true;
                updates.add(acct);
            }
            if (acct.name == 'Test DDCIsGlobalUltimateUpdate Account') {
                system.debug(logginglevel.INFO, 'Test Account Found');
                system.debug(logginglevel.INFO, 'After if IsGlobalUltimate__c =' + acct.IsGlobalUltimate__c);
            }

        }

        update updates;
        system.debug(logginglevel.INFO, 'After update updates');
    }

    global void finish(Database.BatchableContext BC){
        system.debug(logginglevel.INFO, 'Invoked: finish');
    }

}