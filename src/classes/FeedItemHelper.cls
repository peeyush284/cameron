/**
* @author       Peeyush Awadhiya
* @date         07/09/2014
* @description  This class have method that does the following:
* 				1. If the FeedItem is deleted and there is a corresponding Chatter Post custom object record,
*				then it deletes the record as well.
* 
*/


public with sharing class FeedItemHelper {
	
	/**
    * @author       Peeyush Awadhiya
    * @date         07/09/2014
    * @description  This method passes the FeedItem Id to @future method
    * @param        List<FeedItem> feedList : This is list of all feeditems that are being deleted.
    * @return       void 
    */
	
	
	public static void deleteChatterPostWhenFeedItemIsDeleted(List<FeedItem> feedList){
		Set<Id> feedItemIdSet = new Set<Id>();
		for(FeedItem feed:feedList){
			feedItemIdSet.add(feed.Id);
		}
		// Invoke the function for asynchronous delete	
		queryAndDeleteChatterPostRecords(feedItemIdSet);
	}
	
	@future
	public static void queryAndDeleteChatterPostRecords(Set<Id>feedItemIdSet){
		delete [SELECT Id FROM IOI_Chatter_Feed__c WHERE Chatter_Feed_Id__c IN :feedItemIdSet];		
	}
}