/**
* @author       Peeyush Awadhiya
* @date         08/07/2014
* @description  This class contains test methods for the OpportunityPrepopulateEndUserPage. It tests following:
*               1. If opporuntiy is created from Project related list, populate the project, account.
*               2. If the oppotutnity is created from Account related list, populate the end user and account.
*               3. For bucket opportunity, the close date is pre-populated to the last date of current year.
*/


@isTest

public class OpportunityPrepopulateEndUserExtn_TEST{
    static testMethod void testOpptyPrepopulateEndUsrExt(){
        
        // Prepare the data stub
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc);              
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        project.CAM_Final_Destination__c = 'India';
        update project;
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('Bucket', 'RFQ', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Surface';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = project.Id;
        
        oppty.AccountId = acc.Id;
        insert oppty;
        Opportunity_Prepopulate_Fields__c hierarchialCustomSetting;
        if(Opportunity_Prepopulate_Fields__c.getOrgDefaults() == null){
            // Need to change it to org specific values in the production
            hierarchialCustomSetting = new Opportunity_Prepopulate_Fields__c(); 
        }
        else{
            hierarchialCustomSetting = Opportunity_Prepopulate_Fields__c.getOrgDefaults();
            hierarchialCustomSetting.Account__c = 'opp4';
            hierarchialCustomSetting.End_User__c = 'CF00Nc00000015cRm';
            hierarchialCustomSetting.Parent_Project__c = 'CF00Nc00000014Zz1';
            hierarchialCustomSetting.CAM_Project__c = 'CF00Nc00000013e2R';
            hierarchialCustomSetting.CAM_Final_Destination__c = '00Nc00000013kPb';
        }
        
        upsert hierarchialCustomSetting;
        
        // Set the current page and instantiate the extension
        PageReference pageRef = Page.OpportunityPrepopulateEndUserPage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('retURL', '/'+project.Id);
        
        // Query and load the updated field values
        oppty = [SELECT Id, RecordTypeId, AccountId, Account.Name FROM Opportunity WHERE Id =:oppty.Id LIMIT 1];
        
        
        // Use case 1. When the opportunity is created from Project related list
        pageRef.getParameters().put(Opportunity_Prepopulate_Fields__c.getOrgDefaults().CAM_Project__c+'_lkid', project.Id);
        OpportunityPrepopulateEndUserExtension extension = new OpportunityPrepopulateEndUserExtension(new ApexPages.StandardController(oppty));
        
        // 14-NOV-2014 CGP Delete following lines
        //String editPageURL = extension.prepopulateOpportunity().getURL();
        
        //System.assertEquals(editPageURL.contains('/' + Opportunity.SObjectType.getDescribe().getKeyPrefix() + '/e'), true);
        //System.assertEquals(editPageURL.contains('RecordType='+oppty.RecordTypeId), true);
        
        //System.assertEquals(editPageURL.contains(Opportunity_Prepopulate_Fields__c.getOrgDefaults().End_User__c+'_lkid='+acc.Id), true);
        //System.assertEquals(editPageURL.contains(Opportunity_Prepopulate_Fields__c.getOrgDefaults().End_User__c+'='+'CODE+COVERAGE+ACCOUNT'), true);
        //System.assertEquals(editPageURL.contains(Opportunity_Prepopulate_Fields__c.getOrgDefaults().CAM_Final_Destination__c+'=India'), true); 
        
        
        
        
    }
    
    static testMethod void testOpportunityCreationFromProject(){
         
        // Prepare the data stub
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc);              
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        project.CAM_Final_Destination__c = 'India';
        update project;
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('Bucket', 'RFQ', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Surface';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = project.Id;
        
        oppty.AccountId = acc.Id;
        insert oppty;
        Opportunity_Prepopulate_Fields__c hierarchialCustomSetting;
        if(Opportunity_Prepopulate_Fields__c.getOrgDefaults() == null){
            // Need to change it to org specific values in the production
            hierarchialCustomSetting = new Opportunity_Prepopulate_Fields__c(); 
        }
        else{
            hierarchialCustomSetting = Opportunity_Prepopulate_Fields__c.getOrgDefaults();
            hierarchialCustomSetting.Account__c = 'opp4';
            hierarchialCustomSetting.End_User__c = 'CF00Nc00000015cRm';
            hierarchialCustomSetting.Parent_Project__c = 'CF00Nc00000014Zz1';
            hierarchialCustomSetting.CAM_Project__c = 'CF00Nc00000013e2R';
            hierarchialCustomSetting.CAM_Final_Destination__c = '00Nc00000013kPb';
        }
        
        upsert hierarchialCustomSetting;
        
        // Set the current page and instantiate the extension
        PageReference pageRef = Page.OpportunityPrepopulateEndUserPage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('retURL', '/'+project.Id);
        
        // Query and load the updated field values
        oppty = [SELECT Id, RecordTypeId, AccountId, Account.Name FROM Opportunity WHERE Id =:oppty.Id LIMIT 1];
        
        // Use case 2. When the opportunity is created from another Parent project.
        
        pageRef.getParameters().put(Opportunity_Prepopulate_Fields__c.getOrgDefaults().Parent_Project__c+'_lkid', project.Id);
        OpportunityPrepopulateEndUserExtension extension = new OpportunityPrepopulateEndUserExtension(new ApexPages.StandardController(oppty));
        
        // 14-NOV-2014 CGP Delete following 4 lines
        //String editPageURL = extension.prepopulateOpportunity().getURL();
        
        //System.assertEquals(editPageURL.contains(Opportunity_Prepopulate_Fields__c.getOrgDefaults().End_User__c+'_lkid='+acc.Id), true);
        //System.assertEquals(editPageURL.contains(Opportunity_Prepopulate_Fields__c.getOrgDefaults().End_User__c+'='+'CODE+COVERAGE+ACCOUNT'), true);
        //System.assertEquals(editPageURL.contains(Opportunity_Prepopulate_Fields__c.getOrgDefaults().CAM_Final_Destination__c+'=India'), true); 
        
    }
    
     static testMethod void testOpportunityCreationFromEndUser(){
         
        // Prepare the data stub
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc);              
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        project.CAM_Final_Destination__c = 'India';
        update project;
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunity
        oppty = TestUtilities.generateOpportunityWithoutInsert('Bucket', 'RFQ', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Surface';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = project.Id;
        oppty.AccountId = acc.Id;
        insert oppty;
        
        Opportunity_Prepopulate_Fields__c hierarchialCustomSetting;
        if(Opportunity_Prepopulate_Fields__c.getOrgDefaults() == null){
            // Need to change it to org specific values in the production
            hierarchialCustomSetting = new Opportunity_Prepopulate_Fields__c(); 
        }
        else{
            hierarchialCustomSetting = Opportunity_Prepopulate_Fields__c.getOrgDefaults();
            hierarchialCustomSetting.Account__c = 'opp4';
            hierarchialCustomSetting.End_User__c = 'CF00Nc00000015cRm';
            hierarchialCustomSetting.Parent_Project__c = 'CF00Nc00000014Zz1';
            hierarchialCustomSetting.CAM_Project__c = 'CF00Nc00000013e2R';
            hierarchialCustomSetting.CAM_Final_Destination__c = '00Nc00000013kPb';
        }
        
        upsert hierarchialCustomSetting;
        
        // Set the current page and instantiate the extension
        PageReference pageRef = Page.OpportunityPrepopulateEndUserPage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('retURL', '/'+acc.Id);
        pageRef.getParameters().put(Opportunity_Prepopulate_Fields__c.getOrgDefaults().End_User__c+'_lkid', acc.Id);
        pageRef.getParameters().put(Opportunity_Prepopulate_Fields__c.getOrgDefaults().End_User__c, acc.Name);
        
        
        // Query and load the updated field values
        oppty = [SELECT Id, RecordTypeId, AccountId, Account.Name, End_User__c FROM Opportunity WHERE Id =:oppty.Id LIMIT 1];
        
        // Use case 3. When the opportunity is created from Account.
       
        OpportunityPrepopulateEndUserExtension extension = new OpportunityPrepopulateEndUserExtension(new ApexPages.StandardController(oppty));
        
        // 14-NOV-2014 CGP Delete following 4 lines
        //String editPageURL = extension.prepopulateOpportunity().getURL();
        
        //System.assertEquals(editPageURL.contains(Opportunity_Prepopulate_Fields__c.getOrgDefaults().End_User__c+'_lkid='+acc.Id), true);
        //System.assertEquals(editPageURL.contains(Opportunity_Prepopulate_Fields__c.getOrgDefaults().End_User__c+'='+'CODE+COVERAGE+ACCOUNT'), true);
        //System.assertEquals(editPageURL.contains(Opportunity_Prepopulate_Fields__c.getOrgDefaults().CAM_Final_Destination__c+'=India'), true); 
        
        
    }
}