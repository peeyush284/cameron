public class DDCAccountStructure {

    Account acct;

    // Good practice to controller class extensions.
    public DDCAccountStructure() {
    System.debug(LoggingLevel.INFO, 'DDCAccountStructure Constructor');
    }
    
    public DDCAccountStructure(ApexPages.StandardController stdController) {
    System.debug(LoggingLevel.INFO, 'DDCAccountStructure Constructor');
        acct = (Account)stdController.getRecord();
        setcurrentid(acct.Id);
    }
    
//  Copyright (c) 2008, Matthew Friend, Sales Engineering, Salesforce.com Inc.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
//  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//  Neither the name of the salesforce.com nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. 
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


// To adapt this to anouther Object simply search for "Change" to go to the places 
// where the sObject and query must be changed

    //Wrapper class
    public class ObjectStructureMap {

        public String nodeId;
        public Boolean[] levelFlag = new Boolean[]{};
        public Boolean[] closeFlag = new Boolean[]{};
        public String nodeType;
        public Boolean currentNode;
        
        public String region {get; set;}
        public String nodeRegion {get; set;}
        //
        // Change this to your sObject
        //
        public Account account;
        //
        
        public String getnodeId() { return nodeId; }
        public Boolean[] getlevelFlag() { return levelFlag; }
        public Boolean[] getcloseFlag() { return closeFlag; }
        public String getnodeType() { return nodeType; }
        public Boolean getcurrentNode() { return currentNode; }

        //
        // Change this to your sObject
        //
        public Account getaccount() { return account; }
        //
        
        public void setnodeId( String n ) { this.nodeId = n; }
        public void setlevelFlag( Boolean l ) { this.levelFlag.add(l); }
        public void setlcloseFlag( Boolean l ) { this.closeFlag.add(l); }
        public void setnodeType( String nt ) { this.nodeType = nt; }
        public void setcurrentNode( Boolean cn ) { this.currentNode = cn; }
        //
        // Change this to your sObject
        //
        public void setaccount( Account a ) { this.account = a; }
        //
        
        public Boolean getLowAmount()
        {
          return account.DDC_Closed_Won_Value__c <= 50000;
        }
        public Boolean getMediumAmount()
        {
          return account.DDC_Closed_Won_Value__c > 50000 && account.DDC_Closed_Won_Value__c <= 250000;
        }
        public Boolean getHighAmount()
        {
          return account.DDC_Closed_Won_Value__c > 250000;
        }

    public String getSiteDUNS()
    {
      return account.DunsNumber;
    }

    public String getSICCode()
    {
      return account.Sic;
    }

    public String getSICDesc()
    {
      return account.SicDesc;
    }

    public String getTradeStyle()
    {
      return account.Tradestyle;
    }

    public String getCleanStatus()
    {
      return account.CleanStatus;
    }

        //
        // Change the parameters to your sObject
        //
        public ObjectStructureMap(String nodeId, Boolean[] levelFlag,Boolean[] closeFlag , String nodeType, Boolean lastNode, Boolean currentNode, Account a){
            this.nodeId = nodeId;
            this.levelFlag = levelFlag; 
            this.closeFlag = closeFlag;
            this.nodeType = nodeType;
            this.currentNode = currentNode;
            //
            // Change this to your sObject  
            //
            this.account = a;
        }
    }

    // Declare variables
    // 
    public String currentId;
    public String startHere;
    public String regionFilter;
    public String salesView;
    public String creditView;
    
    public List<ObjectStructureMap> asm = new List<ObjectStructureMap>{};
    public Map<String, ObjectStructureMap> masm = new Map<String, ObjectStructureMap>{};
    public List<Integer> maxLevel = new List<Integer>{};
        
    // Allow page to set the current ID
    //
    public void setcurrentId(String cid) {
        currentId = cid;
    }

    public void setStartHere(String here) {
        startHere = here;
    }

    public void setRegionFilter(String filter) {
        regionFilter = filter;
    }

    // Return ObjectStructureMap to page
    //
    public List<ObjectStructureMap> getObjectStructure()
    {
        asm.clear();

        System.debug(LoggingLevel.DEBUG,currentId);        
        if (currentId == null) {
            currentId = System.currentPageReference().getParameters().get('id');
            if ( currentId == null )
              setcurrentid(acct.Id);
        }
        System.debug(LoggingLevel.DEBUG,currentId);
        
        startHere = System.currentPageReference().getParameters().get('startHere');
        regionFilter = System.currentPageReference().getParameters().get('regionFilter');
        salesView = System.currentPageReference().getParameters().get('salesView');
        creditView = System.currentPageReference().getParameters().get('creditView');
        
        System.assertNotEquals(currentId,null,'sObject ID must be provided');
        asm = formatObjectStructure(currentId);
        return asm;
    }

    // Query Account from top down to build the ObjectStructureMap
    //
    public ObjectStructureMap[] formatObjectStructure(String currentId){
    
        List<ObjectStructureMap> asm = new List<ObjectStructureMap>{};
        masm.clear();
        //
        // Change below
        //
        List<Account> al = new List<Account>{};
        //
        List<ID> currentParent = new List<ID>{};
        Map<ID, String> nodeList = new Map<ID, String>{};
        List<String> nodeSortList = new List<String>{};
        List<Boolean> levelFlag = new List<Boolean>{};
        List<Boolean> closeFlag = new List<Boolean>{};
        String nodeId = '0';
        String nodeType = 'child';
        Integer count = 0;
        Integer level = 0;
        Boolean endOfStructure = false;
        String region;
    Id topAccountId;
        
        // Find highest level obejct in the structure
        //
        if (startHere != null )
        {
          topAccountId = currentId;
          currentParent.add(currentId);
        } else {
            if (regionFilter == null )
              region = null;
            else 
            {
            Account a = [Select a.id, a.BillingCountry, a.ParentId From Account a where a.id = :currentId LIMIT 1];
            region = countryContinentMap().get(a.BillingCountry);
            }
      topAccountId = GetTopElement(currentId);
          currentParent.add(topAccountId);
        }
 
        // Loop though all children
        while (!endOfStructure ){

            if(level==0){
                //
                // Change below
                //        
                al = [SELECT a.DunsNumber,
                             a.BillingCountry,
                             a.Type, 
                             a.Industry,
                             a.Site, 
                             a.Sic,
                             a.SicDesc, 
                             a.Tradestyle,
                             a.CleanStatus,
                             a.DDC_Closed_Won_Value__c, 
                             a.ParentId, 
                             a.OwnerId, 
                             a.Name, 
                             a.Id 
                        FROM Account a 
                       WHERE a.id IN :CurrentParent 
                    ORDER BY a.Name];
                //
            }
            else {
                //
                // Change below
                //        
                al = [SELECT a.DunsNumber,
                             a.BillingCountry,
                             a.Type, 
                             a.Industry, 
                             a.Site, 
                             a.Sic,
                             a.SicDesc, 
                             a.Tradestyle,
                             a.CleanStatus,
                             a.DDC_Closed_Won_Value__c, 
                             a.ParentId, 
                             a.OwnerId, 
                             a.Name, 
                             a.Id 
                        FROM Account a 
                       WHERE a.ParentID IN :CurrentParent
                    ORDER BY a.Name];
                //
            }

            if(al.size() == 0){
                endOfStructure = true;
            }
            else {
                currentParent.clear();
                for (Integer i = 0 ; i < al.size(); i++)
                {
                    Account a = al[i];
                    
                    if (level > 0){
                      if ( (String.isNotBlank(region) && ! region.equals(countryContinentMap().get(a.BillingCountry))) ||
                           (String.isNotBlank(salesView) && a.DDC_Closed_Won_Value__c > 0) ||
                           (String.isNotBlank(creditView) && a.DDC_Closed_Won_Value__c == 0)  )
                      {
                        if (a.id != topAccountId)
                          currentParent.add(a.id);
                          
              continue;
                      }

            if ( NodeList.containsKey(a.ParentId))
            {
                          nodeId=NodeList.get(a.ParentId)+'.'+String.valueOf(i);
            } else {
                          nodeId=NodeList.get(topAccountId)+'.'+String.valueOf(i);
            }
                    } else {
                        nodeId=String.valueOf(i);
                    }
          
                    ObjectStructureMap osm = new ObjectStructureMap(nodeID,levelFlag,closeFlag,nodeType,false,false,a);
          osm.region = region;
          osm.nodeRegion = countryContinentMap().get(a.BillingCountry);

                    masm.put(NodeID, osm);
                    currentParent.add(a.id);
                    nodeList.put(a.id,nodeId);
                    nodeSortList.add(nodeId);
                }
                maxLevel.add(level);                
              level++;
            }
            
        }
        
        // Account structure must now be formatted
        //
        
        NodeSortList.sort();
        for (Integer i = 0; i < NodeSortList.size();i++){
            List<String> pnl = new List<String> {};
            List<String> cnl = new List<String> {};
            List<String> nnl = new List<String> {};
            
            if (i > 0){
                String pn = NodeSortList[i-1];
                pnl = pn.split('\\.',-1);
            }

            String cn = NodeSortList[i];
            cnl = cn.split('\\.',-1);

            if (i < NodeSortList.size()-1){
                String nn = NodeSortList[i+1];
                nnl = nn.split('\\.',-1);
            }
            ObjectStructureMap tasm = masm.get(cn);
            if (cnl.size() < nnl.size()){
                //Parent
                if (isLastNode(cnl)){
                    tasm.nodeType='parent_end';
                }
                else {
                    tasm.nodeType='parent';
                }
            }
            else if (cnl.size() > nnl.size()){
                tasm.nodeType='child_end';
                tasm.closeFlag=setcloseFlag(cnl, nnl, tasm.nodeType);
            }
            else {
                tasm.nodeType='child';
            }
            tasm.levelFlag = setlevelFlag(cnl, tasm.nodeType); 
            //
            // Change below
            //
            if (tasm.account.id == currentId) {
                tasm.currentNode=true;
            }
            //
            asm.add(tasm);
        }
        asm[0].nodeType='start';
        asm[asm.size()-1].nodeType='end';
        
        return asm;
    }
    
    // Determin parent elements relationship to current element
    //
    public List<Boolean> setlevelFlag(List<String> nodeElements, String nodeType){
        List<Boolean> flagList = new List<Boolean>{};
        String searchNode = '';
        String workNode = '';
        Integer cn = 0;
            for(Integer i = 0; i < nodeElements.size()-1;i++){
                cn = Integer.valueOf(nodeElements[i]);
                cn++;
                searchNode=workNode + String.valueOf(cn);
                workNode=workNode + nodeElements[i] + '.';
                if (masm.containsKey(searchNode)){
                    flagList.add(true);
                }
                else {
                    flagList.add(false);
                }
            }
        return flagList;
    }
    
    // Determin if the element is a closing element
    //
    public List<Boolean> setcloseFlag(List<String> cnl, List<String> nnl, String nodeType){
        List<Boolean> flagList = new List<Boolean>{};
        String searchNode = '';
        String workNode = '';
        Integer cn = 0;
        for(Integer i = nnl.size(); i < cnl.size();i++){
                    flagList.add(true);
        }
        return flagList;
    }

    // Determin if Element is the bottom node
    //    
    public Boolean isLastNode(List<String> nodeElements){
        String searchNode = '';
        Integer cn = 0;
        for(Integer i = 0; i < nodeElements.size();i++){
            if (i == nodeElements.size()-1){
                cn = Integer.valueOf(nodeElements[i]);
                cn++;
                searchNode=searchNode + String.valueOf(cn);
            }
            else {
                searchNode=searchNode + nodeElements[i] + '.';
            }
        }
        if (masm.containsKey(searchNode)){
            return false;
        }
        else{
            return true;
        }
    }

    // Find the top most element in Heirarchy
    //    
    public String GetTopElement(String objId) {
        Boolean top = false;
        while (!top) {
            //
            // Change below
            //
            Account a = [Select a.id, a.ParentId From Account a where a.id = :objId LIMIT 1];
            //
            
            if (a.ParentID != null) {
                objId = a.ParentID;
            }
            else {
                top=true;
            }
        }
        return objId ;
    }

  private Map<String, String> continentMap()
  {
    return new Map<String, String>
    {
      'Africa'=>'AF',
      'Asia'=>'AS',
      'Europe'=>'EU',
      'North America'=>'NA',
      'South America'=>'SA',
      'Oceania'=>'OC',
      'Antarctica'=>'AN'
    };
  }
  
  private Map<String, String> countryContinentMap()
  {
    return new Map<String, String>
    {
      'Algeria'=>'Africa',
      'Angola'=>'Africa',
      'Benin'=>'Africa',
      'Botswana'=>'Africa',
      'Burkina'=>'Africa',
      'Burundi'=>'Africa',
      'Cameroon'=>'Africa',
      'Cape Verde'=>'Africa',
      'Central African Republic'=>'Africa',
      'Chad'=>'Africa',
      'Comoros'=>'Africa',
      'Congo'=>'Africa',
      'Congo, Democratic Republic of'=>'Africa',
      'Djibouti'=>'Africa',
      'Egypt'=>'Africa',
      'Equatorial Guinea'=>'Africa',
      'Eritrea'=>'Africa',
      'Ethiopia'=>'Africa',
      'Gabon'=>'Africa',
      'Gambia'=>'Africa',
      'Ghana'=>'Africa',
      'Guinea'=>'Africa',
      'Guinea-Bissau'=>'Africa',
      'Ivory Coast'=>'Africa',
      'Kenya'=>'Africa',
      'Lesotho'=>'Africa',
      'Liberia'=>'Africa',
      'Libya'=>'Africa',
      'Madagascar'=>'Africa',
      'Malawi'=>'Africa',
      'Mali'=>'Africa',
      'Mauritania'=>'Africa',
      'Mauritius'=>'Africa',
      'Morocco'=>'Africa',
      'Mozambique'=>'Africa',
      'Namibia'=>'Africa',
      'Niger'=>'Africa',
      'Nigeria'=>'Africa',
      'Rwanda'=>'Africa',
      'Sao Tome and Principe'=>'Africa',
      'Senegal'=>'Africa',
      'Seychelles'=>'Africa',
      'Sierra Leone'=>'Africa',
      'Somalia'=>'Africa',
      'South Africa'=>'Africa',
      'South Sudan'=>'Africa',
      'Sudan'=>'Africa',
      'Swaziland'=>'Africa',
      'Tanzania'=>'Africa',
      'Togo'=>'Africa',
      'Tunisia'=>'Africa',
      'Uganda'=>'Africa',
      'Zambia'=>'Africa',
      'Zimbabwe'=>'Africa',
      'Afghanistan'=>'Asia',
      'Bahrain'=>'Asia',
      'Bangladesh'=>'Asia',
      'Bhutan'=>'Asia',
      'Brunei'=>'Asia',
      'Burma (Myanmar)'=>'Asia',
      'Cambodia'=>'Asia',
      'China'=>'Asia',
      'East Timor'=>'Asia',
      'India'=>'Asia',
      'Indonesia'=>'Asia',
      'Iran'=>'Asia',
      'Iraq'=>'Asia',
      'Israel'=>'Asia',
      'Japan'=>'Asia',
      'Jordan'=>'Asia',
      'Kazakhstan'=>'Asia',
      'Korea, North'=>'Asia',
      'Korea, South'=>'Asia',
      'Kuwait'=>'Asia',
      'Kyrgyzstan'=>'Asia',
      'Laos'=>'Asia',
      'Lebanon'=>'Asia',
      'Malaysia'=>'Asia',
      'Maldives'=>'Asia',
      'Mongolia'=>'Asia',
      'Nepal'=>'Asia',
      'Oman'=>'Asia',
      'Pakistan'=>'Asia',
      'Philippines'=>'Asia',
      'Qatar'=>'Asia',
      'Russian Federation'=>'Asia',
      'Saudi Arabia'=>'Asia',
      'Singapore'=>'Asia',
      'Sri Lanka'=>'Asia',
      'Syria'=>'Asia',
      'Tajikistan'=>'Asia',
      'Thailand'=>'Asia',
      'Turkey'=>'Asia',
      'Turkmenistan'=>'Asia',
      'United Arab Emirates'=>'Asia',
      'Uzbekistan'=>'Asia',
      'Vietnam'=>'Asia',
      'Yemen'=>'Asia',
      'Albania'=>'Europe',
      'Andorra'=>'Europe',
      'Armenia'=>'Europe',
      'Austria'=>'Europe',
      'Azerbaijan'=>'Europe',
      'Belarus'=>'Europe',
      'Belgium'=>'Europe',
      'Bosnia and Herzegovina'=>'Europe',
      'Bulgaria'=>'Europe',
      'Croatia'=>'Europe',
      'Cyprus'=>'Europe',
      'Czech Republic'=>'Europe',
      'Denmark'=>'Europe',
      'Estonia'=>'Europe',
      'Finland'=>'Europe',
      'France'=>'Europe',
      'Georgia'=>'Europe',
      'Germany'=>'Europe',
      'Greece'=>'Europe',
      'Hungary'=>'Europe',
      'Iceland'=>'Europe',
      'Ireland'=>'Europe',
      'Italy'=>'Europe',
      'Latvia'=>'Europe',
      'Liechtenstein'=>'Europe',
      'Lithuania'=>'Europe',
      'Luxembourg'=>'Europe',
      'Macedonia'=>'Europe',
      'Malta'=>'Europe',
      'Moldova'=>'Europe',
      'Monaco'=>'Europe',
      'Montenegro'=>'Europe',
      'Netherlands'=>'Europe',
      'Norway'=>'Europe',
      'Poland'=>'Europe',
      'Portugal'=>'Europe',
      'Romania'=>'Europe',
      'San Marino'=>'Europe',
      'Serbia'=>'Europe',
      'Slovakia'=>'Europe',
      'Slovenia'=>'Europe',
      'Spain'=>'Europe',
      'Sweden'=>'Europe',
      'Switzerland'=>'Europe',
      'Ukraine'=>'Europe',
      'United Kingdom'=>'Europe',
      'Vatican City'=>'Europe',
      'Antigua and Barbuda'=>'North America',
      'Bahamas'=>'North America',
      'Barbados'=>'North America',
      'Belize'=>'North America',
      'Bermuda'=>'North America',
      'Canada'=>'North America',
      'Costa Rica'=>'North America',
      'Cuba'=>'North America',
      'Dominica'=>'North America',
      'Dominican Republic'=>'North America',
      'El Salvador'=>'North America',
      'Grenada'=>'North America',
      'Guatemala'=>'North America',
      'Haiti'=>'North America',
      'Honduras'=>'North America',
      'Jamaica'=>'North America',
      'Mexico'=>'North America',
      'Nicaragua'=>'North America',
      'Panama'=>'North America',
      'Saint Kitts and Nevis'=>'North America',
      'Saint Lucia'=>'North America',
      'Saint Vincent and the Grenadines'=>'North America',
      'Trinidad and Tobago'=>'North America',
      'United States'=>'North America',
      'Australia'=>'Oceania',
      'Fiji'=>'Oceania',
      'Kiribati'=>'Oceania',
      'Marshall Islands'=>'Oceania',
      'Micronesia'=>'Oceania',
      'Nauru'=>'Oceania',
      'New Zealand'=>'Oceania',
      'Palau'=>'Oceania',
      'Papua New Guinea'=>'Oceania',
      'Samoa'=>'Oceania',
      'Solomon Islands'=>'Oceania',
      'Tonga'=>'Oceania',
      'Tuvalu'=>'Oceania',
      'Vanuatu'=>'Oceania',
      'Argentina'=>'South America',
      'Bolivia'=>'South America',
      'Brazil'=>'South America',
      'Chile'=>'South America',
      'Colombia'=>'South America',
      'Ecuador'=>'South America',
      'Guyana'=>'South America',
      'Paraguay'=>'South America',
      'Peru'=>'South America',
      'Suriname'=>'South America',
      'Uruguay'=>'South America',
      'Venezuela'=>'South America'
    };
  }
}