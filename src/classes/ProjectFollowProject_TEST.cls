@isTest (SeeAllData=true)
public class ProjectFollowProject_TEST {
    
    static testMethod void testProjectStatusPermanent() {   
        String atmRole = 'Account Manager';        
        String projRecordType = 'External';
        String projStatus = 'Permanent';                       
        Account a = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember(atmRole, a);  
        CAM_Project__c p = TestUtilities.generateProject(projRecordType, projStatus, a);

        string result = ProjectFollowProject.addFollowers(p.Id,a.Id);

        //ProjectFollowProject.addFollowers(p.Id,a.Id);
        system.debug('result '+ result );                                                    
        system.AssertEquals('1 follower(s) inserted!',result);                  
   }

    static testMethod void testAcctNoTeamMembers() {                                
        String projRecordType = 'External';
        // cpg 04-Nov-2014 changed project status to permanent from in progress
        //String projStatus = 'In Progress';                       
        String projStatus = 'Permanent';                       
        Account a = TestUtilities.generateAccount();
        //AccountTeamMember atm = TestUtilities.generateAccountTeamMember(atmRole, a);  
        CAM_Project__c p = TestUtilities.generateProject(projRecordType, projStatus, a);

        string result = ProjectFollowProject.addFollowers(p.Id,a.Id);
        system.debug('result '+ result );                                                    
        //system.AssertEquals('0 follower(s) inserted!',result);                  
   }
   
}