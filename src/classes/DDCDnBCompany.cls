public with sharing class DDCDnBCompany {
  
  public static String getTokenValue(String pToken, String pJsonStr)
  {
        // Parse JSON response to get the requested token value.
        // This method expect everything to be a string in the json
        // object.     
        JSONParser parser = JSON.createParser(pJsonStr);

        while (parser.nextToken() != null) 
        {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
                (parser.getText() == pToken)) 
            {
                // Get the value. 
                parser.nextToken();
                return parser.getText();
            }
        }
        return null;
  }
  
  public static String getSiteDUNS(String pJsonStr)
  {
    return getTokenValue('SiteDUNS', pJsonStr);
  }
  
  public static String getGlobalUltimateDUNS(String pJsonStr)
  {
    return getTokenValue('GUDN', pJsonStr);
  }
  
  public static String getDomesticUltimateDUNS(String pJsonStr)
  {
    return getTokenValue('DUDN', pJsonStr);
  }
  
  public static String getHQParentDUNS(String pJsonStr)
  {
    return getTokenValue('HPDN', pJsonStr);
  }
  
  public static String getSICCode(String pJsonStr)
  {
    return getTokenValue('SICCode', pJsonStr);
  }
  
  public static String getSICDesc(String pJsonStr)
  {
    return getTokenValue('SICDesc', pJsonStr);
  }
  
  public static String getTradeStyle(String pJsonStr)
  {
    return getTokenValue('TradeStyle', pJsonStr);
  }
  
  public static String jsonStr = 
          '{"DnBCompany":[' +
          '{"GUDN":"000000000","DUDN":"000000000","HPDN":"000000000",' +
           '"SiteDUNS":"000000000","CreditRisk":"Low"}' +
          ']}';

  public static DnBCompany parseJSONString(String pJsonStr) {
  
      // Parse entire JSON response. 
      
      JSONParser parser = JSON.createParser(pJsonStr);
      while (parser.nextToken() != null) {
          // Start at the array of invoices. 
      
          if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
              while (parser.nextToken() != null) {
                  // Advance to the start object marker to 
      
                  //  find next invoice statement object. 
      
                  if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                      // Read entire invoice object, including its array of line items. 
      
                      DnBCompany cmpny = (DnBCompany)parser.readValueAs(DnBCompany.class);
                      system.debug('Global Ultimate: ' + cmpny.GUDN);
                      system.debug('DUDN: ' + cmpny.DUDN);
                      system.debug('HPDN: ' + cmpny.HPDN);
                      system.debug('SiteDUNS: ' + cmpny.SiteDUNS);
                      system.debug('CreditRisk: ' + cmpny.CreditRisk);
                      system.debug('SICCode: ' + cmpny.SICCode);
                      system.debug('SICDesc: ' + cmpny.SICDesc);                      
                      system.debug('TradeStyle: ' + cmpny.TradeStyle);                                            
      
                      String s = JSON.serialize(cmpny);
                      system.debug('Serialized company: ' + s);
  
                      // Skip the child start array and start object markers. 
      
                      parser.skipChildren();
                      return cmpny;
                  }
              }
          }
      }
      return null;
  } 
  
  // Inner classes used for serialization by readValuesAs().  
        
  public class DnBCompany 
  {
      public String GUDN {get; set;}
      public String DUDN {get; set;}
      public String HPDN {get; set;}
      public String SiteDUNS {get; set;}
      public String CreditRisk {get; set;}
      public String SICCode {get; set;}
      public String SICDesc {get; set;}
      public String TradeStyle {get; set;}
  }  
  
}