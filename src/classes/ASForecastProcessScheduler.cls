/**
* @author       Gabor Bay
* @date         09/25/2014
* @description  Scheduler class for the batch class that does the following:
*               1. Delete All from AS Bucket Oppty Object and AS Forecast Oppty Object
*               2. Insert Oppty Schedule Records into AS Bucket Oppty Object.
*               3. Insert AS Bucket Oppty Object Records into AS Forecast Oppty Object.
*               4. Insert Standard Oppty Records into AS Forecast Oppty Object.
*/

global class ASForecastProcessScheduler implements Schedulable {
    
    global void execute(SchedulableContext sch) {
    
        String queryStr = 'SELECT Id FROM Analytic_Snapshot_Bucket_Oppty__c WHERE Name != null';
        ASDeleteASFcstBucketOpptyBatch batchObj = new ASDeleteASFcstBucketOpptyBatch(queryStr);
        ID batchProcessId = Database.executeBatch(batchObj, 100);  
        
        System.debug('Executing the batch with process Id:'+batchProcessId);
    }
}