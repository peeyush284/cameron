/**
* @author       Peeyush Awadhiya
* @date         09/25/2014
* @description  This batch class does the following:
*               1. Deletes records from the "Analytic Snapshot: Forecast Oppty" Object
*               2. Upon completion, it invokes the batch which copies the OpportunityLineItemSchedule records into Analytic_Snapshot_Bucket_Oppty__c records.
*/

global class ASDeleteASFcstOpptyBatch implements Database.batchable<sObject>{
    string query;
    global ASDeleteASFcstOpptyBatch(String queryStr){
        this.query = queryStr;
    }  
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){       
        List<Analytic_Snapshot_Forecast_Oppty__c> asOpportunityToDeleteList = (List< Analytic_Snapshot_Forecast_Oppty__c>) scope;

        // Now delete records from Analytics Snapshot
        if(asOpportunityToDeleteList!=null && !asOpportunityToDeleteList.isEmpty()){
            Database.DeleteResult [] results = Database.delete(asOpportunityToDeleteList, false);
            
            // Iterate through each returned result
            for (Database.DeleteResult sr : results) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('asOpportunity fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }  
    }    
    global void finish(Database.BatchableContext BC){
        // Upon completion, invoke the batch which copies the OpportunityLineItemSchedule records into Analytic_Snapshot_Bucket_Oppty__c records.
        String querySelectStr = 'SELECT Id, OpportunityLineItem.Opportunity.RecordType.Name, OpportunityLineItem.Opportunity.CAM_Business_Unit__c, OpportunityLineItem.Opportunity.CAM_Division__c, OpportunityLineItem.Opportunity.Id, OpportunityLineItem.Opportunity.Name,OpportunityLineItem.Opportunity.Owner_Name_calc__c, Revenue, ScheduleDate, OpportunityLineItem.Opportunity.StageName'; 
        String queryWhereStr = 'WHERE ScheduleDate = THIS_YEAR AND OpportunityLineItem.Opportunity.StageName != \'Closed Cancelled\'';
        String queryStr = querySelectStr+' FROM OpportunityLineItemSchedule '+queryWhereStr;
        
        ASInsrtBcktOpptyIntoASFcstBcktOpptyBatch batchObj = new ASInsrtBcktOpptyIntoASFcstBcktOpptyBatch(queryStr);
        ID batchProcessId = Database.executeBatch(batchObj, 100);  
        
        System.debug('Executing the ASInsrtBcktOpptyIntoASFcstBcktOpptyBatch with process Id:'+batchProcessId);
    }
}