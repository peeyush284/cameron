/**
* @author       Peeyush Awadhiya
* @date         08/14/2014
* @description  This class have test methods that asserts the following:
*               1. If the FeedItem is deleted and there is a corresponding Chatter Post custom object record,
*               then it deletes the record as well.
* 
*/

@isTest

public class FeedItemHelper_TEST{
    
    public static testMethod void testFeedItemHelper(){
         // Prepare the data stub
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('Bucket', 'RFQ', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Surface';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = project.Id;
        
        oppty.AccountId = acc.Id;
        insert oppty;
        
         
        IOI__c ioiRecord = new IOI__c();
        ioiRecord = TestUtilities.generateIOI('Test IOI',acc.Id, oppty.Id, project.Id,'Submitted');
        ioiRecord.Period__c =  period.Id;
        insert ioiRecord;
        
        List<FeedItem> feedItemList = new List<FeedItem>();
        FeedItem accountFeed = TestUtilities.generateFeedItem('Test Feed 1', acc.Id);
        FeedItem opptyFeed = TestUtilities.generateFeedItem('Test Feed 2', oppty.Id);
        FeedItem projectFeed = TestUtilities.generateFeedItem('Test Feed 3', project.Id);
        
        feedItemList=new List<FeedItem>{accountFeed, opptyFeed, projectFeed};
        insert feedItemList;
        
        List<IOI_Chatter_Feed__c> ioiChatterFeedList = new List<IOI_Chatter_Feed__c>();
        
        IOI_Chatter_Feed__c ioiAccountChatterFeed = TestUtilities.generateChatterPostRecord('Account',accountFeed.Id);
        IOI_Chatter_Feed__c ioiOpptyChatterFeed = TestUtilities.generateChatterPostRecord('Opportunity',opptyFeed.Id);
        IOI_Chatter_Feed__c ioiProjectChatterFeed = TestUtilities.generateChatterPostRecord('Project',projectFeed.Id);
        
        ioiChatterFeedList = new List<IOI_Chatter_Feed__c>{ioiAccountChatterFeed,ioiOpptyChatterFeed , ioiProjectChatterFeed};
        insert ioiChatterFeedList;
        
        Test.startTest();
        
            FeedItemHelper.deleteChatterPostWhenFeedItemIsDeleted(feedItemList);
        
        Test.stopTest();
        
        System.assertEquals(0, [SELECT id FROM IOI_Chatter_Feed__c].size());
    }
}