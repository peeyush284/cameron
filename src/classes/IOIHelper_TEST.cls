/**
* @author       Peeyush Awadhiya
* @date         09/03/2014
* @description  Class with test methods for IOIHelper class.
* 
*/

@isTest
public class IOIHelper_TEST{

    public static testMethod void testIOIHelper(){
        // Prepare the data stub
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('Bucket', 'RFQ', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Surface';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = project.Id;
        
        oppty.AccountId = acc.Id;
        insert oppty;
        
        List<IOI__c> ioiRecordList = new List<IOI__c>();
        IOI__c ioiRecord = new IOI__c();
        ioiRecord = TestUtilities.generateIOI('Test IOI',acc.Id, oppty.Id, project.Id,'Submitted');
        ioiRecord.Period__c =  period.Id;
        ioiRecord.IOI_Summary__c = true;
        ioiRecord.Status__c = 'In Progress';
        ioiRecordList.add(ioiRecord);
         
        IOI__c ioiRecord2 = new IOI__c();
        ioiRecord2 = TestUtilities.generateIOI('Second IOI record',acc.Id, oppty.Id, project.Id,'Submitted');
        ioiRecord2.Period__c =  period.Id;
        ioiRecord2.Status__c = 'In Progress';
        ioiRecordList.add(ioiRecord2);
        
        IOI__c ioiRecord3 = new IOI__c();
        ioiRecord3  = TestUtilities.generateIOI('Test IOI',acc.Id, oppty.Id, project.Id,'Submitted');
        ioiRecord3.Period__c =  period.Id;
        ioiRecord3.Opportunity__c = null;
        ioiRecord3.IOI_Summary__c = true;
        ioiRecord3.Status__c = 'In Progress';
        ioiRecordList.add(ioiRecord3);
        
        IOI__c ioiRecord4 = new IOI__c();
        ioiRecord4 = TestUtilities.generateIOI('Test IOI',acc.Id, oppty.Id, project.Id,'Submitted');
        ioiRecord4.Period__c =  period.Id;
        ioiRecord4.Account__c = null;
        ioiRecord4.IOI_Summary__c = false;
        ioiRecord4.Status__c = 'In Progress';
        ioiRecordList.add(ioiRecord4);
        
        IOI__c ioiRecord5 = new IOI__c();
        ioiRecord5 = TestUtilities.generateIOI('Test IOI',acc.Id, oppty.Id, project.Id,'Submitted');
        ioiRecord5.Period__c =  period.Id;
        ioiRecord5.project__c = null;
        ioiRecord5.IOI_Summary__c = true;
        ioiRecord5.Status__c = 'In Progress';
        ioiRecordList.add(ioiRecord5);
        
        insert ioiRecordList;
        
        List <User> runningUser = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND Id!=:UserInfo.getUserId() AND isActive = true LIMIT 1];
        
        if(runningUser!=null && !runningUser.isEmpty()){
            
            for(IOI__c record:ioiRecordList){
                record.OwnerId = runningUser[0].Id;
                record.Status__c = 'Submitted';
                record.Name = record.Name+ ' - Updated';
                record.Description__c = 'Updated Description';
            }
        }
        System.debug('After update of IOI record list:'+ioiRecordList);
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        
        System.runAs(thisUser){
            Test.startTest();
                update ioiRecordList;
            Test.stopTest();
        }
        // Now 3 IOI_Field_Tracking__c records should have been created
        
        System.assertEquals([SELECT Id FROM IOI_Field_Tracking__c].size(),20);
    }
}