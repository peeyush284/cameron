/**
* @author       Peeyush Awadhiya
* @date         08/14/2014
* @description  This class have test methods that asserts the following:
*               1. Selecting IOI records opens up new visualforce page with IOI summary populated with details from selected IOI
* 
*/
@isTest
public class IOICreateSummaryExtension_TEST{
    
        
    public static testMethod void testIOICreateSummaryExtension(){        
         // Prepare the data stub
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('Bucket', 'RFQ', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Surface';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = project.Id;
        
        oppty.AccountId = acc.Id;
        insert oppty;
        
        List<IOI__c> ioiRecordList = new List<IOI__c>();
        IOI__c ioiRecord = new IOI__c();
        ioiRecord = TestUtilities.generateIOI('Test IOI',acc.Id, oppty.Id, project.Id,'Submitted');
        ioiRecord.Period__c =  period.Id;
        ioiRecord.IOI_Summary__c = true;
        ioiRecordList.add(ioiRecord);
         
        IOI__c ioiRecord2 = new IOI__c();
        ioiRecord2 = TestUtilities.generateIOI('Second IOI record',acc.Id, oppty.Id, project.Id,'Submitted');
        ioiRecord2.Period__c =  period.Id;
        ioiRecordList.add(ioiRecord2);
        
        IOI__c ioiRecord3 = new IOI__c();
        ioiRecord3  = TestUtilities.generateIOI('Test IOI',acc.Id, oppty.Id, project.Id,'Submitted');
        ioiRecord3.Period__c =  period.Id;
        ioiRecord3.Opportunity__c = null;
        ioiRecord3.IOI_Summary__c = true;
        ioiRecordList.add(ioiRecord3);
        
        IOI__c ioiRecord4 = new IOI__c();
        ioiRecord4 = TestUtilities.generateIOI('Test IOI',acc.Id, oppty.Id, project.Id,'Submitted');
        ioiRecord4.Period__c =  period.Id;
        ioiRecord4.Account__c = null;
        ioiRecord4.IOI_Summary__c = false;
        ioiRecordList.add(ioiRecord4);
        
        IOI__c ioiRecord5 = new IOI__c();
        ioiRecord5 = TestUtilities.generateIOI('Test IOI',acc.Id, oppty.Id, project.Id,'Submitted');
        ioiRecord5.Period__c =  period.Id;
        ioiRecord5.project__c = null;
        ioiRecord5.IOI_Summary__c = true;
        ioiRecordList.add(ioiRecord5);
        
        insert ioiRecordList;
        
        Set<Id> selectedRecordIds = new Set<Id>{ioiRecord.Id,ioiRecord2.Id};
        
        // Now instantiate the StandardController and start testing
        PageReference pageRef = Page.IOICreateSummaryPage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('ids',String.valueOf(ioiRecord.Id+','+ioiRecord2.Id));
        IOICreateSummaryExtension extension = new IOICreateSummaryExtension(new ApexPages.StandardController(ioiRecord));
        
        System.assertEquals(pageRef.getParameters().get('ids').length()>0,true);
        
        String nextPage = extension.cancelAndGoBack().getUrl();
        
        //System.assert the cancel action
        System.assertEquals(nextPage, String.valueOf('/' + IOI__c.SObjectType.getDescribe().getKeyPrefix() + '/o'));
        
        
    }
    
    public static testMethod void testIOICreateSummary(){
         // Prepare the data stub
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('Bucket', 'RFQ', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Surface';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = project.Id;
        
        oppty.AccountId = acc.Id;
        insert oppty;
        
        List<IOI__c> ioiRecordList = new List<IOI__c>();
        IOI__c ioiRecord = new IOI__c();
        ioiRecord = TestUtilities.generateIOI('Test IOI',acc.Id, oppty.Id, project.Id,'Submitted');
        ioiRecord.Period__c =  period.Id;
        ioiRecord.IOI_Summary__c = true;
        ioiRecordList.add(ioiRecord);
         
        IOI__c ioiRecord2 = new IOI__c();
        ioiRecord2 = TestUtilities.generateIOI('Second IOI record',acc.Id, oppty.Id, project.Id,'Submitted');
        ioiRecord2.Period__c =  period.Id;
        ioiRecordList.add(ioiRecord2);
        
        IOI__c ioiRecord3 = new IOI__c();
        ioiRecord3  = TestUtilities.generateIOI('Test IOI',acc.Id, oppty.Id, project.Id,'Submitted');
        ioiRecord3.Period__c =  period.Id;
        ioiRecord3.Opportunity__c = null;
        ioiRecord3.IOI_Summary__c = true;
        ioiRecordList.add(ioiRecord3);
        
        IOI__c ioiRecord4 = new IOI__c();
        ioiRecord4 = TestUtilities.generateIOI('Test IOI',acc.Id, oppty.Id, project.Id,'Submitted');
        ioiRecord4.Period__c =  period.Id;
        ioiRecord4.Account__c = null;
        ioiRecord4.IOI_Summary__c = false;
        ioiRecordList.add(ioiRecord4);
        
        IOI__c ioiRecord5 = new IOI__c();
        ioiRecord5 = TestUtilities.generateIOI('Test IOI',acc.Id, oppty.Id, project.Id,'Submitted');
        ioiRecord5.Period__c =  period.Id;
        ioiRecord5.project__c = null;
        ioiRecord5.IOI_Summary__c = true;
        ioiRecordList.add(ioiRecord5);
        
        insert ioiRecordList;
        
        Set<Id> selectedRecordIds = new Set<Id>{ioiRecord.Id,ioiRecord2.Id};
        
        IOI_Prepopulate_Fields__c hierarchialCustomSetting;
        if(IOI_Prepopulate_Fields__c.getOrgDefaults() == null){
            // Need to change it to org specific values in the production
            hierarchialCustomSetting = new IOI_Prepopulate_Fields__c();
            
        }
        else{
            hierarchialCustomSetting = IOI_Prepopulate_Fields__c.getOrgDefaults();
            hierarchialCustomSetting.Description__c = '00Nc00000014OTT'; 
            hierarchialCustomSetting.IOI_Summary__c = '00Nc00000015j0n';  
        }
        
        upsert hierarchialCustomSetting;
        System.debug('Custom Setting Data:::'+IOI_Prepopulate_Fields__c.getOrgDefaults());
        
        // Now instantiate the StandardController and start testing
        PageReference pageRef = Page.IOICreateSummaryPage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('ids',String.valueOf(ioiRecord.Id+','+ioiRecord2.Id));
        IOICreateSummaryExtension extension = new IOICreateSummaryExtension(new ApexPages.StandardController(ioiRecord));
        
        System.assertEquals(pageRef.getParameters().get('ids').length()>0,true);
        
        // Now invoke the redirectToEditPage() method
        String editPage = extension.redirectToEditPage().getUrl();
        
        System.assertEquals(editPage.startsWith(String.valueOf('/' + IOI__c.SObjectType.getDescribe().getKeyPrefix() + '/e?')), true);
        /*
        System.assertEquals(editPage.contains(IOI_Prepopulate_Fields__c.getOrgDefaults().Description__c+'='+extension.generateIOISummary(selectedRecordIds)), true);
        System.assertEquals(editPage.contains(IOI_Prepopulate_Fields__c.getOrgDefaults().IOI_Summary__c+'=1'), true);
        System.assertEquals(editPage.contains('nooverride=1'),true);
        */
    
    }
}