global class  AccountTeamAssignmentClass {

/* utility to kill the account team members
for (accountteammember a : [select id from accountteammember]){
    AccountTeamMember delRecord = new AccountTeamMember();
    delRecord.id = a.id;
    delete delRecord;
}
*/

    @future
    public static void AccountTeamAssignment(List<ID> recordIds) {  
        ImmedAccountTeamAssignment(recordIds);
    }
    
    public static void ImmedAccountTeamAssignment(List<ID> recordIds) {  
        //list to hold new account teams
        list<AccountTeamMember> acctMembers = new list<AccountTeamMember>();
        list<Account> AccountsToResetForce = new list<Account>();
        integer loopIterator = 0;
        integer listIdx = 0;
        boolean SkipDistrict = false;
        
        List<id> AssignedIDAdds = new list<id>();
        List<String> AssignedIDRoleAdds = new list<String>();
        
        // map with ID and Account for account data

        // is it possible to get more than one account and have account teams assigned all at once?
        list<Account> accts = [Select a.GeoCriteria__c, a.AssignableRoles__c, a.Direct_Assignment_Criteria__c, a.Force_Account_Team_Assignments__c,
                                    a.CountryCode__c, a.StateCode__c, a.City__c, a.PostalCode__c,
                                    a.BillingCountryCode, a.BillingStateCode, a.BillingCity, a.BillingPostalCode
                                From Account a 
                                where id in :recordIds];
                                
        system.debug(logginglevel.debug,'AccountTeamAssignment accts.size() = ' + accts.size());                        
        
        //create list of roles in string (not in multiselect picklist)
        set<String> UniqueRoles = new set<String>();
        list<String> ParsedRoles = new list<string>();
        for (Account a: accts) {
            if (a.AssignableRoles__c != null) {
                ParsedRoles = a.AssignableRoles__c.split(';');
                UniqueRoles.addall(ParsedRoles);
            }
        }
        
        // get only users with one of the valid roles                           
        list<user> users = [Select Id
                                From user  
                                where role__c = :UniqueRoles and isactive = true];
                                
        system.debug(logginglevel.debug,'AccountTeamAssignment users.size() = ' + users.size());                                                                       
        
        for (Account a: accts) {
            system.debug(logginglevel.debug,'AccountTeamAssignment a.assignableRoles__c = ' + a.assignableRoles__c + ' a.GeoCriteria__c = ' + a.GeoCriteria__c);

            list<District__c> dists = [Select d.Name, d.Id, d.DistrictAssignment__c, d.AssignmentAttribute__c, d.CountryCode__c, d.StateCode__c, d.City__c, d.PostalCode__c, d.Direct_Assignment_Criteria__c, d.user__r.role__c, d.user__r.name
                From District__c d 
                where  
                (
                   (
                       //(d.Direct_Assignment_Criteria__c = null and d.Direct_Assignment_Criteria__c = :a.Direct_Assignment_Criteria__c
                       //)                                  
                       //and 
                       (
                          // Check Full Geographic criteria (Country State City PostalCode)
                          ( d.CountryCode__c = :a.CountryCode__c 
                              and d.StateCode__c = :a.StateCode__c     
                              and d.City__c = :a.City__c 
                              and d.PostalCode__c = :a.PostalCode__c )
                          // Check Partial Geographic criteria (Country State City)
                           or ( d.CountryCode__c = :a.CountryCode__c 
                               and d.StateCode__c = :a.StateCode__c 
                               and d.City__c = :a.City__c 
                               and d.postalcode__c = null)
                              // Check Partial Geographic criteria (Country State)
                           or ( d.CountryCode__c = :a.CountryCode__c 
                               and d.StateCode__c = :a.StateCode__c 
                               and d.City__c = null 
                               and d.postalcode__c = null)
                              // Check Partial Geographic criteria (Country)
                           or ( d.CountryCode__c = :a.CountryCode__c 
                               and d.StateCode__c = null 
                               and d.City__c = null 
                               and d.postalcode__c = null))
                   )
               ) and user__r.id in :users];           

            // sort districts in asc order                        
            dists.sort();   
            
            // create districts list sorted in desc order, so the most 'complete records' are evaluated first
            List<District__c> distsDesc = new List<District__c>();
            for (integer idx = dists.size()-1; idx >=0; idx--) {
                distsDesc.add(dists[idx]);
            }
                                            
            // make a pass for assigning accounts using direct assignments
            for (District__c d: distsDesc) {
                system.debug(logginglevel.debug,'AccountTeamAssignment d.user__r.role__c = ' + d.user__r.role__c);        
        
                if (a.assignableRoles__c.contains(d.user__r.role__c) 
                    && d.Direct_Assignment_Criteria__c != null 
                    && a.Direct_Assignment_Criteria__c != null
                       && d.Direct_Assignment_Criteria__c == a.Direct_Assignment_Criteria__c)
                {
                    system.debug(logginglevel.debug,'AccountTeamAssignment Found -> d.user__r.role__c = ' + d.user__r.role__c);
    
                    //add accountTeamMember
                    AccountTeamMember addRecord = new AccountTeamMember();
                    addRecord.AccountId = a.id;
                    addRecord.TeamMemberRole = d.user__r.role__c;
                    addRecord.UserId = d.user__r.id;
                    acctMembers.add(addRecord);
                    
                    AssignedIDAdds.add(a.id);
                    AssignedIDRoleAdds.add(d.user__r.role__c);
                }                
                else
                {
                    system.debug(logginglevel.debug,'Pass one AccountTeamAssignment Not Found -> d.user__r.role__c = ' + d.user__r.role__c);
                }
            }
            // make a final pass for assigning accounts using only geographic assignments
            for (District__c d: distsDesc) {
                system.debug(logginglevel.debug,'AccountTeamAssignment d.user__r.role__c = ' + d.user__r.role__c);        
        
                if (a.assignableRoles__c.contains(d.user__r.role__c) &&
                       (d.Direct_Assignment_Criteria__c == null))
                {
                    //check to ensure this account has not already had this role added in previous loop
                    
                    SkipDistrict = false;
                    for (integer idx = 0; idx < AssignedIDAdds.size(); idx++)
                    {
                        if (AssignedIDAdds.get(idx) == a.id 
                            && AssignedIDRoleAdds.get(idx) == d.user__r.role__c)
                        {
                            SkipDistrict = true;
                        }
                    }
                    
                
                    if (SkipDistrict == false){
                        system.debug(logginglevel.debug,'AccountTeamAssignment Found -> d.user__r.role__c = ' + d.user__r.role__c);
        
                        //add accountTeamMember
                        AccountTeamMember addRecord = new AccountTeamMember();
                        addRecord.AccountId = a.id;
                        addRecord.TeamMemberRole = d.user__r.role__c;
                        addRecord.UserId = d.user__r.id;
                        acctMembers.add(addRecord);
                        
                        AssignedIDAdds.add(a.id);
                        AssignedIDRoleAdds.add(d.user__r.role__c);                        
                    }
                }                
                else
                {
                    system.debug(logginglevel.debug,'Pass two AccountTeamAssignment Not Found -> d.user__r.role__c = ' + d.user__r.role__c);
                }
            }
            
            if (a.Force_Account_Team_Assignments__c == TRUE || a.AssignableRoles__c != null) {
                a.Force_Account_Team_Assignments__c = FALSE;
                a.AssignableRoles__c = '';
                AccountsToResetForce.add(a);
            }
        }

        //DML
        if(acctMembers.size() > 0){
            insert acctMembers;
        }
        if(AccountsToResetForce.size() > 0){
            update AccountsToResetForce;
        }
    }
    
    public static Boolean nullOrEmpty(Object o) {
      return (null == o) ||
        ((o instanceof String) && (0 == ((String)o).trim().length())) ||
        ((o instanceof List<object>) && (0 == ((List<object>)o).size()));
    }
}