global class AccountProspect{

    @future
    public static void UpdateCreatorShare(List<ID> AccountRecordIds) {
        Map<Id, Id> AccountOwnerAdditionMap = new Map<Id,Id>();        
        List<AccountShare>AccountSharingList = new List<AccountShare>();
           
            for(AccountShare share: [
                SELECT AccountAccessLevel,AccountId,CaseAccessLevel,ContactAccessLevel,Id,IsDeleted,LastModifiedById,LastModifiedDate,OpportunityAccessLevel,RowCause,UserOrGroupId
                FROM AccountShare   
                WHERE AccountId IN :AccountRecordIds
                //AND UserOrGroupId IN :AccountOwnerAdditionMap.values()
                AND AccountAccessLevel = 'Read']){
                    share.AccountAccessLevel = 'Edit';
                    AccountSharingList.add(share);
            }
           
            if(AccountSharingList!=null && !AccountSharingList.isEmpty()){
                System.debug('AccountSharingList has records to upsert');
                Database.UpsertResult [] results1 = Database.upsert(AccountSharingList, false);
                
                // Iterate through each returned result
                for (Database.UpsertResult sr : results1) {
                    if (sr.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed.
                        System.debug('Successfully shared account record. AccountSharingList ID: ' + sr.getId());
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('AccountShare fields that affected this error: ' + err.getFields());
                        }
                    }
                } // End of for loop.  
                  
            } // End of if(AccountSharingList!=null && !AccountSharingList.isEmpty())
            else
            {
                System.debug('AccountSharingList does not have records to upsert');
            }
            // end insert                  
    }
    
    @future
    public static void UpdateCreatorShareToRead(List<ID> AccountRecordIds) {
        Map<Id, Id> AccountOwnerAdditionMap = new Map<Id,Id>();        
        List<AccountShare>AccountSharingList = new List<AccountShare>();
           
            for(AccountShare share: [
                SELECT AccountAccessLevel,AccountId,CaseAccessLevel,ContactAccessLevel,Id,IsDeleted,LastModifiedById,LastModifiedDate,OpportunityAccessLevel,RowCause,UserOrGroupId
                FROM AccountShare   
                WHERE AccountId IN :AccountRecordIds
                //AND UserOrGroupId IN :AccountOwnerAdditionMap.values()
                AND RowCause = 'Team'
                AND AccountAccessLevel = 'Edit']){
                    share.AccountAccessLevel = 'Read';
                    AccountSharingList.add(share);
            }
           
            if(AccountSharingList!=null && !AccountSharingList.isEmpty()){
                System.debug('AccountSharingList has records to upsert');
                Database.UpsertResult [] results1 = Database.upsert(AccountSharingList, false);
                
                // Iterate through each returned result
                for (Database.UpsertResult sr : results1) {
                    if (sr.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed.
                        System.debug('Successfully shared account record. AccountSharingList ID: ' + sr.getId());
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('AccountShare fields that affected this error: ' + err.getFields());
                        }
                    }
                } // End of for loop.  
                  
            } // End of if(AccountSharingList!=null && !AccountSharingList.isEmpty())
            else
            {
                System.debug('AccountSharingList does not have records to upsert');
            }
            // end insert                  
    }    
}