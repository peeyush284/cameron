/**
* @author       Gabor Bay
* @date         09/17/2014
* @description  This class have method that does the following:
*               1. When a new Product is added, add the product to the Standard Pricebook for each currency
*               2. When a new Product is added, add the product to product's division specific Pricebook for each currency
*               3. 
* 
*/

public with sharing class ProductHelper {
    
    public static String standardPriceBookId;
    public static String divisionPriceBookId; 
    
    public static void HandleAfterInsert(list<Product2> newRecords){
        map<string, string> priceBookMap = getPriceBookMap();
        addProductToPriceBooks(newRecords,priceBookMap);
    }
    public static void HandleAfterUpdate (List<Product2> newRecords, Map<Id, Product2> oldRecords){    
        map<string, string> priceBookMap = getPriceBookMap();
        evaluateProducts(newRecords, oldRecords, priceBookMap);         
    }

    private static void evaluateProducts(List<Product2> newRecords, Map<Id, Product2> oldRecords, map<string, string> priceBookMap){
    Set<Id> ProductIdSet = new Set<Id>();   
        for(Product2 newProduct : newRecords){
            divisionPriceBookId = PriceBookMap.get(newProduct.Division__c);         
            if(newProduct.Auto_Associate_to_Price_Books_flg__c == false && oldRecords.get(newProduct.id).Auto_Associate_to_Price_Books_flg__c == true){
                ProductIdSet.add(newProduct.Id);
            }
        }
        if(ProductIdSet!=null && !ProductIdSet.isEmpty()){
            standardPriceBookId = PriceBookMap.get('Standard Price Book');
            queryAndDeletePriceBookEntry(ProductIdSet,standardPriceBookId);
        }
        else{
            addProductToPriceBooks(newRecords,priceBookMap);
        }  
    }

    private static void addProductToPriceBooks(List<Product2> newRecords, map<string, string> priceBookMap){
        //get List of Currencies
        List<CurrencyType> currencyList =  [SELECT IsoCode FROM CurrencyType WHERE IsActive = true];
        standardPriceBookId = priceBookMap.get('Standard Price Book');
             
        List<PricebookEntry> PriceBookEntryList = new List<PricebookEntry>();
        List<String> PriceBookEntryJSONList = new List<String>();  //------------->
             
        for(Product2 newProduct : newRecords){         
            if(newProduct.Auto_Associate_to_Price_Books_flg__c == true){
            //for each product record, add a Standard and Division Pricebook entry for each currency
            //create a list of PriceBook records for insert     
                for(CurrencyType cl: currencyList){             
                    PriceBookEntryJSONList.add(JSON.serialize(new JSONHelper(newProduct.Id,standardPriceBookId, cl.IsoCode, '0', 'true')));                 
                    divisionPriceBookId = priceBookMap.get(newProduct.Division__c);
                    if(divisionPriceBookId!=null){              
                        PriceBookEntryJSONList.add(JSON.serialize(new JSONHelper(newProduct.Id,divisionPriceBookId, cl.IsoCode, '0', 'true')));
                    }else{
                        System.debug('For Product Id: '+ newProduct.Id + ' Pricebook does not exist for division ' + newProduct.Division__c);
                    }           
                } 
            }  
        }

        if(PriceBookEntryJSONList!=null && !PriceBookEntryJSONList.isEmpty()){                  
            addProductToPriceBook(PriceBookEntryJSONList); 
        }    
    } //method

    public static Map<string, string> getPriceBookMap(){    
         List<Pricebook2> PriceBookList =  [SELECT Id, Name, IsStandard FROM Pricebook2 WHERE IsDeleted = false];
        //create a Map to retrieve Pricebook Id by Name
        map<string, string> priceBookMap = new Map<string, string>();
        for(Pricebook2 pb : PriceBookList) {
            priceBookMap.put(pb.Name, pb.Id);
        }
        System.debug('priceBookMap:'+ priceBookMap);
        
        return priceBookMap;
    }

    // Implement JSON class to pass list of objects to bulkify the @future method
    public class JSONHelper{
    
        public String productId {get;set;}
        public String pricebookId {get;set;}
        public String currencyIsoCode {get;set;}
        public String unitPrice {get;set;}
        public String isActive {get;set;}
        
        public JSONHelper(String productId, String pricebookId, String currencyIsoCode, String unitPrice, String isActive){
            this.productId = productId;
            this.pricebookId = pricebookId;
            this.currencyIsoCode = currencyIsoCode;
            this.unitPrice = unitPrice;
            this.isActive = isActive;
        }
    }
    
//    @future
    public static void addProductToPriceBook (List<String> PriceBookEntryJSONList){   //------------->
        
        JSONHelper helper = null;
        
        List<PricebookEntry> PriceBookEntryList = new List<PricebookEntry>();
        for(String str:PriceBookEntryJSONList){
            helper = (JSONHelper) JSON.deserialize(str, JSONHelper.class);
            PricebookEntry pbe = new PricebookEntry(Product2Id = helper.productId, PriceBook2Id = helper.pricebookId, CurrencyIsoCode = helper.currencyIsoCode, UnitPrice = integer.valueOf(helper.unitPrice), IsActive = boolean.valueOf(helper.isActive));
            PriceBookEntryList.add(pbe);
        }       
  
        Database.SaveResult [] results = Database.insert(PriceBookEntryList, false);
                    
        // Iterate through each returned result 
        for (Database.SaveResult sr : results) {
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted PriceBookEntry. Record ID: ' + sr.getId());
            }
            else {
                // Operation failed, so get all errors
                                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('PriceBookEntry fields that affected this error: ' + err.getFields());
                }
            }
        }               
    }
        
 //   @future
    public static void queryAndDeletePriceBookEntry(Set<Id>ProductIdSet, String stdrdPriceBookId){  
        List<PricebookEntry>divisionPBEToDeleteList = new List<PricebookEntry>();
        for(PricebookEntry pbeDivision:[SELECT Id FROM PricebookEntry WHERE Product2Id IN :ProductIdSet AND PriceBook2Id <> :stdrdPriceBookId]){
                divisionPBEToDeleteList.add(pbeDivision);
        }
        
        // Now delete Division Pricebook Entries
        if(divisionPBEToDeleteList!=null && !divisionPBEToDeleteList.isEmpty()){
            Database.DeleteResult [] results = Database.delete(divisionPBEToDeleteList, false);
            
            // Iterate through each returned result
            for (Database.DeleteResult sr : results) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('PriceBookEntry fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }        
              
        List<PricebookEntry>standardPBEToDeleteList = new List<PricebookEntry>();        
        for(PricebookEntry pbeStandard:[SELECT Id FROM PricebookEntry WHERE Product2Id IN :ProductIdSet]){
                standardPBEToDeleteList.add(pbeStandard);
        }

        // Now delete Standard Pricebook Entries
        if(standardPBEToDeleteList!=null && !standardPBEToDeleteList.isEmpty()){
            Database.DeleteResult [] results = Database.delete(standardPBEToDeleteList, false);
            
            // Iterate through each returned result
            for (Database.DeleteResult sr : results) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('PriceBookEntry fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
    }
}