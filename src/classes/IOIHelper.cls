/**
* @author       Peeyush Awadhiya
* @date         07/15/2014
* @description  This class have methods that does the following:
*               1. When a IOI record is updated it captures the old and new values as related custom object record.
*               2. It tracks following fields: Item of Interest, Description, Owner, Status.
*               3. Populate and sync the period based on IOI_Date__c field.
*               
*/

public with sharing class IOIHelper {
    
    /**
    * @author       Peeyush Awadhiya
    * @date         07/15/2014
    * @description  This method does the following:
    *               1. When a IOI record is updated it captures the old and new values as related custom object record.
    *               2. It tracks following fields:
    *               Item of Interest, Description, Owner, Status
    * @param        List<IOI__c> newIOIList : This is list of all IOI that are being Updated.
    * @param        Map<Id,IOI__c> oldIOIMap : Map of old values
    * @return       void 
    */
    
    public static void trackIOIUpdates(list<IOI__c> newIOIList, Map<Id,IOI__c> oldIOIMap){
        
        List<String> trackingRecordList = new List<String>();
        
        for(IOI__c ioiRecord:newIOIList){
            // Track only if the owner is not the originator of IOI record.
            if(ioiRecord.OwnerId!=ioiRecord.CreatedById && ioiRecord.Status__c!='In Progress'){
                // Name
                if(ioiRecord.Name!=oldIOIMap.get(ioiRecord.Id).Name){
                    trackingRecordList.add(JSON.serialize(new IOIJSONHelper(ioiRecord.Id,'Name', oldIOIMap.get(ioiRecord.Id).Name, ioiRecord.Name, ioiRecord.LastModifiedById)));   
                }
                // Description
                if(ioiRecord.Description__c!=oldIOIMap.get(ioiRecord.Id).Description__c){
                    trackingRecordList.add(JSON.serialize(new IOIJSONHelper(ioiRecord.Id,'Description', oldIOIMap.get(ioiRecord.Id).Description__c, ioiRecord.Description__c, ioiRecord.LastModifiedById)));   
                }
                // Owner
                if(ioiRecord.OwnerId!=oldIOIMap.get(ioiRecord.Id).OwnerId){
                    trackingRecordList.add(JSON.serialize(new IOIJSONHelper(ioiRecord.Id,'Owner', oldIOIMap.get(ioiRecord.Id).OwnerId, ioiRecord.OwnerId, ioiRecord.LastModifiedById)));   
                }
                // Status
                if(ioiRecord.Status__c!=oldIOIMap.get(ioiRecord.Id).Status__c){
                    trackingRecordList.add(JSON.serialize(new IOIJSONHelper(ioiRecord.Id,'Status', oldIOIMap.get(ioiRecord.Id).Status__c, ioiRecord.Status__c, ioiRecord.LastModifiedById)));   
                }
            } // End of if
            
        } // End of for
        
        if(trackingRecordList!=null && !trackingRecordList.isEmpty()){
            captureIOIFieldUpdate(trackingRecordList);
        }
    }
    
    
    
    /**
    * @author       Peeyush Awadhiya
    * @date         07/15/2014
    * @description  This method does the following:
    *               1. When a IOI record is updated it captures the old and new values as related custom object record.
    *               2. It tracks following fields:
    *               Item of Interest, Description, Owner, Status
    * @param        String ioiRecordId, String fieldAPI, String oldValue, String newValue, String modifiedById
    * @return       void 
    */
    
    @future
    
    public static void captureIOIFieldUpdate(List<String> fieldTrackingRecords){
        
        /*
        IOI_Field_Tracking__c tracking = new IOI_Field_Tracking__c(IOI__c = ioiRecordId, Field__c = fieldAPI, value__c = newValue, Old_Value__c = oldValue, Modified_By__c = modifiedById);
        try
        {
            insert tracking;
        }
        catch(Exception ex)
        {
            System.debug('Exception caught while insering IOI_Field_Tracking__c record'+ex.getMessage());    
        }
        */
        
        IOIJSONHelper helper = null;
        
        List<IOI_Field_Tracking__c> fieldTrackingList = new List<IOI_Field_Tracking__c>();
        for(String str:fieldTrackingRecords){
            helper = (IOIJSONHelper) JSON.deserialize(str, IOIJSONHelper.class);
            IOI_Field_Tracking__c tracking = new IOI_Field_Tracking__c(IOI__c = helper.ioiRecordId, Field__c = helper.fieldAPI, value__c = helper.newValue, Old_Value__c = helper.oldValue, Modified_By__c = helper.modifiedById);
            fieldTrackingList.add(tracking);
        }
        if(fieldTrackingList!=null && !fieldTrackingList.isEmpty()){
            Database.SaveResult [] results = Database.insert(fieldTrackingList, false);
            
            // Iterate through each returned result
            for (Database.SaveResult sr : results) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted IOI Field Tracking. Record ID: ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('IOI Field Tracking fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
    }
    
    /**
    * @author       Peeyush Awadhiya
    * @date         10/30/2014
    * @description  This method does the following:
    *               3. Populate and sync the period based on IOI_Date__c field.
    *               
    * 
    */
    public static void populatePeriod(List<IOI__c> ioiRecords){
        Set<Date> dateSet = new Set<Date>();
        
        Date minStartDate = Date.Today();
        Date maxEndDate = Date.Today();
        
        // Iterate through all IOI records and add all the IOI_Date__c to set
        for(IOI__c ioi:ioiRecords){
            
            // Save the minimum and maximum date to define the period date range
            if(minStartDate>ioi.IOI_Date__c){
                minStartDate = ioi.IOI_Date__c;
            }
            if(maxEndDate<ioi.IOI_Date__c){
                maxEndDate =  ioi.IOI_Date__c;   
            }
        }
        // Now query all the periods 
        List<Period__c> periodList = [SELECT id, StartDate__c, EndDate__c FROM Period__c WHERE StartDate__c <=:minStartDate OR EndDate__c>=:maxEndDate ];
        System.debug(minStartDate+'######'+maxEndDate+'#######'+periodlist);
        // Iterate and populate the period
        for(IOI__c ioi:ioiRecords){
            // Compare the IOI_Date__c with retrieved periods
            for(Period__c period:periodList){
                // If the IOI date is in the period range, then assign the period.
                if(ioi.IOI_Date__c >= period.startDate__c && ioi.IOI_Date__c <= period.endDate__c){
                    ioi.Period__c = period.Id;
                }
            
            }    
        
        }
          
    }
    
    
    // Implement JSON class to pass list of objects to bulkify the @future method
    public class IOIJSONHelper{
    
        public String ioiRecordId {get;set;}
        public String fieldAPI {get;set;}
        public String oldValue {get;set;}
        public String newValue {get;set;}
        public String modifiedById {get;set;}
        
        public IOIJSONHelper(String ioiRecordId, String fieldAPI, String oldValue, String newValue, String modifiedById){
            this.ioiRecordId = ioiRecordId;
            this.fieldAPI = fieldAPI;
            this.oldValue = oldValue;
            this.newValue = newValue;
            this.modifiedById = modifiedById;
        }
    }
}