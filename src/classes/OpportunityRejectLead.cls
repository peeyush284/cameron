/**
 * @author      Salesforce {GB}
 * @date        5/27/2014
 * @description From button click
 * @  Set close date to "Today"
 * @  Set stage to "Closed - Cancelled"
 * @  Set Description to "Lead Rejected"
 */
 
global class OpportunityRejectLead{

    WebService static String rejectLead(Id opportunityId) { 
        String responseStr = 'Could not execute the service.';
        List<Opportunity> leadListToReject = new List<Opportunity>();
        for(Opportunity rejectLead:[SELECT Id, CloseDate, description, StageName FROM Opportunity WHERE Id=:opportunityId]){
            rejectLead.CloseDate = Date.Today();
            rejectLead.StageName = 'Closed Rejected';
            rejectLead.Description = 'Lead Rejected';
            leadListToReject.add(rejectLead);
        }
        
        if(leadListToReject!=null && !leadListToReject.isempty())
        {
            Database.SaveResult [] saveResult = Database.update(leadListToReject,false);
            for(Database.SaveResult result:saveResult){
                if(!result.isSuccess()){
                    for(Database.Error err : result.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Opportunity fields that affected this error: ' + err.getFields());
                    }
                    responseStr='Exception caught while adding Opportunities';                  
                }
                else{                   
                    //responseStr= OpptyList.size()+' opportunities(s) inserted!'; 
                    responseStr = string.valueof(leadListToReject.size()+' Opportunity saved');                    
                }               
            }
        }           
        
        return responseStr;
    }
}