/**
* @author       Peeyush Awadhiya
* @date         09/25/2014
* @description  This contains method that does the following:
*               1. If a note is created related to the Quote Tracking object, then copy the note text to Quote tracking custom field.
*               
*/             


public class NoteHelper{
    
    public static void handleBefores(List<Note> newlyCreatedNotes){
        
        SetValidationRules('Note');
        // Use case 1. If a note is created related to the Quote Tracking object, then copy the note text to Quote tracking custom field.
        //validateQuoteTrackingWithLatestNote(newlyCreatedNotes);
        //reSetValidationRules('Note');
    }
    
    public static void handleAfterInsert(List<Note> newlyCreatedNotes){
        
        //SetValidationRules('Note');
        // Use case 1. If a note is created related to the Quote Tracking object, then copy the note text to Quote tracking custom field.
        validateQuoteTrackingWithLatestNote(newlyCreatedNotes);
        reSetValidationRules('Note');
    }
    
    public static void SetValidationRules(string objectName)
    {
        ValidationRuleOverride__c v = ValidationRuleOverride__c.getInstance();
        if (v == null) // custom setting not set 
        {
            v = new ValidationRuleOverride__c();
            v.ObjectNames__c = objectName;
            upsert v;
            system.debug('NoteHelper (new) SetValidationRules for ' + objectName);
        }
        else if (v.ObjectNames__c == '') 
        {
            v.ObjectNames__c = objectName;
            upsert v;
            system.debug('NoteHelper (upd) SetValidationRules for ' + objectName);
        }
        else
            system.debug('NoteHelper (left alone) SetValidationRules v.ObjectNames__c = ' + v.ObjectNames__c);
    }
    
    public static void reSetValidationRules(string objectName)
    {
        ValidationRuleOverride__c v = ValidationRuleOverride__c.getInstance();
        if (v == null)
        {
            v = new ValidationRuleOverride__c();
            v.ObjectNames__c = '';
            upsert v;
        }
        else if (v.ObjectNames__c == objectName) // if the custom setting is set for this object
        {
            v.ObjectNames__c = '';
            upsert v;
        }
        
    }    
    
    public static void HandleAfterUpdate (List<Note> newRecords, Map<Id, Note> oldRecords)
    {
        //SetValidationRules('Note');
        // Use case 2. If a note is updated that is related to the Quote Tracking object, then copy the note text to Quote tracking custom field.
        validateQuoteTrackingWithLatestNote(newRecords);
        reSetValidationRules('Note');
    }
    
    private static void validateQuoteTrackingWithLatestNote(List<Note> newlyCreatedNotes){
        Map<Id, String> createdByMap = new Map<Id,String>();
        Set<Id>userIdSet = new Set<Id>();
        for(Note newNote:newlyCreatedNotes){
            userIdSet.add(newNote.CreatedById);    
        }
        
        for(User usr:[SELECT Id, Name FROM User WHERE Id IN:userIdSet]){
            createdByMap.put(usr.Id, usr.Name);
        }
        
        Map<Id, String> newNoteMap = new Map<Id, String>();
        for(Note newNote:newlyCreatedNotes){
            // If parentId is QuoteTracking custom object
            if(newNote.ParentId.getSObjectType() == QuoteTracking__c.getSObjectType()){
                if(!createdByMap.containsKey(newNote.CreatedById)){
                    newNoteMap.put(newNote.ParentId, String.valueOf('Created Date: '+newNote.CreatedDate.Date().format()+'\n'+newNote.Body));
                }
                else{
                    newNoteMap.put(newNote.ParentId, String.valueOf('Created Date: '+newNote.CreatedDate.Date().format()+' '+'Created By: '+createdByMap.get(newNote.CreatedById)+'\n'+newNote.Body));
                }
            }
        }
        
        if(newNoteMap!=null && !newNoteMap.isEmpty()){
            updateQuoteTrackingWithLatestNote(newNoteMap);
        }
    }
    
    @future
    private static void updateQuoteTrackingWithLatestNote(Map<Id, String> newNoteMap){
        List<QuoteTracking__c> quotesToUpdate = new List<QuoteTracking__c>();
        
        for(QuoteTracking__c quote:[SELECT Id, Last_Note__c, Last_Note_Long_Tex__c, CreatedBy.Name FROM QuoteTracking__c WHERE Id IN:newNoteMap.keySet()]){
            if(newNoteMap.containsKey(quote.Id)){
                // Trim the string to 255 characters for text area field.
                String noteStr = String.valueOf(newNoteMap.get(quote.Id));
                
                if(noteStr.length()>=255){
                    quote.Last_Note__c = noteStr.substring(0,254);
                }
                else{
                    quote.Last_Note__c = noteStr;  
                }
                quote.Last_Note_Long_Tex__c = newNoteMap.get(quote.Id);
                quotesToUpdate.add(quote);
            }    
        } // End of for loop
        
        if(quotesToUpdate!=null && !quotesToUpdate.isEmpty()){
            System.debug('--- There are quotesToUpdate'); 

            Database.SaveResult [] saveResults = Database.update(quotesToUpdate,false);
            for(Database.SaveResult result:saveResults){
                if(!result.isSuccess()){
                    for(Database.Error err : result.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('QuoteTracking__c fields that affected this error: ' + err.getFields());
                    }
                }
            }
        } // End of if    
    }
}