/**
* @author       Chris Pitman
* @date         10/09/2014
* @description  
*/


@isTest

public class ASForecast_TEST {

    static testMethod void test0(){
        SchedulableContext sc = null;
        ASForecastProcessScheduler myClass = new ASForecastProcessScheduler();
        myClass.execute(sc);
    }
    
    static testMethod void test1(){
        List < Analytic_Snapshot_Bucket_Oppty__c > addList = new List< Analytic_Snapshot_Bucket_Oppty__c >();
        
        for (integer idx = 0; idx < 10; idx++) {
            Analytic_Snapshot_Bucket_Oppty__c rec = new Analytic_Snapshot_Bucket_Oppty__c();
            rec.Division__c = 'Drilling';
            addlist.add(rec);
        }
        insert addlist;
        
        List < Analytic_Snapshot_Bucket_Oppty__c > myList = [Select ID from Analytic_Snapshot_Bucket_Oppty__c LIMIT 10];
        Database.BatchableContext bc = null;    
        ASDeleteASFcstBucketOpptyBatch myClass = new ASDeleteASFcstBucketOpptyBatch('Select ID from Analytic_Snapshot_Bucket_Oppty__c LIMIT 10');
        myClass.execute(bc, (List <sObject>) myList);
        myClass.finish(bc);
    }
    
    static testMethod void test1a(){
        List < Analytic_Snapshot_Forecast_Oppty__c > addList = new List< Analytic_Snapshot_Forecast_Oppty__c >();
        
        for (integer idx = 0; idx < 10; idx++) {
            Analytic_Snapshot_Forecast_Oppty__c rec = new Analytic_Snapshot_Forecast_Oppty__c();
            rec.Division__c = 'Drilling';
            addlist.add(rec);
        }
        insert addlist;
        
        List < Analytic_Snapshot_Forecast_Oppty__c > myList = [Select ID from Analytic_Snapshot_Forecast_Oppty__c LIMIT 10];
        Database.BatchableContext bc = null;    
        ASDeleteASFcstOpptyBatch myClass = new ASDeleteASFcstOpptyBatch('Select ID from Analytic_Snapshot_Bucket_Oppty__c LIMIT 10');
        myClass.execute(bc, (List <sObject>) myList);
        myClass.finish(bc);
    }
    
    static testMethod void test2(){
        List <Analytic_Snapshot_Bucket_Oppty__c> addList = new List<Analytic_Snapshot_Bucket_Oppty__c>();
        
        for (integer idx = 0; idx < 10; idx++) {
            Analytic_Snapshot_Bucket_Oppty__c rec = new Analytic_Snapshot_Bucket_Oppty__c();
            rec.Division__c = 'Drilling';
            addlist.add(rec);
        }
        insert addlist;
        
        SchedulableContext sc = null;
        ASForecastProcessScheduler myClass = new ASForecastProcessScheduler();
        myClass.execute(sc);        
    }
    
    static testMethod void test3() {
        List <Analytic_Snapshot_Bucket_Oppty__c > addList = new List<Analytic_Snapshot_Bucket_Oppty__c>();
        
        for (integer idx = 0; idx < 10; idx++) {
            Analytic_Snapshot_Bucket_Oppty__c rec = new Analytic_Snapshot_Bucket_Oppty__c();
            rec.Division__c = 'Drilling';
            if (idx < 5)
                rec.Schedule_Date__c = date.parse('12/31/2014');
            else
                rec.Schedule_Date__c = date.parse('12/31/2015');
            addlist.add(rec);
        }
        insert addlist;
        
        List <Analytic_Snapshot_Bucket_Oppty__c> myList = [Select ID,
                                                            Schedule_Date__c,
                                                            Business_Unit__c,
                                                            Division__c,
                                                            Forecast_Category__c,
                                                            Forecast__c,
                                                            Opportunity_ID__c,
                                                            Opportunity_Name__c,
                                                            Opportunity_Owner__c,
                                                            Schedule_Amount__c,
                                                            Stage__c,
                                                            Opportunity_Type__c from Analytic_Snapshot_Bucket_Oppty__c LIMIT 10];
        Database.BatchableContext bc = null;    
        ASInsrtASFcstBcktOpptyToASFcstOpptyBatch myClass = new ASInsrtASFcstBcktOpptyToASFcstOpptyBatch('Select ID, '+
                                                            'Schedule_Date__c, '+
                                                            'Business_Unit__c, '+
                                                            'Division__c, '+
                                                            'Forecast_Category__c, '+
                                                            'Forecast__c, '+
                                                            'Opportunity_ID__c, '+
                                                            'Opportunity_Name__c, '+
                                                            'Opportunity_Owner__c, '+
                                                            'Schedule_Amount__c, '+
                                                            'Stage__c, '+
                                                            'Opportunity_Type__c from Analytic_Snapshot_Bucket_Oppty__c LIMIT 10');
        myClass.execute(bc, (List <sObject>) myList);
        myClass.finish(bc);    
    }
    
    static testMethod void test4() {
        /*
        List <Analytic_Snapshot_Bucket_Oppty__c > addList = new List<Analytic_Snapshot_Bucket_Oppty__c>();
        
        for (integer idx = 0; idx < 10; idx++) {
            Analytic_Snapshot_Bucket_Oppty__c rec = new Analytic_Snapshot_Bucket_Oppty__c();
            rec.Division__c = 'Drilling';
            if (idx < 5)
                rec.Schedule_Date__c = date.parse('12/31/2014');
            else
                rec.Schedule_Date__c = date.parse('12/31/2015');
            addlist.add(rec);
        }
        insert addlist;
        */
        
        List <OpportunityLineItemSchedule> myList = [Select ID from OpportunityLineItemSchedule LIMIT 10];
        Database.BatchableContext bc = null;    
        ASInsrtBcktOpptyIntoASFcstBcktOpptyBatch myClass = new ASInsrtBcktOpptyIntoASFcstBcktOpptyBatch('Select ID from OpportunityLineItemSchedule LIMIT 10');
        myClass.execute(bc, (List <sObject>) myList);
        myClass.finish(bc);    
    }   
    
    static testMethod void test5() {

        List <Opportunity > addList = new List<Opportunity>();
        Account acc;
        
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Drilling||Acct Mgr|Primary', acc);      
        
        Id BucketRecordTypeId = Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get('Bucket').getRecordTypeId();
        Id UniverseRecordTypeId = Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get('Universe').getRecordTypeId();        
        
        for (integer idx = 0; idx < 10; idx++) {
            Opportunity rec = new Opportunity();
            rec.CAM_Division__c = 'Drilling';
            rec.RecordTypeId = BucketRecordTypeId;
            rec.End_User__c = acc.id;
            rec.name = 'Opp 1' + idx;  

            if (idx < 5){
                rec.StageName = 'Closed';
                rec.CloseDate = date.parse('12/31/2014');
            }
            else {
                rec.StageName = 'Open';
                rec.CloseDate = date.parse('12/31/2015');
            }
            addlist.add(rec);
        }
        for (integer idx = 0; idx < 10; idx++) {
            Opportunity rec = new Opportunity();
            rec.CAM_Division__c = 'Drilling';
            rec.RecordTypeId = UniverseRecordTypeId;
            rec.End_User__c = acc.id;   
            rec.name = 'Opp 2' + idx;         

            if (idx < 5){
                rec.StageName = 'Closed Won';
                rec.CloseDate = date.parse('12/31/2014');
            }
            else {
                rec.StageName = 'Universe';
                rec.CloseDate = date.parse('12/31/2015');
            }
            addlist.add(rec);
        }        
        insert addlist;

        
        List < Opportunity > myList = [Select ID, 
                                              RecordType.Name,
                                              Amount,
                                              CloseDate,
                                              CAM_Business_Unit__c,
                                              CAM_Division__c,
                                              ForecastCategoryName,
                                              Name,
                                              Owner_Name_calc__c,
                                              StageName,
                                              CAM_Forecast_Amount__c
                                              from Opportunity LIMIT 10];
        Database.BatchableContext bc = null;    
        ASInsrtStandardOpptyIntoASFcstOpptyBatch myClass = new ASInsrtStandardOpptyIntoASFcstOpptyBatch('Select ID, ' +
                                                                                                        'RecordType.Name, '+
                                                                                                        'Amount, '+
                                                                                                        'CloseDate, '+
                                                                                                        'CAM_Business_Unit__c, '+
                                                                                                        'CAM_Division__c, '+
                                                                                                        'ForecastCategoryName, ' +
                                                                                                        'Name, ' +
                                                                                                        'Owner_Name_calc__c, '+
                                                                                                        'StageName, '+
                                                                                                        'CAM_Forecast_Amount__c '+
                                                                                                        'from Opportunity LIMIT 10');
        myClass.execute(bc, (List <sObject>) myList);
        myClass.finish(bc);    
    }       
}