/**
* @author       Peeyush Awadhiya
* @date         10/10/2014
* @description  Trigger on opportunity product object. It perfoms the following:
*               1. When a new non-bucket OpportunityLineItem record is created, it copies it to Opportunity Reporting object.
*               2. When a new non-bucket OpportunityLineItem record is updated, it updates it to the related Opportunity Reporting object.
*               3. When a new non-bucket OpportunityLineItem record is deleted, it deletes all related Opportunity Reporting object.
*/



public class OpportunityLineItemHelper{
    
    public static void HandleAfterInsert(List<OpportunityLineItem> newRecords){
        // Use Case  1. When a new non-bucket OpportunityLineItem record is created, it copies it to Opportunity Reporting object.
        createOpportunityReportingRecords(newRecords);
    }
    
    public static void HandleAfterUpdate(List<OpportunityLineItem> newRecords, Map<Id,OpportunityLineItem> oldRecordsMap){
        // 2. When a new non-bucket OpportunityLineItem record is updated, it updates it to the related Opportunity Reporting object.
        updateOpportunityReportingRecords(newRecords,oldRecordsMap);
    }
    public static void HandleBeforeDelete(Map<Id,OpportunityLineItem> oldRecordsMap){
        deleteOpportunityReportingRecords(oldRecordsMap);
    }
    
    private static void createOpportunityReportingRecords(List<OpportunityLineItem> newRecords){
        List<Opportunity_Reporting__c> reportingList = new List<Opportunity_Reporting__c>();
        newRecords = [
                         SELECT Id, Name, Opportunity.OwnerId, Opportunity.ForecastCategoryName, Opportunity.Amount,
                         Opportunity.CloseDate, OpportunityId, Opportunity.RecordType.Name, Opportunity.StageName, CurrencyISOCode,
                         opportunity.CAM_Forecast_Amount__c, PricebookEntry.Product2.Name, TotalPrice, Forecast_Amount_Formula__c,
                         Order_Type__c, Product_Class__c, QuantityCalc__c, Manufacturing_Plant__c, Market_Requested_Delivery_Weeks__c 
                         FROM OpportunityLineItem WHERE Id IN :newRecords 
                     ];
                   
        for(OpportunityLineItem opLine:newRecords){
            
            if(opLine.Opportunity.RecordType.Name!='Bucket'){
                Opportunity_Reporting__c reporting = new Opportunity_Reporting__c(
                    OwnerId = opLine.Opportunity.OwnerId,
                    Forecast_Category__c = opLine.Opportunity.ForecastCategoryName,
                    Opportunity_Booking_Date__c = opLine.Opportunity.CloseDate,
                    Opportunity_Name__c = opLine.OpportunityId,
                    Opportunity_Product_Name__c = opLine.PricebookEntry.Product2.Name,
                    Opportunity_Type__c = opLine.Opportunity.RecordType.Name,
                    Opportunity_Amount__c = opLine.TotalPrice,
                    Forecast_Amount__c = opLine.Forecast_Amount_Formula__c, 
                    Source_Id__c = opLine.Id,
                    CurrencyISOCode = opLine.CurrencyISOCode,
                    Order_Type__c = opLine.Order_Type__c,
                    Product_Class__c = opLine.Product_Class__c,
                    Quantity__c = opLine.QuantityCalc__c,
                    Manufacturing_Plant__c = opLine.Manufacturing_Plant__c,
                    Market_Requested_Delivery_Weeks__c = opLine.Market_Requested_Delivery_Weeks__c    
                );
                /*
                if(opLine.Opportunity.StageName == 'Closed Won'){
                    reporting.Opportunity_Amount__c = 0;
                    reporting.Forecast_Amount__c = 0;
                    reporting.Opportunity_Closed_Amount__c = opLine.Opportunity.Amount;
                }
                else{
                    reporting.Opportunity_Amount__c = opLine.Opportunity.Amount;
                    reporting.Forecast_Amount__c = opLine.Opportunity.CAM_Forecast_Amount__c;
                    reporting.Opportunity_Closed_Amount__c = 0;
                }
                */
                reportingList.add(reporting);   
            }
        } // End of for loop
        
        if(reportingList!=null && !reportingList.isEmpty()){
            
            List<Database.UpsertResult> reportingUpsertResult = Database.upsert(reportingList,false);
            
            for(Database.UpsertResult result:reportingUpsertResult){
                if(!result.isSuccess()){
                    // Operation failed, so get all errors                
                    for(Database.Error err : result.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Opportunity Reporting fields that affected this error: ' + err.getFields());
                    }
                }
                
            } // End of for loop
        
        } // End of if(reportingList!=null && !reportingList.isEmpty())
    }
    
    private static void updateOpportunityReportingRecords(List<OpportunityLineItem> newRecords, Map<Id,OpportunityLineItem> oldRecordsMap){
        
        List<Opportunity_Reporting__c> reportingList = new List<Opportunity_Reporting__c>();
        Map <Id, Opportunity_Reporting__c> reportingMap = new Map <Id, Opportunity_Reporting__c>();
        
        newRecords = [
                         SELECT Id, Name, Opportunity.OwnerId, Opportunity.ForecastCategoryName, Opportunity.Amount,
                         Opportunity.CloseDate, OpportunityId, Opportunity.RecordType.Name, Opportunity.StageName,CurrencyISOCode,
                         opportunity.CAM_Forecast_Amount__c, PricebookEntry.Product2.Name, TotalPrice, Forecast_Amount_Formula__c,
                         Order_Type__c, Product_Class__c, QuantityCalc__c, Manufacturing_Plant__c, Market_Requested_Delivery_Weeks__c 
                         FROM OpportunityLineItem WHERE Id IN :newRecords 
                     ];
        
        for(Opportunity_Reporting__c reporting:
            [
                SELECT Forecast_Amount__c, Forecast_Category__c, Opportunity_Amount__c, Opportunity_Booking_Date__c,  
                Opportunity_Closed_Amount__c, Opportunity_Name__c, CurrencyISOCode,    
                Opportunity_Product_Name__c, Opportunity_Type__c, Source_Id__c,
                Order_Type__c, Product_Class__c, Quantity__c, Manufacturing_Plant__c, Market_Requested_Delivery_Weeks__c 
                FROM Opportunity_Reporting__c
                WHERE Source_Id__c IN :oldRecordsMap.keySet()
            ]
        ){
            reportingMap.put(reporting.Source_Id__c, reporting);    
        }
        
        Opportunity_Reporting__c reporting = new Opportunity_Reporting__c();
        
        for(OpportunityLineItem opLine:newRecords){
            
            if(opLine.Opportunity.RecordType.Name!='Bucket'){
                if(reportingMap.containsKey(opLine.Id))
                {
                    reporting = reportingMap.get(opLine.Id); 
                }
                else{
                    reporting = new Opportunity_Reporting__c();
                }
                reporting.OwnerId = opLine.Opportunity.OwnerId;
                reporting.Forecast_Category__c = opLine.Opportunity.ForecastCategoryName;
                reporting.Opportunity_Booking_Date__c = opLine.Opportunity.CloseDate;
                reporting.Opportunity_Name__c = opLine.OpportunityId;
                reporting.Opportunity_Product_Name__c = opLine.PricebookEntry.Product2.Name;
                reporting.Opportunity_Type__c = opLine.Opportunity.RecordType.Name;
                reporting.Source_Id__c = opLine.Id;
                reporting.Opportunity_Amount__c = opLine.TotalPrice;
                reporting.Forecast_Amount__c = opLine.Forecast_Amount_Formula__c;
                reporting.Order_Type__c = opLine.Order_Type__c;
                reporting.CurrencyISOCode = opLine.CurrencyISOCode;
                reporting.Product_Class__c = opLine.Product_Class__c;
                reporting.Quantity__c = opLine.QuantityCalc__c;
                reporting.Manufacturing_Plant__c = opLine.Manufacturing_Plant__c;
                reporting.Market_Requested_Delivery_Weeks__c = opLine.Market_Requested_Delivery_Weeks__c;
                     
                /*
                if(opLine.Opportunity.StageName == 'Closed Won'){
                    reporting.Opportunity_Amount__c = 0;
                    reporting.Forecast_Amount__c = 0;
                    reporting.Opportunity_Closed_Amount__c = opLine.Opportunity.Amount;
                }
                else{
                    reporting.Opportunity_Amount__c = opLine.Opportunity.Amount;
                    reporting.Forecast_Amount__c = opLine.Opportunity.CAM_Forecast_Amount__c;
                    reporting.Opportunity_Closed_Amount__c = 0;
                }
                */
                reportingList.add(reporting);
            }
        } // End of for
        
        if(reportingList!=null && !reportingList.isEmpty()){
            
            List<Database.UpsertResult> reportingUpsertResult = Database.upsert(reportingList,false);
            
            for(Database.UpsertResult result:reportingUpsertResult){
                if(!result.isSuccess()){
                    // Operation failed, so get all errors                
                    for(Database.Error err : result.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Opportunity Reporting fields that affected this error: ' + err.getFields());
                    }
                }
                
            } // End of for loop
        
        } // End of if(reportingList!=null && !reportingList.isEmpty())
    }
    
    private static void deleteOpportunityReportingRecords(Map<Id,OpportunityLineItem> oldRecordsMap){
        
        Set<Id> opportunityReportingParentIds = new Set<Id>();
        opportunityReportingParentIds.addAll(oldRecordsMap.keySet());
        
        for(OpportunityLineItemSchedule schedule:[SELECT Id FROM OpportunityLineItemSchedule WHERE OpportunityLineItemId IN :oldRecordsMap.keySet()]){
             opportunityReportingParentIds.add(schedule.Id);    
        }
        
        List<Opportunity_Reporting__c> reportingList = new List<Opportunity_Reporting__c> (
            [
                SELECT Id, Source_Id__c FROM Opportunity_Reporting__c
                WHERE Source_Id__c IN :opportunityReportingParentIds
            ]
        );
        if(reportingList!=null && !reportingList.isEmpty()){
            
            List<Database.DeleteResult> reportingDeleteResult = Database.delete(reportingList,false);
            
            for(Database.DeleteResult result:reportingDeleteResult){
                if(!result.isSuccess()){
                    // Operation failed, so get all errors                
                    for(Database.Error err : result.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Opportunity Reporting fields that affected this error: ' + err.getFields());
                    }
                }
                
            } // End of for loop
        
        } // End of if(reportingList!=null && !reportingList.isEmpty())
    } 
}