/**
* @author       Peeyush Awadhiya
* @date         09/08/2014
* @description  This class contains test method to test IOIPrepopulateExtension
* 
*/

@isTest

public class IOIPrepopulateExtension_TEST {
    
    public static testMethod void testIOIPrepopulateFrom(){
        
         // Prepare the data stub
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        
        //Contact
        Contact contactRec = new Contact(AccountId = acc.Id, LastName = 'Test Contact');
        insert contactRec;
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('Bucket', 'RFQ', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Surface';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = project.Id;
        
        oppty.AccountId = acc.Id;
        insert oppty;
        
        List<IOI__c> ioiRecordList = new List<IOI__c>();
        IOI__c ioiRecord = new IOI__c();
        ioiRecord = TestUtilities.generateIOI('Test IOI',acc.Id, oppty.Id, project.Id,'Submitted');
        ioiRecord.Period__c =  period.Id;
        ioiRecord.IOI_Summary__c = true;
        ioiRecordList.add(ioiRecord);
         
        IOI__c ioiRecord2 = new IOI__c();
        ioiRecord2 = TestUtilities.generateIOI('Second IOI record',acc.Id, oppty.Id, project.Id,'Submitted');
        ioiRecord2.Period__c =  period.Id;
        ioiRecordList.add(ioiRecord2);
        
        IOI__c ioiRecord3 = new IOI__c();
        ioiRecord3  = TestUtilities.generateIOI('Test IOI',acc.Id, oppty.Id, project.Id,'Submitted');
        ioiRecord3.Period__c =  period.Id;
        ioiRecord3.Opportunity__c = null;
        ioiRecord3.IOI_Summary__c = true;
        ioiRecordList.add(ioiRecord3);
        
        IOI__c ioiRecord4 = new IOI__c();
        ioiRecord4 = TestUtilities.generateIOI('Test IOI',acc.Id, oppty.Id, project.Id,'Submitted');
        ioiRecord4.Period__c =  period.Id;
        ioiRecord4.Account__c = null;
        ioiRecord4.IOI_Summary__c = false;
        ioiRecordList.add(ioiRecord4);
        
        IOI__c ioiRecord5 = new IOI__c();
        ioiRecord5 = TestUtilities.generateIOI('Test IOI',acc.Id, oppty.Id, project.Id,'Submitted');
        ioiRecord5.Period__c =  period.Id;
        ioiRecord5.project__c = null;
        ioiRecord5.IOI_Summary__c = true;
        ioiRecordList.add(ioiRecord5);
        
        insert ioiRecordList;
        
        IOI_Prepopulate_Fields__c hierarchialCustomSetting;
        
        if(IOI_Prepopulate_Fields__c.getOrgDefaults() == null){
            // Need to change it to org specific values in the production
            hierarchialCustomSetting = new IOI_Prepopulate_Fields__c(); 
        }
        else{
            hierarchialCustomSetting = IOI_Prepopulate_Fields__c.getOrgDefaults();
            hierarchialCustomSetting.Description__c = '00Nc00000014OTT'; 
            hierarchialCustomSetting.IOI_Summary__c = '00Nc00000015j0n';
            hierarchialCustomSetting.Visibility_Level__c = '00Nc00000014jiZ';  
            hierarchialCustomSetting.Period__c = 'CF00Nc00000014OyM';
            hierarchialCustomSetting.Next_Reviewer__c = 'CF00Nc00000015Eqn';
            hierarchialCustomSetting.Account__c = 'CF00Nc00000014OTE';
            hierarchialCustomSetting.Opportunity__c = 'CF00Nc00000014OT9';
            hierarchialCustomSetting.Project__c = 'CF00Nc00000014QOQ';
            hierarchialCustomSetting.CAM_Contact__c = 'CF00Nc00000016nmE';
        }
        
        upsert hierarchialCustomSetting;
        
        PageReference pageRef = Page.IOIPrePopulatePage;
        Test.setCurrentPage(pageRef);
        
        // Use case 1: When IOI is created from Account 
        pageRef.getParameters().put('retURL','/'+acc.Id);
        
        IOIPrepopulateExtension extension = new IOIPrepopulateExtension(new ApexPages.StandardController(ioiRecord));
        String editPageURL = extension.returnEditPage().getURL();
        
        // Assert the PageReference
        System.assertEquals(editPageURL.contains(IOI_Prepopulate_Fields__c.getOrgDefaults().Account__c+'_lkid='+acc.Id), true);
        
        // Use case 2: When IOI is created from Opportunity 
        pageRef.getParameters().put('retURL','/'+oppty.Id);
        extension = new IOIPrepopulateExtension(new ApexPages.StandardController(ioiRecord));
        editPageURL = extension.returnEditPage().getURL();
        
        // Assert the PageReference
        System.assertEquals(editPageURL.contains(IOI_Prepopulate_Fields__c.getOrgDefaults().Opportunity__c+'_lkid='+oppty.Id), true);
        /* Commented since the IOIs created from Opportunity related list won't have account and project populated */
        //System.assertEquals(editPageURL.contains(IOI_Prepopulate_Fields__c.getOrgDefaults().Account__c+'_lkid='+acc.Id), true);
        //System.assertEquals(editPageURL.contains(IOI_Prepopulate_Fields__c.getOrgDefaults().Project__c+'_lkid='+project.Id), true);
        
        // Use case 3: When IOI is created from Project. Opportunity shouldn't be populated
        pageRef.getParameters().put('retURL','/'+project.Id);
        extension = new IOIPrepopulateExtension(new ApexPages.StandardController(ioiRecord));
        editPageURL = extension.returnEditPage().getURL();
        
        // Assert the PageReference
        System.assertEquals(editPageURL.contains(IOI_Prepopulate_Fields__c.getOrgDefaults().Opportunity__c+'_lkid='+oppty.Id), false);
        System.assertEquals(editPageURL.contains(IOI_Prepopulate_Fields__c.getOrgDefaults().Account__c+'_lkid='+acc.Id), true);
        System.assertEquals(editPageURL.contains(IOI_Prepopulate_Fields__c.getOrgDefaults().Project__c+'_lkid='+project.Id), true);
        
        // Use case 4: When IOI is created from Contact. Opportunity and Project shouldn't be populated
        pageRef.getParameters().put('retURL','/'+contactRec.Id);
        extension = new IOIPrepopulateExtension(new ApexPages.StandardController(ioiRecord));
        editPageURL = extension.returnEditPage().getURL();
        
        // Assert the PageReference
        System.assertEquals(editPageURL.contains(IOI_Prepopulate_Fields__c.getOrgDefaults().CAM_Contact__c+'_lkid='+contactRec.Id), true);
        System.assertEquals(editPageURL.contains(IOI_Prepopulate_Fields__c.getOrgDefaults().Account__c+'_lkid='+acc.Id), true);
        System.assertEquals(editPageURL.contains(IOI_Prepopulate_Fields__c.getOrgDefaults().Opportunity__c+'_lkid='+oppty.Id), false);
        System.assertEquals(editPageURL.contains(IOI_Prepopulate_Fields__c.getOrgDefaults().Project__c+'_lkid='+project.Id), false);
    }
}