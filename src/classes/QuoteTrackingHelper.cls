/**
* @author       Peeyush Awadhiya
* @date         09/24/2014
* @description  This class have methods that does the following:
*               1. When a new quote tracking record is created, it adds the Outside Sales User to the Opportunity Sales Team as Secondary Sales.
*               2. When Outside sales changes on quote tracking record, the new Outside sales user is added to the Opportunity Sales Team as Secondary Sales.
*               3. When a new quote is created, send a notification to whole sales team.
*
*/


public class QuoteTrackingHelper {
    
    public static void HandleBeforeActions(List<QuoteTracking__c> newRecords){
        SetValidationRules('QuoteTracking__c');
    }       

        
    // Handle after insert
    public static void HandleAfterInsert(List<QuoteTracking__c> newRecords){
        //SetValidationRules('QuoteTracking__c');
        // Add all new Outside Sales users to the opportunity Sales team as Secondary Sales.
        handleOutsideSalesTeamAssignment(newRecords);
        notifySalesTeamOfNewQuote(newRecords);
        // confirm that End User of Quote Tracking matched End User of Opportunity (if one is related)
        handleOppEndUserSync(newRecords);
        ResetValidationRules('QuoteTracking__c');
    }
    
    
    // Handle after update
    public static void HandleAfterUpdate( List<QuoteTracking__c> newRecords, Map<Id, QuoteTracking__c> oldRecordMap){
        //SetValidationRules('QuoteTracking__c');
        // When Outside sales changes on quote tracking record, the new Outside sales user is added to the Opportunity Sales Team as Secondary Sales.
        handleOutsideSalesPersonChange(newRecords, oldRecordMap);
        // confirm that End User of Quote Tracking matched End User of Opportunity (if one is related)
        handleOppEndUserSync(newRecords);
        ResetValidationRules('QuoteTracking__c');
    }
    
    public static void SetValidationRules(string objectName)
    {
        ValidationRuleOverride__c v = ValidationRuleOverride__c.getInstance();
        if (v == null) // custom setting not set 
        {
            v = new ValidationRuleOverride__c();
            v.ObjectNames__c = objectName;
            upsert v;
            system.debug('QuoteTrackingHelper (new) SetValidationRules = ' + objectName);
        }
        else if (v.ObjectNames__c == null) 
        {
            v.ObjectNames__c = objectName;
            upsert v;
            system.debug('QuoteTrackingHelper (upd) SetValidationRules = ' + objectName);
        }
        else
            system.debug('QuoteTrackingHelper (left alone) SetValidationRules v.ObjectNames__c = ' + v.ObjectNames__c);
    }
    
    public static void reSetValidationRules(string objectName)
    {
        ValidationRuleOverride__c v = ValidationRuleOverride__c.getInstance();
        if (v == null)
        {
            v = new ValidationRuleOverride__c();
            v.ObjectNames__c = '';
            upsert v;
        }
        else if (v.ObjectNames__c == objectName) // if the custom setting is set for this object
        {
            v.ObjectNames__c = '';
            upsert v;
        }
        
    }    
    
    private static void handleOutsideSalesTeamAssignment(List<QuoteTracking__c> newRecords){
        List<OpportunityTeamMember> salesTeam = new List<OpportunityTeamMember>();
       
        for(QuoteTracking__c quote: newRecords){
            if(quote.Opportunity__c!=null && quote.Outside_Sales_Person__c!=null && quote.Outside_Sales_Person__c!=quote.Opportunity__r.OwnerId){
                salesTeam.add(new OpportunityTeamMember(OpportunityId = quote.Opportunity__c, UserId = quote.Outside_Sales_Person__c, TeamMemberRole = System.Label.Sales_Team_Outside_Role));  
            }
        }
        
        if(salesTeam!=null && !salesTeam.isEmpty()){
            Database.SaveResult [] results = Database.insert(salesTeam, false);
            
            // Iterate through each returned result
            for (Database.SaveResult sr : results) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted Opportunity Team Member. OpportunityTeamMember ID: ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('OpportunityTeamMember fields that affected this error: ' + err.getFields());
                    }
                }
            }
            
        } // End of if
        
    }
    
    private static void handleOutsideSalesPersonChange(List<QuoteTracking__c> newRecords, Map<Id, QuoteTracking__c> oldRecordMap){
        List<QuoteTracking__c> updatedQuoteList = new List<QuoteTracking__c>();
        for(QuoteTracking__c updatedQuote:newRecords){
            // Check if the Outside Sales Person has been updated
            if(updatedQuote.Outside_Sales_Person__c!= null && updatedQuote.Outside_Sales_Person__c!=oldRecordMap.get(updatedQuote.Id).Outside_Sales_Person__c){
                updatedQuoteList.add(updatedQuote);     
            }
        } // End of for loop
        
        if(updatedQuoteList!=null && !updatedQuoteList.isEmpty()){
            handleOutsideSalesTeamAssignment(updatedQuoteList);
        }
    }
    
    private static void notifySalesTeamOfNewQuote(List<QuoteTracking__c> newRecords){
        Map<Id,Id> quoteOpportunityIdMap = new Map<Id,Id>();
        for(QuoteTracking__c quoteRec:newRecords){
            quoteOpportunityIdMap.put(quoteRec.Id,quoteRec.Opportunity__c);   
        }        
        if(quoteOpportunityIdMap!=null && !quoteOpportunityIdMap.isEmpty()){
            notifySalesTeam(quoteOpportunityIdMap);        
        }
    }
    
    @future
    private static void notifySalesTeam(Map<Id,Id> quoteOpportunityTeamMap){
        List<EmailTemplate>templateList = [SELECT Id, Name FROM EmailTemplate WHERE Name ='NotifySalesTeamOfNewQuoteTracking' LIMIT 1];
        Map<Id, List<OpportunityTeamMember>> opportunitySalesTeamMap = new Map<Id, List<OpportunityTeamMember>>(); 
         
        List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
          
        if(templateList!=null && !templateList.isEmpty()){
            for(Opportunity op:[SELECT Id,(SELECT Id, UserId, OpportunityId FROM OpportunityTeamMembers) FROM Opportunity WHERE Id IN:quoteOpportunityTeamMap.values()]){
                opportunitySalesTeamMap.put(op.Id,op.OpportunityTeamMembers);     
            }    
            
            for(Id quoteId:quoteOpportunityTeamMap.keySet()){
                if(quoteOpportunityTeamMap.containsKey(quoteId) && opportunitySalesTeamMap.containsKey(quoteOpportunityTeamMap.get(quoteId))){
                    for(OpportunityTeamMember teamMember:opportunitySalesTeamMap.get(quoteOpportunityTeamMap.get(quoteId))){
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        mail.setTargetObjectId(teamMember.UserId);
                        mail.setSaveAsActivity(false);
                        mail.setWhatId(quoteId);
                        mail.setTemplateId(templateList[0].Id);
                        emailList.add(mail);
                    } 
                }        
            } // End of for(Id quoteId:quoteOpportunityTeamMap.keySet())
            
            if(emailList!=null && !emailList.isEmpty()){
                LIST<Messaging.SendEmailResult> emailResults = Messaging.sendEmail(emailList);
                
                if (emailResults!=null && !emailResults.isEmpty() && !emailResults.get(0).isSuccess()) {                      
                        System.debug('Error generated when sending mail - ' + emailResults.get(0).getErrors()[0].getMessage());
                }                           
            }  // End of if(emailList!=null && !emailList.isEmpty()) 
              
        } // End of if(templateList!=null && !templateList.isEmpty())
        
    } // End of method
    
    private static void handleOppEndUserSync(List<QuoteTracking__c> newRecords){
        integer idx = 0;
        list<id> qtid = new list<id>();
       
        for(QuoteTracking__c quote: newRecords){
            if (quote.division_calc__c == 'Process Systems' && 
                    quote.Opportunity__c!=null) {
                qtid.add(quote.id);
            }
            idx++;
        }        
        
        if (qtid.size()>0) {
            OpportunitySyncQuoteTracking.SyncQuoteTrackingRecordsToOpp(qtid);
        }
    }
    
}