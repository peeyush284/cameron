public with sharing class DDCDnBCompanyController {

    public class DnBCompanyStructure
    {
        public Map<String, String> legalStatusMap = new Map<String, String>();
        public Map<String, String> MSCMap = new Map<String, String>();
    
        public String errMsg {get; set;}
        
        public Boolean isFromAccount {get; set;}
        public Boolean isFromLead {get; set;}
        
        public Account account {get; set;}
        public Lead lead {get; set;}
        public DandBCompany company {get; set;}
        public DandBCompany globalUltimate {get; set;}
        
        public String getSmallBusiness()
        {
            if (String.isBlank(company.SmallBusiness) || company.SmallBusiness.equalsIgnoreCase('N'))
                return 'Not small business site';
            else if (company.SmallBusiness.equalsIgnoreCase('Y'))
                return 'Small business site';

            return 'Unknown or Not Available';
        }
        
        public String getMinorityOwned()
        {
            if (String.isBlank(company.MinorityOwned) || company.MinorityOwned.equalsIgnoreCase('N'))
                return 'Not minority-owned';
            else if (company.MinorityOwned.equalsIgnoreCase('Y'))
                return 'Minority-owned';

            return 'Unknown or Not Available';
        }
        
        public String getWomenOwned()
        {
            if (String.isBlank(company.WomenOwned) || company.WomenOwned.equalsIgnoreCase('N'))
                return 'Not owned by a women or unknown';
            else if (company.WomenOwned.equalsIgnoreCase('Y'))
                return 'Owned by a women';

            return 'Unknown or Not Available';
        }
        
        public String getSubsidiary()
        {
            if (String.isBlank(company.Subsidiary) || company.Subsidiary.equals('0'))
                return 'Not subsidiary of another organization';
            else if (company.Subsidiary.equals('3') || company.Subsidiary.equals('1'))
                return 'Subsidiary of another organization';

            return 'Unknown or Not Available';
        }
        
        public String getMarketingPreScreen()
        {
            if (String.isBlank(company.MarketingPreScreen))
                return 'Unknown or Not Available';
            else if (company.MarketingPreScreen.equalsIgnoreCase('L'))
                return 'Low risk of delinquency';
            else if (company.MarketingPreScreen.equalsIgnoreCase('M'))
                return 'Medium risk of delinquency';
            else if (company.MarketingPreScreen.equalsIgnoreCase('H'))
                return 'High risk of delinquency';

            return 'Unknown or Not Available';
        }   

        public String getLocationStatus()
        {
            if (String.isBlank(company.LocationStatus))
                return 'Unknown or Not Available';
            else if (company.LocationStatus.equals('1'))
                return 'Headquarters';
            else if (company.LocationStatus.equals('2'))
                return 'Branch';
            else if (company.LocationStatus.equals('0'))
                return 'Single Location';
            else if (company.LocationStatus.equals('4'))
                return 'Branch/Division';
            else if (company.LocationStatus.equals('5'))
                return 'Subsidiary';

            return 'Unknown or Not Available';
        }   

        public String getOutOfBusiness()
        {
            if (String.isBlank(company.OutOfBusiness) || company.OutOfBusiness.equals('N'))
                return 'Not Out of Business';
            else if (company.LocationStatus.equals('Y'))
                return 'Out of Business';

            return 'Unknown or Not Available';
        }   

        public String getLegalStatus()
        {
            if (String.isBlank(company.LegalStatus))
                return 'Unknown or Not Available';
            else if (legalStatusMap.containsKey(company.LegalStatus))
                return legalStatusMap.get(company.LegalStatus).capitalize();

            return 'Unknown or Not Available';
        }   

        public String getMarketingSegmentationCluster()
        {
            if (String.isBlank(company.MarketingSegmentationCluster))
                return 'Unknown or Not Available';
            else if (MSCMap.containsKey(company.MarketingSegmentationCluster))
                return MSCMap.get(company.MarketingSegmentationCluster).capitalize();

            return 'Unknown or Not Available';
        }   
    }
    
    // Declare variables
    // 
    public String currentId {get; set;}
    public String objectType {get; set;}
    public Id companyId;
    
    public DnBCompanyStructure companyStructure { get; set; }
    
    // Good practice for controller class extensions.
    public DDCDnBCompanyController() {
        System.debug(LoggingLevel.INFO, 'DDCDnBCCompanyController Constructor');
        companyStructure = new DnBCompanyStructure();
        companyStructure.isFromAccount = false;
        companyStructure.isFromLead = false;
        companyStructure.legalStatusMap = initLegalStatusMap();
        companyStructure.MSCMap = initMarketingSegmentationClusterMap();
    }
        
    public DnBCompanyStructure getDnBCompany()
    {
        if (currentId == null) {
            currentId = System.currentPageReference().getParameters().get('id');
            
            if ( currentId == null )
            {
                companyStructure.errMsg = 'Object Id must be provided';
                return companyStructure;
            }
        }
        
        if (objectType == null) {
            objectType = System.currentPageReference().getParameters().get('objectType');

            if ( objectType == null )
            {
                companyStructure.errMsg = 'Object Type must be provided';
                return companyStructure;
            }
        }
        
        if ( objectType.equalsIgnoreCase('Lead') )
        {
            companyStructure.isFromAccount = false;
            companyStructure.isFromLead = true;
            
            companyStructure.lead = [SELECT Id, DandBCompanyId
                                       FROM Lead
                                      WHERE Id = :currentId
                                    ];
            companyId = companyStructure.lead.DandBCompanyId;
        } 
        else if ( objectType.equalsIgnoreCase('Account') )
        {
            companyStructure.isFromAccount = true;
            companyStructure.isFromLead = false;
            
            companyStructure.account = [SELECT Id, DandBCompanyId
                                          FROM Account
                                         WHERE Id = :currentId
                                       ];
            companyId = companyStructure.account.DandBCompanyId;
        } else {
            companyStructure.errMsg = 'Requires an object type of Lead or Account';
            return companyStructure;
        }
                
        try
        {
            companyStructure.company = [SELECT  Id,
                                               Name,
                                               City,
                                               Country,
                                               Description,
                                               DomesticUltimateBusinessName,
                                               DomesticUltimateDunsNumber,
                                               DunsNumber,
                                               EmployeesHere,
                                               EmployeesHereReliability,
                                               EmployeesTotal,
                                               EmployeesTotalReliability,
                                               FamilyMembers,
                                               Fax,
                                               FifthNaics,
                                               FifthNaicsDesc,
                                               FifthSic,
                                               FifthSicDesc,
                                               FipsMsaCode,
                                               FipsMsaDesc,
                                               FourthNaics,
                                               FourthNaicsDesc,
                                               FourthSic,
                                               FourthSicDesc,
                                               GeoCodeAccuracy,
                                               GlobalUltimateBusinessName,
                                               GlobalUltimateDunsNumber,
                                               GlobalUltimateTotalEmployees,
                                               ImportExportAgent,
                                               Latitude,
                                               LegalStatus,
                                               LocationStatus,
                                               Longitude,
                                               MailingCity,
                                               MailingCountry,
                                               MailingPostalCode,
                                               MailingState,
                                               MailingStreet,
                                               MarketingPreScreen,
                                               MarketingSegmentationCluster,
                                               MinorityOwned,
                                               NationalId,
                                               NationalIdType,
                                               OutOfBusiness,
                                               OwnOrRent,
                                               ParentOrHqBusinessName,
                                               ParentOrHqDunsNumber,
                                               Phone,
                                               PostalCode,
                                               PrimaryNaics,
                                               PrimaryNaicsDesc,
                                               PrimarySic,
                                               PrimarySicDesc,
                                               PublicIndicator,
                                               SalesVolume,
                                               SalesVolumeReliability,
                                               SecondNaics,
                                               SecondNaicsDesc,
                                               SecondSic,
                                               SecondSicDesc,
                                               SixthNaics,
                                               SixthNaicsDesc,
                                               SixthSic,
                                               SixthSicDesc,
                                               SmallBusiness,
                                               State,
                                               StockExchange,
                                               StockSymbol,
                                               Street,
                                               Subsidiary,
                                               SystemModstamp,
                                               ThirdNaics,
                                               ThirdNaicsDesc,
                                               ThirdSic,
                                               ThirdSicDesc,
                                               TradeStyle1,
                                               TradeStyle2,
                                               TradeStyle3,
                                               TradeStyle4,
                                               TradeStyle5,
                                               URL,
                                               UsTaxId,
                                               WomenOwned,
                                               YearStarted 
                                          FROM DandBCompany 
                                         WHERE id = :companyId
                                      ];
        } catch (Exception e) {
            companyStructure.errMsg = 'No D&B Company data available.';
            companyStructure.company = new DandBCompany();
        }
        
        try
        {
            companyStructure.globalUltimate = [SELECT  Id,
                                               Name,
                                               City,
                                               Country,
                                               Description,
                                               DomesticUltimateBusinessName,
                                               DomesticUltimateDunsNumber,
                                               DunsNumber,
                                               EmployeesHere,
                                               EmployeesHereReliability,
                                               EmployeesTotal,
                                               EmployeesTotalReliability,
                                               FamilyMembers,
                                               Fax,
                                               FifthNaics,
                                               FifthNaicsDesc,
                                               FifthSic,
                                               FifthSicDesc,
                                               FipsMsaCode,
                                               FipsMsaDesc,
                                               FourthNaics,
                                               FourthNaicsDesc,
                                               FourthSic,
                                               FourthSicDesc,
                                               GeoCodeAccuracy,
                                               GlobalUltimateBusinessName,
                                               GlobalUltimateDunsNumber,
                                               GlobalUltimateTotalEmployees,
                                               ImportExportAgent,
                                               Latitude,
                                               LegalStatus,
                                               LocationStatus,
                                               Longitude,
                                               MailingCity,
                                               MailingCountry,
                                               MailingPostalCode,
                                               MailingState,
                                               MailingStreet,
                                               MarketingPreScreen,
                                               MarketingSegmentationCluster,
                                               MinorityOwned,
                                               NationalId,
                                               NationalIdType,
                                               OutOfBusiness,
                                               OwnOrRent,
                                               ParentOrHqBusinessName,
                                               ParentOrHqDunsNumber,
                                               Phone,
                                               PostalCode,
                                               PrimaryNaics,
                                               PrimaryNaicsDesc,
                                               PrimarySic,
                                               PrimarySicDesc,
                                               PublicIndicator,
                                               SalesVolume,
                                               SalesVolumeReliability,
                                               SecondNaics,
                                               SecondNaicsDesc,
                                               SecondSic,
                                               SecondSicDesc,
                                               SixthNaics,
                                               SixthNaicsDesc,
                                               SixthSic,
                                               SixthSicDesc,
                                               SmallBusiness,
                                               State,
                                               StockExchange,
                                               StockSymbol,
                                               Street,
                                               Subsidiary,
                                               SystemModstamp,
                                               ThirdNaics,
                                               ThirdNaicsDesc,
                                               ThirdSic,
                                               ThirdSicDesc,
                                               TradeStyle1,
                                               TradeStyle2,
                                               TradeStyle3,
                                               TradeStyle4,
                                               TradeStyle5,
                                               URL,
                                               UsTaxId,
                                               WomenOwned,
                                               YearStarted 
                                          FROM DandBCompany 
                                         WHERE DunsNumber = :companyStructure.company.GlobalUltimateDunsNumber
                                      ];
        } catch (Exception e) {
            //companyStructure.errMsg = 'No D&B Company Global Ultimate data available.';
            companyStructure.globalUltimate = new DandBCompany();
        }
        
        return companyStructure;                         
    }    


    private Map<String, String> initMarketingSegmentationClusterMap()
    {
        return new Map<String, String>
            {'1'=>'High Tension Branches in Insurance/Utility Industries',
             '2'=>'Bustling Manufacturers & Business Services',
             '3'=>'The Withering Branch Company',
             '4'=>'Rapid Growth Large Business Services',
             '5'=>'Sunny Branches in Insurance/Utility Industries',
             '6'=>'Labor Intensive Giants',
             '7'=>'Entrepreneur & Co.',
             '8'=>'Frugal & Associates',
             '9'=>'Spartans',
             '10'=>'Struggling Start Ups',
             '11'=>'The Hectic Venture Company',
             '12'=>'Established Shingle Company',
             '13'=>'Industry Inc.',
             '14'=>'Landmark Business Services',
             '15'=>'The Test Of Time Company',
             '16'=>'Powerhouse 6000',
             '17'=>'In Good Hands',
             '18'=>'Sudden Growth Giants',
             '19'=>'Active Traders',
             '20'=>'Old Core Proprietors',
             '21'=>'Solid & Sons',
             '22'=>'Main Street USA'
            };
    }
    
    private Map<String, String> initLegalStatusMap()
    {
        return new Map<String, String>
            {'000'=>'unknown',
            '003'=>'corporation',
            '008'=>'joint venture',
            '009'=>'master limited partnership',
            '010'=>'general partnership',
            '011'=>'limited partnership',
            '012'=>'partnership of unknown type',
            '013'=>'proprietorship',
            '014'=>'limited liability',
            '015'=>'friendly society',
            '030'=>'trust',
            '050'=>'government body',
            '070'=>'crown corporation',
            '080'=>'institution',
            '090'=>'estate',
            '099'=>'industry cooperative',
            '100'=>'cooperative',
            '101'=>'non profit organization',
            '102'=>'private limited company',
            '103'=>'partnership partially limited by shares',
            '104'=>'temporary association',
            '105'=>'registered proprietorship',
            '106'=>'limited partnership with shares',
            '107'=>'unregistered proprietorship',
            '108'=>'community of goods',
            '109'=>'reciprocal guarantee company',
            '110'=>'cooperative society with ltd liability',
            '111'=>'civil company',
            '112'=>'de facto partnership',
            '113'=>'foundation',
            '114'=>'association',
            '115'=>'public company',
            '116'=>'civil law partnership',
            '117'=>'incorporated by act of Parliament',
            '118'=>'local government body',
            '119'=>'private unlimited company',
            '120'=>'foreign company',
            '121'=>'private company limited by guarantee',
            '122'=>'civil partnership',
            '125'=>'public limited company',
            '126'=>'registered partnership',
            '127'=>'society',
            '128'=>'government owned company',
            '129'=>'government institute',
            '130'=>'public institute',
            '131'=>'plant',
            '132'=>'hotel',
            '133'=>'division',
            '140'=>'joint shipping company',
            '142'=>'limited liability corporation',
            '143'=>'branch',
            '144'=>'concern address',
            '145'=>'insurance company',
            '146'=>'private foundation',
            '147'=>'county institution',
            '148'=>'municipal institution',
            '149'=>'vestry',
            '150'=>'public undertaking',
            '151'=>'faeroese company',
            '152'=>'greenland limited',
            '153'=>'greenland private limited',
            '154'=>'sole proprietorship',
            '155'=>'sole proprietorship or partnership',
            '160'=>'unregistered partnership',
            '161'=>'civil association',
            '162'=>'association in participation',
            '163'=>'mutual insurance association',
            '164'=>'stock company with variable capital',
            '166'=>'cooperative production society',
            '167'=>'joint stock company',
            '168'=>'ltd responsibility cooperative society',
            '169'=>'national credit society',
            '170'=>'offene erwerbsgesellschaft',
            '171'=>'ltd liability company with variable cap',
            '180'=>'kommandit erwerbsgesellschaft',
            '185'=>'public credit institution',
            '186'=>'working group',
            '190'=>'union',
            '200'=>'personal partnership',
            '210'=>'real estate partnership',
            '220'=>'agricultural collective interest company',
            '230'=>'defacto business organization',
            '240'=>'government/municipal establishment',
            '250'=>'housing company',
            '260'=>'voluntary association',
            '270'=>'mortgage association',
            '280'=>'cooperative society',
            '290'=>'cooperative bank',
            '300'=>'savings bank',
            '301'=>'small individual business',
            '310'=>'economic association',
            '320'=>'insurance limited company',
            '330'=>'government authority',
            '340'=>'group',
            '350'=>'housing cooperative society',
            '360'=>'mutual assistance business organization',
            '370'=>'provident business organization',
            '380'=>'limited company',
            '390'=>'simple partnership',
            '400'=>'mixed company',
            '410'=>'commercial collective company',
            '420'=>'commercial company',
            '430'=>'representative office',
            '440'=>'bank',
            '450'=>'industry and equity company',
            '451'=>'trading society',
            '452'=>'government institution',
            '460'=>'open stock corporation',
            '470'=>'trusteeship',
            '480'=>'private business',
            '490'=>'decentralized public organization',
            '500'=>'stock company',
            '502'=>'tenant owner\'s society',
            '510'=>'civil society',
            '520'=>'society for capitalization of savings',
            '530'=>'limited cooperative company',
            '540'=>'mutual insurance society',
            '550'=>'simple limited partnership',
            '560'=>'named collective company',
            '570'=>'non profit association',
            '580'=>'corporation with variable capital',
            '590'=>'joint corporation',
            '600'=>'consortium',
            '610'=>'personal firm',
            '620'=>'corporation with authorized capital',
            '630'=>'corporation with open capital',
            '640'=>'bank for capitalization of savings',
            '650'=>'closed stock corporation',
            '660'=>'commercial and industrial corporation',
            '670'=>'commercial corporation',
            '680'=>'industrial corporation',
            '690'=>'financial institution',
            '700'=>'contract mining company',
            '710'=>'contracting company',
            '720'=>'non profit international organization',
            '730'=>'international organization',
            '740'=>'ltd co auth capital-regd co open cap',
            '750'=>'organization',
            '755'=>'unlimited company',
            '760'=>'farmer\'s association',
            '770'=>'economic assoc/tenant owners\' society',
            '780'=>'mining company',
            '790'=>'shipping company',
            '800'=>'simple company',
            '810'=>'private firm',
            '820'=>'family foundation',
            '830'=>'county',
            '840'=>'county association',
            '850'=>'county council',
            '860'=>'regional social insurance office',
            '870'=>'unit within the Swedish church',
            '880'=>'public corporation/institution',
            '881'=>'statutory body',
            '890'=>'mortgage/security association',
            '891'=>'government agency',
            '892'=>'mutual company',
            '893'=>'special corporation',
            '894'=>'central bank for agriculture & forestry',
            '895'=>'Austrian legal entity',
            '896'=>'establishment',
            '900'=>'supporting association',
            '905'=>'administration',
            '910'=>'unemployment office',
            '915'=>'liaison office',
            '920'=>'foreign legal person',
            '925'=>'cooperative union with guaranteed liab',
            '930'=>'Swedish legal person',
            '935'=>'cooperative union with limited liability',
            '940'=>'unlimited partnership',
            '945'=>'cooperative society with unlimited liab',
            '950'=>'foreign branch',
            '955'=>'cooperative society with guaranteed liab',
            '960'=>'incorporated foundation',
            '965'=>'business not formally registered',
            '970'=>'incorporated non profit association',
            '971'=>'state owned enterprise',
            '972'=>'free trd. zone entp. proc. prvd. smpl.',
            '973'=>'limited holding company',
            '974'=>'govt. dept. or non-profit organization',
            '975'=>'government department',
            '976'=>'collectively owned enterprise',
            '977'=>'domestic and foreign joint venture',
            '978'=>'domestic and foreign cooperative venture',
            '980'=>'educational foundation',
            '985'=>'unlimited company ltd. liab. shareholder',
            '990'=>'medical corporation',
            '991'=>'private limited liability company',
            '992'=>'public limited liability company',
            '993'=>'exempt limited liability company',
            '994'=>'deemed public limited company',
            '995'=>'private company limited by shares',
            '999'=>'securities fund'};
    }
}