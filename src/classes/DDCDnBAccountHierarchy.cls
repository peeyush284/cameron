public with sharing class DDCDnBAccountHierarchy {
 
    public static boolean accountHrchyTriggerDisabled = false;

    //Method used for initialization
    public static String buildAccountHierarchy ()
    {
        System.debug(LoggingLevel.INFO, 'buildAccountHierarchy()');

        List<Account> accounts = null;
         
        try
        {
            if(!Test.isRunningTest()){
                 accounts = [SELECT Id, Name, ParentId, 
                               DunsNumber, 
                               DandBCompany.GlobalUltimateDunsNumber,
                               DandBCompany.DomesticUltimateDunsNumber,                               
                               DandBCompany.ParentOrHqDunsNumber
                          FROM Account
                         WHERE DunsNumber != null
                           AND DandBCompany.GlobalUltimateDunsNumber != null
                           AND DandBCompany.GlobalUltimateDunsNumber != '000000000'
                           AND DandBCompany.ParentOrHqDunsNumber != null
                           AND DandBCompany.ParentOrHqDunsNumber != '000000000'
                       ];
            } else {
            accounts = [SELECT Id, Name, ParentId, 
                               DunsNumber, 
                               DandBCompany.GlobalUltimateDunsNumber,
                               DandBCompany.DomesticUltimateDunsNumber,                               
                               DandBCompany.ParentOrHqDunsNumber
                          FROM Account
                         WHERE DunsNumber != null
                           AND DandBCompany.GlobalUltimateDunsNumber != null
                           AND DandBCompany.GlobalUltimateDunsNumber != '000000000'
                           AND DandBCompany.ParentOrHqDunsNumber != null
                           AND DandBCompany.ParentOrHqDunsNumber != '000000000' limit 10
                       ];
           }
                                         
        } catch (Queryexception qe) {
            return 'INFO: No accounts match criteria.';
        }                   

        return buildAccountHierarchy(accounts);
        
        //return 'Done';
    }

    //Method allows for the intialization of a single account by Id.
    public static String buildAccountHierarchy (Id pAccountId)
    {
        System.debug(LoggingLevel.INFO, 'buildAccountHierarchy(Id)');

        List<Id> accounts = new List<Id>();
        accounts.add(pAccountId);
        
        return buildAccountHierarchy(accounts);
        
        //return 'Done';
    }

    // Method used from account triggers.
    public static String buildAccountHierarchy (List<Id> pAccounts)
    {
        System.debug(LoggingLevel.INFO, 'buildAccountHierarchy(Ids)');

        List<Account> accounts = null;
        
        try
        {
            accounts = [SELECT Id, Name, ParentId, 
                               DunsNumber, 
                               DandBCompany.GlobalUltimateDunsNumber, 
                               DandBCompany.DomesticUltimateDunsNumber, 
                               DandBCompany.ParentOrHqDunsNumber
                          FROM Account
                         WHERE Id IN :pAccounts
                           AND DunsNumber != null
                           AND DandBCompany.GlobalUltimateDunsNumber != null
                           AND DandBCompany.GlobalUltimateDunsNumber != '000000000'
                           AND DandBCompany.ParentOrHqDunsNumber != null
                           AND DandBCompany.ParentOrHqDunsNumber != '000000000'
                       ];                                        
        } catch (Queryexception qe) {
            System.debug(LoggingLevel.DEBUG,'INFO: No accounts match criteria.');
            return 'INFO: No accounts match criteria.';
        }                   

        return buildAccountHierarchy(accounts);
        
        //return 'Done';
    }

    // Method used for folks that like to pass Ids as strings.
    public static String buildAccountHierarchy (List<String> pAccounts)
    {
        System.debug(LoggingLevel.INFO, 'buildAccountHierarchy(Strings)');

        List<Account> accounts = null;
        
        try
        {
            accounts = [SELECT Id, Name, ParentId, 
                               DunsNumber, 
                               DandBCompany.GlobalUltimateDunsNumber, 
                               DandBCompany.DomesticUltimateDunsNumber, 
                               DandBCompany.ParentOrHqDunsNumber
                          FROM Account
                         WHERE Id IN :pAccounts
                           AND DunsNumber != null
                           AND DandBCompany.GlobalUltimateDunsNumber != null
                           AND DandBCompany.GlobalUltimateDunsNumber != '000000000'
                           AND DandBCompany.ParentOrHqDunsNumber != null
                           AND DandBCompany.ParentOrHqDunsNumber != '000000000'
                       ];
        } catch (Queryexception qe) {
            System.debug(LoggingLevel.DEBUG,'INFO: No accounts match criteria.');
            return 'INFO: No accounts match criteria.';
        }                   

        return buildAccountHierarchy(accounts);
        
        //return 'Done';
    }

    // Method used for folks that like to pass Ids as strings.
    public static String buildAccountHierarchy (List<Account> pAccounts, Boolean pFromTrigger)
    {
        System.debug(LoggingLevel.INFO, 'buildAccountHierarchy(List<Account>, Boolean)');

        List<Account> accounts = null;
          
    if (pFromTrigger != null && pFromTrigger == true)
    {
          try
          {
              accounts = [SELECT Id, Name, ParentId, 
                                 DunsNumber, 
                                 DandBCompany.GlobalUltimateDunsNumber, 
                                 DandBCompany.DomesticUltimateDunsNumber, 
                                 DandBCompany.ParentOrHqDunsNumber
                            FROM Account
                           WHERE Id IN :pAccounts
                             AND DunsNumber != null
                             AND DandBCompany.GlobalUltimateDunsNumber != null
                             AND DandBCompany.GlobalUltimateDunsNumber != '000000000'
                             AND DandBCompany.ParentOrHqDunsNumber != null
                             AND DandBCompany.ParentOrHqDunsNumber != '000000000'
                         ];
          } catch (Queryexception qe) {
              System.debug(LoggingLevel.DEBUG,'INFO: No accounts match criteria.');
              return 'INFO: No accounts match criteria.';
          }
    } else {
      accounts = pAccounts;
    }                   
  
        return buildAccountHierarchy(accounts);
        
        //return 'Done';
    }

    // Method that does all the work.
    public static String buildAccountHierarchy (List<Account> pAccounts)
    {
        System.debug(LoggingLevel.INFO, 'buildAccountHierarchy(Accounts)');

        // cgp: if the List argument is null or has no records, return -- there is no processing needed
        if ( pAccounts == null || pAccounts.size() == 0)        
            return 'INFO: No accounts passed.';
        
        // Build a map of the accounts for which we can bild a hierarchy.
        Map<String, Account> acctMap = new Map<String, Account>();
        
    System.debug(LoggingLevel.INFO, 'Step 1 Build acctMap');        
    
    // cgp: is there is 1 record in the List argument and that record's Account Dunsnumber is also the GlobalUltimateDunsNumber for the account    
    //Deal with the sending of the Global Ultimate and build downward linkage.
    if (pAccounts.size() == 1 && pAccounts.get(0).DunsNumber == pAccounts.get(0).DandBCompany.GlobalUltimateDunsNumber )
    {
        Account account = pAccounts.get(0);
        System.debug(LoggingLevel.DEBUG, account);
      
//      if ( )
//      {
            try
            {
                List<Account> accounts = [SELECT Id, Name, ParentId, 
                                                 DunsNumber, 
                                                 DandBCompany.GlobalUltimateDunsNumber, 
                                                 DandBCompany.DomesticUltimateDunsNumber, 
                                                 DandBCompany.ParentOrHqDunsNumber
                                            FROM Account
                                           WHERE DandBCompany.GlobalUltimateDunsNumber = :account.DandBCompany.GlobalUltimateDunsNumber
                                          ];
                                          
                for ( Account acct : accounts )
                {
                    acctMap.put(acct.DunsNumber, acct); 
                }
            } catch (Queryexception qe) {
                System.debug(LoggingLevel.DEBUG,'INFO: No accounts with Global Ultimate as parent.');
                return 'INFO: No accounts with Global Ultimate as Parent.';
            }
//      }
    } else {                   

          for (Account acct : pAccounts)
          {
              // cgp: if the Account's DunsNumber is the same as the GlobaleUltimateDunsNumber or the parentOrHqDunsnumber is zeros then 
              // cgp:     do nothing
              // cgp: else
              // cgp: add the Account to the Map
              if (acct.DunsNumber == acct.DandBCompany.GlobalUltimateDunsNumber ||
                  acct.DandBCompany.ParentOrHqDunsNumber == '000000000')
              {
                  System.debug(LoggingLevel.DEBUG,'INFO: DunsNumber == GlobalUltimateDunsNumber Or ParentOrHqDunsNumber == 000000000');
              }
              else
              {
                  System.debug(LoggingLevel.DEBUG,'INFO: AcctMap.put ' + acct.DunsNumber + ',' + acct.id);              
                  acctMap.put(acct.DunsNumber, acct); 
              }
          }
    }
        
         System.debug(LoggingLevel.DEBUG, acctMap.size());
         System.debug(LoggingLevel.DEBUG, acctMap.keySet().size());
        if ( acctMap.size() == 0 )
            return 'INFO: No accounts belong to a hierarchy';           
        
        // Look up the parents that are not already included in the list.
        List<String> missingParents;
        
    System.debug(LoggingLevel.INFO, 'Step 2 Build missingParents');                

    // cgp: truly scarey -- need to query for missing parents and if we are going to get hosed by the Governor limits
    // cgp: call a future module (or maybe a batch apex module) to deal with it
    // This will loop until all parents have been found and 
    // hopefully before limits are reached.
    while (true)
    {
          missingParents = new List<String>();
                       
          // cgp: look at every site (dunsnumber) in the AccountMap
          for (String site : acctMap.keySet())
          {
              // cgp: set the parentSite, domesticSite and Globalsite for the current site
              String parentSite = acctMap.get(site).DandBCompany.ParentOrHqDunsNumber;            
              String domesticSite = acctMap.get(site).DandBCompany.DomesticUltimateDunsNumber;          
              String globalSite = acctMap.get(site).DandBCompany.GlobalUltimateDunsNumber;
              
              // cgp: is the GlobalSite (dunsnumber) is not in the acctMap, add it to the MissingParents list
              if ( ! acctMap.containsKey(globalSite) )
              {
                  System.debug(LoggingLevel.DEBUG, 'GUDN: '+globalSite);
                  missingParents.add(globalSite);
              }
              // cgp: is the domesticSite (dunsnumber) is not in the acctMap, add it to the MissingParents list              
              if ( ! acctMap.containsKey(domesticSite) )
              {
                  System.debug(LoggingLevel.DEBUG, 'DUDN: '+domesticSite);
                  missingParents.add(domesticSite);
              }
              // cgp: is the parentSite (dunsnumber) is not in the acctMap, add it to the MissingParents list              
              if ( ! acctMap.containsKey(parentSite) )
              {
                  System.debug(LoggingLevel.DEBUG, 'HQPN: '+parentSite);
                  missingParents.add(parentSite);
              }
          }
          
          // If no parents needed to be added we can stop looping.
          if (missingParents.size() == 0)
          {
              System.debug(LoggingLevel.DEBUG, 'no Missing Parents');
            break;
          }
            
          // Now go get the parent accounts that were missing.
          List<Account> parents = null;
          
          // cgp: query account information for dunsnumbers in the MissingParents list 
          // cgp: OR DandBCompany.ParentOrHqDunsNumber in the MissingParents list
          // cgp: OR DandBCompany.ParentOrHqDunsNumber in the AcctMap map
          try
          {
              parents = [SELECT Id, Name, ParentId, 
                                DunsNumber, 
                                DandBCompany.GlobalUltimateDunsNumber,
                                DandBCompany.DomesticUltimateDunsNumber, 
                                DandBCompany.ParentOrHqDunsNumber
                           FROM Account
                          WHERE DunsNumber IN :missingParents
                             OR DandBCompany.ParentOrHqDunsNumber IN :missingParents
                             OR DandBCompany.ParentOrHqDunsNumber IN :acctMap.keySet()
                             Limit :((limits.getLimitQueryRows() - limits.getQueryRows()))
                        ];
          } catch (Queryexception qe) {
              System.debug(LoggingLevel.DEBUG, 'Nothing to be done.');
          }
          
          // cgp: remove the duplicates from the Parents List
          // Find the Site DUNS that already exist.
          List<Integer> existing = new List<Integer>();
          for (Integer i=parents.size()-1; i>=0; i--)
          {
            if (acctMap.containsKey(parents.get(i).DunsNumber))
              parents.remove(i);
          }
          // Ending looking for parents if none are returned.
          if (parents == null || parents.size() == 0)
          {
            break;
          }
          else // Otherwise add the ones returned to the map to be processed.
          {
              // cgp: add the remaining parents from the list to the AccountMap
              for (Account parent : parents)
              {
                  acctMap.put(parent.DunsNumber, parent);
              }
          }
      }
        
        // Lets set up the hierarchy.
        List<Account> updates = new List<Account>(); 
        Set<String> dunsSet = new Set<String>();     

        System.debug(LoggingLevel.DEBUG, 'Step 3 Beginning Adding records to DunsSet');
    // Organize the list so that Globals are added first
        for (Account site : acctMap.values())
        {
            // cgp: consider this data on an acctMap entry
            // cgp: DunsNumber = '000000001'
            // cgp: ParentOrHqDunsNumber = '000000002'            
            // cgp: DomesticUltimateDunsNumber = '000000003'                  
            // cgp: DomesticUltimateDunsNumber = '000000004' 
            
            System.debug(LoggingLevel.DEBUG, 'DunsSet a-'+site.DandBCompany.GlobalUltimateDunsNumber);
            System.debug(LoggingLevel.DEBUG, 'DunsSet d-'+site.DandBCompany.DomesticUltimateDunsNumber);
            System.debug(LoggingLevel.DEBUG, 'DunsSet p-'+site.DandBCompany.ParentOrHqDunsNumber);            
            System.debug(LoggingLevel.DEBUG, 'DunsSet s-'+site.DunsNumber);             
                        
      dunsSet.add('a-'+site.DandBCompany.GlobalUltimateDunsNumber);
      dunsSet.add('d-'+site.DandBCompany.DomesticUltimateDunsNumber);
      dunsSet.add('p-'+site.DandBCompany.ParentOrHqDunsNumber);
      dunsSet.add('s-'+site.DunsNumber);
      
            // cgp: dunsSet now has four entries
            // cgp: record 1 = "a-000000004"  ("a-"+GlobalUltimateDunsNumber)
            // cgp: record 2 = "d-000000003"  ("d-"+DomesticUltimateDunsNumber)
            // cgp: record 3 = "p-000000002"  ("p-"+ParentOrHqDunsNumber)            
            // cgp: record 4 = "s-000000002"  ("s-"+DunsNumber)              
        }
        System.debug(LoggingLevel.DEBUG, 'Finished Adding records to DunsSet');
         
        List<String> dunsList = new List<String>(dunsSet);
        dunsList.sort();
        // cgp: List of records now sorted (ordered by) with GlobalUltimates, then DomesticUltimates, then ParentOrHqs and finally the lowest accounts
        
        System.debug(LoggingLevel.DEBUG, 'Looping thru DunsList'); 
        for (String site : dunsList)
        {
            System.debug(LoggingLevel.DEBUG, 'Site string = ' + site);
          System.debug(LoggingLevel.DEBUG, site.substring(2));
  
            // cgp: get the Account record at the Site value (substring(2) is skipping the "x-" prefix
            Account siteAccount = acctMap.get(site.substring(2));
            // cgp: initialize parent to null
            Account parentAccount = null;
            
            // cgp: if the SiteAccount is null OR its dunsNumber is the same as its GlobalUltimateDunsNumber then
            // cpg:     skip this processing and return to the loop of for look to get the next site String
            if (siteAccount == null || siteAccount.DunsNumber == siteAccount.DandBCompany.GlobalUltimateDunsNumber)
            {
                System.debug(LoggingLevel.DEBUG, 'Continue Loop(Site is null or DunsNumber == GlobalUltimateNumber)' ); 
                continue;
            }
                
            // cgp: get the parent Account for the given site    
            // If the parent is not good then try the domestic otherwise use the global as the parent.
            parentAccount = acctMap.get(siteAccount.DandBCompany.ParentOrHqDunsNumber);

            if ( parentAccount == null )
            {
                // cgp: added bracket
                System.debug(LoggingLevel.DEBUG, 'Initial parentAccount == null' );                
              if (siteAccount.DunsNumber != siteAccount.DandBCompany.DomesticUltimateDunsNumber)
              {
                  // cgp: added bracket         
                  System.debug(LoggingLevel.DEBUG, 'setting parentAccount to DomesticUltimateDunsNumber' ); 
                  parentAccount = acctMap.get(siteAccount.DandBCompany.DomesticUltimateDunsNumber);
                  // cgp: added closing bracket
              }                  
                // cgp: added closing bracket
            }
            if ( parentAccount == null )
            {
                System.debug(LoggingLevel.DEBUG, 'Secondary parentAccount == null' );               
                System.debug(LoggingLevel.DEBUG, 'setting parentAccount to GlobalUltimateDunsNumber' ); 
                // cgp: added bracket            
                parentAccount = acctMap.get(siteAccount.DandBCompany.GlobalUltimateDunsNumber);
                // cgp: added closing bracket
            }                
                
            if (parentAccount != null)
            {
                System.debug(LoggingLevel.DEBUG, 'parentAccount != null' );                             
                if (siteAccount.ParentId != parentAccount.Id)
                {
                    System.debug(LoggingLevel.DEBUG, 'siteAccount.ParentId != parentAccount.Id adding to Updates list' );                 
                    siteAccount.ParentId = parentAccount.Id;
                    updates.add(siteAccount);
                }
                else
                {
                    System.debug(LoggingLevel.INFO, 'siteAccount.ParentId == parentAccount.Id Not adding to Updates');    
                }
            }
            // cgp: added else condition
            else
            {
                System.debug(LoggingLevel.DEBUG, 'still parentAccount == null' );
            }
        }
        
        System.debug(LoggingLevel.DEBUG, 'buildAccountHierarchy: About to update '+updates.size()+' records...');
          
        // Update the hierarchy.
        if (updates.size() > 0)
        {
            DDCDnBAccountHierarchy.accountHrchyTriggerDisabled = true;
            update updates;
            DDCDnBAccountHierarchy.accountHrchyTriggerDisabled = false;
        }
            
         System.debug(LoggingLevel.DEBUG, 'SUCCESS: Done');
        return 'SUCCESS: Done';
    }

}