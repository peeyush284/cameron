/*
* @author Peeyush Awadhiya
* @date 08/20/2014
* @description : The class have test methods that tests following:
*                1. When a team member is added to the Sales team, they should automatically follow the record.
*                2. When a team member is removed, the member should "unfollow" the opportunity record.
*/

@isTest

public class OpportunityTeamMmbrFollwOpptyHelper_TEST{
    
    static testMethod void testSalesTeamMemberFollowOppty(){
           
        // Prepare the data stub
        Account acc, newAcc, newAcc1;
        CAM_Project__c project = new CAM_Project__c(); 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('PS|CPS|Acct Mgr|Primary', acc);              
        
       
        User anotherUser = [SELECT Id, isActive FROM User WHERE Id!=:UserInfo.getUserId() AND isActive = true LIMIT 1];
      
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunity
        oppty = TestUtilities.generateOpportunityWithoutInsert('In the Funnel/Best Few', 'In the Funnel', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Process Systems';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Business_Unit__c = 'CPS';
        oppty.CAM_Final_Destination__c = 'Spain';
        oppty.CAM_Project__c = project.Id;
        
        oppty.AccountId = acc.Id;
        oppty.Sub_Vendor__c = acc.Id;
        
        insert oppty; 
        
        
        oppty = [SELECT Id, CAM_Division__c, CAM_Business_Unit__c,Primary_Account_Manager_calc__c FROM Opportunity WHERE Id=:oppty.Id LIMIT 1];
        
        OpportunityTeamMember salesTeamMember = new OpportunityTeamMember(UserId = anotherUser.Id, TeamMemberRole = oppty.Primary_Account_Manager_calc__c, OpportunityId = oppty.Id);
        
        // Use case 1. When a team member is added to the Sales team, they should automatically follow the record.
        Test.startTest();
            insert salesTeamMember;
        Test.stopTest();
        
        // Now the EntitySubscription record would have been created
        
        //CGP 14-NOV-2014 deleted following line
        //System.assertEquals([SELECT Id, PARENTID, SUBSCRIBERID FROM EntitySubscription WHERE PARENTID = :oppty.Id AND SUBSCRIBERID =:anotherUser.Id].size(),1);
        
        // Use case 2. When a team member is removed, the member should "unfollow" the opportunity record.
        
        //CGP 14-NOV-2014 deleted following line  
        //delete salesTeamMember;
        
        //CGP 14-NOV-2014 deleted following line        
        //System.assertEquals([SELECT Id, PARENTID, SUBSCRIBERID FROM EntitySubscription WHERE PARENTID = :oppty.Id AND SUBSCRIBERID =:anotherUser.Id].size(),0);
        
        
    }

}