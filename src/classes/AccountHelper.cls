/**
 * @author      Salesforce {CP}
 * @date        9/4/2014
 * @description AccountHelper to support:
 *              1) DDC Account Hierarchy Structure Maintenance 
 *              2) Account Team Assignment Processing
 *
 *              
 */

public with sharing class AccountHelper {

    public static void HandleAfterInsert (List<Account> newRecords){
        system.debug('+++AccountHelper - Enter HandleAfterInsert');
        
        Map<id, id> account2UserMap = new Map<id, id>();
        
        List<AccountTeamMember> accountTeamMemberList = new List<AccountTeamMember>();
        String accountTeamRole ='';
        Id prospectRecordTypeId = Schema.SobjectType.Account.getRecordTypeInfosByName().get('Prospect Account').getRecordTypeId();
        List<Account> accounts = new List<Account>();
        list<ID>AccountID = new list<id>();
        
        /*
        for (Account acct : newRecords){
            account2UserMap.put(acct.id, userInfo.getUserid());
        }
        
        list<UserRecordAccess> URA = [select RecordId, HasEditAccess FROM UserRecordAccess where RecordId = :account2UserMap.keySet() and UserId = :account2UserMap.values()];
        map<id,UserRecordAccess> URArecs = new map<id,UserRecordAccess>();
        URArecs.putall(URA);

        for (Account acct : newRecords){
            
            if (URArecs.containsKey(acct.id))
            {
                system.debug('+++ +++HandleAfterInsert -> adding account record ');
                accounts.add(acct.clone(true,true));
            }
            // Add the accountId and OwnerId to the map for the prospect accounts
            if(acct.RecordTypeId == prospectRecordTypeId){              
                accountTeamMemberList.add(new AccountTeamMember(AccountId = acct.Id, UserId = acct.OwnerId,  TeamMemberRole = acct.Primary_Account_Manager_Calc__c));
                AccountID.add(acct.Id);
            }
        }     
        */ 
        for (Account acct : newRecords){
            
            // Add the accountId and OwnerId to the map for the prospect accounts
            if(acct.RecordTypeId == prospectRecordTypeId){              
                accountTeamMemberList.add(new AccountTeamMember(AccountId = acct.Id, UserId = acct.OwnerId,  TeamMemberRole = acct.Primary_Account_Manager_Calc__c));
                AccountID.add(acct.Id);
            }
        }    
        
        if (accounts == null || accounts.size() == 0)
        { 
            system.debug('+++ +++HandleAfterInsert -> no call forbuildAccountHierarchy ');
        }
        else
        {    
            system.debug('+++ +++HandleAfterInsert -> calling DDCDnbAccountHierarchy ');
            //DDCDnbAccountHierarchy.buildAccountHierarchy(accounts, true);
        }
        // Add all account owners to the account team.
        if(accountTeamMemberList!=null && !accountTeamMemberList.isEmpty()){
            Database.SaveResult [] results = Database.insert(accountTeamMemberList, false);
            
            // Iterate through each returned result
            for (Database.SaveResult sr : results) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted Account Team Member. AccountTeamMember ID: ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('AccountTeamMember fields that affected this error: ' + err.getFields());
                    }
                }
            } // end of for loop
            
            if (AccountID.size() > 0) {
                AccountProspect.UpdateCreatorShare (AccountID);
            }
        } // end of if(accountTeamMemberList!=null && !accountTeamMemberList.isEmpty())
        system.debug('+++AccountHelper - Exit HandleAfterInsert');
    }
    
    
    public static void HandleAfterUpdate (List<Account> newRecords, Map<Id, Account> oldRecords){
        system.debug('+++AccountHelper - Enter HandleAfterUpdate');    
        
        list<ID>AccountID = new list<id>();
        List<Account> accounts = new List<Account>();
        
        /*
        Map<id, id> account2UserMap = new Map<id, id>();
        for (Account acct : newRecords){
            account2UserMap.put(acct.id, userInfo.getUserid());
        }
        
        list<UserRecordAccess> URA = [select RecordId, HasEditAccess FROM UserRecordAccess where RecordId = :account2UserMap.keySet() and UserId = :account2UserMap.values()];
        map<id,UserRecordAccess> URArecs = new map<id,UserRecordAccess>();
        URArecs.putall(URA);

        for (Account acct : newRecords){
            
            if (URArecs.containsKey(acct.id))
            {
                system.debug('+++ +++HandleAfterUpdate -> adding account record ');
                accounts.add(acct.clone(true,true));
            }
        }   
        */      
        
       
        for (Integer i=0; i < newRecords.size(); i++)
        {
            Account acct = newrecords[i];
            {
                // If a value was provided when there was not one before do not run the hierarchy code.
                if ( oldrecords.get(newRecords[i].Id).ParentId == null && newRecords[i].ParentId != null )
                    continue;
                    
                // If a different value was passed from the edit before the trigger keep it.    
                if ( oldrecords.get(newRecords[i].Id).ParentId != null && 
                     newRecords[i].ParentId != null &&
                     oldrecords.get(newRecords[i].Id).ParentId != newRecords[i].ParentId )
                    continue;
                    
                // May need to consider something there it leaves the parent id null if
                // the user removes it.  This would make it so that it would never automatically
                // put an account in a hierarchy once it had been removed.

                //accounts.add(newRecords[i].clone(true,true));
                accounts.add(acct.clone(true,true));
            }  
        }     
        if (accounts == null || accounts.size() == 0)
        { 
            system.debug('+++ +++HandleAfterUpdate -> no call forbuildAccountHierarchy ');
        }
        else
        {
            system.debug('+++ +++HandleAfterUpdate -> calling buildAccountHierarchy ');
            //DDCDnbAccountHierarchy.buildAccountHierarchy(accounts, true); 
        }
            
        // Handle Account Team Assignments
        if(!system.isBatch()) {
            HandleAccountTeamAssignment(newRecords, oldRecords); 
        }
        system.debug('+++AccountHelper - Exit HandleAfterUpdate');  
    }
    public static void HandleAfterDelete (List<Account> delRecords){
        system.debug('+++AccountHelper - Enter HandleAfterDelete');  
            
        List<Account> accounts = new List<Account>();

        try
            {
                List<String> parents = new List<String>();
                 
                for (Account acct : delRecords)
                {
                    if (acct.DunsNumber != null)
                        parents.add(acct.DunsNumber);
                }
                accounts = [SELECT Id, Name, ParentId,
                                   DunsNumber, 
                                   DandBCompany.GlobalUltimateDunsNumber, 
                                   DandBCompany.DomesticUltimateDunsNumber, 
                                   DandBCompany.ParentOrHqDunsNumber
                              FROM Account
                             WHERE DandBCompany.ParentOrHqDunsNumber IN :parents
                           ];
            } catch (Queryexception qe) {
                // Nothing to be done.
            } 
         
        if (accounts == null || accounts.size() == 0)
        {}// I prefer positive logic 
        else
        {
            //DDCDnbAccountHierarchy.buildAccountHierarchy(accounts, true);
            }
            
        system.debug('+++AccountHelper - Exit HandleAfterDelete');     
    }
    static void HandleAccountTeamAssignment(List<Account> newRecords, Map<Id, Account>oldRecords) {
        system.debug('+++AccountHelper - Enter HandleAccountTeamAssignment');
        
        List<ID> futureIDs = new List<id>();
        List<ID> AccountIDS = new List<id>();
        
        Id ProspectRecordTypeId = Schema.Sobjecttype.Account.getRecordTypeInfosByName().get('Prospect Account').getRecordTypeId();
        Id DNBRecordTypeId = Schema.Sobjecttype.Account.getRecordTypeInfosByName().get('D&B Account').getRecordTypeId();        

        for (Account a: newRecords){
            if (a.RecordTypeID == DNBRecordTypeId && oldRecords.get(a.Id).RecordTypeID == ProspectRecordTypeId)
            { 
                AccountIDS.add(a.id);
            } 
        }
        
        for (Account a: newRecords){
            if (a.GeoCriteria__c != oldRecords.get(a.Id).GeoCriteria__c 
                || a.AssignableRoles__c != oldRecords.get(a.Id).AssignableRoles__c
                || a.Force_Account_Team_Assignments__c == TRUE) { 
                // An alignment field has been changed!
                // add the record to the FutureRecord List 
                futureIDs.add(a.id);
            } 
        }
        
        if (futureIDs.size() > 0) {
            AccountTeamAssignmentClass.AccountTeamAssignment(futureIDs);
        }
        
        if (AccountIDS.size() > 0) {
            AccountProspect.UpdateCreatorShareToRead(AccountIDS);
        }
        system.debug('+++AccountHelper - Exit HandleAccountTeamAssignment');
    }
    
 
    
}