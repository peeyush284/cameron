/**
* @author       Peeyush Awadhiya
* @date         09/25/2014
* @description  This batch class does the following:
*               1. Copies the OpportunityLineItemSchedule records into Analytic_Snapshot_Bucket_Oppty__c records.
*               2. Upon completion, it invokes the batch which copies AS Bucket Oppty Object Records into AS Forecast Oppty Object
*/

global class ASInsrtBcktOpptyIntoASFcstBcktOpptyBatch implements Database.batchable<sObject>{
    string query;
    global ASInsrtBcktOpptyIntoASFcstBcktOpptyBatch(String queryStr){
        this.query = queryStr;
    }  
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){       
        
        List<Analytic_Snapshot_Bucket_Oppty__c>asOpportunityScheduleToCopyList = new List<Analytic_Snapshot_Bucket_Oppty__c>();
        for(OpportunityLineItemSchedule schedule:(List<OpportunityLineItemSchedule>) scope){
            asOpportunityScheduleToCopyList.add(
                new Analytic_Snapshot_Bucket_Oppty__c(
                    Opportunity_Type__c = schedule.OpportunityLineItem.Opportunity.RecordType.Name,
                    Business_Unit__c = schedule.OpportunityLineItem.Opportunity.CAM_Business_Unit__c,
                    Division__c = schedule.OpportunityLineItem.Opportunity.CAM_Division__c,
                    Opportunity_ID__c = schedule.OpportunityLineItem.Opportunity.Id,
                    Opportunity_Name__c = schedule.OpportunityLineItem.Opportunity.Name,
                    Opportunity_Owner__c = schedule.OpportunityLineItem.Opportunity.Owner_Name_calc__c,
                    Schedule_Amount__c = schedule.Revenue,
                    Schedule_Date__c = schedule.ScheduleDate
                )
            );
            

        }
        // Now delete records from Analytics Snapshot
        if(asOpportunityScheduleToCopyList!=null && !asOpportunityScheduleToCopyList.isEmpty()){
            Database.SaveResult [] results = Database.insert(asOpportunityScheduleToCopyList, false);
            
            // Iterate through each returned result
            for (Database.SaveResult sr : results) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Analytic Snapshot: Bucket Oppty fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }  
    }    
    global void finish(Database.BatchableContext BC){
        // Upon completion, invoke the batch which copies the AS Bucket Oppty Object Records into AS Forecast Oppty Object.
        String querySelectStr = 'SELECT Id, Name, Business_Unit__c, Division__c, Forecast__c, Forecast_Category__c, Opportunity_ID__c,  Opportunity_Name__c, Opportunity_Owner__c,  Opportunity_Type__c, Schedule_Date__c, Schedule_Amount__c, Stage__c';
        String queryStr = querySelectStr+' FROM Analytic_Snapshot_Bucket_Oppty__c WHERE Name != \'\'';
        ASInsrtASFcstBcktOpptyToASFcstOpptyBatch batchObj = new ASInsrtASFcstBcktOpptyToASFcstOpptyBatch(queryStr);
        ID batchProcessId = Database.executeBatch(batchObj, 100);  
        
        System.debug('Executing the ASInsrtASFcstBcktOpptyToASFcstOpptyBatch with process Id:'+batchProcessId);
    }
}