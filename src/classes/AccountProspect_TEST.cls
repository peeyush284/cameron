@isTest
private class AccountProspect_TEST {
    
    // Test Inserting Real Accounts
    static TestMethod void Test1_HandleAfterInsertWithAccts()
    {
    
        Id ProspectRecordTypeId = Schema.Sobjecttype.Account.getRecordTypeInfosByName().get('Prospect Account').getRecordTypeId();
        List<Account> newAccounts = new List<Account>();
        Account myNew = new Account(name = 'Test Account ', billingCountry = 'United States', recordtypeid = ProspectRecordTypeID);
        
        newAccounts.add(myNew);

        insert newAccounts;
        
        AccountHelper.HandleAfterInsert(newAccounts);
        
        //system.AssertEqual(1,1);
    }  
    
    // Test Inserting Real Accounts
    static TestMethod void Test1_HandleAfterUpdateWithAccts()
    {    
        Id ProspectRecordTypeId = Schema.Sobjecttype.Account.getRecordTypeInfosByName().get('Prospect Account').getRecordTypeId();
        List<Account> newAccounts = new List<Account>();
        Account myNew = new Account(name = 'Test Account ', billingCountry = 'United States', recordtypeid = ProspectRecordTypeID);
        
        newAccounts.add(myNew);

        insert newAccounts;
        
        AccountHelper.HandleAfterInsert(newAccounts);

        newAccounts[0].COE_Approval__c = true;
        update newAccounts;
        
        //system.AssertEqual(1,1);
    }    
}