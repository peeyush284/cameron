/**
* @author       Peeyush Awadhiya
* @date         10/28/2014
* @description  This batch class does the following:
*               1. Queries for all opportunities and related account team members.
*               2. Updates the division specific account team owner to Account_Team_Owner__c field on opportunity. 
*/

@isTest
public class OpportunityAccountTeamMemberAssign_TEST{
    
    public static testMethod void testOpportunityAccountTeamMemberAssign(){
        // Prepare data stub
        Account acc;
        CAM_Project__c project = new CAM_Project__c(); 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('V&M|EPV|Acct Mgr|Primary', acc);              
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('In the Funnel/Best Few', 'Best Few', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Valves & Measurement';
        oppty.CAM_Business_Unit__c = 'EPV';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = project.Id;
        
        User runningUser = [SELECT Id FROM User WHERE camDivision__c = 'Valves & Measurement' AND camBusinessUnit__c = 'EPV' AND isActive = true LIMIT 1];
        
            System.runAs(runningUser){
            oppty.AccountId = acc.Id;
            insert oppty;
             
             
            Test.startTest();
                String queryStr = 'SELECT Id, AccountId, Primary_Account_Manager_calc__c, Account_Team_Owner__c FROM Opportunity WHERE End_User__r.Name!=\'Multiple Accounts\'';
                OpportunityAccountTeamMemberAssignBatch batchObj = new OpportunityAccountTeamMemberAssignBatch(queryStr);
                ID batchProcessId = Database.executeBatch(batchObj, 20);  
            Test.stopTest();
            
            oppty = [SELECT Id, OwnerId, Primary_Account_Manager_calc__c FROM Opportunity WHERE Id=:oppty.Id LIMIT 1];
            //atm = [SELECT Id, AccountId, UserId, TeamMemberRole FROM AccountTeamMember WHERE AccountId =: acc.Id AND Id =: atm.Id LIMIT 1];
            //System.assertEquals(oppty.Primary_Account_Manager_calc__c ,atm.TeamMemberRole);
            //System.assertEquals(oppty.OwnerId, atm.UserId);
        }
    }

}