/**
* @author       Peeyush Awadhiya
* @date         09/23/2014
* @description  This class does the following:
*               1. When the Quote Tracking record is created from the opportunity related list, it populates the Quote tracking fields.
*/

public with sharing class QuoteTrackingPrepopulateExtension {
    public String retURL {get;set;}
    public Id opportunityId {get;set;}
    
    public QuoteTrackingPrepopulateExtension(ApexPages.StandardController controller) {
        retURL = ApexPages.CurrentPage().getParameters().get('retURL');
    }
    public PageReference returnEditPage(){
        String objectPrefix = QuoteTracking__c.SObjectType.getDescribe().getKeyPrefix();
        system.debug('returnEditPage: retURL = ' + retURL);
        system.debug('returnEditPage: objectPrefix = ' + objectPrefix);
        PageReference returnPage = new PageReference('/' + objectPrefix + '/e');
              
        if(retURL!=null && retURL!='' && retURL!=String.valueOf('/'+objectPrefix+'/o') && !retURL.contains(String.valueOf(objectPrefix+'?fcf=')) && !retURL.contains(objectPrefix)){
            
            system.debug('retURL:  = ' + retURL);
            // CGP 30-Jan-2015 removed the following line
            //opportunityId = retURL.subString(1, retURL.length());  
            // CGP 30-Jan-2015 inserted the following line to account for accessing opportunity record from global search          
            opportunityId = retURL.subString(1, 16);  
            system.debug('returnEditPage: opportunityId = ' + opportunityId);

            //29-Jan-2015 CGP commented out the following line of code
            //List<Opportunity> opptyList = [SELECT Id, Name, End_User__c, End_User__r.Name, AccountId, Market_Segment__c, CAM_Final_Destination__c, OwnerId, Owner.Name, Primary_Account_Manager_calc__c FROM Opportunity WHERE Id = :opportunityId LIMIT 1];
            //29-Jan-2015 CGP removed AccountID from query string
            List<Opportunity> opptyList = [SELECT Id, Name, End_User__c, End_User__r.Name, Market_Segment__c, CAM_Final_Destination__c, OwnerId, Owner.Name, Primary_Account_Manager_calc__c FROM Opportunity WHERE Id = :opportunityId LIMIT 1];            
            if(opptyList!=null && !opptyList.isEmpty() && opportunityId.getSObjectType() == Opportunity.getSObjectType()){
                Opportunity oppty = opptyList[0];
                
                // Query and populate the primary account manager based on Business Unit/Division
                //List<AccountTeamMember> primaryAccountManager = [SELECT Id, UserId, TeamMemberRole, User.Name FROM AccountTeamMember WHERE AccountId =: oppty.AccountId AND TeamMemberRole =:oppty.Primary_Account_Manager_calc__c LIMIT 1];
                
                returnPage.getParameters().put(Quote_Tracking_Prepopulate_Field__c.getOrgDefaults().End_User__c+'_lkid',oppty.End_User__c);
                returnPage.getParameters().put(Quote_Tracking_Prepopulate_Field__c.getOrgDefaults().End_User__c,oppty.End_User__r.Name);
                returnPage.getParameters().put(Quote_Tracking_Prepopulate_Field__c.getOrgDefaults().Market_Segment__c,oppty.Market_Segment__c);
                returnPage.getParameters().put(Quote_Tracking_Prepopulate_Field__c.getOrgDefaults().Ultimate_Destination__c,oppty.CAM_Final_Destination__c);
                /*
                if(primaryAccountManager!=null && !primaryAccountManager.isEmpty()){
                
                    returnPage.getParameters().put(Quote_Tracking_Prepopulate_Field__c.getOrgDefaults().Outside_Sales_Person__c+'_lkid',primaryAccountManager[0].Id);
                    returnPage.getParameters().put(Quote_Tracking_Prepopulate_Field__c.getOrgDefaults().Outside_Sales_Person__c, primaryAccountManager[0].User.Name);
                }
                */
                returnPage.getParameters().put(Quote_Tracking_Prepopulate_Field__c.getOrgDefaults().Opportunity__c+'_lkid',oppty.Id);    
                returnPage.getParameters().put(Quote_Tracking_Prepopulate_Field__c.getOrgDefaults().Opportunity__c,oppty.Name);    
                        
            }
            
        }  
        returnPage.getParameters().put('retURL',retURL);
        returnPage.getParameters().put('nooverride','1');
        return returnPage;      
    }
}