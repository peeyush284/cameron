/**
* @author       Peeyush Awadhiya
* @date         07/03/2014
* @description  This class serves as webservice class to be invoked from a custom buttom on IOI object 
*               It does following:
*               1. On click of the custom button "Submit Chatter Posts":
*                   A. If Status = Submitted and not already posted, it creates a FeedItem on related account, opportunity, project records.
*                   B. Creates custom object Chatter Post records with Ids of FeedItems posted.
*                   C. Set Visibility picklist value to 1 Default Public Read only.
*
*               2. On click of the custom button "Delete Chatter Posts":
*                   A. Delete FeedItems on related account, opportunity, project records.
*                   B. Delete all custom object Chatter Post records with Ids of FeedItems posted.
*                   C. Set Visibility picklist value to 0 Private Owner Manager only.
*
*               3. On click of the custom "Submit" button:
*                   A. Updates the current owner as "Previous Owner".
*                   B. Updates the "Next Reviwer" as current Owner".
*                   C. Updates the "IOI Reviewer" on new owner as "Next Reviewer" on IOI record.
*                   D. Create Apex sharing record for IOI__c record with previous owner.
*
*               4. On click of the custom "Recall" button:
*                   A. Updates the previous owner as current owner.
*                   B. Updates the "IOI Reviewer" of new owner as "Next Reviewer".
*                   C. Updates the current owner as previous owner on IOI record.
*/

global class IOISetVisibility {
    
    
    /**
    * @author       Peeyush Awadhiya
    * @date         07/07/2014
    * @description  This method asynchronously post the chatter post of IOI information to Account, Opportunity and Project record 
    *               and saves the chatter post Id as related custom object record.
    * @param        String ioiRecordId: This argument is the id of IOI record.
    * @return       void 
    */ 
    
    webservice static string submitChatterPost(String ioiRecordId){
        String response = '';
        List<FeedItem> feedItemList = new List<feedItem>();
        IOI__c ioiRecord = new IOI__c();
        String bodyStr = '';
        List<IOI_Chatter_Feed__c> ioiChatterFeedList = new List<IOI_Chatter_Feed__c>();
        List<IOI__c> ioiRecordList = [SELECT Id, Name, OwnerId, CreatedById, Account__c,CAM_Contact__c,CAM_Contact__r.Name, Account__r.Name, Description__c, Opportunity__c, Opportunity__r.Name, Project__c, Project__r.Name, Status__c, Visibility_Level__c,Published_to_Chatter__c, CreatedBy.Name, CreatedDate FROM IOI__c WHERE Id=:ioiRecordId LIMIT 1 FOR UPDATE ];
        Set<Id>feedItemIdSet = new Set<Id>();
        Map<Id, String> objectTypeMap = new Map<Id, String>();
        
        if(ioiRecordList!=null && !ioiRecordList.isEmpty())
        {
            ioiRecord = ioiRecordList[0];
            if(ioiRecord.Status__c!='Submitted'){
                return 'You cannot submit the chatter post until the status is \'Submitted\'.';
            }
            /* Commented by Peeyush Awadhiya on 08/27/2014 to change the ownership restrictions*/
            
            else if(ioiRecord.CreatedById!=UserInfo.getUserId()){
                return 'You cannot submit the chatter post since you are not the record owner.';
            }
            
            else if(ioiRecord.Published_to_Chatter__c == true){
                return 'IOI has already been published to Chatter.';   
            }
            else {
                bodyStr +='IOI: '+ioiRecord.Name+'\n';
                bodyStr +='Description: '+ (ioiRecord.Description__c!=null?ioiRecord.Description__c:'')+'\n';
                bodyStr +='Project: '+ (ioiRecord.Project__r.Name!=null?ioiRecord.Project__r.Name:'' )+'\n';
                bodyStr +='Account: '+ (ioiRecord.Account__r.Name!=null?ioiRecord.Account__r.Name:'') +'\n';
                bodyStr +='Opportunity: '+(ioiRecord.Opportunity__r.Name !=null?ioiRecord.Opportunity__r.Name:'' )+'\n';
                bodyStr +='Contact: '+(ioiRecord.CAM_Contact__r.Name !=null?ioiRecord.CAM_Contact__r.Name:'' )+'\n';
                bodyStr +='IOI.Created: '+ioiRecord.CreatedBy.Name+', '+ioiRecord.CreatedDate.format() ;
                
                if(ioiRecord.Account__c!=null){
                    feedItemList.add(new FeedItem(parentId = ioiRecord.Account__c, Body = bodyStr));
                } 
                if(ioiRecord.Opportunity__c!=null){
                    feedItemList.add(new FeedItem(parentId = ioiRecord.Opportunity__c, Body = bodyStr));
                } 
                if(ioiRecord.Project__c!=null){
                    feedItemList.add(new FeedItem(parentId = ioiRecord.Project__c, Body = bodyStr));
                }
                if(ioiRecord.CAM_Contact__c!=null){
                    feedItemList.add(new FeedItem(parentId = ioiRecord.CAM_Contact__c, Body = bodyStr));
                }
                if(feedItemList!=null && !feedItemList.isEmpty()){
                    Database.SaveResult [] results = Database.insert(feedItemList,false);
                    
                    for(Database.SaveResult result:results){
                        if(result.isSuccess()){
                            ioiChatterFeedList.add(new IOI_Chatter_Feed__c(IOI__c = ioiRecord.Id, Chatter_Post__c = bodyStr, Chatter_Feed_Id__c = result.getId()));    
                            feedItemIdSet.add(result.getId());
                        }
                        else{
                            for(Database.Error err : result.getErrors()) {
                                System.debug('The following error has occurred.');                    
                                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                System.debug('FeedItem fields that affected this error: ' + err.getFields());
                            }
                        } // end of if
                    }
                    
                } // End of if statement   
                
                // Populate the Object type map
                
                for(FeedItem feed:[SELECT Id, ParentId FROM FeedItem WHERE ID IN:feedItemIdSet]){
                    if(feed.ParentId.getSObjectType() == Account.sObjectType){
                        objectTypeMap.put(feed.Id,'Account');    
                    }
                    if(feed.ParentId.getSObjectType() == Opportunity.sObjectType){
                        objectTypeMap.put(feed.Id,'Opportunity');    
                    }
                    if(feed.ParentId.getSObjectType() == CAM_Project__c.sObjectType){
                        objectTypeMap.put(feed.Id,'Project');    
                    }
                    if(feed.ParentId.getSObjectType() == Contact.sObjectType){
                        objectTypeMap.put(feed.Id,'Contact');    
                    }
                }
                
                // Now update the Object field
                
                for(IOI_Chatter_Feed__c chatterPost:ioiChatterFeedList){
                    if(objectTypeMap.containsKey(chatterPost.Chatter_Feed_Id__c)){
                        chatterPost.Object__c = objectTypeMap.get(chatterPost.Chatter_Feed_Id__c);
                    }
                
                }
                
                if(ioiChatterFeedList!=null && !ioiChatterFeedList.isEmpty()){
                    Database.SaveResult [] results = Database.insert(ioiChatterFeedList,false); 
                    
                    for(Database.SaveResult result:results){
                        if(!result.isSuccess()){
                           
                            for(Database.Error err : result.getErrors()) {
                                System.debug('The following error has occurred.');                    
                                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                System.debug('IOI Chatter Feed fields that affected this error: ' + err.getFields());
                            }
                        } // end of if
                    } 
                } 
                
                ioiRecord.Visibility_Level__c = System.Label.IOI_Set_Visibility_to_1_Default_Public_Read_only;
                ioiRecord.Published_to_Chatter__c = true;
                update ioiRecord;
                return 'Success'; 
                
            } // end of else 
            
                
        }
        
        return response;
        
    }
    
    /**
    * @author       Peeyush Awadhiya
    * @date         07/07/2014
    * @description  This method asynchronously deletes the chatter post of IOI information posted to Account, Opportunity and Project record 
    *               and saves the chatter post Id as related custom object record.
    * @param        String ioiRecordId: This argument is the id of IOI record.
    * @return       void 
    */ 
    
    
    
    webservice static string deleteChatterPost(String ioiRecordId){
        String response = 'Deleting Chatter Posts.Please wait.......';
        
        List<FeedItem> feedItemsToDelete = new List<FeedItem>();
        Set<Id> feedItemIdSet = new Set<Id>();
        IOI__c ioiRecord = new IOI__c();
        
        List<IOI__c> ioiRecordList = [SELECT Id, OwnerId,CreatedById, Name, Account__c, Account__r.Name, Description__c, Opportunity__c, Opportunity__r.Name, Project__c, Project__r.Name, Status__c, Visibility_Level__c,Published_to_Chatter__c, CreatedBy.Name, CreatedDate FROM IOI__c WHERE Id=:ioiRecordId LIMIT 1 FOR UPDATE ];
        
        if(ioiRecordList!=null && !ioiRecordList.isEmpty())
        {
            ioiRecord = ioiRecordList[0];
            
        }     
        if(ioiRecord!=null && ioiRecord.CreatedById!=UserInfo.getUserId()){
            response = 'You cannot remove the chatter post since you have not created the IOI record.';
            return response;
        }
        else{    
            List<IOI_Chatter_Feed__c> chatterFeedList = new List<IOI_Chatter_Feed__c>();
            
            for(IOI_Chatter_Feed__c chatterFeed:[
                                                    SELECT IOI__c, Chatter_Feed_Id__c
                                                    FROM IOI_Chatter_Feed__c
                                                    WHERE IOI__c = :ioiRecordId
                                                ]){
                                                    feedItemIdSet.add(chatterFeed.Chatter_Feed_Id__c);
                                                    chatterFeedList.add(chatterFeed);
                                                }
            feedItemsToDelete = new List<FeedItem>([SELECT Id FROM FeedItem WHERE ID IN :feedItemIdSet]);
            
            
            
            if(feedItemsToDelete!=null && !feedItemsToDelete.isEmpty()){
                
                delete feedItemsToDelete; 
      
            }
            
            if(chatterFeedList!=null && !chatterFeedList.isEmpty()){
                delete chatterFeedList;
            }
                
            
           
            
            ioiRecord.Published_to_Chatter__c = false;
            ioiRecord.Visibility_Level__c = System.Label.IOI_Set_Visibility_to_0_Private_Owner_Manager_only;
            update ioiRecord;     
             
        }
        
        return response; 
    }
    
    /**
    * @author       Peeyush Awadhiya
    * @date         07/014/2014
    * @description  This method asynchronously does the following:
    * 
    *                   A. Updates the current owner as "Previous Owner".
    *                   B. Updates the "Next Reviewer" as current Owner".
    *                   C. Updates the "IOI Reviewer" on new owner as "Next Reviewer" on IOI record.
    * @param        String ioiRecordId: This argument is the id of IOI record.
    * @return       String response 
    */ 
    
    webservice static String assignReviewers(String ioiRecordId){
        string response = '';
        IOI__c ioiRecord = new IOI__c();
        
        List<IOI__c> ioiRecordList = [SELECT Id, OwnerId, Name, Previous_Owner__c,Next_Reviewer__c, Next_Reviewer__r.IOI_Reviewer__c,Status__c, Visibility_Level__c,Published_to_Chatter__c FROM IOI__c WHERE Id=:ioiRecordId LIMIT 1 FOR UPDATE ];
        
        if(ioiRecordList!=null && !ioiRecordList.isEmpty())
        {
            ioiRecord = ioiRecordList[0];
            if(ioiRecord.Next_Reviewer__c == null){
                return 'Cannot submit if Next Reviewer is not populated.';
            }
            else if(ioiRecord.OwnerId!=UserInfo.getUserId()){
                return 'Cannot submit if the current user is not the owner of IOI record.';
            }
            else{
            
                ioiRecord.Previous_Owner__c = ioiRecord.OwnerId;
                ioiRecord.OwnerId = ioiRecord.Next_Reviewer__c;
                ioiRecord.Next_Reviewer__c = ioiRecord.Next_Reviewer__r.IOI_Reviewer__c;
                ioiRecord.Status__c = 'Submitted';
                
                update ioiRecord;
                
                // Now share the record with previous owner.
                IOI__Share shareIOIRecordWithPreviousOwner = new IOI__Share();
                shareIOIRecordWithPreviousOwner.AccessLevel = 'read';
                shareIOIRecordWithPreviousOwner.ParentId = ioiRecord.Id;
                shareIOIRecordWithPreviousOwner.UserOrGroupId = ioiRecord.Previous_Owner__c;
                shareIOIRecordWithPreviousOwner.RowCause = Schema.IOI__Share.RowCause.Previous_Owner_Access__c;
                
                try{
                    insert shareIOIRecordWithPreviousOwner;
                }catch(Exception e){
                    System.debug('Exception caught while inserting apex managed sharing record:'+e.getMessage());
                }
                response = 'Success';
            }
        } 
        return response;    
    }
    
    /**
    * @author       Peeyush Awadhiya
    * @date         07/15/2014
    * @description  This method asynchronously does the following:
    * 
    *                  A. Updates the previous owner as current owner.
    *                  B. Updates the "IOI Reviewer" of new owner as "Next Reviewer".
    *                  C. Updates the current owner as previous owner on IOI record.
    * @param        String ioiRecordId: This argument is the id of IOI record.
    * @return       String
    */ 
    
    webservice static String validateCurrentUser(String ioiRecordId){
        
        String response = '';
        
        IOI__c ioiRecord = new IOI__c();
        
        List<IOI__c> ioiRecordList = [SELECT Id, OwnerId, Name, Previous_Owner__c,Next_Reviewer__c, Previous_Owner__r.IOI_Reviewer__c, LastModifiedById, Status__c FROM IOI__c WHERE Id=:ioiRecordId LIMIT 1 ];
        
        if(ioiRecordList!=null && !ioiRecordList.isEmpty())
        {
            ioiRecord = ioiRecordList[0];
            if(ioiRecord.Status__c != 'Submitted'){
                return 'You cannot recall if the status is not \'Submitted\'';
            }
            else if(ioiRecord.Previous_Owner__c != UserInfo.getUserId()){
                return 'You cannot recall if you are not the previous owner.';
            }
            else if(ioiRecord.OwnerId == ioiRecord.LastModifiedById){
                return 'You cannot recall if the IOI record has been updated by the current owner';
            }
            else{
                ioiRecord.Next_Reviewer__c = ioiRecord.Previous_Owner__r.IOI_Reviewer__c;
                Id newOwner = ioiRecord.Previous_Owner__c;
                ioiRecord.Previous_Owner__c = ioiRecord.OwnerId;
                ioiRecord.OwnerId = newOwner;
                ioiRecord.Status__c ='Recalled';
                update ioiRecord;
                response = 'Success';    
            }
            return response;
            
        }
        else return response;
        
    }
    
}