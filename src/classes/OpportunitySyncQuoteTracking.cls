global class OpportunitySyncQuoteTracking {
    
    @future
    public static void SyncQuoteTrackingRecords(List<ID> OppRecordIds) {
        list<QuoteTracking__Share> QTSharingList = new list<QuoteTracking__Share>();
        list<QuoteTracking__Share> QTSharingSet = new list<QuoteTracking__Share>();
        Map<Id, Id> QTOwnerAdditionMap = new Map<Id,Id>();            

        // Gather related Quote Tracking Records
        list<Opportunity> lOppRecs = [Select id, End_User__c, CAM_Final_Destination__c
                                From opportunity  
                                where id in :OppRecordIds];
                                
        map<id, opportunity> mOppRecs = new Map<ID, Opportunity>(lOppRecs);                                 
                                
        list<QuoteTracking__c> qtRecs = [Select id, Opportunity__c, End_User__c, opportunity__r.ownerid, OwnerId
                                From QuoteTracking__c  
                                where Opportunity__c in :OppRecordIds
                                and Division_Calc__c = 'Process Systems'];
                                
        list<quotetracking__c> updQtRecs = new list<quotetracking__c>();
        opportunity t_opp;
                                
        for (QuoteTracking__c qt: qtRecs){
            t_opp = mOppRecs.get(qt.Opportunity__c);
            
            if (qt.end_user__c != t_opp.end_user__c) {                
                qt.end_user__c = t_opp.end_user__c;
                qt.Ultimate_Destination__c = (t_opp.CAM_Final_Destination__c != null) ? t_opp.CAM_Final_Destination__c : 'Unknown';
                updQtRecs.add(qt);
            }
            
            if(qt.opportunity__r.ownerid!=qt.OwnerId){  
                QTOwnerAdditionMap.put(qt.Id, qt.opportunity__r.ownerid); 
                System.debug('+++ Found Opp Owner to add to Share ' + qt.opportunity__r.ownerid);
            }
        }       
        if (updQtRecs.size() > 0){
            update updQtRecs;
        } 
        
        System.debug('+++ Before Change of Share ');
        for(QuoteTracking__Share share: [
            SELECT Id, UserOrGroupId,RowCause, ParentID, AccessLevel
            FROM QuoteTracking__Share   
            WHERE ParentID IN :QTOwnerAdditionMap.keySet()
            AND UserOrGroupId IN :QTOwnerAdditionMap.values()
            AND AccessLevel = 'Read']){
            share.AccessLevel = 'Edit';
            QTSharingSet.add(share); 
            System.debug('+++ adding QTSharingSet record ');
        }
        System.debug('+++ After Change of Share ');       
        
        if(QTSharingSet==null || QTSharingSet.isEmpty())
        {
            System.debug('+++ QTSharingSet is empty ');
            QuoteTracking__Share Share2 = new QuoteTracking__Share();
            for (QuoteTracking__c qt: qtRecs){  
                System.debug('+++ Creating New Share for ' + qt.opportunity__r.ownerid);
                Share2.UserOrGroupId = qt.opportunity__r.ownerid;
                //Share2.RowCause = 'Rule'; default to 'Manual'
                Share2.ParentID = qt.id;
                Share2.AccessLevel = 'Edit';
                System.debug('QuoteTracking__Share: Adding to QTShareSet');
                System.debug('QuoteTracking__Share: UserOrGroupId' + Share2.UserOrGroupId);
                System.debug('QuoteTracking__Share: RowCause' + Share2.RowCause);
                System.debug('QuoteTracking__Share: ParentID' + Share2.ParentID);            
                System.debug('QuoteTracking__Share: AccessLevel' + Share2.AccessLevel);             
                QTSharingSet.add(Share2);                     
            }              
        }
        else {
            System.debug('+++ QTSharingSet is not empty ');
        }
        
        // ensure no duplicates are in the Upsert list
        for (integer idx = 0; idx<QTSharingSet.size(); idx++)
        {
            boolean Found = false;
            for (integer jdx = 0; jdx<QTSharingList.size(); jdx++)
            {
                if (QTSharingList[jdx].parentid == QTSharingSet[idx].parentid 
                    && QTSharingList[jdx].UserOrGroupId == QTSharingSet[idx].UserOrGroupId
                    && QTSharingList[jdx].RowCause == QTSharingSet[idx].RowCause
                    && QTSharingList[jdx].AccessLevel == QTSharingSet[idx].AccessLevel)
                {
                    Found = True;
                }
            }   
            if (!found)
            {
                QTSharingList.add(QTSharingSet[idx]);
            }
        }
        //QTSharingList.addAll(QTSharingSet);
        // Now share the opportunity and grant read/write access to secondary sales.
        if(QTSharingList!=null && !QTSharingList.isEmpty()){
            Database.UpsertResult [] results = Database.upsert(QTSharingList, false);
                    
            // Iterate through each returned result
            for (Database.UpsertResult sr : results) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed.
                    System.debug('Successfully shared QT record. QuoteTracking__Share ID: ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('QuoteTracking__Share fields that affected this error: ' + err.getFields());
                    }
                }
            } // End of for loop.                
        } // End of if(QTSharingList!=null && !QTSharingList.isEmpty())                                           
    }
    
    @future
    public static void SyncQuoteTrackingRecordsToOpp(List<ID> QuoteTrackingIDs) {
        list<QuoteTracking__Share> QTSharingList = new list<QuoteTracking__Share>();
        list<QuoteTracking__Share> QTSharingSet = new list<QuoteTracking__Share>();
        Map<Id, Id> QTOwnerAdditionMap = new Map<Id,Id>();            
    
        // Gather related Quote Tracking Records
        list<QuoteTracking__c> QTRecs = [Select id, 
                                                End_User__c, 
                                                Ultimate_Destination__c, 
                                                Opportunity__r.end_user__c, 
                                                Opportunity__r.CAM_Final_Destination__c,
                                                opportunity__r.ownerid, 
                                                OwnerId
                                From QuoteTracking__c  
                                where id in :QuoteTrackingIDs];  
                                
        list<QuoteTracking__c> updQtRecs = new list<QuoteTracking__c>();                                                    
                                
        for (QuoteTracking__c qt: QTRecs){
            system.debug('+++ qt.id = ' + qt.id);
            system.debug('+++ qt.Opportunity__r.end_user__c = ' + qt.Opportunity__r.end_user__c);
            system.debug('+++ qt.Opportunity__r.CAM_Final_Destination__c = ' + qt.Opportunity__r.CAM_Final_Destination__c);
            
            if (qt.end_user__c != qt.Opportunity__r.end_user__c || qt.Ultimate_Destination__c != qt.Opportunity__r.CAM_Final_Destination__c) {                
                qt.end_user__c = qt.Opportunity__r.end_user__c;
                // if the opp final destination is not null, set the ultimate destination Opp's Final Destination, else 'Unknown'
                qt.Ultimate_Destination__c = (qt.Opportunity__r.CAM_Final_Destination__c != null) ? qt.Opportunity__r.CAM_Final_Destination__c : 'Unknown';
                updQtRecs.add(qt);
            }
            
            if(qt.opportunity__r.ownerid!=qt.OwnerId){  
                QTOwnerAdditionMap.put(qt.Id, qt.opportunity__r.ownerid); 
                System.debug('+++ Found Opp Owner to add to Share ' + qt.opportunity__r.ownerid);
            }
            
        }       
        if (updQtRecs.size() > 0){
            update updQtRecs;
        }    
        
        System.debug('+++ Before Change of Share ');
        for(QuoteTracking__Share share: [
            SELECT Id, UserOrGroupId,RowCause, ParentID, AccessLevel
            FROM QuoteTracking__Share   
            WHERE ParentID IN :QTOwnerAdditionMap.keySet()
            AND UserOrGroupId IN :QTOwnerAdditionMap.values()
            AND AccessLevel = 'Read']){
            share.AccessLevel = 'Edit';
            QTSharingSet.add(share); 
            System.debug('+++ adding QTSharingSet record ');
        }
        System.debug('+++ After Change of Share ');       
        
        if(QTSharingSet==null || QTSharingSet.isEmpty())
        {
            System.debug('+++ QTSharingSet is empty ');
            QuoteTracking__Share Share2 = new QuoteTracking__Share();
            for (QuoteTracking__c qt: qtRecs){  
                System.debug('+++ Creating New Share for ' + qt.opportunity__r.ownerid);
                Share2.UserOrGroupId = qt.opportunity__r.ownerid;
                //Share2.RowCause = 'Rule'; default to 'Manual'
                Share2.ParentID = qt.id;
                Share2.AccessLevel = 'Edit';
                System.debug('QuoteTracking__Share: Adding to QTShareSet');
                System.debug('QuoteTracking__Share: UserOrGroupId' + Share2.UserOrGroupId);
                System.debug('QuoteTracking__Share: RowCause' + Share2.RowCause);
                System.debug('QuoteTracking__Share: ParentID' + Share2.ParentID);            
                System.debug('QuoteTracking__Share: AccessLevel' + Share2.AccessLevel);             
                QTSharingSet.add(Share2);                     
            }              
        }
        else {
            System.debug('+++ QTSharingSet is not empty ');
        }
        
        // ensure no duplicates are in the Upsert list
        for (integer idx = 0; idx<QTSharingSet.size(); idx++)
        {
            boolean Found = false;
            for (integer jdx = 0; jdx<QTSharingList.size(); jdx++)
            {
                if (QTSharingList[jdx].parentid == QTSharingSet[idx].parentid 
                    && QTSharingList[jdx].UserOrGroupId == QTSharingSet[idx].UserOrGroupId
                    && QTSharingList[jdx].RowCause == QTSharingSet[idx].RowCause
                    && QTSharingList[jdx].AccessLevel == QTSharingSet[idx].AccessLevel)
                {
                    Found = True;
                }
            }   
            if (!found)
            {
                QTSharingList.add(QTSharingSet[idx]);
            }
        }
        //QTSharingList.addAll(QTSharingSet);
        // Now share the opportunity and grant read/write access to secondary sales.
        if(QTSharingList!=null && !QTSharingList.isEmpty()){
            Database.UpsertResult [] results = Database.upsert(QTSharingList, false);
                    
            // Iterate through each returned result
            for (Database.UpsertResult sr : results) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed.
                    System.debug('Successfully shared QT record. QuoteTracking__Share ID: ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('QuoteTracking__Share fields that affected this error: ' + err.getFields());
                    }
                }
            } // End of for loop.                
        } // End of if(QTSharingList!=null && !QTSharingList.isEmpty())                                           
                             
    }    

}