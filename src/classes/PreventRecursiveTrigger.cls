/**
 * @author      Peeyush Awadhiya
 * @date        09/09/2014
 * @description This class has a static boolean variable which prevents the after update triggger 
 *              to fire again, in case the record is updated via workflow field update.
 *
 */


public Class PreventRecursiveTrigger{

    private static boolean run = true;
    
    public static boolean runOnce(){
        if(run){
            run = false;
            return true;
        } 
        else{
            return run;
        }   
    }

}