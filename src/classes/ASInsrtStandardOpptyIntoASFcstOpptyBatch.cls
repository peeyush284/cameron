/**
* @author       Peeyush Awadhiya
* @date         09/25/2014
* @description  This batch class does the following:
*               1. Copies the opportunity records into AS Forecast Oppty Object
*
*/

global class ASInsrtStandardOpptyIntoASFcstOpptyBatch implements Database.batchable<sObject>{
    string query;
    global ASInsrtStandardOpptyIntoASFcstOpptyBatch(String queryStr){
        this.query = queryStr;
    }  
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){       
        
        List<Analytic_Snapshot_Forecast_Oppty__c> asForecastOpportunityToCopyList = new List<Analytic_Snapshot_Forecast_Oppty__c>();
        for(Opportunity oppty:(List<Opportunity>) scope){
            
            Analytic_Snapshot_Forecast_Oppty__c temp = new Analytic_Snapshot_Forecast_Oppty__c(
                Opportunity_Type__c = oppty.RecordType.Name,
                Amount__c = oppty.Amount,
                Booking_Date__c = oppty.CloseDate,
                Business_Unit__c = oppty.CAM_Business_Unit__c,
                Division__c = oppty.CAM_Division__c,
                Forecast_Category__c = oppty.ForecastCategoryName,
                Opportunity_ID__c = oppty.Id,
                Opportunity_Name__c = oppty.Name,
                Opportunity_Owner__c = oppty.Owner_Name_calc__c,
                Stage__c = oppty.StageName
                
            );

            if(oppty.StageName =='Closed Won'){
                temp.Amount__c = 0;
                temp.Forecast_Amount__c = 0;
                temp.Closed_Amount__c = oppty.Amount;
            }
            else{
                temp.Amount__c = oppty.Amount;
                temp.Forecast_Amount__c = oppty.CAM_Forecast_Amount__c;
                temp.Closed_Amount__c = 0;
            }
            asForecastOpportunityToCopyList.add(temp);
        }
        
        // Now delete records from Analytics Snapshot
        if(asForecastOpportunityToCopyList!=null && !asForecastOpportunityToCopyList.isEmpty()){
            Database.SaveResult [] results = Database.insert(asForecastOpportunityToCopyList, false);
            
            // Iterate through each returned result
            for (Database.SaveResult sr : results) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Analytic Snapshot: Forecast Oppty fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }  
    }    
    global void finish(Database.BatchableContext BC){
       
    }
}