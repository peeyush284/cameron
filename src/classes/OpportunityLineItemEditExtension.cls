/**
 * @author      Peeyush Awadhiya
 * @date        08/06/2014
 * @description Visualforce extension for editing opportunity line item characteristics.
 *
 */


global with sharing class OpportunityLineItemEditExtension {
  
    public OpportunityLineItem opptyLineItem {get;set;}
    
    public List<PicklistValues__c> charactersticsList {get;set;}
    public String selectedBrand {get;set;}
    public String selectedModel {get;set;}
    public String selectedBrandStr {get;set;}
    public String selectedModelStr {get;set;}
    
    public boolean showEditMode {get;set;}
    public Map<String, String> fieldMap {get;set;}
    public String queryStr {get;set;}
    
    public Map<String, List<SelectOption>> fieldPickList {get;set;}
    public String selectedPicklistId {get;set;}
    public Map<String, boolean> showCustomQuantity {get;set;}
    public String returnURL {get;set;}
    public Map<String,String> pickListValueMap {get;set;}
    
    public String selectedPicklistAPI {get;set;}
    public Integer marketRequestedDeliveryMonth {get;set;}
    public Integer marketRequestedDeliveryWeek {get;set;}
    public String customDeliveryDate{get;set;}
    public boolean showErrorMsg {get;set;}  
    public boolean disableForm {get;set;}
    
    public Map<String, List<PicklistValues__c>> charactersticMapAdded {get;set;}
    public Map<String,Id> fieldAPIRecordMap {get;set;}
    public List<SelectOption> invoiceLocationOptions {get;set;}
    public Map<String, List<Schema.FieldSetMember>> fieldSetMap{get;set;} 
 
    public Set<String> queryFields{get;set;}
    
    public OpportunityLineItemEditExtension(ApexPages.StandardController controller) {
        system.debug('---> OpportunityLineItemEditExtension.OpportunityLineItemEditExtension');
        
        // Make the page IE compatible
        Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=Edge'); 
        
        // Get the record
        queryFields = new Set<String>{
            'Product_Division__c','Group__c','Opportunity.CloseDate','Aftermarket__c',
            'Invoice_Location__c','Requested_Shipment_Date__c','Opportunity_Business_Unit_Picklist__c',
            'Market_Requested_Delivery_Date__c','Invoice_Location__c','Product_Characterstics__c','Product_Characterstics_Alt__c',
            'Market_Requested_Delivery_Months__c','Market_Requested_Delivery_Weeks__c','Market_Requested_Delivery_Date__c',
            'Rig_Type__c','Forecasted__c','Order_Type__c', 'Brand__c','Model__c','Quantity__c','UnitPrice', 'Opportunity.Primary_Account_Manager_calc__c',
            'Quantity','Product_Characterstics__c', 'PricebookEntry.Product2.Name','PricebookEntry.Product2.Division__c','Opportunity.AccountId', 
            'OpportunityId','Product_Hierarchy_calc__c','Opportunity.RecordType.Name', 'Opportunity.StageName', 'Opportunity.isClosed'
        }; 
        
        showErrorMsg = false;
        disableForm = false;
        fieldSetMap = new Map<String, List<Schema.FieldSetMember>>();                                                              
        for(Schema.FieldSet fs:Schema.SObjectType.OpportunityLineItem.fieldSets.getMap().values()){
            
            fieldSetMap.put(fs.getLabel(),fs.getFields());
            for(Schema.FieldSetMember f : fs.getFields()) {
                queryFields.add(f.getFieldPath());
            }

        }
         
        List<String> queryFieldList = new List<String>();
            
        queryFieldList.addAll(queryFields);
        
        if(!Test.isRunningTest()){
           
            if(queryFieldList!=null && !queryFieldList.isEmpty()){
                controller.addFields(queryFieldList);
            }
        }   
        
        
        
        opptyLineItem = (OpportunityLineItem)controller.getRecord(); 
        
        // If no account primary manager is found, then throw an error on the page
        List<AccountTeamMember> primaryAccountManager = [SELECT Id FROM AccountTeamMember WHERE AccountId =: opptyLineItem.Opportunity.AccountId AND TeamMemberRole =:opptyLineItem.Opportunity.Primary_Account_Manager_calc__c LIMIT 1];
        if(primaryAccountManager !=null && !primaryAccountManager.isEmpty()){
        }
        else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,System.Label.Assign_Opportunity_To_Account_Team_Owner_Error_Message);
            ApexPages.addMessage(myMsg);
            showErrorMsg = true;
            disableForm = true;      
        }
        
        // If opportunity is closed then don't allow users to edit product characterisitics.
        if(opptyLineItem.Opportunity.isClosed){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,System.Label.Opportunity_Product_Closed_Opportunity);
            ApexPages.addMessage(myMsg);
            showErrorMsg = true;  
        }
        
        selectedBrand = opptyLineItem.Brand__c;
        selectedModel = opptyLineItem.Model__c;
        customDeliveryDate = formatDate(String.valueOf(opptyLineItem.Market_Requested_Delivery_Date__c));
        
        showCustomQuantity = new Map<String, boolean>();

        for(Division_Settings__c showQuantity: Division_Settings__c.getall().values()){
            showCustomQuantity.put(showQuantity.Name, showQuantity.Use_Standard_Quantity__c);  
        }
        
        pickListValueMap = new Map<String, String>();
        fieldMap = new Map<String, String>();
        fieldPickList = new Map<String, List<SelectOption>>();
        invoiceLocationOptions = new List<SelectOption>();
        charactersticsList = new List<PicklistValues__c>();
        
        returnURL = String.valueOf(System.URL.getSalesforceBaseUrl().toExternalForm() +'/'+opptyLineItem.OpportunityId);
        showEditMode = false;
        
        populateCharactersticBasedOnFormula();
        
          
    }
    public String formatDate(String dateVal){
        system.debug('---> OpportunityLineItemEditExtension.formatDate');
        String returnFormat = '';
        if(dateVal!=null && dateVal!=''){
            List<String> splitStringArray = dateVal.split('-');
            if(splitStringArray!=null && splitStringArray.size()==3){
                returnFormat = splitStringArray[1]+'/'+splitStringArray[2]+'/'+splitStringArray[0];
            }
        }
        return returnFormat;
    }
    public void calculateRequestedDeliveryDate(){
        system.debug('---> OpportunityLineItemEditExtension.calculateRequestedDeliveryDate');
        system.debug('\t opptyLineItem.Opportunity.CloseDate = ' + opptyLineItem.Opportunity.CloseDate);
        system.debug('\t marketRequestedDeliveryMonth = ' + marketRequestedDeliveryMonth);
        Date calculatedDate = opptyLineItem.Opportunity.CloseDate.addMonths(marketRequestedDeliveryMonth);
        if(marketRequestedDeliveryMonth!=null){
            
            customDeliveryDate = String.valueOf(Date.newInstance(calculatedDate.year(),calculatedDate.month(),Date.daysInMonth(calculatedDate.year(),calculatedDate.month())).format());
        }
        
    }
    
    public void calculateRequestedDeliveryDateInWeeks(){
        system.debug('---> OpportunityLineItemEditExtension.calculateRequestedDeliveryDateInWeeks');
        if(marketRequestedDeliveryWeek!=null){
            
            customDeliveryDate = String.valueOf(opptyLineItem.Opportunity.CloseDate.addDays(marketRequestedDeliveryWeek*7).format());
        }
        
    }
    
    
    public void populateCharactersticBasedOnFormula(){
        system.debug('---> OpportunityLineItemEditExtension.populateCharactersticBasedOnFormula');
        // Get the product record
        List<PicklistValues__c> controllingRecord = [SELECT Id FROM PicklistValues__c WHERE External_Id__c = :opptyLineItem.Product_Hierarchy_calc__c LIMIT 1];        
        
        if(controllingRecord !=null && !controllingRecord.isEmpty()){
             system.debug('ControllingRecord = ' + controllingRecord[0].Id);
             system.debug('opptyLineItem.Product_Hierarchy_calc__c = ' + opptyLineItem.Product_Hierarchy_calc__c);             
             populateLayoutData(controllingRecord[0].Id);
        }
    }
    public PageReference cancelEdit(){
        system.debug('---> OpportunityLineItemEditExtension.cancelEdit');
        return new PageReference('/'+opptyLineItem.OpportunityId);
    }
    
    public void populateLayoutData(String selectedId){
        system.debug('---> OpportunityLineItemEditExtension.populateLayoutData');
        fieldAPIRecordMap = new Map<String,Id>();
        fieldPickList = new Map<String, List<SelectOption>>();
        if(selectedId!=null){
            fieldMap = new Map<String,String>();
            queryStr ='SELECT Id, Product_Division__c,Product_Characterstics__c,Product_Characterstics_Alt__c';
            Id parentId = selectedId;
            
            for(PicklistValues__c pickListRec:[
                                                  SELECT Id, Name, Field_API__c, Parent_Pick_List_Value__c, Type__c, Field_Type__c, Field_Sequence__c,Hierarchy_Level__c,
                                                  (
                                                      SELECT Id, Name, Field_API__c, Parent_Pick_List_Value__c, Type__c, Parent_API__c, Hierarchy_Level__c
                                                      FROM Pick_List_Values__r
                                                      WHERE Type__c = 'CHARVAL'
                                                      AND Parent_API__c !=''
                                                      AND Parent_Field_Type__c = 'Picklist'
                                                      ORDER BY Field_Sequence__c ASC
                                                  )
                                                  FROM PicklistValues__c
                                                  WHERE Type__c = 'CHARTYPE'
                                                  AND Parent_Pick_List_Value__c =:parentId
                                                  ORDER BY Hierarchy_Level__c, Field_Sequence__c ASC
                                            ]){
                                            
                                            // Populate the API name-value map
                                            fieldMap.put(pickListRec.Field_API__c, '');
                                            // Populate the field list
                                            charactersticsList.add(pickListRec);
                                            fieldAPIRecordMap.put(pickListRec.Field_API__c, pickListRec.Id);
                                            queryStr+=', '+pickListRec.Field_API__c;
       
                                            if(pickListRec.Field_Type__c == 'Picklist' && pickListRec.Pick_List_Values__r!=null){
                                                fieldPickList.put(pickListRec.Field_API__c, new List<SelectOption>{new SelectOption('-- None --','-- None --')});
                                                for(PicklistValues__c pickListValue:pickListRec.Pick_List_Values__r){
                                                    if(fieldPickList.containsKey(pickListRec.Field_API__c)){
                                                        fieldPickList.get(pickListRec.Field_API__c).add(new SelectOption(pickListValue.Name,pickListValue.Name));   
                                                    }
                                                    else{
                                                        fieldPickList.put(pickListRec.Field_API__c, new List<SelectOption>{new SelectOption(pickListValue.Name,pickListValue.Name)});
                                                    }
                                                    pickListValueMap.put(String.valueOf(pickListRec.Field_API__c+':'+pickListValue.Name),pickListValue.Id);
                                                    
                                                } 
                                                
                                                   
                                            }
            
            } // End of for loop
            
            queryStr+=' FROM OpportunityLineItem WHERE Id='+'\''+opptyLineItem.Id+'\' LIMIT 1';
            
            system.debug('queryStr = ' + queryStr);
            
            List<OpportunityLineItem> opList = Database.query(queryStr);
            
            if(opList!=null && !opList.isEmpty()){
            	//<AK 4/28/15 : Added 'Drilling' Product Division in the where clause to fix the issue where Characteristics Picklist weren't populated with the persisted values on OpportunityLineOrder
                //<AK 4/28/15 : Commenting the if condition to allow all!
                //if(opList[0].Product_Division__c =='CPS' || opList[0].Product_Division__c =='CPS Aftermarket' || opList[0].Product_Division__c =='Wellhead' || opList[0].Product_Division__c =='Midstream' || opList[0].Product_Division__c =='Drilling'){
                
                    system.debug('product Division is  = ' + opList[0].Product_Division__c);
                    
                    for(String fieldAPI:fieldMap.keySet()){
                        if(opList[0].get(fieldAPI)!=null){
                            system.debug('fieldMap being set to   = ' + String.valueOf(opList[0].get(fieldAPI)));
                            fieldMap.put(fieldAPI, String.valueOf(opList[0].get(fieldAPI))); 
                        }   
                    }
                //}
            }
            
        } // End if method
    
    } 
    
    public void reCalculateCharacterstics(){
        system.debug('---> OpportunityLineItemEditExtension.reCalculateCharacterstics');
        Set<String> fieldStr = new Set<String>{'Id', 'Product_Division__c','Product_Characterstics__c','Product_Characterstics_Alt__c'};
        List<PicklistValues__c> oldCharacterstics = new List<PicklistValues__c>();
        // How to remove previous characterstics?
        for(integer i = 0;i<charactersticsList.size();i++){
            fieldStr.add(charactersticsList[i].Field_API__c); 
        }
        String key = String.valueOf(selectedPicklistAPI+':'+selectedPicklistId);
        
        Id parentFieldRecordId = fieldAPIRecordMap.get(selectedPicklistAPI);
          
        String removeCharactersticsQuery = 'SELECT Id, Field_API__c, Characterstics_Hierarchy__c FROM PicklistValues__c WHERE Type__c = \'CHARTYPE\' AND Characterstics_Hierarchy__c LIKE \'%'+Id.valueOf(parentFieldRecordId)+'%\' LIMIT 1000';
        
        Map<Id,PicklistValues__c> charactersticsToRemove = new Map<Id,PicklistValues__c>();
        for(PicklistValues__c characterstic: Database.query(removeCharactersticsQuery)){
            charactersticsToRemove.put(characterstic.Id,characterstic);    
        }
        
        if(charactersticsToRemove!=null && !charactersticsToRemove.isEmpty()){
            for(Id charactersticId:charactersticsToRemove.keySet()){
                
                for(integer i=0;i<charactersticsList.size();i++){
                    if(charactersticId == charactersticsList[i].Id){
                        charactersticsList.remove(i);
                        if(fieldMap.containsKey(charactersticsToRemove.get(charactersticId).Field_API__c)){
                            fieldMap.remove(charactersticsToRemove.get(charactersticId).Field_API__c);
                        }   
                    }
                    
                }   
            }
        }
        
        if(pickListValueMap.containsKey(key)){
            queryStr ='SELECT ';
            
            Id parentId = pickListValueMap.get(key);
            
            for(PicklistValues__c pickListRec:[
                                                  SELECT Id, Name, Field_API__c, Parent_Pick_List_Value__c, Parent_Pick_List_Value__r.Parent_API__c , Type__c, Field_Type__c, Field_Sequence__c,Hierarchy_Level__c,
                                                  (
                                                      SELECT Id, Name, Field_API__c, Parent_Pick_List_Value__c, Type__c, Parent_API__c,Hierarchy_Level__c
                                                      FROM Pick_List_Values__r
                                                      WHERE Type__c = 'CHARVAL'
                                                      AND Parent_API__c !=''
                                                      AND Parent_Field_Type__c = 'Picklist'
                                                      ORDER BY Field_Sequence__c ASC
                                                  )
                                                  FROM PicklistValues__c
                                                  WHERE Type__c = 'CHARTYPE'
                                                  AND Parent_Pick_List_Value__c =:parentId
                                                  ORDER BY Hierarchy_Level__c, Field_Sequence__c ASC
                                            ]){
                                            
                                            // Populate the API name-value map
                                            
                                            fieldMap.put(pickListRec.Field_API__c, '');
                                           
                                            // Populate the field list
                                            // Add the API to query
                                            fieldStr.add(pickListRec.Field_API__c);
                                            charactersticsList.add(pickListRec);
                                            fieldAPIRecordMap.put(pickListRec.Field_API__c,pickListRec.Id);
                                            if(pickListRec.Parent_Pick_List_Value__r.Parent_API__c !=null && pickListRec.Parent_Pick_List_Value__r.Parent_API__c !=''){
                                                fieldStr.add(pickListRec.Parent_Pick_List_Value__r.Parent_API__c);
                                            }
                                            
       
                                            if(pickListRec.Field_Type__c == 'Picklist' && pickListRec.Pick_List_Values__r!=null){
                                                fieldPickList.put(pickListRec.Field_API__c, new List<SelectOption>{new SelectOption('-- None --','-- None --')});
                                                for(PicklistValues__c pickListValue:pickListRec.Pick_List_Values__r){
                                                    if(fieldPickList.containsKey(pickListRec.Field_API__c)){
                                                        fieldPickList.get(pickListRec.Field_API__c).add(new SelectOption(pickListValue.Name,pickListValue.Name));   
                                                    }
                                                    else{
                                                        fieldPickList.put(pickListRec.Field_API__c, new List<SelectOption>{new SelectOption(pickListValue.Name,pickListValue.Name)});
                                                    }
                                                    pickListValueMap.put(String.valueOf(pickListRec.Field_API__c+':'+pickListValue.Name),pickListValue.Id);
                                                }    
                                            }
            
            } // End of for loop
            
            for(String fieldAPI:fieldStr){
                queryStr+=fieldAPI+',';
            }
            
            queryStr=queryStr.substring(0,queryStr.length()-1)+' FROM OpportunityLineItem WHERE Id='+'\''+opptyLineItem.Id+'\' LIMIT 1';
            
            List<OpportunityLineItem> opList = Database.query(queryStr);
            
            if(opList!=null && !opList.isEmpty()){
                if(opList[0].Product_Division__c =='CPS' || opList[0].Product_Division__c =='CPS Aftermarket' || opList[0].Product_Division__c =='Wellhead' || opList[0].Product_Division__c =='Midstream'){
                    for(String fieldAPI:fieldMap.keySet()){
                        if(opList[0].get(fieldAPI)!=null && fieldMap.get(fieldAPI)==''){
                            fieldMap.put(fieldAPI, String.valueOf(opList[0].get(fieldAPI))); 
                        }   
                    }
                }
            }  
              
        }
        System.debug('Characteristic list before sort:'+charactersticsList);
        // Sort characteristic list
        List<PicklistWrapper> wrapperForSort = new List<PicklistWrapper>();
        for(PicklistValues__c record:charactersticsList){
            wrapperForSort.add(new PicklistWrapper(record));    
        }
        wrapperForSort.sort();
        charactersticsList = new List<PicklistValues__c>();
        for(PicklistWrapper wrapperInstance:wrapperForSort){
            charactersticsList.add(wrapperInstance.picklistValueRecord);   
        }
        System.debug('Characteristic list after sort:'+charactersticsList);
    }
    
    public void reRenderCharacterstics(){
        system.debug('---> OpportunityLineItemEditExtension.reRenderCharacterstics');
        charactersticsList = new List<PicklistValues__c>();
        if(selectedPicklistId!='None' && selectedPicklistId!='' && selectedPicklistId!='-- None --' && selectedPicklistId!=null){
            populateLayoutData(selectedPicklistId);
        }
        else{
            populateCharactersticBasedOnFormula();
        }
        // If no characterstics are found then display characterstics by formula
        if(!(charactersticsList != null && !charactersticsList.isEmpty())){
            populateCharactersticBasedOnFormula();  
        }
    }
    
    // CGP 14-Nov-2014 changed saveLineItems type to pagereference
    // CGP 14-Nov-2014 removed change saveLineItems type to pagereference
    public void saveLineItems(){
    //public PageReference saveLineItems(){
        system.debug('---> OpportunityLineItemEditExtension.saveLineItems');
        Map<String, Schema.SObjectField> M = Schema.SObjectType.OpportunityLineItem.fields.getMap(); 
        String characterstics ='';
        
        system.debug('+++ SaveLineItem');
        if(fieldMap!=null && !fieldMap.isEmpty() && charactersticsList!=null){
            
            
            for(PicklistValues__c record:charactersticsList){
                if(fieldMap.containsKey(record.Field_API__c)){
                    String key = record.Field_API__c;
                    
                    // CGP 21-Feb-2015 Removed following line
                    //opptyLineItem.put(key,fieldMap.get(key));                                       
                                        
                    if(fieldMap.get(key)!= '-- None --' && fieldMap.get(key)!=''){
                        characterstics+=M.get(key).getDescribe().getLabel()+': '+fieldMap.get(key)+' | ';
                        system.debug('+++ Field_API__c = ' + record.Field_API__c +
                                 ' fieldMap.get(key) = ' + fieldMap.get(key));
                        // CGP 21-Feb-2015 Added following line to set opportunityLineItem field values         
                        opptyLineItem.put(record.Field_API__c, fieldMap.get(key)); 
                    } 
                    
                }
            }
           
        }
        
        if(characterstics!=''){
            opptyLineItem.Product_Characterstics__c = characterstics.substring(0,characterstics.length()-2);
            opptyLineItem.Product_Characterstics_Alt__c = opptyLineItem.Product_Characterstics__c;
        }
        else {
            //opptyLineItem.Product_Characterstics__c = characterstics;
        }
        if(selectedBrand!='-- None --')
        {
            system.debug('+++ Updating Brand__c from selectedBrand' + selectedBrand);
            opptyLineItem.put('Brand__c',selectedBrand);
        }
        else{
            system.debug('+++ Updating Brand__c to null selectedBrand = ' + selectedBrand);
            // CGP 21-Feb-2015 Removed following line
            //opptyLineItem.put('Brand__c',null);
        }
        if(selectedModel!='-- None --')
        {
            system.debug('+++ Updating Model__c from selectedModel' + selectedModel);
            opptyLineItem.put('Model__c',selectedModel);
        }
        else{
            system.debug('+++ Updating Model__c to null selectedModel = ' + selectedModel);        
            // CGP 21-Feb-2015 Removed following line
            //opptyLineItem.put('Model__c',null);
        }
        
        // 10-Feb-2015 CGP setting default value to 0 when null
        if (opptyLineItem.UnitPrice == null)
        {
            opptyLineItem.UnitPrice = 0;
        }
        
        // 14-Nov-2014 CGP moved Update opptyLineItem inside of Try/Catch statement
        // 14-Nov-2014 CGP removed changes
        //try{
        
            update opptyLineItem;
            
        //}catch(Exception ex){
        //    showErrorMsg = true;
        //    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, ex.getMessage()));
        //}
        // 14-Nov-2014 CGP added return null;
        // 14-Nov-2014 CGP removed return null;
        //return null;
        
    }
    // Custom sort
    global class PicklistWrapper implements Comparable {
        
        public PicklistValues__c picklistValueRecord;
        
        // Constructor
        public PicklistWrapper(PicklistValues__c picklistValueRecord) {
            this.picklistValueRecord = picklistValueRecord;
        }
        
        // Compare picklistValueRecord based on the hierarchy level.
        global Integer compareTo(Object compareTo) {
            // Cast argument to OpportunityWrapper
            PicklistWrapper compareToPicklistValue = (PicklistWrapper)compareTo;
            
            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;
            if (picklistValueRecord.Hierarchy_level__c > compareToPicklistValue.picklistValueRecord.Hierarchy_level__c) {
                // Set return value to a positive value.
                returnValue = 1;
            } else if (picklistValueRecord.Hierarchy_level__c < compareToPicklistValue.picklistValueRecord.Hierarchy_level__c) {
                // Set return value to a negative value.
                returnValue = -1;
            }
            
            return returnValue;       
        }
    }
    
    global class SelectOptionWrapper{
        
        public String option {get;set;}
        public String id {get;set;}
        
        public SelectOptionWrapper(String option, String id){
            this.option = option;
            this.id = id;
        }
    }
    
    @readOnly
    @remoteAction
    global static List<SelectOptionWrapper> populateBrand(String opportunityLineItemId){
        system.debug('---> OpportunityLineItemEditExtension.populateBrand');
        List<SelectOptionWrapper> optionWrapperList = new List<SelectOptionWrapper>();
        
        // Get OpportunityLineItem Group
        List<OpportunityLineItem> opList = [SELECT Id, Name, Group__c, Model__c, Brand__c, PricebookEntry.Product2.Name, Product_Division__c FROM OpportunityLineItem WHERE Id=:opportunityLineItemId LIMIT 1];
        
        if(opList!=null && !opList.isEmpty()){
            String groupStr = opList[0].Product_Division__c+'|'+opList[0].Group__c;
            for(PicklistValues__c pickListGroupRec:[
                                                          SELECT Id, Name, Field_API__c, Parent_Pick_List_Value__c, Type__c, Field_Type__c,
                                                          (
                                                              SELECT Id, Name, Field_API__c, Parent_Pick_List_Value__c, Type__c, Parent_API__c
                                                              FROM Pick_List_Values__r
                                                              WHERE Type__c = 'LEVEL2'
                                                          )
                                                          FROM PicklistValues__c
                                                          WHERE Type__c = 'LEVEL1'
                                                          AND External_Id__c =:groupStr
                                                   ]){
                                            
                for(PicklistValues__c pickListBrandRec:pickListGroupRec.Pick_List_Values__r){
                    optionWrapperList.add(new SelectOptionWrapper(pickListBrandRec.Name,pickListBrandRec.Id));          
                }
            }
        } 
        
        return optionWrapperList;    
    }
    @readOnly
    @remoteAction
    global static List<SelectOptionWrapper> populateModel(String selectedBrandId){
        system.debug('---> OpportunityLineItemEditExtension.populateModel');
        List<SelectOptionWrapper> optionWrapperList = new List<SelectOptionWrapper>();
        for(PicklistValues__c pickListBrandRec:[
                                                      SELECT Id, Name, Field_API__c, Parent_Pick_List_Value__c, Type__c, Field_Type__c,
                                                      (
                                                          SELECT Id, Name, Field_API__c, Parent_Pick_List_Value__c, Type__c, Parent_API__c
                                                          FROM Pick_List_Values__r
                                                          WHERE Type__c = 'LEVEL3'
                                                      )
                                                      FROM PicklistValues__c
                                                      WHERE Type__c = 'LEVEL2'
                                                      AND Id =:selectedBrandId
                                               ]){
                                            
                for(PicklistValues__c pickListModelRec:pickListBrandRec.Pick_List_Values__r){
                    optionWrapperList.add(new SelectOptionWrapper(pickListModelRec.Name,pickListModelRec.Id));          
                }
        }
        return optionWrapperList;
    }
    
}