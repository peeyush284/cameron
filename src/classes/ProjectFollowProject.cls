/**
 * @author      Salesforce {GB}
 * @date        5/27/2014
 * @description From button click on Project, add Account Team Members to the Project Chatter Feed as followers
 */
 
global class ProjectFollowProject{

    WebService static String addFollowers(Id ProjectId, Id AccountId) {
        String responseStr = '';        
        Set<String> profileValidationSet = new Set<String>();
        profileValidationSet.addAll(String.valueOf(System.Label.Project_Allow_Cameron_Admin_Profile_To_Assign_Lead).split(','));
        
        CAM_Project__c proj = [SELECT Id, CAM_Startup_Date__c, Owner.Profile.Name FROM CAM_Project__c WHERE Id =: projectId LIMIT 1];
        
        if(proj.CAM_Startup_Date__c == null){
            return System.Label.Project_On_Stream_Date_Missing;    
        }
        else if(!profileValidationSet.contains(proj.Owner.Profile.Name)){
            return System.Label.Project_Owned_By_Cameron_Admin;
        }
        
        //Create list wehre account team member are not in the project followers query      
        List<AccountTeamMember> listAcctTeamMembers  =[SELECT UserId FROM AccountTeamMember WHERE AccountId = :AccountId AND UserId NOT IN (SELECT SubscriberId FROM EntitySubscription WHERE ParentId = :ProjectId)];             
        list<EntitySubscription>FollowList=New list <EntitySubscription>(); //**

        //Add users to the project chatter feed
        for(AccountTeamMember objAccountTeamMember: listAcctTeamMembers){
            EntitySubscription follow = new EntitySubscription (parentId = ProjectId,subscriberid = objAccountTeamMember.UserId);           
            FollowList.add(follow); 
        }
        
        
        
        //responseStr= '0 follower(s) inserted!';
        responseStr= string.valueof(FollowList.size() +' follower(s) inserted!');
        if(FollowList<>null && !FollowList.isempty())
        {
            Database.SaveResult [] saveResult = Database.insert(FollowList,false);
            for(Database.SaveResult result:saveResult){
                if(!result.isSuccess()){
                    for(Database.Error err : result.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('EntitySubscription fields that affected this error: ' + err.getFields());
                    }
                    responseStr='Exception caught while adding Chatter Followers';                  
                }
                else{                   
                    //responseStr= FollowList.size()+' follower(s) inserted!';
                    responseStr= string.valueof(FollowList.size() +' follower(s) inserted!');                    
                }               
            }
        }           
        
        return responseStr;
    }
      
}