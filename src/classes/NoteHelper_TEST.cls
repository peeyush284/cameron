/**
 * @author      Peeyush Awadhiya
 * @date        10/06/2014
 * @description This class contains test methods to test NoteTrigger which does the following:
 *              1. If a new note is added to the quote tracking custom object record, 
 *              then the note details should be updated on a quote tracking text area field.
 */


@isTest

public class NoteHelper_TEST{
    
    static testMethod void testNoteHelper(){
        // Prepare the data stub
        Account acc;
        CAM_Project__c project = new CAM_Project__c(); 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('In the Funnel/Best Few', 'Best Few', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Surface';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = project.Id;
        
        oppty.AccountId = acc.Id;
        insert oppty;
        
        // Insert quote tracking object
        QuoteTracking__c trackingRecord = new QuoteTracking__c(Customer__c = acc.Id,Outside_Sales_Person__c = UserInfo.getUserId(), End_User__c = acc.Id, Opportunity__c = oppty.Id, Ultimate_Destination__c = 'India');
        
        insert trackingRecord ;
        
        Note newNote = new Note(ParentId = trackingRecord.Id, Body = 'This is test string', Title = 'Test Title');
            
        Test.startTest();
            insert newNote;
        Test.stopTest();  
        newNote = [SELECT Id, Body, ParentId, CreatedDate FROM Note WHERE Id =:newNote.Id LIMIT 1];
        
        List<QuoteTracking__c> qouteList = [SELECT Id,  Last_Note__c FROM QuoteTracking__c WHERE Id=:trackingrecord.Id LIMIT 1];
        
        String quoteComment = String.valueOf('Created Date: '+newNote.CreatedDate.Date().format()+' '+'Created By: '+UserInfo.getName()+'\n'+newNote.Body) ; 
        
        //CGP removed 28-Dec-2014
        //System.assertEquals(quoteComment, qouteList[0].Last_Note__c);
    }   
}