/**
* @author       Peeyush Awadhiya
* @date         08/14/2014
* @description  This class have test methods that asserts the following:
*               1. Selecting IOI records opens up new visualforce page with IOI summary populated with details from selected IOI
* 
*/
@isTest(seeAllData=true)
public class ProjectAddProjectLeadExtension_TEST{
    
        
    public static testMethod void test0(){        
         // Prepare the data stub
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        // Now instantiate the StandardController and start testing
        PageReference pageRef = Page.ProjectAddProjectLeadPage;
        Test.setCurrentPage(pageRef);
        //pageRef.getParameters().put('ids',String.valueOf(ioiRecord.Id+','+ioiRecord2.Id));
        ProjectAddProjectLeadExtension extension = new ProjectAddProjectLeadExtension(new ApexPages.StandardController(project));
        
        extension.validateTeamMembers();
        
        extension.assignLeads();
        
        extension.cancel();
        
        //System.assertEquals(pageRef.getParameters().get('ids').length()>0,true);
        
        //String nextPage = extension.cancelAndGoBack().getUrl();
        
        //System.assert the cancel action
        //System.assertEquals(nextPage, String.valueOf('/' + IOI__c.SObjectType.getDescribe().getKeyPrefix() + '/o'));
        
    }
    
    public static testMethod void test1(){        
         // Prepare the data stub
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        project.Status__c = 'In Progress';
        update project;
        
        // Now instantiate the StandardController and start testing
        PageReference pageRef = Page.ProjectAddProjectLeadPage;
        Test.setCurrentPage(pageRef);
        //pageRef.getParameters().put('ids',String.valueOf(ioiRecord.Id+','+ioiRecord2.Id));
        ProjectAddProjectLeadExtension extension = new ProjectAddProjectLeadExtension(new ApexPages.StandardController(project));
        
        extension.validateTeamMembers();
        
        extension.assignLeads();
        
        extension.cancel();
        
        //System.assertEquals(pageRef.getParameters().get('ids').length()>0,true);
        
        //String nextPage = extension.cancelAndGoBack().getUrl();
        
        //System.assert the cancel action
        //System.assertEquals(nextPage, String.valueOf('/' + IOI__c.SObjectType.getDescribe().getKeyPrefix() + '/o'));
        
    }    
    
    public static testMethod void test2(){        
         // Prepare the data stub
        User activeUser = [Select id from user where profile.name = 'System Administrator' and isactive = true  limit 1]; 
        
        System.runAs(activeUser) {
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        project.CAM_Startup_Date__c = date.today();
        //project.owner = activeUser.id;
        update project;
        
        // Now instantiate the StandardController and start testing
        PageReference pageRef = Page.ProjectAddProjectLeadPage;
        Test.setCurrentPage(pageRef);
        //pageRef.getParameters().put('ids',String.valueOf(ioiRecord.Id+','+ioiRecord2.Id));
        ProjectAddProjectLeadExtension extension = new ProjectAddProjectLeadExtension(new ApexPages.StandardController(project));
        
        extension.validateTeamMembers();
        
        extension.assignLeads();
        
        extension.cancel();
        }
        
        //System.assertEquals(pageRef.getParameters().get('ids').length()>0,true);
        
        //String nextPage = extension.cancelAndGoBack().getUrl();
        
        //System.assert the cancel action
        //System.assertEquals(nextPage, String.valueOf('/' + IOI__c.SObjectType.getDescribe().getKeyPrefix() + '/o'));
        
    }    
    
   public static testMethod void test3(){        
         // Prepare the data stub
        User activeUser = [Select id from user where profile.name = 'System Administrator'  and isactive = true limit 1];
        
        System.runAs(activeUser) {
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        project.CAM_Startup_Date__c = null;
        //project.owner = user.id;
        update project;
        
        // Now instantiate the StandardController and start testing
        PageReference pageRef = Page.ProjectAddProjectLeadPage;
        Test.setCurrentPage(pageRef);
        //pageRef.getParameters().put('ids',String.valueOf(ioiRecord.Id+','+ioiRecord2.Id));
        ProjectAddProjectLeadExtension extension = new ProjectAddProjectLeadExtension(new ApexPages.StandardController(project));
        
        extension.validateTeamMembers();
        
        extension.assignLeads();
        
        extension.cancel();
        }
        
        //System.assertEquals(pageRef.getParameters().get('ids').length()>0,true);
        
        //String nextPage = extension.cancelAndGoBack().getUrl();
        
        //System.assert the cancel action
        //System.assertEquals(nextPage, String.valueOf('/' + IOI__c.SObjectType.getDescribe().getKeyPrefix() + '/o'));
        
    }    
   
   
   public static testMethod void test4(){        
         // Prepare the data stub
        User activeUser = [Select id from user where profile.name = 'System Administrator'  and isactive = true limit 1];
        
        System.runAs(activeUser) {
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        
        AccountTeamMember atm1 = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        project.CAM_Startup_Date__c = date.today();
        project.Status__c = 'In Progress';
        //project.owner = user.id;
        update project;
        
        // Now instantiate the StandardController and start testing
        PageReference pageRef = Page.ProjectAddProjectLeadPage;
        Test.setCurrentPage(pageRef);
        //pageRef.getParameters().put('ids',String.valueOf(ioiRecord.Id+','+ioiRecord2.Id));
        ProjectAddProjectLeadExtension extension = new ProjectAddProjectLeadExtension(new ApexPages.StandardController(project));
        
        extension.validateTeamMembers();
        
        extension.assignLeads();
        
        extension.cancel();
        }
        
        //System.assertEquals(pageRef.getParameters().get('ids').length()>0,true);
        
        //String nextPage = extension.cancelAndGoBack().getUrl();
        
        //System.assert the cancel action
        //System.assertEquals(nextPage, String.valueOf('/' + IOI__c.SObjectType.getDescribe().getKeyPrefix() + '/o'));
        
    }    
    
   public static testMethod void test5(){        
         // Prepare the data stub
        User activeUser = [Select id from user where profile.name = 'System Administrator'  and isactive = true limit 1];
        
        User SalesUser = [Select id from user where profile.name = 'CAM Sales - All' and isactive = true and Create_Opportunity__c = true limit 1];
        
        System.runAs(activeUser) {
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        
        AccountTeamMember atm = new AccountTeamMember();
        atm.AccountID = acc.Id;
        atm.TeamMemberRole = 'Surface|SUR|Acct Mgr|Primary';       
        atm.UserId = SalesUser.Id;
        insert atm;
        //AccountTeamMember atm1 = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        project.CAM_Startup_Date__c = date.today();
        project.Status__c = 'Permanent';
        //project.owner = user.id;
        update project;
        
        // Now instantiate the StandardController and start testing
        PageReference pageRef = Page.ProjectAddProjectLeadPage;
        Test.setCurrentPage(pageRef);
        //pageRef.getParameters().put('ids',String.valueOf(ioiRecord.Id+','+ioiRecord2.Id));
        ProjectAddProjectLeadExtension extension = new ProjectAddProjectLeadExtension(new ApexPages.StandardController(project));
        
        extension.validateTeamMembers();
        
        extension.assignLeads();
        
        extension.cancel();
        }
        
        //System.assertEquals(pageRef.getParameters().get('ids').length()>0,true);
        
        //String nextPage = extension.cancelAndGoBack().getUrl();
        
        //System.assert the cancel action
        //System.assertEquals(nextPage, String.valueOf('/' + IOI__c.SObjectType.getDescribe().getKeyPrefix() + '/o'));
        
    }        
    
   public static testMethod void test6(){        
         // Prepare the data stub
        User activeUser = [Select id from user where name like 'Cameron Admin%'  and isactive = true limit 1];
        
        User SalesUser = [Select id from user where profile.name = 'CAM Sales - All' and isactive = true and Create_Opportunity__c = true limit 1];
        
        User OtherSalesUser = [Select id from user where profile.name = 'CAM Sales - All' and isactive = true and Create_Opportunity__c = true and id != :salesuser.id limit 1];
        
        insert new Prevent_Project_Leads__c(Name='Surface|SUR|Acct Mgr|Primary');
        
        System.runAs(activeUser) {
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        
        AccountTeamMember atm = new AccountTeamMember();
        atm.AccountID = acc.Id;
        atm.TeamMemberRole = 'Surface|SUR|Acct Mgr|Primary';       
        atm.UserId = SalesUser.Id;
        insert atm;
     
        //AccountTeamMember atm1 = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        project.CAM_Startup_Date__c = date.today();
        project.Status__c = 'Permanent';
        //project.owner = user.id;
        update project;
        
        // Now instantiate the StandardController and start testing
        PageReference pageRef = Page.ProjectAddProjectLeadPage;
        Test.setCurrentPage(pageRef);
        //pageRef.getParameters().put('ids',String.valueOf(ioiRecord.Id+','+ioiRecord2.Id));
        ProjectAddProjectLeadExtension extension = new ProjectAddProjectLeadExtension(new ApexPages.StandardController(project));
        
        extension.validateTeamMembers();
        
        extension.assignLeads();
        
        extension.cancel();
        }
        
        //System.assertEquals(pageRef.getParameters().get('ids').length()>0,true);
        
        //String nextPage = extension.cancelAndGoBack().getUrl();
        
        //System.assert the cancel action
        //System.assertEquals(nextPage, String.valueOf('/' + IOI__c.SObjectType.getDescribe().getKeyPrefix() + '/o'));
        
    }     
    
   public static testMethod void test7(){        
         // Prepare the data stub
        User activeUser = [Select id from user where name like 'Cameron Admin%'  and isactive = true limit 1];
        
        User SalesUser = [Select id from user where profile.name = 'CAM Sales - All' and isactive = true and Create_Opportunity__c = true limit 1];
        
        User OtherSalesUser = [Select id from user where profile.name = 'CAM Sales - All' and isactive = true and Create_Opportunity__c = true and id != :salesuser.id limit 1];
        
        //insert new Prevent_Project_Leads__c(Name='Corporate Accounts');
        
        system.debug('Test 7 System.Label.Project_Allow_Cameron_Admin_Profile_To_Assign_Lead = ' + System.Label.Project_Allow_Cameron_Admin_Profile_To_Assign_Lead);
        
        System.runAs(activeUser) {
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        
        AccountTeamMember atm = new AccountTeamMember();
        atm.AccountID = acc.Id;
        atm.TeamMemberRole = 'Surface|SUR|Acct Mgr|Primary';       
        atm.UserId = SalesUser.Id;
        insert atm;
     
        //AccountTeamMember atm1 = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        project.CAM_Startup_Date__c = date.today();
        project.Status__c = 'XXX';
        //project.owner = user.id;
        update project;
        
        // Now instantiate the StandardController and start testing
        PageReference pageRef = Page.ProjectAddProjectLeadPage;
        Test.setCurrentPage(pageRef);
        //pageRef.getParameters().put('ids',String.valueOf(ioiRecord.Id+','+ioiRecord2.Id));
        ProjectAddProjectLeadExtension extension = new ProjectAddProjectLeadExtension(new ApexPages.StandardController(project));
        
        extension.validateTeamMembers();
        
        extension.assignLeads();
        
        extension.cancel();
        }
        
        //System.assertEquals(pageRef.getParameters().get('ids').length()>0,true);
        
        //String nextPage = extension.cancelAndGoBack().getUrl();
        
        //System.assert the cancel action
        //System.assertEquals(nextPage, String.valueOf('/' + IOI__c.SObjectType.getDescribe().getKeyPrefix() + '/o'));
        
    }          
            
    
 }