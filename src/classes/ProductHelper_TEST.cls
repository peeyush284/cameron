/**
* @author       Gabor Bay
* @date         09/25/2014
* @description  Class with test methods for ProductHelper class.
* 
*/

@isTest

public class ProductHelper_TEST{
/*
    public static testMethod void testProductHelper(){
        // Prepare the data stub
        Product prod;
        
        // Product
        prod = TestUtilities.generateProduct();
                   
        insert ioiRecordList;
        
        List <User> runningUser = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND Id!=:UserInfo.getUserId() LIMIT 1];
        
        if(runningUser!=null && !runningUser.isEmpty()){
            
            for(IOI__c record:ioiRecordList){
                record.OwnerId = runningUser[0].Id;
                record.Status__c = 'Submitted';
                record.Name = record.Name+ ' - Updated';
                record.Description__c = 'Updated Description';
            }
        }
        System.debug('After update of IOI record list:'+ioiRecordList);
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        
        System.runAs(thisUser){
            Test.startTest();
                update ioiRecordList;
            Test.stopTest();
        }
        // Now 3 IOI_Field_Tracking__c records should have been created
        
        System.assertEquals([SELECT Id FROM IOI_Field_Tracking__c].size(),20);

    }
*/
    public static testMethod void test0(){
        // Prepare the data stub
        Product2 prod;
        
        // Product
        prod = TestUtilities.generateProduct('Product Name 1', 'Drilling', 'Drilling', '', '');
        insert prod;

    }
    
    public static testMethod void test1(){
        // Prepare the data stub
        Product2 prod;
        
        // Product
        prod = TestUtilities.generateProduct('Product Name 1', 'Drilling', 'Drilling', '', '');
        prod.Auto_Associate_to_Price_Books_flg__c = True;        
        insert prod;
        
        prod.Description = 'new Description' ;
        prod.Auto_Associate_to_Price_Books_flg__c = False;
        update prod;
    }    
}