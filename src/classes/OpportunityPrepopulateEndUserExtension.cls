/**
* @author       Peeyush Awadhiya
* @date         08/07/2014
* @description  This class serves as an extension to the OpportunityPrepopulateEndUserPage. It covers following:
*               1. If opporuntiy is created from Project related list, populate the project, account.
*               2. If the oppotutnity is created from Account related list, populate the end user and account.
*               3. For bucket opportunity, the close date is pre-populated to the last date of current year.
*               4. Pre-populate the current running user's business unit.
*/


public with sharing class OpportunityPrepopulateEndUserExtension {
    
    public Opportunity opportunityRec {get;set;}
    
    public Id projectId {get;set;}
    public String projectKey {get;set;}
    public String projectName {get;set;}
    
    public String retURL {get;set;}
    
    public String accountId {get;set;}
    public String accountKey {get;set;}
    public String accountName {get;set;}
    public boolean isBucketOpportunity {get;set;}
    
    public OpportunityPrepopulateEndUserExtension(ApexPages.StandardController controller) {
    
        isBucketOpportunity = false;
        if(!Test.isRunningTest()){
            controller.addFields(new List<String>{'AccountId', 'Account.Name','RecordTypeId','CloseDate'});
        }
        opportunityRec = (Opportunity)controller.getRecord();
        
        
        if(ApexPages.CurrentPage().getParameters().get(Opportunity_Prepopulate_Fields__c.getOrgDefaults().CAM_Project__c+'_lkid')!=null){
            projectKey = Opportunity_Prepopulate_Fields__c.getOrgDefaults().CAM_Project__c;
            projectName = ApexPages.CurrentPage().getParameters().get(Opportunity_Prepopulate_Fields__c.getOrgDefaults().CAM_Project__c);
            projectId = ApexPages.CurrentPage().getParameters().get(Opportunity_Prepopulate_Fields__c.getOrgDefaults().CAM_Project__c+'_lkid');   
        }
        else  if(ApexPages.CurrentPage().getParameters().get(Opportunity_Prepopulate_Fields__c.getOrgDefaults().Parent_Project__c+'_lkid')!=null){
            projectKey = Opportunity_Prepopulate_Fields__c.getOrgDefaults().Parent_Project__c;
            projectName = ApexPages.CurrentPage().getParameters().get(Opportunity_Prepopulate_Fields__c.getOrgDefaults().Parent_Project__c); 
            projectId = ApexPages.CurrentPage().getParameters().get(Opportunity_Prepopulate_Fields__c.getOrgDefaults().Parent_Project__c+'_lkid');  
        }
        
        if(ApexPages.CurrentPage().getParameters().get(Opportunity_Prepopulate_Fields__c.getOrgDefaults().End_User__c)!=null){
            accountKey = Opportunity_Prepopulate_Fields__c.getOrgDefaults().End_User__c;
            accountId = ApexPages.CurrentPage().getParameters().get(Opportunity_Prepopulate_Fields__c.getOrgDefaults().End_User__c+'_lkid');
            accountName =  ApexPages.CurrentPage().getParameters().get(Opportunity_Prepopulate_Fields__c.getOrgDefaults().End_User__c);      
        }
        
        if(opportunityRec.RecordTypeId == Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get('Bucket').getRecordTypeId()){
            isBucketOpportunity = true;
        }
        retURL = String.valueOf(ApexPages.CurrentPage().getParameters().get('retURL')).subString(1,String.valueOf(ApexPages.CurrentPage().getParameters().get('retURL')).length());
    }
    
    public PageReference prepopulateOpportunity(){
        
        PageReference returnPage = new PageReference('/' + Opportunity.SObjectType.getDescribe().getKeyPrefix() + '/e');
        returnPage.getParameters().put('retURL',opportunityRec.Id);
        returnPage.getParameters().put('RecordType',opportunityRec.RecordTypeId);
        
        
        
        if(isBucketOpportunity){
            returnPage.getParameters().put('opp9',String.valueOf(Date.newInstance(Date.Today().Year(),12,31).format()));
        }
        List<Account> accountList = [SELECT Id, Name, BillingCountry FROM Account WHERE Id =:opportunityRec.AccountId LIMIT 1];
        
        if(accountList!=null && !accountList.isEmpty()){
            returnPage.getParameters().put(Opportunity_Prepopulate_Fields__c.getOrgDefaults().End_User__c,accountList[0].Name);
            returnPage.getParameters().put(Opportunity_Prepopulate_Fields__c.getOrgDefaults().End_User__c+'_lkid',opportunityRec.AccountId);
            returnPage.getParameters().put(Opportunity_Prepopulate_Fields__c.getOrgDefaults().CAM_Final_Destination__c,accountList[0].BillingCountry);
            returnPage.getParameters().put(Opportunity_Prepopulate_Fields__c.getOrgDefaults().Account__c,accountList[0].Name);
            returnPage.getParameters().put(Opportunity_Prepopulate_Fields__c.getOrgDefaults().Account__c+'_lkid',opportunityRec.AccountId);
            returnPage.getParameters().put('retURL',opportunityRec.AccountId);
        }
        
        if(projectId!=null){
        
            List<CAM_Project__c> projectList = [SELECT Id, Name, CAM_Owner_End_User__c,CAM_Final_Destination__c, CAM_Owner_End_User__r.Name FROM CAM_Project__c WHERE Id =:projectId LIMIT 1];
            
            returnPage.getParameters().put(projectKey+'_lkid', projectId); 
            returnPage.getParameters().put(projectKey, projectName); 
            
            if(projectList!=null && !projectList.isEmpty()){
                returnPage.getParameters().put(Opportunity_Prepopulate_Fields__c.getOrgDefaults().End_User__c+'_lkid', projectList[0].CAM_Owner_End_User__c);
                returnPage.getParameters().put(Opportunity_Prepopulate_Fields__c.getOrgDefaults().End_User__c, projectList[0].CAM_Owner_End_User__r.Name);    
                returnPage.getParameters().put(Opportunity_Prepopulate_Fields__c.getOrgDefaults().CAM_Final_Destination__c, projectList[0].CAM_Final_Destination__c);
            }
            
            returnPage.getParameters().put('retURL',retURL);   
        }
        
        if(accountId!=null){
        
            returnPage.getParameters().put(accountKey+'_lkid',accountId);
            returnPage.getParameters().put(accountKey,accountName);
            
            returnPage.getParameters().put(Opportunity_Prepopulate_Fields__c.getOrgDefaults().Account__c,accountName);
            returnPage.getParameters().put(Opportunity_Prepopulate_Fields__c.getOrgDefaults().Account__c+'_lkid',accountId);
            List<Account> accountListVar = [SELECT Id, BillingCountry FROM Account WHERE Id=:accountId LIMIT 1];
            
            if(accountListVar!=null && !accountListVar.isEmpty()){
                returnPage.getParameters().put(Opportunity_Prepopulate_Fields__c.getOrgDefaults().CAM_Final_Destination__c, accountListVar[0].BillingCountry);
            }
            returnPage.getParameters().put('retURL',retURL);        
        }
        
        returnPage.getParameters().put('nooverride','1');
        // Populate the running user's buisness unit to control the picklist values of the Type field
        
        String businessUnit = '';
        List<User> runningUser = [SELECT Id, camBusinessUnit__c, camDivision__c FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        if(runningUser!=null && !runningUser.isEmpty()){
            businessUnit = runningUser[0].camBusinessUnit__c;
        }
        returnPage.getParameters().put(Opportunity_Prepopulate_Fields__c.getOrgDefaults().Business_Unit_Picklist__c,businessUnit);
        
        return returnPage ;
        
    }
}