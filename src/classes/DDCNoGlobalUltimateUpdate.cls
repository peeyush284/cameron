global class DDCNoGlobalUltimateUpdate implements Database.Batchable<sObject>
{
    private static String OBJECT_QUERY = 'SELECT Id, Name, NoGlobalUltimate__c, ' +
                                                'DunsNumber, DandBCompany.GlobalUltimateDunsNumber, ' + 
                                                'DandBCompany.DomesticUltimateDunsNumber, ' +
                                                'DandBCompany.ParentOrHqDunsNumber ' +
                                           'FROM Account ' +
                                          'WHERE DandBCompany.Id != null ' +
                                            'AND DandBCompany.ParentOrHqDunsNumber != \'000000000\'' +
                                       'ORDER BY DandBCompany.GlobalUltimateDunsNumber';
    public String query;
    public Map<String, List<Account>> globalUltimateMap = new Map<String, List<Account>>();
   
    global DDCNoGlobalUltimateUpdate ()
    {
        query = OBJECT_QUERY;
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
       return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        List<SObject> updates = new List<SObject>();
        Account acct;
        List<Account> guAccounts;
        String save_gudn = '';
        
        // Create a map of he global ultimates and the associated accounts.
        for (SObject account : scope)
        {
            acct = (Account) account;
            if (acct.DandBCompany.GlobalUltimateDunsNumber.equals(save_gudn))
            {
                guAccounts.add(acct);
            } else {
                if (guAccounts != null)
                {
                   globalUltimateMap.put(save_gudn, guAccounts);
                }
                save_gudn = acct.DandBCompany.GlobalUltimateDunsNumber;
                guAccounts = new List<Account>();
                guAccounts.add(acct);
            }
        }

        List<Account> globalUltimates = [SELECT Id, Name, DunsNumber
                                           FROM Account
                                          WHERE DunsNumber IN :globalUltimateMap.keySet()
                                        ];

        // remove the global ultimates that have been found.
        for (Account guAcct : globalUltimates)
        {
            globalUltimateMap.remove(guAcct.DunsNumber);
        }

        // Add acount to the list that requires update.
        for (List<Account> accounts : globalUltimateMap.values())
        {
            for (Account account : accounts)
            {
                account.NoGlobalUltimate__c = true;
                updates.add(account);
            }
        }
        update updates;
    }

    global void finish(Database.BatchableContext BC){}

}