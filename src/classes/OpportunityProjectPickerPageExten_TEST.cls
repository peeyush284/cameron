/**
* @author       Peeyush Awadhiya
* @date         07/29/2014
* @description  This class contains unit tests to test the extension OpportunityProjectPickerPageExtension of OpportunityProjectPickerPage.
*               It tests the following:
*               1. User should be able to pick a project and save the project on Opportunity 
*               2. If no matching projects are found, then provision probationary project and save it related to opportunity.
*/

@isTest

public with sharing class OpportunityProjectPickerPageExten_TEST {
    
    
    /**
    * @author       Peeyush Awadhiya
    * @date         08/04/2014
    * @description  unit tests to test the extension OpportunityProjectPickerPageExtension of OpportunityProjectPickerPage.
    *               It tests the following:
    *               1. User should be able to pick a project and save the project on Opportunity 
    * @param        void
    * @return       void 
    */
    
    public  static testMethod void testProjectPicker(){
     
        // Prepare the data stub
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc);              
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        project.CAM_Final_Destination__c = 'Spain';
        project.CAM_Owner_End_User__c = acc.Id;
        update project;
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunity
        oppty = TestUtilities.generateOpportunityWithoutInsert('Bucket', 'RFQ', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Surface';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = null;
        oppty.CAM_Final_Destination__c = 'Spain';
        oppty.AccountId = acc.Id;
        insert oppty;
 
        
        // Now instantiate the StandardController and start testing
        PageReference pageRef = Page.OpportunityProjectPickerPage;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('opportunityId', oppty.Id);
        
        OpportunityProjectPickerPageExtension extension =  new OpportunityProjectPickerPageExtension(new ApexPages.StandardController(oppty));
        
        // Now the project list should have one project
        System.assertEquals(extension.matchingProjects.size(),1);
        
        // Select the project and update opportunity
        extension.projectId = project.Id;
        
        Test.startTest();
            String detailPageURL = extension.updateSelectedProjectOnOpportunity().getURL();
        Test.stopTest();
        
        //System.assertEquals(detailPageURL.contains('/'+oppty.Id),true);
        
        oppty = [SELECT Id, CAM_Project__c, Parent_Project__c, RecordTypeId, OwnerId, AccountId, End_User__c, End_User__r.Name, Account.Name, CAM_Final_Destination__c, Popup_Viewed__c FROM Opportunity WHERE Id=:oppty.Id LIMIT 1];
        System.assertEquals(oppty.CAM_Project__c, project.Id);
        System.assertEquals(oppty.Popup_Viewed__c, true);
        
        
              
    }
    // Use case 2. Create probationary project
    public  static testMethod void testCreateProbationaryProjectAndRedirect(){
     
        // Prepare the data stub
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc);              
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        project.CAM_Final_Destination__c = 'India';
        project.CAM_Owner_End_User__c = acc.Id;
        update project;
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunity
        oppty = TestUtilities.generateOpportunityWithoutInsert('Bucket', 'RFQ', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Surface';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = null;
        oppty.CAM_Final_Destination__c = 'Spain';
        oppty.AccountId = acc.Id;
        insert oppty;
        
        
        // Now instantiate the StandardController and start testing
        PageReference pageRef = Page.OpportunityProjectPickerPage;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('opportunityId', oppty.Id);
        
        OpportunityProjectPickerPageExtension extension =  new OpportunityProjectPickerPageExtension(new ApexPages.StandardController(oppty));
        
        // Now the project list should have no project
        System.assertEquals(extension.matchingProjects.size(),0);
        
        Test.startTest();
            extension.probationaryProjectName = 'Test Name';
            extension.redirectToProbationaryProjectPage();
            String detailPageURL = extension.createProbationaryProjectAndRedirect().getURL();
        Test.stopTest();
        
        //System.assertEquals(detailPageURL.contains('/'+oppty.Id),true);
        
        oppty = [SELECT Id, CAM_Project__c, Parent_Project__c, RecordTypeId, OwnerId, AccountId, End_User__c, End_User__r.Name, Account.Name, CAM_Final_Destination__c, Popup_Viewed__c FROM Opportunity WHERE Id=:oppty.Id LIMIT 1];
        String projectName = String.valueOf('Project: '+oppty.End_User__r.Name+' '+oppty.CAM_Final_Destination__c);
        List<CAM_Project__c> probationaryProjectList = [SELECT Id, CAM_Owner_End_User__c, CAM_Final_Destination__c, Name, CAM_Phase__c FROM CAM_Project__c WHERE Name =: projectName AND CAM_Phase__c = 'Phase 0' AND CAM_Owner_End_User__c =: oppty.End_User__c AND CAM_Final_Destination__c = :oppty.CAM_Final_Destination__c LIMIT 1];
        
        if(probationaryProjectList!=null && !probationaryProjectList.isEmpty()){
            System.assertEquals(probationaryProjectList.size(),1);
            System.assertEquals(oppty.CAM_Project__c, probationaryProjectList[0].Id);
        }      
        System.assertEquals(oppty.Popup_Viewed__c, true);
        
        
    }
    
    
    // Use case 3. Cancel the selection
    public  static testMethod void testCancelAndRedirect(){
     
        // Prepare the data stub
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc);              
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        project.CAM_Final_Destination__c = 'India';
        project.CAM_Owner_End_User__c = acc.Id;
        update project;
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunity
        oppty = TestUtilities.generateOpportunityWithoutInsert('Bucket', 'RFQ', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Surface';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = null;
        oppty.CAM_Final_Destination__c = 'India';
        oppty.AccountId = acc.Id;
        insert oppty;
        
        
        // Now instantiate the StandardController and start testing
        PageReference pageRef = Page.OpportunityProjectPickerPage;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('opportunityId', oppty.Id);
        
        OpportunityProjectPickerPageExtension extension =  new OpportunityProjectPickerPageExtension(new ApexPages.StandardController(oppty));
        
        // Now the project list should have one project
        System.assertEquals(extension.matchingProjects.size(),1);
        System.assertEquals(oppty.CAM_Project__c, null);
        System.assertEquals(oppty.Popup_Viewed__c, false);
        
        Test.startTest();
            String detailPageURL = extension.cancelProjectSelection().getURL();
        Test.stopTest();
        
        System.assertEquals(detailPageURL.contains('/'+oppty.Id),true);
        
        oppty = [SELECT Id, CAM_Project__c, Parent_Project__c, RecordTypeId, OwnerId, AccountId, End_User__c, End_User__r.Name, Account.Name, CAM_Final_Destination__c, Popup_Viewed__c FROM Opportunity WHERE Id=:oppty.Id LIMIT 1];
        System.assertEquals(oppty.CAM_Project__c, null);
        System.assertEquals(oppty.Popup_Viewed__c, true);
        
        
    }
}