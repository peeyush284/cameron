@isTest
public class TestUtilities {

        private static User u = [SELECT UserName,Email FROM User 
                                WHERE isActive = true 
                                  and role__c != null
                                  and UserRole.DeveloperName != null
                                LIMIT 1];
        //Account
        private static string acctName = 'CODE COVERAGE ACCOUNT';
        
        //Project
        private static string projName = 'CODE COVERAGE PROJECT';
               
        //Opportunity
        private static string opptyName = 'CODE COVERAGE OPPORTUNITY';
        private static string opptyDescription = 'CODE COVERAGE TEST';
        private static date opptyCloseDate = date.today()+90; 
        
     public static Account generateAccount(){
        boolean success = false;
        integer numberOfAttempts = 0;
        Account a = new account();
        id acctRecordTypeId = Schema.Sobjecttype.Account.getRecordTypeInfosByName().get('Prospect Account').getRecordTypeId();
        a.name = acctName;
        a.BillingCountry = 'India';
        a.BillingState = 'Assam';
        a.billingStreet = '123 India St';
        a.billingCity = 'Assamopolis';
        a.recordTypeId = acctRecordTypeId;
        /*
        a.AssignableRoles__c = 'Drilling|Drilling|Acct Mgr|Primary;OSS|Processing Systems|Acct Mgr|Primary;'+
                                'OSS|Production Systems|Acct Mgr|Primary;OSS|Swivel & Marine Sys|Acct Mgr|Primary;'+
                                'OSS|Subsea Services|Acct Mgr|Primary;PS|Wellhead|Acct Mgr|Primary;PS|CPS|Acct Mgr|Primary;'+
                                'SUR|Artificial Lift|Acct Mgr|Primary;SUR|CAMSHALE|Acct Mgr|Primary;'+
                                'SUR|Flow Control|Acct Mgr|Primary;SUR|SUR|Acct Mgr|Primary;V&M|DSV|Acct Mgr|Primary;'+
                                'V&M|EPV|Acct Mgr|Primary;V&M|Ledeen|Acct Mgr|Primary;V&M|Measurement|Acct Mgr|Primary';
        */                        
        insert a;
        system.debug('--- TestUtilities.Account Created static user id = ' + u.id);
        return a;
     }
     
     public static AccountTeamMember generateAccountTeamMember(String atmRole, Account a){  
        system.debug('=== generateAccountTeamMember atrRole = ' + atmRole + ' User.id = ' + u.Id); 
        AccountTeamMember atm = new AccountTeamMember();
        try {
            atm = [select id, TeamMemberRole, AccountID, UserId from AccountTeamMember where AccountID = :a.id and UserId = :u.Id limit 1];
            system.debug('=== Located AccountTeamMember.  User Role = ' + atm.TeamMemberRole); 
        } catch (exception e) {
            atm.AccountID = a.Id;
            if (atmRole == 'Drilling||Acct Mgr|Primary')
            {
                atmRole = 'Drilling|Drilling|Acct Mgr|Primary';
            }
            atm.TeamMemberRole = atmRole;       
            atm.UserId = u.Id;
            system.debug('=== Inserting AccountTeamMember.  User Role = ' + atm.TeamMemberRole); 
            system.debug('--- TestUtilities.generateAccountTeamMember static user id = ' + u.id);
            //insert atm;
        }
  
        return atm;
     }   
        
     public static CAM_Project__c generateProject(string projRecordType, string projStatus, Account a){
        id projRecordTypeId = Schema.Sobjecttype.CAM_Project__c.getRecordTypeInfosByName().get(projRecordType).getRecordTypeId();
        CAM_Project__c p = new CAM_Project__c();
        p.name = projName;
        p.CAM_Owner_End_User__c = a.Id;
        p.Status__c = projStatus;
        p.RecordTypeId = projRecordTypeId;
        p.CAM_Startup_Date__c = Date.Today();
        insert p;
        return p;
     }

     public static Opportunity generateOpportunity(string opptyRecordType, string opptyStage, CAM_Project__c p){
        id opptyRecordTypeId = Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get(opptyRecordType).getRecordTypeId();
        Opportunity o = new Opportunity();
        o.name = opptyName;
        o.CloseDate = opptyCloseDate;
        o.RecordTypeId = opptyRecordTypeId;
        o.StageName = opptyStage;
        o.CAM_Project__c = p.Id;
        o.Description = opptyDescription;
        try {
            insert o;
        } catch(DmlException e) {
            for (Integer i=0;i<10000;i++){
            }
            insert o;
        }
        
        return o;
     }
    
    /**
    * @author       Peeyush Awadhiya
    * @date         07/03/2014
    * @description  Utility method to create and return the opportunity record without inserting it.
    * @param        String Name, String division, string level1, string level2, string level3
    * @return       Product2 
    */
    
    public static Opportunity generateOpportunityWithoutInsert(string opptyRecordType, string opptyStage, CAM_Project__c p){
        id opptyRecordTypeId = Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get(opptyRecordType).getRecordTypeId();
        Opportunity o = new Opportunity();
        o.name = opptyName;
        o.CloseDate = opptyCloseDate;
        o.RecordTypeId = opptyRecordTypeId;
        o.StageName = opptyStage;
        o.CAM_Project__c = p.Id;
        o.Description = opptyDescription;
        //insert o;
        return o;
     }
    
    /**
    * @author       Peeyush Awadhiya
    * @date         07/03/2014
    * @description  Utility method to create and return the product record.
    * @param        String Name, String division, string level1, string level2, string level3
    * @return       Product2 
    */
    public static Product2 generateProduct(String prodName, String division, string level1, string level2, string level3){
        Product2 product = new Product2(Name = prodName, Division__c = division, Group__c = level1, Brand__c = level2, Model__c = level3, isActive = true, CanUseQuantitySchedule = true,   CanUseRevenueSchedule = true);
        return product; 
    }
    
    /**
    * @author       Peeyush Awadhiya
    * @date         07/03/2014
    * @description  Utility method to create and return the pricebook record.
    * @param        String Name
    * @return       PriceBook2 
    */
    
    public static Pricebook2 generatePricebook(String pricebookName){
        return new Pricebook2(Name = pricebookName, isActive = true);
    }
        
        
    /**
    * @author       Peeyush Awadhiya
    * @date         07/09/2014
    * @description  Utility method to create and return the feeditem record.
    * @param        String Body, Id ParentId
    * @return       FeedItem 
    */      
   
    public static FeedItem generateFeedItem(String bodyStr, Id parentRecId){
        return new FeedItem(body =  bodyStr, parentId = parentRecId);
    }
    
    /**
    * @author       Peeyush Awadhiya
    * @date         07/09/2014
    * @description  Utility method to create and return the IOI record.
    * @param        String Body, Id ParentId
    * @return       IOI__c
    */      
    
    public static IOI__c generateIOI(String name, Id accountId, Id opportunityId, Id projectId, String Status){
        return new IOI__c(name = name, Account__c = accountId, Opportunity__c = opportunityId, Project__c = projectId, Status__c = status);
    }
    
     /**
    * @author       Peeyush Awadhiya
    * @date         07/09/2014
    * @description  Utility method to create and return the Chatter Post record.
    * @param        String Object, Id feedId
    * @return       IOI_Chatter_Feed__c
    */      
    
    public static IOI_Chatter_Feed__c generateChatterPostRecord(String ObjectName, Id feedId){
        return new IOI_Chatter_Feed__c(Chatter_Feed_Id__c = feedId, Object__c = ObjectName);
    }
}