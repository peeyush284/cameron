/**
* @author       Peeyush Awadhiya
* @date         10/27/2014
* @description  This batch class does the following:
*               1. Copies the OpportunityLineItems into Opportunity_Reporting__c object.
*               
*/

global class OpportunityReportingCopyLineItemBatch implements Database.batchable<sObject>{
    string query;
    global OpportunityReportingCopyLineItemBatch(String queryStr){
        this.query = queryStr;
    }  
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){       
        List<OpportunityLineItem> lineItemList = (List<OpportunityLineItem>) scope;
        List<Opportunity_Reporting__c> reportingsToInsert = new List<Opportunity_Reporting__c>();
        
         for(OpportunityLineItem opLine:lineItemList){
            
            if(opLine.Opportunity.RecordType.Name!='Bucket'){
                Opportunity_Reporting__c reporting = new Opportunity_Reporting__c(
                    OwnerId = opLine.Opportunity.OwnerId,
                    Forecast_Category__c = opLine.Opportunity.ForecastCategoryName,
                    Opportunity_Booking_Date__c = opLine.Opportunity.CloseDate,
                    Opportunity_Name__c = opLine.OpportunityId,
                    Opportunity_Product_Name__c = opLine.PricebookEntry.Product2.Name,
                    Opportunity_Type__c = opLine.Opportunity.RecordType.Name,
                    Opportunity_Amount__c = opLine.TotalPrice,
                    Forecast_Amount__c = opLine.Forecast_Amount_Formula__c, 
                    Source_Id__c = opLine.Id,
                    CurrencyISOCode = opLine.CurrencyISOCode,
                    Order_Type__c = opLine.Order_Type__c,
                    Product_Class__c = opLine.Product_Class__c,
                    Quantity__c = opLine.QuantityCalc__c,
                    Manufacturing_Plant__c = opLine.Manufacturing_Plant__c,
                    Market_Requested_Delivery_Weeks__c = opLine.Market_Requested_Delivery_Weeks__c
                );
               
                reportingsToInsert.add(reporting);   
            }
            
        } // End of for loop
        
        // Now insert the opportunity reporting records.
        if(reportingsToInsert!=null && !reportingsToInsert.isEmpty()){
            Database.SaveResult [] results = Database.Insert(reportingsToInsert, false);
            
            // Iterate through each returned result
            for (Database.SaveResult sr : results) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Opportunity Reporting fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }  
    }    
    global void finish(Database.BatchableContext BC){
       
    }
}