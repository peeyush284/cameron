global class DDCOrgDnBAccountHierarchy implements Database.Batchable<sObject>
{ 
    public static boolean accountHrchyTriggerDisabled = false;

    private static String ACCT_HRCHY_QUERY = 'SELECT Id, Name, ParentId, ' + 
                                                    'DunsNumber, ' +
                                                    'DandBCompany.GlobalUltimateDunsNumber, ' + 
                                                    'DandBCompany.DomesticUltimateDunsNumber, ' + 
                                                    'DandBCompany.ParentOrHqDunsNumber ' +
                                               'FROM Account ' +
                                              'WHERE DunsNumber != null ' +
                                                'AND DandBCompany.GlobalUltimateDunsNumber != null ' +
                                                'AND DandBCompany.GlobalUltimateDunsNumber != \'000000000\'' +
                                                'AND DandBCompany.ParentOrHqDunsNumber != null ' +
                                                'AND DandBCompany.ParentOrHqDunsNumber != \'000000000\'';
/*                                                
    private static String ACCT_HRCHY_QUERY = 'SELECT Id, Name, ParentId, ' + 
                                             'Site_DUNS_Nbr__c, ' +
                                             'Global_Ultimate_DUNS_Nbr__c, ' +
                                             'Domestic_Ultimate_DUNS_Nbr__c, ' +
                                             'HQ_Parent_DUNS_Nbr__c ' +
                                             'FROM Account ' +
                                            'WHERE Site_DUNS_Nbr__c != null ' +
                                              'AND Global_Ultimate_DUNS_Nbr__c != null';
*/
    public String query;
   
    global DDCOrgDnbAccountHierarchy ()
    {
        query = ACCT_HRCHY_QUERY;
        system.debug(LoggingLevel.DEBUG, 'DDCOrgDnbAccountHierarchy query = '+ query);
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
       System.debug(LoggingLevel.DEBUG, 'DDCOrgDnbAccountHierarchy Database.QueryLocator query = '+ query); 
       return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        integer idx = 0;
        System.debug(LoggingLevel.DEBUG, 'DDCOrgDnbAccountHierarchy execute query = '+ query); 
        for ( sObject result : scope )
        {
            idx++;
            System.debug(LoggingLevel.DEBUG, 'DDCDnbAccountHierarchy.buildAccountHierarchy idx = '+ idx);
            DDCDnbAccountHierarchy.buildAccountHierarchy((Id)result.get('Id'));
            if (Test.isRunningTest() && idx == 10) {
                break;
            }
        }        
    }

    global void finish(Database.BatchableContext BC){}

}