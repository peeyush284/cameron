/**
* @author       Peeyush Awadhiya
* @date         09/30/2014
* @description  This class serves as an extension to the ProjectAddProjectLeadPage. 
*/


public with sharing class ProjectAddProjectLeadExtension {
    
    public boolean showErrorMsg {get;set;}
    public CAM_Project__c project {get;set;}
    public List<Opportunity> leadList {get;set;}
    public String errorMessage {get;set;}
    
    public String bindingOppty {get; set;}
    public List<AccountTeamMemberWrapper> accountTeamMemberList{get;set;}
    
    public ProjectAddProjectLeadExtension(ApexPages.StandardController controller) {
        if(!Test.isRunningTest()){
             controller.addFields(new List<String>{'CAM_Owner_End_User__r.Name','Owner.Name','Owner.Profile.Name','Status__c','CAM_Owner_End_User__c','CAM_Startup_Date__c', 'CAM_Final_Destination__c', 'Name'});    
        }
        showErrorMsg = false;
        project = (CAM_Project__c) controller.getRecord(); 
        accountTeamMemberList = new List<AccountTeamMemberWrapper>();
        bindingOppty = project.CAM_Owner_End_User__r.Name + ' - ' + project.Name;
          
    }
    
    public void validateTeamMembers(){
        
        if(project!=null){
            
            Set<String> validateProjectLeads = new Set<String>();
            boolean hasDuplicateTeamRole = false;
            Map<String, Id> accountTeamMemberRoleDeDupeMap = new Map <String, Id> ();
            Set<Id> ownersToExclude = new Set<Id>();
            
            // Selectively load the team members, excluding those who already own other related opportunities or have Team role which has been excluded.
            for (Opportunity relatedOpportunity: [SELECT Id, OwnerId, End_User__c 
                                                  FROM Opportunity 
                                                  WHERE ((Parent_Project__c =: project.Id OR CAM_Project__c=:project.Id) 
                                                  AND End_User__c =:project.CAM_Owner_End_User__c) LIMIT 2000]){
                ownersToexclude.add(relatedOpportunity.OwnerId);
            }
            
            /*
            * @author  Peeyush Awadhiya
            * @date 10/04/2014
            * @description Exclude the team members whose roles are defined in Prevent Lead Assignment custom settings.
            *
            */
            for(Prevent_Project_Leads__c setting:Prevent_Project_Leads__c.getall().values()){
                validateProjectLeads.add(setting.Name);    
            }
            
            for (AccountTeamMember acctTeamMember: [SELECT UserId, User.Name,User.camDivision__c, User.camBusinessUnit__c, User.Create_Opportunity__c, TeamMemberRole 
                                                    FROM AccountTeamMember WHERE AccountId = :project.CAM_Owner_End_User__c 
                                                    AND TeamMemberRole NOT IN:validateProjectLeads 
                                                    AND User.Create_Opportunity__c = true 
                                                    AND User.Id NOT IN:ownersToexclude]){ 
                
                accountTeamMemberList.add(new AccountTeamMemberWrapper(acctTeamMember));  
                if(!accountTeamMemberRoleDeDupeMap.containsKey(acctTeamMember.TeamMemberRole )){
                    accountTeamMemberRoleDeDupeMap.put(acctTeamMember.TeamMemberRole, acctTeamMember.Id);   
                }
                else{
                    hasDuplicateTeamRole = true;
                } 
            } 
            
            Set<String> profileValidationSet = new Set<String>();
            profileValidationSet.addAll(String.valueOf(System.Label.Project_Allow_Cameron_Admin_Profile_To_Assign_Lead).split(','));
            
            // Validate On Stream Date
            if(project.CAM_Startup_Date__c == null){
                
                errorMessage = System.Label.Project_On_Stream_Date_Missing;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,errorMessage);
                ApexPages.addMessage(myMsg);
                showErrorMsg = true;  
            }
            
            // Validate if project is owned by Cameron Admin
            
            else if(!profileValidationSet.contains(project.Owner.Profile.Name)) {
                
                errorMessage = System.Label.Project_Owned_By_Cameron_Admin;
                ApexPages.Message myMsg =  new ApexPages.Message(ApexPages.Severity.ERROR,errorMessage);
                ApexPages.addMessage(myMsg);
                showErrorMsg = true;     
            }
            // Validate COE status = Permanent
            else if(project.Status__c != 'Permanent'){
                errorMessage = System.Label.Project_Status_Is_Permanent;
                ApexPages.Message myMsg =  new ApexPages.Message(ApexPages.Severity.ERROR,errorMessage);
                ApexPages.addMessage(myMsg);
                showErrorMsg = true;      
            }
            else if(hasDuplicateTeamRole){
                
                /*
                * @author  Peeyush Awadhiya
                * @date 10/01/2014
                * @description When the COE member clicks on Add Project Leads, prior to creating the Project Leads, query for duplicate Account Team Roles
                *
                *              1. If any duplicates exist: Show a message that says "Duplicate Account Team Members exist.  Please resolve Account Team Membership duplicates and then Add Project Leads" 
                *              Do not create any Project Leads
                *              2. If no duplicates exists: Create Project Leads for all account team members with roles that are not in the Custom Object for Preventing Project Leadsnames
                */
                
                
                
                errorMessage = System.Label.Project_Duplicate_Roles_In_Account_Team;
                ApexPages.Message myMsg =  new ApexPages.Message(ApexPages.Severity.WARNING, errorMessage);
                ApexPages.addMessage(myMsg);
                showErrorMsg = true;    
            }  
            
            else if(accountTeamMemberList == null || accountTeamMemberList.isEmpty()){
                 
                errorMessage = System.Label.Project_No_Account_Team_Member_Qualified;
                ApexPages.Message myMsg =  new ApexPages.Message(ApexPages.Severity.ERROR,errorMessage);
                ApexPages.addMessage(myMsg);
                showErrorMsg = true;  
            }
           
        }      
    }
    
    public PageReference assignLeads(){
        
        Map<String, String> duplicateTeamMemberRole = new Map<String,String>();
        String opptyStage = Label.Project_Create_Opportunity_Button_Stage;
        Id opptyRecordTypeId = Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get(Label.Project_Create_Opportunity_Button_RecordType).getRecordTypeId();
                
        Set<Id> selectedTeamMembers = new Set<Id>();
        List<Opportunity> opptyList = new List<Opportunity>();
        
        for(AccountTeamMemberWrapper wrapper:accountTeamMemberList){
            if(wrapper.assignLead){
                selectedTeamMembers.add(wrapper.teamMember.UserId); 
                if(duplicateTeamMemberRole.containsKey(wrapper.teamMember.TeamMemberRole)){
                    errorMessage = System.Label.Project_Team_Members_With_Duplicate_Role_Selected;
                    ApexPages.Message myMsg =  new ApexPages.Message(ApexPages.Severity.ERROR,errorMessage);
                    ApexPages.addMessage(myMsg);
                    showErrorMsg = true;  
                    return null;    
                }
                else{
                    duplicateTeamMemberRole.put(wrapper.teamMember.TeamMemberRole, wrapper.teamMember.TeamMemberRole);
                }   
            }
        }
        
        if(!showErrorMsg && (errorMessage == System.Label.Project_Duplicate_Roles_In_Account_Team || errorMessage == null || errorMessage == '')){ 
            // If members are selected then assign the leads 
            if(selectedTeamMembers!=null && !selectedTeamMembers.isEmpty()){
                List<User> userToAssignLeads = [SELECT Id, camDivision__c, camBusinessUnit__c FROM User WHERE Id IN :selectedTeamMembers AND Create_Opportunity__c = true];
                
                for(User objUser: userToAssignLeads){
            
                        Opportunity oppty = new Opportunity (
                                                            Name = bindingOppty,
                                                            OwnerId = objUser.Id,                                  
                                                            Parent_Project__c = project.Id,
                                                            RecordTypeId = opptyRecordTypeId,
                                                            StageName = opptyStage,
                                                            CloseDate = project.CAM_Startup_Date__c,
                                                            End_User__c = project.CAM_Owner_End_User__c,
                                                            AccountId = project.CAM_Owner_End_User__c,
                                                            CAM_Final_Destination__c = project.CAM_Final_Destination__c,
                                                            CAM_Division__c = objUser.camDivision__c,
                                                            CAM_Business_Unit__c = objUser.camBusinessUnit__c
                                                            );
                        
                        if (objUser.camBusinessUnit__c == null && objUser.camDivision__c == null){
                            oppty.Name = project.CAM_Owner_End_User__r.Name + ' - ' + project.Name;
                        }
                        else if (objUser.camBusinessUnit__c == null) {
                            oppty.Name = project.CAM_Owner_End_User__r.Name + ' - ' + project.Name + ' - ' + objUser.camDivision__c;
                        }
                        else {
                            oppty.Name = project.CAM_Owner_End_User__r.Name + ' - ' + project.Name + ' - ' + objUser.camDivision__c + '/' + objUser.camBusinessUnit__c;
                        }
                        
                        OpptyList.add(oppty);
                    } // End of for loop
                    
                    // Now insert the leads
                    
                    String responseStr = string.valueof(OpptyList.size());
                    
                    if(OpptyList!=null && !OpptyList.isempty())
                    {
                        Database.SaveResult [] saveResult = Database.insert(OpptyList,false);
                        for(Database.SaveResult result:saveResult){
                            if(!result.isSuccess()){
                                for(Database.Error err : result.getErrors()) {
                                    System.debug('The following error has occurred.');                    
                                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                    System.debug('Opportunity fields that affected this error: ' + err.getFields());
                                }
                                responseStr='Exception caught while adding Opportunities';                  
                            }
                            else{                   
                                responseStr= string.valueof(OpptyList.size());                    
                            }               
                        }
                    }  // End of if (OpptyList!=null && !OpptyList.isempty())  
                        
                return (new ApexPages.StandardController(project)).view();
                
            } // End of if
            else
            {
                // If no member selected then throw error message
                
                errorMessage = System.Label.Project_Select_Atleast_One_Team_Member;
                ApexPages.Message myMsg =  new ApexPages.Message(ApexPages.Severity.WARNING,errorMessage);
                ApexPages.addMessage(myMsg);
                showErrorMsg = true;
                return null;    
    
            } // End of else
        }
        else{
            if(errorMessage!='' && errorMessage!=null){
                ApexPages.Message myMsg =  new ApexPages.Message(ApexPages.Severity.WARNING,errorMessage);
                ApexPages.addMessage(myMsg);
                showErrorMsg = true;
            }
            return null;    
        }   
           
    } // End of method
    
    public PageReference cancel(){
        return (new ApexPages.StandardController(project)).view();    
    }
    
    public class AccountTeamMemberWrapper {
    
        public AccountTeamMember teamMember {get;set;}
        public boolean assignLead {get;set;}
        
        public AccountTeamMemberWrapper(AccountTeamMember teamMember){
            this.teamMember =  teamMember;
            this.assignLead = false;
        }
    }
    

}