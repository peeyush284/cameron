/**
* @author       Peeyush Awadhiya
* @date         07/02/2014
* @description  This class serves as an extension to the Page "ProjectEstimatedValuePage". 
*               It tests following
*               1. Whenever any user opens the detail page of object record Project, the onload method queries
*               all the open/won opportunities related to the project and updates the Project Est. Value custom field. 
*/


global without sharing class ProjectExtension {

    public ProjectExtension(ApexPages.StandardController controller) {

    }


    
   
    /**
    * @author       Peeyush Awadhiya
    * @date         06/24/2014
    * @description  This method queries for all Opportunity StageName picklist values and filters those values which are either Won or Open. 
    *               It then sums up all related opportunity amount and updates the project record.
    * @param        void
    * @return       void 
    */
    @remoteAction
    global static void calculateProjectEstimatedValue(Id projectId){
        CAM_Project__c project;
        List<CAM_Project__c> projectList = [SELECT Id, CAM_Est_Value__c, Total_Amount__c FROM CAM_Project__c WHERE ID =:projectId LIMIT 1];
        if(projectList!=null && !projectList.isEmpty()){
            project = projectList[0];
        }
        if(project!=null && project.Id!=null){
            Set<String> stageSet = new Set<String>();
            double estProjVal = 0.0;
            double opportunityAmount = 0.0;
            //Id soldToCameronAccountId = Project_Exclude_Opportunity_Sold_To_Acc__c.getOrgDefaults().Sub_Vendor__c;
            // Get all stagenames which are either won or open
            for(OpportunityStage stage:[
                SELECT Id, isActive, isWon, isClosed, MasterLabel
                FROM OpportunityStage
                WHERE (isActive = true AND (isWon = true OR isClosed = false))
            ]){
                stageSet.add(stage.MasterLabel);
            }
            
            // Now pull all open/won opportunities and calculate the sum of the amount
            for(Opportunity Oppty:[
                                    SELECT Id, Amount, Sub_Vendor__c, CAM_Forecast_Amount__c
                                    FROM Opportunity
                                    WHERE StageName IN :stageSet
                                    AND (CAM_Project__c =:project.Id OR Parent_Project__c =:project.Id)
                                    //AND Sub_Vendor__c !=:soldToCameronAccountId
                                        
                                ]){
                                    if(Oppty.Amount!=null){
                                        estProjVal += Oppty.CAM_Forecast_Amount__c; 
                                        opportunityAmount += Oppty.Amount;
                                    }  
                                }
            project.CAM_Est_Value__c = estProjVal;
            project.Total_Amount__c = opportunityAmount;
            // Save the record
            try{
                update project;
            }catch(Exception ex){
                System.debug('Exception caught while updating the project record:'+ex.getMessage());
            }
        }
    }
}