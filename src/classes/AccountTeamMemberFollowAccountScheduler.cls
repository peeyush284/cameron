/**
* @author       Peeyush Awadhiya
* @date         09/23/2014
* @description  Scheduler class for the batch class that does the following:
*               1. Queries for all account team member created on the same day and inserts EntitySubscription record for each of the team member.
*               2. Since the platform doesn't allow triggers on AccountTeamMember object, the batch apex approach was adopted. 
*/

global class AccountTeamMemberFollowAccountScheduler implements Schedulable {
    
    global void execute(SchedulableContext sch) {
    
        String queryStr = 'SELECT Id, AccountId, UserId FROM AccountTeamMember WHERE CreatedDate = TODAY';
        AccountTeamMemberFollowAccountBatch batchObj = new AccountTeamMemberFollowAccountBatch(queryStr);
        ID batchProcessId = Database.executeBatch(batchObj, 100);  
        
        System.debug('Executing the batch with process Id:'+batchProcessId);
    }
}