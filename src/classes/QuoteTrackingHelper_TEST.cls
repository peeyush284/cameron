/**
* @author       Peeyush Awadhiya
* @date         10/24/2014
* @description  Test method for quote tracking helper class.
*               1. When a new quote tracking record is created, it adds the Outside Sales User to the Opportunity Sales Team as Secondary Sales.
*               2. When Outside sales changes on quote tracking record, the new Outside sales user is added to the Opportunity Sales Team as Secondary Sales.
*               3. When a new quote is created, send a notification to whole sales team.
* 
*/

@isTest

public class QuoteTrackingHelper_TEST{

    public static testMethod void testQuoteCreation(){
        // Prepare the data stub
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('Bucket', 'RFQ', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Surface';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = project.Id;
        
        oppty.AccountId = acc.Id;
        insert oppty;
        User salesUser = [SELECT Id FROM User WHERE camDivision__c = 'Process Systems' and camBusinessUnit__c  = 'CPS' and isActive = true AND ID!=:UserInfo.getUserId() LIMIT 1];
        // Now insert the quote
        Test.startTest();
            QuoteTracking__c quote = new QuoteTracking__c(Opportunity__c = oppty.Id, Outside_Sales_Person__c = salesUser.Id, Ultimate_Destination__c ='USA', End_User__c = acc.Id);
            insert quote;
        Test.stopTest();
        
        // Assert whether the user has been added to the sales team?
        System.assertEquals([SELECT Id, UserId, OpportunityId FROM OpportunityTeamMember WHERE OpportunityId =:oppty.Id AND UserId =:salesUser.Id].size(),1);
    } 
    
    public static testMethod void testQuoteUpdate(){
        // Prepare the data stub
        Account acc;
        CAM_Project__c project; 
        Opportunity oppty = new Opportunity(); 
        
        // Account
        acc = TestUtilities.generateAccount();
        AccountTeamMember atm = TestUtilities.generateAccountTeamMember('Surface|SUR|Acct Mgr|Primary', acc); 
        
        // Project
        project = TestUtilities.generateProject('External', 'Pre Feed', acc);
        
        Period__c period = new Period__c(Name = 'Test Period', Type__c = 'Week', StartDate__c = Date.Today(), EndDate__c = Date.Today().addDays(5));
        insert period;
        
        // Opportunit
        oppty = TestUtilities.generateOpportunityWithoutInsert('Bucket', 'RFQ', project);
        oppty.CloseDate = Date.newInstance(Date.Today().year(), 12, 31);
        oppty.CAM_Division__c = 'Process Systems';
        oppty.Business_Unit_Picklist__c = 'CPS';
        oppty.End_User__c = acc.Id;
        oppty.CAM_Project__c = project.Id;
        
        oppty.AccountId = acc.Id;
        insert oppty;
        User salesUser = [SELECT Id FROM User WHERE camDivision__c = 'Process Systems' and camBusinessUnit__c = 'CPS' and isActive = true AND ID!=:UserInfo.getUserId() LIMIT 1];
        // Now insert the quote
        
        QuoteTracking__c quote = new QuoteTracking__c(Opportunity__c = oppty.Id, Outside_Sales_Person__c = salesUser.Id, Ultimate_Destination__c ='USA', End_User__c = acc.Id);
        insert quote;

        product2 prod = new product2(IsActive = true, 
                                    class__c = 'Gas Treating' ,
                                    brand__c = 'Cameron', 
                                    Division__c = 'CPS',  
                                    //Business_Unit__c = 'Process Systems', 
                                    Name = 'Amine Gas Sweetening',
                                    group__c = 'Amine Gas Sweetening'
                                    );

        QuoteTrackingLineItem__c qtItem = new QuoteTrackingLineItem__c(CurrencyIsoCode= 'USD',
            Material__c= 'Carbon Steel',
            Opportunity_Business_Unit__c= 'CPS',
            Opportunity_Division__c= 'Process Systems',
            Primary_Product__c= 'Yes',
            Product_Brand__c= prod.brand__c,
            Product_Class__c= prod.class__c,
            Products__c= prod.id,
            Quantity__c= 1,
            Quote_Tracking__c= quote.id,
            Unique_Primary_Product_Helper__c= prod.id,
            UnitPrice__c= 1000);
        
        insert qtItem;        
        
        User anotherSalesUser = [SELECT Id FROM User WHERE  camDivision__c = 'Process Systems' and camBusinessUnit__c = 'CPS' and isActive = true AND ID!=:UserInfo.getUserId() AND Id!=:salesUser.Id LIMIT 1];
        Test.startTest();
            quote.Outside_Sales_Person__c = anotherSalesUser.Id;
            update quote;       
        Test.stopTest();
        
        
        // Assert whether the user has been added to the sales team?
        System.assertEquals([SELECT Id, UserId, OpportunityId FROM OpportunityTeamMember WHERE OpportunityId =:oppty.Id AND UserId =:anotherSalesUser.Id].size(),1);
    } 

}